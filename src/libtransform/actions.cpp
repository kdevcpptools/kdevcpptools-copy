/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "actions.h"

#include <QtCore/QRegExp>
#include <QtCore/QString>

#include <language/editor/simplecursor.h>
#include <language/editor/simplerange.h>
#include <language/editor/rangeinrevision.h>

#include "replacementgenerator.h"

#include <libquery/queryuses/queryhit.h>

using namespace KDevelop;

/// Action

Action::~Action() { }

/// InsertAction[Private]

class InsertActionPrivate
{
  public:
    InsertAction::Location mLocation;
    QueryHit::Item         mItem;
    QString                mText;

    InsertActionPrivate(InsertAction::Location location, QueryHit::Item item, QString const &text)
      : mLocation(location), mItem(item), mText(text)
    { }
};

InsertAction::InsertAction(Location location, QueryHit::Item item, QString const &text)
  : d(new InsertActionPrivate(location, item, text))
{}

InsertAction::~InsertAction()
{
  delete d;
}

DocumentChangePointer InsertAction::changeForUse(KDevelop::CodeRepresentation::Ptr orignalData, QueryHit::Ptr use)
{
  Q_UNUSED(orignalData);

  SimpleRange range = use->rangeForItem(d->mItem).castToSimpleRange();
  if (!range.isValid())
    return DocumentChangePointer();

  switch (d->mLocation) {
    case Before:
      return DocumentChangePointer(new DocumentChange(use->file(), SimpleRange(range.start, range.start), QString(), d->mText));
    case After:
      return DocumentChangePointer(new DocumentChange(use->file(), SimpleRange(range.end, range.end), QString(), d->mText));
  };

  Q_ASSERT(false);
  return DocumentChangePointer();
}

bool InsertAction::isEmpty() const
{
  return d->mText.isEmpty();
}

/// ReplaceAction[Private]

class ReplaceActionPrivate
{
  public:
    QueryHit::Item           mItem;
    QueryHit::ItemWithOffset mItemWithOffset;
    int                      mOffset;
    QString                  mReplacementText;

    ReplaceActionPrivate(QueryHit::Item item, QString const &typeReplacement)
      : mItem(item)
      , mOffset(-1)
      , mReplacementText(typeReplacement)
    { }

    ReplaceActionPrivate(QueryHit::ItemWithOffset item,
                         int offset,
                         QString const &itemReplacement)
      : mItemWithOffset(item)
      , mOffset(offset)
      , mReplacementText(itemReplacement)
    { }
};

static QRegExp sTemplateRegExp = QRegExp("\\$\\{(TypeTemplateArg)\\[(\\d+)\\]\\}");

ReplaceAction::ReplaceAction(QueryHit::Item item, const QString& replacement)
  : d(new ReplaceActionPrivate(item, replacement))
{ }

ReplaceAction::ReplaceAction(QueryHit::ItemWithOffset item, uint offset, QString const &replacement)
  : d(new ReplaceActionPrivate(item, offset, replacement))
{ }

ReplaceAction::~ReplaceAction()
{
  delete d;
}

DocumentChangePointer ReplaceAction::changeForUse(KDevelop::CodeRepresentation::Ptr orignalData, QueryHit::Ptr use)
{
  // TODO: Return an actionResult in stead of a DocumentChange to support proper
  //       error reporting.
  if (d->mOffset == -1 && d->mItem != QueryHit::All) {
    // We're replacing an item without an offset.
    SimpleRange const range = use->rangeForItem(d->mItem).castToSimpleRange();
    if (!range.isValid())
      return DocumentChangePointer();

    return DocumentChangePointer(new DocumentChange(use->file(), range, orignalData->rangeText(range.textRange()), d->mReplacementText));
  }

  ReplacementGenerator generator;
  ReplacementResult result = generator.generate(d->mReplacementText, use, orignalData);

  if (!result.isValid()) {
    qDebug() << "[ReplaceAction::changeForUse] Replacement generation error:" << result.status() << result.errorString();
    return DocumentChangePointer();
  }

  SimpleRange range;
  if (d->mItem == QueryHit::All)
    range = use->range().castToSimpleRange();
  else
    range = use->rangeForOffsetItem(d->mItemWithOffset, d->mOffset).castToSimpleRange();

  if (!range.isValid())
    return DocumentChangePointer();

  return DocumentChangePointer(new DocumentChange(use->file(), range, orignalData->rangeText(range.textRange()), result.text()));
}

bool ReplaceAction::isEmpty() const
{
  // You can never know when a replacement action is empty. This also depends on
  // the query. If no text is set it might be that it replaces parts of the
  // documents with emtpy strings.
  return false;
}
