/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "transformrule.h"

#include <libquery/queryuses/classfunctioncall.h>
#include <libquery/queryuses/functioncall.h>

class TransformRulePrivate
{
  public:
    QList<Condition>   mIfConditions;
    QList<Action::Ptr> mIfActions;
    QList<Action::Ptr> mElseActions;
    QList<Action::Ptr> mActions;

  public:
    TransformRulePrivate() { }
};

TransformRule::TransformRule()
  : d(new TransformRulePrivate)
{ }

TransformRule::TransformRule(TransformRule const &other)
  : d(new TransformRulePrivate)
{
  *d = *other.d;
}

TransformRule::~TransformRule()
{
  delete d;
}

TransformRule& TransformRule::operator=(TransformRule const &other)
{
  if (&other == this)
    return *this;

  *d = *other.d;
  return *this;
}

bool TransformRule::isValid() const
{
  return !d->mActions.isEmpty() || (!d->mIfConditions.isEmpty() && !d->mIfActions.isEmpty());
}

bool TransformRule::appliesTo(QueryHit::Ptr use) const
{
  if (!d->mActions.isEmpty()) {
    return true;
  }
  if (!d->mElseActions.isEmpty()) {
    return true;
  }
  foreach(const Condition &condition, d->mIfConditions) {
    if (!condition.appliesTo(use)) {
      return false;
    }
  }
  return !d->mIfActions.isEmpty();
}

QList< Action::Ptr > TransformRule::actionsForUse(QueryHit::Ptr use) const
{
  QList< Action::Ptr > actions;
  bool conditionsApply = true;
  foreach(const Condition &condition, d->mIfConditions) {
    if (!condition.appliesTo(use)) {
      conditionsApply = false;
      break;
    }
  }
  if (conditionsApply) {
    actions += d->mIfActions;
  } else {
    actions += d->mElseActions;
  }
  actions += d->mActions;
  return actions;
}

void TransformRule::setActions(QList<Action::Ptr> const &actions)
{
  Q_ASSERT(!actions.isEmpty());
  d->mActions = actions;
}

void TransformRule::setIfActions(QList<Condition>const & conditions, QList<Action::Ptr> const &actions)
{
  Q_ASSERT(!conditions.isEmpty());
  Q_ASSERT(!actions.isEmpty());
  d->mIfConditions = conditions;
  d->mIfActions = actions;
}

void TransformRule::setElseActions(QList<Action::Ptr> const &action)
{
  Q_ASSERT(!d->mIfActions.isEmpty());
  d->mElseActions = action;
}
