/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "transform.h"
#include "transform_p.h"

using namespace KDevelop;

Transform::Transform(Query::Ptr const &query)
  : d(new TransformPrivate(query))
{ }

Transform::~Transform()
{
  delete d;
}

void Transform::addTransformRule(TransformRule const &rule)
{
  d->mRules.append(rule);
}

bool Transform::appliesTo(QueryHit::Ptr use) const
{
  if (use->inMacro()) {
    return false;
  }

  foreach (TransformRule const &rule, d->mRules)
    if (rule.appliesTo(use))
      return true;

  // No rule applied.
  return false;
}

QList<DocumentChangePointer> Transform::changesForUse(QueryHit::Ptr use)
{
  Q_ASSERT(use->inMacro() || appliesTo(use));

  QList<DocumentChangePointer> changes;
  if (use->inMacro())
    return changes;

  CodeRepresentation::Ptr codeRepresentation = createCodeRepresentation(use->file());
  foreach (TransformRule const &rule, d->mRules) {
    QList<Action::Ptr> actions = rule.actionsForUse(use);

    foreach (Action::Ptr const &action, actions) {
      // TODO: Use ActionResult here and do proper error reporting.
      DocumentChangePointer change = action->changeForUse(codeRepresentation, use);
      if (!change) {
        continue;
      }
      //TODO: handle conflicts?
      changes << change;
    }
  }

  return changes;
}

Query::Ptr Transform::query() const
{
  return d->mQuery;
}
