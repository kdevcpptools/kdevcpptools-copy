/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ACTIONRESULT_H
#define ACTIONRESULT_H

#include <QtCore/QSharedDataPointer>

#include <language/codegen/documentchangeset.h>

class QString;
class ResultPrivate;

class Result
{
  public:
    enum Status {
      ReplacmentInvalidPlaceHolder,
      RelacementMissingUseItem,
      Ok,
      Unknown
    };

  public:
    Result();
    Result(Result const &);
    virtual ~Result();
    Result &operator=(Result const &);

    QString errorString() const;

    virtual bool isValid() const = 0;

    void setErrorString(QString const &errorString);

  protected:
    QSharedDataPointer<ResultPrivate> d;
};

class ReplacementResult : public Result
{
  public:
    enum Status {
      InvalidPlaceHolder,
      MissingUseItem,
      Ok,
      Unknown
    };

  public:
    virtual bool isValid() const;

    void setStatus(Status status);

    void setText(QString const &text);

    Status status() const;

    QString text() const;
};

class ActionResult : public Result
{
  public:
    enum Status {
      Ok
    };

  public:
    /// For succeeded actions.
    ActionResult(KDevelop::DocumentChangePointer change);

    /// For actions that could not be performed.
    ActionResult(Status status);

    KDevelop::DocumentChangePointer documentChange() const;

    virtual bool isValid() const;

    void setStatus(Status status);

    Status status() const;
};

#endif // ACTIONRESULT_H
