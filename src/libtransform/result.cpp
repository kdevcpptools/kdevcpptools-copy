/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "result.h"

#include <QtCore/QString>

using namespace KDevelop;

/// ResultPrivate

class ResultPrivate : public QSharedData
{
  public:
    /// Shared
    QString                   mErrorString;

    /// Replacement
    ReplacementResult::Status mReplaceStatus;
    QString                   mText;

    /// Action
    ActionResult::Status      mActionStatus;
    DocumentChangePointer     mDocumentChange;
};

/// Result

Result::Result() : d(new ResultPrivate)
{ }

Result::Result(Result const &other) : d(other.d)
{ }

Result &Result::operator=(Result const &other)
{
  if (this != &other)
    d = other.d;

  return *this;
}

Result::~Result()
{ }

QString Result::errorString() const
{
  return d->mErrorString;
}

void Result::setErrorString(QString const &errorString)
{
  d->mErrorString = errorString;
}

/// ReplacementResult

bool ReplacementResult::isValid() const
{
  return d->mReplaceStatus == ReplacementResult::Ok;
}

void ReplacementResult::setStatus(Status status)
{
  d->mReplaceStatus = status;
}

void ReplacementResult::setText(QString const &text)
{
  d->mText = text;
}

ReplacementResult::Status ReplacementResult::status() const
{
  return d->mReplaceStatus;
}

QString ReplacementResult::text() const
{
  return d->mText;
}


ActionResult::ActionResult(DocumentChangePointer change)
{
  Q_ASSERT(change);
  d->mActionStatus = ActionResult::Ok;
  d->mDocumentChange = change;
}

/// For actions that could not be performed.
ActionResult::ActionResult(Status status)
{
  d->mActionStatus = status;
}

DocumentChangePointer ActionResult::documentChange() const
{
  return d->mDocumentChange;
}

bool ActionResult::isValid() const
{
  return d->mActionStatus == Ok && d->mDocumentChange;
}

void ActionResult::setStatus(Status status)
{
  d->mActionStatus = status;
}

ActionResult::Status ActionResult::status() const
{
  return d->mActionStatus;
}
