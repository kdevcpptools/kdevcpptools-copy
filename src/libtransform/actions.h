/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ACTION_H
#define ACTION_H

#include <QtCore/QList>
#include <QtCore/QSharedPointer>

#include "cpptransformengine_export.h"
#include "result.h"
#include <libquery/queryuses/queryhit.h>

#include <language/codegen/documentchangeset.h>
#include <language/codegen/coderepresentation.h>

class DeclarationUse;

/// An Action represents an atomic modification of an document. I.e. an insert
/// at one location or a replace of one range.
class CPPTRANSFORMENGINE_EXPORT Action
{
  public:
    typedef QSharedPointer<Action> Ptr;

  public:
    virtual ~Action();

    /// Executes the action for given used. The DocumentChangeSet will not be
    /// changed by the Action but used to retrieve information needed for performing
    /// the action.
    virtual KDevelop::DocumentChangePointer changeForUse(KDevelop::CodeRepresentation::Ptr orignalData, QueryHit::Ptr use) = 0;

    virtual bool isEmpty() const = 0;
};

class CPPTRANSFORMENGINE_EXPORT InsertAction : public Action
{
  public:
    typedef QSharedPointer<InsertAction> Ptr;

    /// NOTE: This is probably too simplistic for other cases but for the QString
    ///       ctor transforms good enough.
    enum Location {
      After,
      Before
    };

  public: /// Functions
    InsertAction(Location location, QueryHit::Item, QString const &text);
    ~InsertAction();

    virtual KDevelop::DocumentChangePointer changeForUse(KDevelop::CodeRepresentation::Ptr orignalData, QueryHit::Ptr use);

    virtual bool isEmpty() const;


  private: /// Members
    class InsertActionPrivate *d;

    /// Disable copying
    InsertAction(InsertAction const &other);
    InsertAction& operator=(InsertAction const &other);
};

class CPPTRANSFORMENGINE_EXPORT ReplaceAction : public Action
{
  public:
    typedef QSharedPointer<ReplaceAction> Ptr;

  public: /// Functions
    /// Replace the type with @param typeReplacement
    ReplaceAction(QueryHit::Item item, QString const &replacement);

    /// Replace a function or template param @param offset with @param replacement
    ReplaceAction(QueryHit::ItemWithOffset item, uint offset, QString const &replacement);

    virtual ~ReplaceAction();

    virtual KDevelop::DocumentChangePointer changeForUse(KDevelop::CodeRepresentation::Ptr orignalData, QueryHit::Ptr use);

    virtual bool isEmpty() const;

  private: /// Members
    class ReplaceActionPrivate *d;

    /// Disable copying
    ReplaceAction(ReplaceAction const &other);
    ReplaceAction& operator=(ReplaceAction const &other);
};

#endif // ACTION_H
