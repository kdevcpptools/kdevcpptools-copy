/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "cpptransformengine.h"

#include <QtCore/QMap>

#include <language/duchain/indexedstring.h>

#include "cppqueryengine.h"
#include "query.h"
#include "queryuses/queryhit.h"
#include "queryresult.h"

#include "transform.h"

#include <language/duchain/topducontext.h>

#include <language/codegen/documentchangeset.h>
#include <KMessageBox>
#include <KLocalizedString>
#include <QApplication>

using namespace KDevelop;


/// Needed so we can use the TransformDocumentPair as a key for a map.
#if QT_VERSION < 0x040700
template <class T, class X>
bool operator<(QSharedPointer<T> const &ptr1, QSharedPointer<X> const &ptr2 )
{
  return ptr1.data() < ptr2.data();
}
#endif

typedef QList<QueryHit>            DeclarationUseList;
typedef QPair<Query::Ptr, IndexedString> QueryDocumentPair;

class CppTransformEnginePrivate
{
  public:
    CppTransformEnginePrivate()
    { }

    DocumentChangeSet::ChangeResult applyChanges(const QList<DocumentChangePointer> changes);
};

DocumentChangeSet::ChangeResult CppTransformEnginePrivate::applyChanges(const QList< DocumentChangePointer > changes)
{
  DocumentChangeSet changeSet;
  foreach (const DocumentChangePointer& change, changes) {
    changeSet.addChange(change);
  }
  return changeSet.applyAllChanges();
}


CppTransformEngine::CppTransformEngine() : d(new CppTransformEnginePrivate)
{ }

CppTransformEngine::~CppTransformEngine()
{
  delete d;
}

void CppTransformEngine::apply(Transform::Ptr const &transform, QueryHit::Ptr use)
{
  DocumentChangeSet::ChangeResult result = d->applyChanges(transform->changesForUse(use));
  if (!result.m_success) {
    KMessageBox::warningYesNo(QApplication::activeWindow(), result.m_failureReason,
                              i18n("Could not apply transformation for query %1", transform->query()->id()));
  }
}

void CppTransformEngine::apply(const Transform::Ptr& transform, const QueryResult& result)
{
  QList<DocumentChangePointer> changes;
  foreach(const QueryHit::Ptr& use, result.hits()) {
    changes += transform->changesForUse(use);
  }
  DocumentChangeSet::ChangeResult r = d->applyChanges(changes);
  if (!r.m_success) {
    KMessageBox::warningYesNo(QApplication::activeWindow(), r.m_failureReason,
                              i18n("Could not apply transformation for query %1", transform->query()->id()));
  }
}
