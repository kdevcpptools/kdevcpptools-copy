/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "condition.h"

#include <libquery/queryuses/classfunctioncall.h>

Condition::Condition(bool always) : mAlways(always), mValid(true)
{}

Condition::Condition(Property property, const QVariant& expectedValue)
  : mAlways(false)
  , mProperty(property)
  , mExpectedValue(expectedValue)
{
  switch (property) {
    case ImplicitCtorCall:
    case ImplicitOnThis:
      mValid = expectedValue.canConvert(QVariant::Bool);
      break;
    case ArgCount:
      mValid = expectedValue.canConvert(QVariant::Int);
      break;
    default: // Invalid
      mValid = false;
  }
}

bool Condition::appliesTo(QueryHit::Ptr use) const
{
  if (mAlways) {
    return true;
  } else {
    switch(mProperty) {
      case ImplicitCtorCall:
        if (ClassFunctionCall::Ptr cfCall = use.dynamicCast<ClassFunctionCall>()) {
          return cfCall->implicitCtorCall() == mExpectedValue.toBool();
        }
        break;
      case ImplicitOnThis:
        if (ClassFunctionCall::Ptr cfCall = use.dynamicCast<ClassFunctionCall>()) {
          return cfCall->implicitOnThis() == mExpectedValue.toBool();
        }
        break;
      case ArgCount:
        if (FunctionCall::Ptr fCall = use.dynamicCast<FunctionCall>()) {
          bool ok;
          return fCall->argumentCount() == mExpectedValue.toInt(&ok) && ok;
        }
        break;
      case Invalid:
        Q_ASSERT_X(0, "Condition::appliesTo", "do not call appliesTo on an invalid condition");
    }
    return false;
  }
}

bool Condition::isValid() const
{
  return mValid;
}