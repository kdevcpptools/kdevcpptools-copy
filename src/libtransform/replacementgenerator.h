/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef REPLACEMENTGENERATOR_H
#define REPLACEMENTGENERATOR_H

#include "result.h"

#include <libquery/queryuses/queryhit.h>

#include <language/codegen/coderepresentation.h>

class DeclarationUse;
class QString;

/**
 * Supported place holders:
 * ${
 *
 */
class ReplacementGenerator
{
  public:
    ReplacementGenerator();
    ~ReplacementGenerator();

    /**
     * Parses @param text and replaces the place holders with the approriate
     * information retrieved from the use and the code representation of the
     * unchanged file in which the use ocures. If the text contains a placeholder
     * that is not set in the use, the generation will be considered as a failed
     * one. The resulting text should not be used for transformations in that case.
     */
    ReplacementResult generate(QString const &text, QueryHit::Ptr use, KDevelop::CodeRepresentation::Ptr const &orignalFile);

  private:
    class ReplacementGeneratorPrivate * const d;

    /// Not meant for copying around.
    ReplacementGenerator(ReplacementGenerator const &);
    ReplacementGenerator& operator=(ReplacementGenerator const &);
};

#endif // REPLACEMENTGENERATOR_H
