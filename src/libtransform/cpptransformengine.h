/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CPPTRANSFORMENGINE_H
#define CPPTRANSFORMENGINE_H

#include <QtCore/QList>
#include <QtCore/QSharedPointer>

#include <libquery/cppqueryengine.h>

#include "cpptransformengine_export.h"
#include "transform.h"

class CPPTRANSFORMENGINE_EXPORT CppTransformEngine : public CppQueryEngine
{
  public:
    typedef QSharedPointer<CppTransformEngine> Ptr;

  public:
    CppTransformEngine();
    CppTransformEngine(CppTransformEngine const &other);
    ~CppTransformEngine();

    CppTransformEngine& operator=(CppTransformEngine const &other);

    /// Apply @p transform to @p use.
    void apply(Transform::Ptr const &transform, QueryHit::Ptr use);

    /// Apply @p transform to all uses in @p result.
    void apply(Transform::Ptr const &transform, QueryResult const &result);

  private:
    class CppTransformEnginePrivate * const d;
};

#endif // CPPTRANSFORMENGINE_H
