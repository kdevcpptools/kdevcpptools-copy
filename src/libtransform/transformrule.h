/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef TRANSFORMRULE_H
#define TRANSFORMRULE_H

#include "actions.h"
#include "condition.h"

class CPPTRANSFORMENGINE_EXPORT TransformRule
{
  public:
    TransformRule();
    TransformRule(TransformRule const &other);

    ~TransformRule();

    TransformRule& operator=(TransformRule const &other);

    bool isValid() const;

    /// @return True when there is at least one action that should be applied to @p use.
    bool appliesTo(QueryHit::Ptr use) const;
    /// @return List of actions that should be applied for the given @p use.
    QList<Action::Ptr> actionsForUse(QueryHit::Ptr use) const;

    /// Set the actions that will always be executed.
    void setActions(QList<Action::Ptr> const &actions);

    /// Sets the actions that are executed when their @p conditions are met. Else the @p elseActions() get executed.
    void setIfActions(QList<Condition> const &conditions, QList<Action::Ptr> const &actions);

    /// Sets the actions that are executed when the conditoins for the @p ifActions() are _not_ met.
    void setElseActions(QList<Action::Ptr> const &actions);


  private:
    class TransformRulePrivate * const d;
};

#endif // TRANSFORMRULE_H
