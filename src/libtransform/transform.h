/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <QtCore/QSharedPointer>

#include "query.h"
#include "queryresult.h"

#include "cpptransformengine_export.h"

#include <language/codegen/documentchangeset.h>

class TransformRule;

class CPPTRANSFORMENGINE_EXPORT Transform
{
  public:
    typedef QSharedPointer<Transform> Ptr;

  public:
    Transform(Query::Ptr const &query);
    ~Transform();

    void addTransformRule(TransformRule const &rule);

    /// Returns true when there is one or more transformation rule that applies
    /// to given @param use or when a rule does not apply but does have "else actions".
    bool appliesTo(QueryHit::Ptr use) const;

    /**
     * Get changes for this transformation respective @p use.
     * The default implementation does nothing even when non-empty transform rules are set.
     */
    QList<KDevelop::DocumentChangePointer> changesForUse(QueryHit::Ptr use);

    Query::Ptr query() const;

  protected:
    class TransformPrivate * const d;
};

#endif // TRANSFORM_H
