/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "replacementgenerator.h"

#include <QtCore/QDebug>
#include <QtCore/QRegExp>

#include <language/editor/simplerange.h>

#include <libquery/queryuses/functioncall.h>
#include <libquery/queryuses/classfunctioncall.h>
#include <libquery/queryuses/classuse.h>

using namespace KDevelop;

/// ReplacementGeneratorPrivate

class ReplacementGeneratorPrivate
{
  public: /// Static members
    static QRegExp sFunctionsExp; // Does a greedy match
    static QRegExp sItemExp;
    static QRegExp sOffsetItemExp;

  public: /// Members
    CodeRepresentation::Ptr mRepresentation;
    ReplacementResult  mResult;
    QString            mResultText;
    QueryHit::Ptr      mUse;

  public:  /// Functions
    QString replaceItemPlaceHolder(QString const &placeHolder);
    QString replaceOffsetItemPlaceHolder(QString const &placeHolder, unsigned offset);
    QString replacePlaceHolder(QString const &placeHolder);
};

QRegExp ReplacementGeneratorPrivate::sFunctionsExp
  = QRegExp("^literalVal\\((.*)\\)"); // Do greedy match

QRegExp ReplacementGeneratorPrivate::sItemExp
  = QRegExp("^(FunctionId|TypeId|ObjectId|Accessor|MemberId)$");

QRegExp ReplacementGeneratorPrivate::sOffsetItemExp
  = QRegExp("^(Arg|FunctionTemplateArg|TypeTemplateArg|MemberTemplateArg)\\[(\\d+)\\]$");


QString ReplacementGeneratorPrivate::replaceItemPlaceHolder(QString const &placeHolder)
{
  RangeInRevision range = RangeInRevision::invalid();
  ReplacementResult::Status status = ReplacementResult::MissingUseItem;

  if (placeHolder == "All") {
    range = mUse->range();
  } else if (placeHolder == "FunctionId") {
    FunctionCall::Ptr funcCall = mUse.dynamicCast<FunctionCall>();
    if ( funcCall ) {
      range = funcCall->functionIdentifierRange();
    } else {
      status = ReplacementResult::InvalidPlaceHolder;
    }
  } else if (placeHolder == "TypeId") {
    ClassUse::Ptr classUse = mUse.dynamicCast<ClassUse>();
    if ( classUse ) {
      range = classUse->identifierRange();
    } else {
      status = ReplacementResult::InvalidPlaceHolder;
    }
  } else if (placeHolder == "ObjectId" || placeHolder == "Accessor" || placeHolder == "MemberId") {
    ClassFunctionCall::Ptr funcCall = mUse.dynamicCast<ClassFunctionCall>();
    if ( funcCall ) {
      if (placeHolder == "ObjectId") {
        range = funcCall->object()->range();
      } else if (placeHolder == "Accessor") {
        range = funcCall->accessorRange();
      } else if (placeHolder == "MemberId") {
        range = funcCall->functionIdentifierRange();
      }
    } else {
      status = ReplacementResult::InvalidPlaceHolder;
    }
  }
  if (range.isValid())
    return mRepresentation->rangeText(range.castToSimpleRange().textRange());

  mResult.setStatus(status);
  mResult.setErrorString(placeHolder);
  return QString();
}

QString ReplacementGeneratorPrivate::replaceOffsetItemPlaceHolder(QString const &placeHolder, unsigned offset)
{
  RangeInRevision range = RangeInRevision::invalid();
  ReplacementResult::Status status = ReplacementResult::InvalidPlaceHolder;

  if (placeHolder == "Arg" || placeHolder == "FunctionTemplateArg" || placeHolder == "MemberTemplateArg") {
    FunctionCall::Ptr funcCall = mUse.dynamicCast<FunctionCall>();
    if (funcCall) {
      if ( placeHolder == "Arg" ) {
        range = funcCall->argument(offset)->range();
      } else {
        range = funcCall->templateArgumentRange(offset);
      }
    }
  } else if (placeHolder == "TypeTemplateArg") {
    ClassUse::Ptr classUse = mUse.dynamicCast<ClassUse>();
    if (classUse) {
      range = classUse->templateArgumentRange(offset);
    }
  }

  if (range.isValid())
    return mRepresentation->rangeText(range.castToSimpleRange().textRange());

  mResult.setStatus(status);
  mResult.setErrorString(placeHolder + '[' + QString::number(offset) + ']');
  return QString();
}

QString ReplacementGeneratorPrivate::replacePlaceHolder(QString const &placeHolder)
{
  if (mResult.status() != ReplacementResult::Ok)
    return QString();

  if (sFunctionsExp.indexIn(placeHolder) != -1) {
    QString replacement = replacePlaceHolder(sFunctionsExp.cap(1));
    if (mResult.status() != ReplacementResult::Ok)
      return QString();

    // The only function we support is literalVal now so chop of the '"' from
    // begin and end if needed.
    if (replacement.startsWith('"'))
      replacement = replacement.right(replacement.size() - 1);
    if (replacement.endsWith('"'))
      replacement.chop(1);

    return replacement;
  } else  if (sItemExp.indexIn(placeHolder) != -1) {
    return replaceItemPlaceHolder(placeHolder);
  } else if (sOffsetItemExp.indexIn(placeHolder) != -1) {
    return replaceOffsetItemPlaceHolder(sOffsetItemExp.cap(1), sOffsetItemExp.cap(2).toUInt());
  } else {
    mResult.setStatus(ReplacementResult::InvalidPlaceHolder);
    mResult.setErrorString(placeHolder);
    return QString();
  }

  Q_ASSERT(false);
  return QString();
}

/// ReplacementGenerator

ReplacementGenerator::ReplacementGenerator()
  : d(new ReplacementGeneratorPrivate)
{ }

ReplacementGenerator::~ReplacementGenerator()
{
  delete d;
}

ReplacementResult ReplacementGenerator::generate(QString const &text,
                                                 QueryHit::Ptr use,
                                                 CodeRepresentation::Ptr const &representation)
{
  d->mUse = use;
  d->mRepresentation = representation;
  d->mResult = ReplacementResult();
  d->mResult.setStatus(ReplacementResult::Ok);
  d->mResultText = text;

  QRegExp placeHolderExp("\\$\\{(.*)\\}");
  placeHolderExp.setMinimal(true);

  int pos = 0;
  QString replacement;
  while ((pos = placeHolderExp.indexIn(d->mResultText, pos)) != -1
          && d->mResult.status() == ReplacementResult::Ok) {
    replacement = d->replacePlaceHolder(placeHolderExp.cap(1));

    if (d->mResult.status() !=  ReplacementResult::Ok)
      return d->mResult;

    d->mResultText.replace(pos, placeHolderExp.matchedLength(), replacement);
    pos += replacement.length();
  }

  d->mResult.setStatus(ReplacementResult::Ok);
  d->mResult.setText(d->mResultText);
  return d->mResult;
}
