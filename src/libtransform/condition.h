/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CONDITION_H
#define CONDITION_H

#include <QtCore/QVariant>

#include <libquery/queryuses/queryhit.h>

#include "cpptransformengine_export.h"

class CPPTRANSFORMENGINE_EXPORT Condition
{
public:
  enum Property {
    Invalid,
    ImplicitCtorCall,
    ImplicitOnThis,
    ArgCount
  };

  explicit Condition(bool always = true);
  Condition(Property property, QVariant const &expectedValue);

  bool appliesTo(QueryHit::Ptr use) const;
  bool isValid() const;

private:
  bool     mAlways : 1;
  bool     mValid  : 1;
  Property mProperty;
  QVariant mExpectedValue;
};

#endif // CONDITION_H