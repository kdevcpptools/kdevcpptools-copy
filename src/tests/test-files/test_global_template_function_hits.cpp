class TestType {};

template<typename T> void test0() {}
template<typename T1, typename T2> void test0() {}
template<typename T1, typename T2, typename T3> void test0() {}
template<typename T> void test1(char *x) {}
template<typename T> void test1(int anIntArgument) {}
template<typename T> void test2(int foo, char const * bar) {}
template<typename T> void test3(int foo, char const * bar, char const* bar2 = "") {}

int main(int argc, char *argv[])
{
  test0<char>();
  test0<TestType>();
  test0<float>();

  test0<char, float>();
  test0<TestType, char, float>();
}
