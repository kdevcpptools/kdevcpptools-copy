#include "qobject.h"

void child() {
  QObject* obj;
  obj->child("foo");
  obj->child("foo", "asdfasdf");
  obj->child("foo", 0);
  obj->child("foo", 0, TRUE);
  obj->child("foo", "asdasdf", TRUE);
  obj->child("foo", "asdasdf", FALSE);
  obj->child("foo", 0, FALSE);
}

#include "qobjectlist.h"

void queryList() {
  QObject* obj;
  QObjectList* queries0 = obj->queryList();
  QObjectList* queries1 = obj->queryList("someParent");
  QObjectList* queries2 = obj->queryList("someParent", "someObjName");
  QObjectList* queries3 = obj->queryList("someParent", "someObjName", TRUE);
  queries3 = obj->queryList("someParent", "someObjName", FALSE);
  QObjectList* queries4 = obj->queryList("someParent", "someObjName", TRUE, TRUE);
  queries4 = obj->queryList("someParent", "someObjName", TRUE, 1);
  queries4 = obj->queryList("someParent", "someObjName", TRUE, true);
  queries4 = obj->queryList("someParent", "someObjName", TRUE, FALSE);
  queries4 = obj->queryList("someParent", "someObjName", TRUE, 0);
  queries4 = obj->queryList("someParent", "someObjName", TRUE, false);
}

int main() {
  return 0;
}