#include "qtooltip.h"

void add() {
  QWidget* myWidget;
  QToolTip::add(myWidget, "someString");
  QToolTip::add(myWidget, QString("asdfadsf"));
  QString someStr = "asdf";
  QToolTip::add(myWidget, someStr);
  // these should not match:
  QToolTip::add(myWidget, someStr, 0, "asdfasdf");
  QToolTip::add(myWidget, someStr, new QToolTipGroup, QString("asdf"));
}

int main() {
  return 0;
}