#include "qptrlist.h"
#include "qwidget.h"
#include "qstring.h"

void qPtrList() {
  QPtrList<int> t1;
  QPtrList<QWidget> t2;
  QPtrList<QString> t3;
}

void isEmpty() {
  QPtrList<QWidget> t1;
  t1.isEmpty();
  QPtrList<t1>().isEmpty();
}

void next() {
  QPtrList<QWidget> t1;

  QWidget* it = t1.first();
  while (it != NULL)
    it = t1.next();
}

void contains() {
  QWidget * foo = new QWidget();
  QPtrList<QWidget> list;
  list.append(foo);
  Q_ASSERT(list.contains(list));
}

void remove() {
  QWidget* foo = new QWidget();
  QPtrList<QWidget> list;
  list.append(foo);
  Q_ASSERT(list.remove(0));
  list.append(foo);
  Q_ASSERT(list.remove(foo));
  uint idx = 5;
  Q_ASSERT(!list.remove(idx));
  Q_ASSERT(list.remove(idx - 2*2));
  list.append(foo);
  QWidget* bar = foo;
  Q_ASSERT(list.remove(bar));
}

int main() {
  return 0;
}
