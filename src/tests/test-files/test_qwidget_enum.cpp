namespace Qt
{
  enum WidgetFlags {
    WNone = 0,
    WDestructiveClose,
    WAnotherFlag
  };
}

class QWidget
{
public:
  QWidget(QWidget *parent = 0, int flags = Qt::WNone) {}
};

class MyWidget : public QWidget
{
public:
  MyWidget(QWidget *parent = 0, int flags = Qt::WNone) : QWidget(parent, flags) {}
};

class AnotherWidget : public QWidget
{
public:
  AnotherWidget(QWidget *parent = 0, int flags = Qt::WNone) : QWidget(parent, flags) {}
};

class YetAnotherWidget : public QWidget
{
public:
  YetAnotherWidget(QWidget *parent = 0) : QWidget(parent, Qt::WDestructiveClose) {}
};

class AndYetAnotherWidget : public AnotherWidget
{
public:
  AndYetAnotherWidget(QWidget *parent = 0, int flags = Qt::WNone) : AnotherWidget(parent, flags) {}
};

int main(int argc, char* argv[])
{
  MyWidget widget1 = MyWidget(0, Qt::WDestructiveClose); // Find and port
  AnotherWidget widget2 = AnotherWidget(0, Qt::WDestructiveClose); // Find and port
  AnotherWidget widget3 = AnotherWidget(0); // Do not find and do not port
  AndYetAnotherWidget widget4 = AndYetAnotherWidget(0, Qt::WDestructiveClose); // Find and port

  MyWidget *widget5 = new MyWidget(0, Qt::WDestructiveClose); // Find and port

  return 0;
}
