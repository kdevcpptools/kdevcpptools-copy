void test0() {}
void test1(char *x) {}
void test1(int anIntArgument) {}
void test2(int foo, char const * bar) {}
void test3(int foo, char const * bar, char const* bar2 = "someDefaultValue") {}
enum TestEnum{Foo, Bar}; void test4(TestEnum arg = Foo) {}
int main(int argc, char *argv[])
{
  test0();

  test1("%1 blah %1");
  test1("Some text");
  test1(123);

  int x = 0;
  char * y = "more text";
  test2(x, y);
  test2(1234, "and even more text");
  test2(453945, "%1 blah %1");

  test3(x, y, y);
  test3(x, y, "asdasd");
  test3(x + x^2 -x, "%1 blah %1", y);
  test3(3456, "2nd literal", "%2 blah %2");
  test3(x + 234, "asdasd"); // use default argument
  test3(x + 234, "asdasd", "someDefaultValue"); // use default argument

  test4();
  test4(Foo);
  test4(Bar);

  #define ASDF(bar) test4(bar)
  ASDF(Bar);
  #define LALA(x) if( !(x) ) { test1(5); }
  LALA(true)
}
