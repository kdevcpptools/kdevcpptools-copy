template<typename T> class Test { };
template<typename T1, typename T2> class Test2 { };
template<typename T1, typename T2, typename T3> class Test3 { };

Test<int> t = Test<int>();

int main(int argc, char* argv[])
{
  Test<double> t; // Shadows the global one.
  Test<char> t2;
  Test<unsigned> *foo = new Test<unsigned>();
  delete foo;

  Test2<int, char> t21 = Test2<int, char>();
  Test2<unsigned, double>* t23 = new Test2<unsigned, double>();
  Test2<unsigned, double> t24;

  Test3<int, char, double> t31 = Test3<int, char, double>();
  Test3<int, float, long int> t32;
  Test3<unsigned, double, Test<float> >* t33 = new Test3<unsigned, double, Test<float> >();
  return 0;
}
