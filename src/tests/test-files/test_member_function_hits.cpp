class Test
{
public:
  void foo() {}
  static void staticFoo() { }
  void bar()
  {
    foo(); // implicit call on this
    this->foo(); // explicit call on this
  }

  void test1(int arg1) { }
  void test2(int arg1, char const *arg2) { }
  void test3(int arg1, char const *arg2, float arg3) { }

  Test* self() { return this; }
  Test copy() { return Test; }
};

Test someMethodReturningTest() { return Test(); }

int main(int argc, char *argv[])
{
  Test t;
  t.foo();
  t.bar();
  (&t)->foo();
  (&t)->bar();
  t.self()->foo();
  t.copy().foo();

  Test::staticFoo();
  t.staticFoo();

  int number = 234;
  char const *c = "testcharliteral";
  t.test1(1324234);
  t.test1(number);
  t.test2(number, c);
  t.test2(number, "dsafsdf");
  t.test2(234, "dsafsdf");

  float fnumber =  3.14;
  t.test3(number, c, fnumber);

  someMethodReturningTest().test3(123, "asdasd", 5.6);

  #define FOO(bar, str, f) t.test3(bar, str, f)
  FOO(234, "asdf", fnumber);
  #define LALA(x) if( !(x) ) { t.test1(5); }
  LALA(true)

  Test* t2 = new Test;
  dynamic_cast<Test*>(t2)->foo();
}
