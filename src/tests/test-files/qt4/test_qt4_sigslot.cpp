#include <QtCore/QObject>

class Foo : public QObject {
  Q_OBJECT
  public:
    void someExistingNonSlot();

  public slots:
    void someExistingPublicSlot();
  protected slots:
    void someExistingProtectedSlot();
  private slots:
    void someExistingPrivateSlot();

  signals:
    void someExistingSignal();
};

class Bar : public Foo {
  Q_OBJECT
  public:
    Bar(Foo* parent) {
      connect(parent, SIGNAL(someExistingSignal()), parent, SLOT(someExistingPublicSlot()));
      connect(parent, SIGNAL(someExistingSignal()), parent, SLOT(someExistingProtectedSlot()));
      connect(parent, SIGNAL(someExistingSignal()), parent, SLOT(someExistingPrivateSlot()));
    }
};

int main() {
  Foo* obj1 = new Foo;
  Bar* obj2 = new Bar(obj1);
  QObject::connect(obj1, SIGNAL(someExistingSignal()), obj1, SLOT(someExistingPublicSlot()));
  QObject::connect(obj1, SIGNAL(someExistingSignal()), obj1, SLOT(someExistingProtectedSlot()));
  QObject::connect(obj1, SIGNAL(someExistingSignal()), obj1, SLOT(someExistingPrivateSlot()));
}