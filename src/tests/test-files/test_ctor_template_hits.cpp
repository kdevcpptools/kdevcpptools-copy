template<typename T>
class Test
{
public:
  Test() {}
  Test(Test const &other) {}
  Test(T x) {}
};

template<typename U, typename V>
class SubTest : public Test<U>
{
public:
  SubTest() : Test<U>() {}
  SubTest(SubTest const &other) : Test<U>(other) {}
  SubTest(U x) : Test<U>(x) {}
  SubTest(U arg1, V arg2) : Test<U>(arg1) {}
};

class Test2
{
public:
  template<typename T>
  Test2(T arg) {}
};

class SubTest2 : public Test2
{
public:
  template<typename T>
  SubTest2(T arg) : Test2(arg) {}
};

int main(int argc, char *argv[])
{
  Test<int> t1;             // No use of Test() reported here, not supported
  Test<char> t2 = Test<char>();
  Test<char> t3(t2);
  Test<char> t4 = Test<char>(t2);
  Test<int> t5 = Test<int>(Test<int>(12345));
  Test<int> t6(5);
  Test2 _t1(5);
  Test2 _t2 = Test2('c');
  Test2 _t3 = Test2(t1);

  int foo = 12345;
  Test<int> t7(5);
  Test<int> t8(foo);
  Test<int> t9(5 + foo);
  Test2 _t4 = Test2(foo);

  char *charFoo = "foo char";
  SubTest<int, const char*> t10(foo, charFoo);
  SubTest2 _t5 = SubTest2(t10);

  // Same bunch of tests using the new initializer
  Test<int> *t11 = new Test<int>; // No use of Test() reported here, not supported
  Test<int> *t12 = new Test<int>(5);

  Test2* _t6 = new SubTest2(t12);
  return 0;
}
