class Test
{
public:
  Test() {}
  Test(Test const &other) {}
  Test(int x) {}
  Test(char const *arg1, char const *arg2) {}
  Test(char const *arg1, char const *arg2, int arg3) {}
};

class SubTest : public Test
{
public:
  SubTest() : Test() {}
  SubTest(SubTest const &other) : Test(other) {}
  SubTest(int x) : Test(x) {}
  SubTest(char const *arg1, char const *arg2) : Test(arg1, arg2) {}
  SubTest(char const *arg1, char const *arg2, int arg3) : Test(arg1, arg2) {}
  SubTest(int arg0, char const *arg1, char const *arg2) : Test(arg0) {}
};

int main(int argc, char *argv[])
{
  Test t1;             // No use of Test() reported here, not supported
  Test t2 = Test();
  Test t3(t2);
  Test t4 = Test(t2);
  Test t5 = Test(Test(12345));

  int foo = 12345;
  Test t6(5);
  Test t7(foo);
  Test t8(5 + foo);

  char *charFoo = "foo char";
  char *charBar = "bar char";
  Test t9("some text", "more text");
  Test t10(charFoo, "more text");
  Test t11("some text", charBar);
  Test t12(charFoo, charBar);

  Test t13 = Test("some text", "more text");
  Test t14 = Test(charFoo, "more text");
  Test t15 = Test("some text", charBar);
  Test t16 = Test(charFoo, charBar);

  Test t17(charFoo, "Text", foo + 100);
  Test t18("Plain test text", "Text", foo);
  Test t19(charFoo, charBar, foo);
  Test t20(charFoo, charBar, 23234234);

  // Same bunch of tests using the new initializer
  Test *t20 = new Test; // No use of Test() reported here, not supported
  Test *t21 = new Test();
  Test *t22 = new Test(t2);
  Test *t23 = new Test(Test(12345));

  Test *t24 = new Test("some text", "more text");
  Test *t25 = new Test(charFoo, "more text");
  Test *t26 = new Test("some text", charBar);
  Test *t27 = new Test(charFoo, charBar);

  Test *t28 = new Test(charFoo, "Text", foo + 100);
  Test *t29 = new Test("Plain test text", "Text", foo);
  Test *t30 = new Test(charFoo, charBar, foo);
  Test *t31 = new Test(charFoo, charBar, 23234234);

  Test t32();
  return 0;
}
