class Test {};

class SubTest : public Test {}; // Not found by VariableQuery

class UsingTest
{
public:
  UsingTest(Test const &test) {}

  void setTest1(Test const &);

  void setTest2(Test *t) {}

  void setTest3(Test const *t) {}

  void setTest4(Test const * const t) {}
};

void UsingTest::setTest1(Test const &var)
{ }

int main(int argc, char *argv[])
{
  Test *t;
  Test t2;
  Test const *t3 = &t2;
  Test &t4 = *t;
  Test const &t5 = *t3;

  return 0;
}
