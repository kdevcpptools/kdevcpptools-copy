class Test { };

Test t = Test();

int main(int argc, char* argv[])
{
  Test t; // Shadows the global one.
  Test t2;
  Test *foo = new Test();
  delete foo;
  return 0;
}

