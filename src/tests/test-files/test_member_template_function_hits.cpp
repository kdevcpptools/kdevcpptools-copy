class Test
{
public:
  template<typename T>
  void t1_0() {}
  template<typename T>
  static void t1_0_static(T arg) {}
  template<typename T, typename U>
  static void t2_static(T a1, U a2) {}
};

int main(int argc, char *argv[])
{
  Test t;
  t.t1_0<int>();
  t.t1_0<char>();

  t.t1_0_static<int>(5);
  t.t1_0_static(5);
  Test::t1_0_static<int>(1);
  Test::t1_0_static(1);
  char c = 'X';
  t.t1_0_static(c);
  Test::t1_0_static(c);

  t.t2_static("asdfasdf", 'c');
  t.t2_static('c', "asdfsadf");
  const char* str = "xycv";
  Test::t2_static(str, c);
}
