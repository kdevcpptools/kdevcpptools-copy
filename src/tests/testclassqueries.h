#ifndef TESTCLASSQUERIES_H
#define TESTCLASSQUERIES_H

#include "testbase.h"

class TestTypeQueries : public TestBase
{
  Q_OBJECT

private slots:
  void initTestCase();

  void testClassQueries();
  void testSingleTemplateArgClassQuery();
  void testSingleTemplateArgRestrictedClassQuery();
  void testTwoTemplateArgsClassQuery();
  void testTwoTemplateArgsRestrictedClassQuery();
  void testThreeTemplateArgsClassQuery();
  void testThreeTemplateArgsRestrictedClassQuery();

  void testMemberQueriesZeroArgs();
  void testMemberQueriesOneArg();
  void testMemberQueriesTwoArgs();
  void testMemberQueriesThreeArgs();

  void testMemberQueriesSingleTemplate();
  void testMemberQueriesTwoTemplate();

  void testCtorQueryZeroArgs();
  void testCtorQueryOneArg();
  void testCtorQueryTwoArgs();
  void testCtorQueryThreeArgs();

  void testCtorQuerySingleTemplate();

  void testSubclassOf();
};

#endif // TESTCLASSQUERIES_H
