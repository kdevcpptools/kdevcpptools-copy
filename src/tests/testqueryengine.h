/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef TESTQUERYENGINE_H
#define TESTQUERYENGINE_H

#include <QtCore/QObject>

#include <languages/cpp/parser/control.h>

#include <libportdb/cppportingdatabase.h>

namespace KDevelop {
class TopDUContext;
}

class TestQueryEngine : public QObject
{
  Q_OBJECT

  public:
    TestQueryEngine();

  private slots:
    void initTestCase();
    void cleanupTestCase();

    void testGlobalFunctionUses();
    void testGlobalTemplateFunctionUses();
    void testCtorUses();
    void testMemberFunctionUses();
    void testTemplateTypeMemberFunctionUses();
    // void testMemberTemplateFunctionUses(); // Not implemented yet.
    // void testTemplateTypeMemberFunctionUses(); // Not implemented yet.
    void testQWidgetSubclasses();
    void testQWidgetSubclassCtorUses();
    void testQWidgetSubclassCtorUsesRestricted();

  private: /// Functions
    void execQuery(QString const &file, QString const &query, int expectedUseCount, bool expectFail = false);
    KDevelop::TopDUContext *parse(QString const &file);

  private: /// Members
    Control                                 mControl;
    CppPortingDatabase                      mDatabase;
    QPair<QString, KDevelop::TopDUContext*> mLastContext;
    UseQueryResult                          mQueryResult;
};

#endif // TESTQUERYENGINE_H
