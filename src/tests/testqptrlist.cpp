/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "testqptrlist.h"

#include <QTest>

QTEST_MAIN(TestQPtrList)

void TestQPtrList::initTestCase()
{
  mHelper->loadDatabase("qt3-to-qt4/qptrlist.pd");
}

void TestQPtrList::classUsage()
{
  EXEC_CLASS_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>", 8);
  mHelper->dumpResult(result);
  ClassUse::Ptr use = result.hits<ClassUse>().at(0);
  VERIFY_RANGE(use->range(), 5, 2, 5, 15);
  VERIFY_RANGE(use->identifierRange(), 5, 2, 5, 10);
  QCOMPARE(use->templateArgumentCount(), 1);
  VERIFY_RANGE(use->templateArgumentRange(0), 5, 11, 5, 14);

  use = result.hits<ClassUse>().at(1);
  VERIFY_RANGE(use->range(), 6, 2, 6, 19);
  VERIFY_RANGE(use->identifierRange(), 6, 2, 6, 10);
  QCOMPARE(use->templateArgumentCount(), 1);
  VERIFY_RANGE(use->templateArgumentRange(0), 6, 11, 6, 18);

  use = result.hits<ClassUse>().at(2);
  VERIFY_RANGE(use->range(), 7, 2, 7, 19);
  VERIFY_RANGE(use->identifierRange(), 7, 2, 7, 10);
  QCOMPARE(use->templateArgumentCount(), 1);
  VERIFY_RANGE(use->templateArgumentRange(0), 7, 11, 7, 18);
}

void TestQPtrList::isEmpty()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>::isEmpty()", 2);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 12, 2, 12, 14);
  VERIFY_RANGE(use->object()->range(), 12, 2, 12, 4);
  VERIFY_RANGE(use->accessorRange(), 12, 4, 12, 5);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  VERIFY_RANGE(use->functionIdentifierRange(), 12, 5, 12, 12);
  QCOMPARE(use->argumentCount(), 0);
  QCOMPARE(use->templateArgumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 13, 2, 13, 26);
  VERIFY_RANGE(use->functionIdentifierRange(), 13, 17, 13, 24);
  VERIFY_RANGE(use->object()->range(), 13, 2, 13, 16);
  VERIFY_RANGE(use->accessorRange(), 13, 16, 13, 17);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);
  QCOMPARE(use->templateArgumentCount(), 0);
}

void TestQPtrList::next()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>::next()", 1);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 21, 9, 21, 18);
  VERIFY_RANGE(use->object()->range(), 21, 9, 21, 11);
  VERIFY_RANGE(use->accessorRange(), 21, 11, 21, 12);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  VERIFY_RANGE(use->functionIdentifierRange(), 21, 12, 21, 16);
  QCOMPARE(use->argumentCount(), 0);
  QCOMPARE(use->templateArgumentCount(), 0);
}

void TestQPtrList::contains()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>::contains(const T*)", 1);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 28, 11, 28, 30);
  VERIFY_RANGE(use->functionIdentifierRange(), 28, 16, 28, 24);
  VERIFY_RANGE(use->object()->range(), 28, 11, 28, 15);
  VERIFY_RANGE(use->accessorRange(), 28, 15, 28, 16);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 28, 25, 28, 29);

  lock.unlock();

  // same as above, just more specific
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<QWidget>::contains(const QWidget*)", 1);

  lock.lock();
  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 28, 11, 28, 30);
  VERIFY_RANGE(use->functionIdentifierRange(), 28, 16, 28, 24);
  VERIFY_RANGE(use->object()->range(), 28, 11, 28, 15);
  VERIFY_RANGE(use->accessorRange(), 28, 15, 28, 16);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 28, 25, 28, 29);
}

void TestQPtrList::remove()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>::remove(const T*)", 2);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 37, 11, 37, 27);
  VERIFY_RANGE(use->functionIdentifierRange(), 37, 16, 37, 22);
  VERIFY_RANGE(use->object()->range(), 37, 11, 37, 15);
  VERIFY_RANGE(use->accessorRange(), 37, 15, 37, 16);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 37, 23, 37, 26);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 43, 11, 43, 27);
  VERIFY_RANGE(use->functionIdentifierRange(), 43, 16, 43, 22);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 43, 23, 43, 26);

  lock.unlock();
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qptrlist-test.cpp", "QPtrList<T>::remove(uint)", 3);
  lock.lock();

  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 35, 11, 35, 25);
  VERIFY_RANGE(use->functionIdentifierRange(), 35, 16, 35, 22);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 35, 23, 35, 24);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 39, 12, 39, 28);
  VERIFY_RANGE(use->functionIdentifierRange(), 39, 17, 39, 23);
  VERIFY_RANGE(use->object()->range(), 39, 12, 39, 16);
  VERIFY_RANGE(use->accessorRange(), 39, 16, 39, 17);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 39, 24, 39, 27);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 40, 11, 40, 33);
  VERIFY_RANGE(use->functionIdentifierRange(), 40, 16, 40, 22);
  QCOMPARE(use->argumentCount(), 1);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 40, 23, 40, 32);
}
