/*
   Copyright 2010 Milian Wolff <mail@milianw.de>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include <KApplication>
#include <KCmdLineArgs>
#include <KAboutData>

#include <QDebug>
#include <QFile>
#include <QTimer>

#include "libportdb/cppportingdatabase.h"
#include "libportdb/xmlparser.h"
#include "libquery/cppqueryengine.h"
#include "libquery/queryresult.h"

#include "tests/testhelper.h"
#include <QDirIterator>
#include <language/duchain/parsingenvironment.h>

class Manager : public QObject {
  Q_OBJECT
public:
  Manager(KCmdLineArgs *args)
    : m_args(args)
  {
  }
  virtual ~Manager()
  { }

public slots:
  void init();

private:
  void getDirContents(const QString& dir, QStringList& files);
  KCmdLineArgs *m_args;
};

void Manager::getDirContents(const QString& dir, QStringList& files)
{
  QDirIterator contents(dir);
  while(contents.hasNext()) {
    QString newPath = contents.next();
    if(newPath.endsWith(".")) {
      continue;
    }
    QFileInfo info(newPath);
    if (!info.isReadable()) {
      continue;
    }
    if (info.isFile()) {
      files << newPath;
    } else {
      getDirContents(newPath, files);
    }
  }
}


void Manager::init()
{
  QTextStream out(stdout);
  QTextStream err(stderr);

  QString databaseFile;
  if (m_args->isSet("database")) {
    databaseFile = m_args->getOption("database");
  } else if(m_args->isSet("d")) {
    databaseFile = m_args->getOption("d");
  } else {
    err << i18n("ERROR: No database file given.") << endl;
    QApplication::exit(1);
    return;
  }

  XmlParser parser;
  CppPortingDatabase database;
  out << i18n("reading database '%1'...", databaseFile) << flush;
  XmlParser::ParseResult parseResult = parser.parse(databaseFile, &database);
  out << i18n(" done") << endl;
  if (parseResult == XmlParser::FileNotFound) {
    err << i18n("ERROR: Database file '%1' does not exist.", databaseFile) << endl;
    QApplication::exit(2);
    return;
  } else if (parseResult == XmlParser::FileNotReadable) {
    err << i18n("ERROR: Database file '%1' is not readable.", databaseFile) << endl;
    QApplication::exit(3);
    return;
  } else if (parseResult == XmlParser::Invalid) {
    err << i18n("ERROR: Database file '%1' is invalid.", databaseFile) << endl << parser.errorMessages().join("\n") << endl;
    QApplication::exit(4);
    return;
  }

  QStringList cppFiles;
  QStringList toParse;
  toParse << m_args->getOptionList("p") << m_args->getOptionList("parse");
  if (toParse.isEmpty()) {
    err << i18n("ERROR: Nothing to parse given.") << endl;
    QApplication::exit(6);
    return;
  }
  foreach(const QString& path, toParse) {
    QFileInfo info(path);
    if (!info.exists()) {
      err << i18n("ERROR: File or directory '%1' does not exist.", path) << endl;
      QApplication::exit(7);
      return;
    } else if (!info.isReadable()) {
      err << i18n("ERROR: File or directory '%1' is not readable.", path) << endl;
      QApplication::exit(8);
      return;
    } else if (!info.isFile()) {
      getDirContents(path, cppFiles);
    } else {
      cppFiles << info.absoluteFilePath();
    }
  }

  QList<Query::Ptr> queries;
  if (m_args->count() > 0) {
    bool gotQueryError = false;
    for (int i = 0; i < m_args->count(); ++i) {
      if (!database.containsQuery(m_args->arg(i))) {
        err << i18n("ERROR: Unknown query '%1'.", m_args->arg(i)) << endl;
        gotQueryError = true;
        continue;
      }
      queries << database.query(m_args->arg(i));
    }
    if (gotQueryError) {
      QApplication::exit(5);
      return;
    }
  } else {
    queries = database.queries();
  }

  if (queries.isEmpty()) {
    err << i18n("ERROR: no queries selected") << endl;
    QApplication::exit(9);
    return;
  }

  if (cppFiles.isEmpty()) {
    err << i18n("ERROR: no files to parse") << endl;
    QApplication::exit(10);
    return;
  }

  TestHelper helper;

  QMap<QString, int> hitsPerFile;
  QMap<QString, int> hitsPerQuery;

  foreach( const QString path, cppFiles) {
    out << i18n("getting DUChain for file '%1'...", path) << flush;
    KDevelop::ReferencedTopDUContext context = helper.parse(path);
    out << i18n(" done") << endl;
    if (!context) {
      out << i18n("\tno context received, skipping file '%1'", path) << endl;
      continue;
    }

    CppQueryEngine engine;

    foreach(const Query::Ptr& query, queries) {
      QueryResult result = engine.query(context, query);
      if (result.hits().isEmpty()) {
        continue;
      }
      out << i18n("results of query %1", query->id()) << endl;
      hitsPerFile[path] += result.hits().size();
      hitsPerQuery[query->id()] += result.hits().size();
      helper.dumpResult(result);
      out << endl;
    }
  }

  out << endl << "==== SUMMARY ====" << endl;
  QMultiMap<int, QString> fileForHits;
  QMap< QString, int >::const_iterator it = hitsPerFile.constBegin();
  while (it != hitsPerFile.constEnd()) {
    if (it.value() > 0) {
      fileForHits.insert(it.value(), it.key());
    }
    ++it;
  }
  out << "## FILES ##" << endl;
  QMultiMap< int, QString >::const_iterator it2 = fileForHits.constBegin();
  while (it2 != fileForHits.constEnd()) {
    out << it2.key() << '\t' << it2.value() << endl;
    ++it2;
  }
  QMultiMap<int, QString> queryForHits;
  it = hitsPerQuery.constBegin();
  while (it != hitsPerQuery.constEnd()) {
    if (it.value() > 0) {
      queryForHits.insert(it.value(), it.key());
    }
    ++it;
  }
  out << endl << "## QUERIES ##" << endl;
  it2 = queryForHits.constBegin();
  while (it2 != queryForHits.constEnd()) {
    out << it2.key() << '\t' << it2.value() << endl;
    ++it2;
  }

  out << endl << "done..." << endl;
  QApplication::exit();
}

int main(int argc, char** argv) {
  KAboutData aboutData( "cppquery-cli", 0, ki18n( "cppquery-cli" ),
                        "1", ki18n("Cpp Query CLI runner"), KAboutData::License_GPL,
                        ki18n( "(c) 2010 Milian Wolff" ), KLocalizedString(), "http://gitorious.org/kdevcpptools"
                                                                                );
  KCmdLineArgs::init( argc, argv, &aboutData, KCmdLineArgs::CmdLineArgNone );
  KCmdLineOptions options;

  options.add("d <DATABASE>").add("database <DATABASE>", ki18n("PortDB file"));
  options.add("p <FILEORFOLDER>").add("parse <FILEORFOLDER>", ki18n("Cpp file or directory of Cpp files to run the query on.\n"
                                                                      "Can be specified multiple times."));
  options.add("+queries", ki18n("Optionally defines which queries should be run.\n"
                                "By default all queries in the database will be run."));

  KCmdLineArgs::addCmdLineOptions( options );

  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

  KApplication app(false);
  Manager m(args);
  QTimer::singleShot(0, &m, SLOT(init()));
  return app.exec();
}

#include "cppquery-cli.moc"
