/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "testqt4sigslot.h"

#include <QTest>

QTEST_MAIN(TestQt4SigSlot)

void TestQt4SigSlot::initTestCase()
{
  mHelper->loadDatabase("qt4/qt-sigslot.pd");
}

void TestQt4SigSlot::testConnect()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt4/test_qt4_sigslot.cpp",
                           "QObject::connect(const QObject*, const char *, const QObject*, const char*, Qt::ConnectionType)", 6);
  mHelper->dumpResult(result);
}

