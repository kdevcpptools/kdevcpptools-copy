/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "testenumqueries.h"

#include <QTest>

#include "libquery/queries/enumquery.h"
#include "libquery/queryuses/enumuse.h"

QTEST_MAIN(TestEnumQueries)

void TestEnumQueries::initTestCase()
{
  mHelper->loadDatabase("new_test_queries.pd");
}

void TestEnumQueries::enumTypeUse()
{
  EXEC_ENUM_QUERY(mHelper, "test_enum_hits.cpp", "Foo", 3);

  mHelper->dumpResult(result);
  QList< QSharedPointer< EnumUse > > uses = result.hits<EnumUse>();
  VERIFY_RANGE(uses[0]->range(), 7, 2, 7, 5);
  VERIFY_RANGE(uses[1]->range(), 8, 2, 8, 5);
  VERIFY_RANGE(uses[2]->range(), 9, 2, 9, 5);
}

void TestEnumQueries::enumValueUse()
{
  EXEC_ENUM_QUERY(mHelper, "test_enum_hits.cpp", "Foo::FooVal1", 1);
  QList< QSharedPointer< EnumUse > > uses = result.hits<EnumUse>();
  VERIFY_RANGE(uses[0]->range(), 7, 13, 7, 20);

  EXEC_ENUM_QUERY(mHelper, "test_enum_hits.cpp", "Foo::FooVal2", 1);
  uses = result.hits<EnumUse>();
  VERIFY_RANGE(uses[0]->range(), 8, 13, 8, 20);

  EXEC_ENUM_QUERY(mHelper, "test_enum_hits.cpp", "Foo::FooVal3", 1);
  uses = result.hits<EnumUse>();
  VERIFY_RANGE(uses[0]->range(), 9, 13, 9, 20);
}
