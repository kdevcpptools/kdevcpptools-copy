#ifndef TESTHELPER_H
#define TESTHELPER_H

#include <QtCore/QPair>

#include "libportdb/cppportingdatabase.h"
#include "libquery/queryresult.h"
#include "libquery/cppqueryengine.h"

#include <language/duchain/topducontext.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/interfaces/iastcontainer.h>

#define VERIFY_RANGE(range, sLine, sColumn, eLine, eColumn) \
  QVERIFY(range.isValid()); \
  QCOMPARE(range.start.line, sLine); \
  QCOMPARE(range.start.column, sColumn); \
  QCOMPARE(range.end.line, eLine); \
  QCOMPARE(range.end.column, eColumn);

#define EXEC_QUERY(helper, file, queryId, expectedCount, type, resultType) \
  QVERIFY(helper->database()->containsQuery(queryId)); \
  QVERIFY(helper->database()->query<type>(queryId) != 0); \
  result = helper->execQuery<type>(file, queryId); \
  QCOMPARE(result.hits<resultType>().count(), expectedCount)

#define EXEC_CLASS_QUERY(helper, file, queryId, expectedCount) \
  EXEC_QUERY(helper, file, queryId, expectedCount, ClassQuery, ClassUse);

#define EXEC_FUNCTION_QUERY(helper, file, queryId, expectedCount) \
  EXEC_QUERY(helper, file, queryId, expectedCount, FunctionQuery, FunctionCall);

#define EXEC_CLASSFUNCTION_QUERY(helper, file, queryId, expectedCount) \
  EXEC_QUERY(helper, file, queryId, expectedCount, ClassFunctionQuery, ClassFunctionCall);

#define EXEC_ENUM_QUERY(helper, file, queryId, expectedCount) \
  EXEC_QUERY(helper, file, queryId, expectedCount, EnumQuery, EnumUse);

namespace KDevelop {
class TestProjectController;
class TestCore;
}

class TestHelper
{
public:
  TestHelper();
  ~TestHelper();

  CppPortingDatabase *database() const;

  template<typename T>
  QueryResult execQuery(QString const &file, QString const &queryId);

  void loadDatabase(QString const &databaseFileName);

  /// dumps queries to stdout (for debugging)
  void dumpQueries() const;
  /// dumps result to stdout (for debugging)
  void dumpResult(QueryResult const &result) const;

  KDevelop::ReferencedTopDUContext parse(QString const &fileName) const;

private:
  CppPortingDatabase                     *mDatabase;
  QPair<QString, KDevelop::ReferencedTopDUContext> mLastContext;

  KDevelop::TestProjectController* m_projectController;
  KDevelop::TestCore* m_core;
};

template<typename T>
QueryResult TestHelper::execQuery(const QString& file, const QString& queryId)
{
  KDevelop::ReferencedTopDUContext context = mLastContext.second;
  if (mLastContext.first != file) {
    context = parse(file);
    Q_ASSERT(context->ast());
    mLastContext = qMakePair(file, context);
  }

  KDevelop::DUChainWriteLocker lock(KDevelop::DUChain::lock());

  Q_ASSERT(context->ast());

  QSharedPointer<T> query = mDatabase->query<T>(queryId);

  CppQueryEngine engine;
  QueryResult result = engine.query(context, query);
  return result;
}

#endif // TESTHELPER_H
