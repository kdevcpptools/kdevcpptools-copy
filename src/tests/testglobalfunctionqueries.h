#ifndef TESTGLOBALFUNCTIONQUERIES_H
#define TESTGLOBALFUNCTIONQUERIES_H

#include "testbase.h"

class TestGlobalFunctionQueries : public TestBase
{
  Q_OBJECT
private slots:
  void initTestCase();

  void testZeroArugments();
  void testOneArgument();
  void testTwoArguments();
  void testTwoArgumentsRestricted();
  void testThreeArguments();
  void testThreeArgumentsRestricted();

  void testOneTemplateArugment();
  void testTwoTemplateArguments();
  void testThreeTemplateArguments();

  void testDefaultArgumentRestriction();
};

#endif // TESTGLOBALFUNCTIONQUERIES_H
