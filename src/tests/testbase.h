/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#ifndef TESTBASE_H
#define TESTBASE_H

#include <QObject>

#include "testhelper.h"

#include <libquery/queryuses/classuse.h>
#include <libquery/queryuses/classfunctioncall.h>
#include <libquery/queryuses/functioncall.h>

class TestBase : public QObject {
  Q_OBJECT

public:
  TestBase();

private Q_SLOTS:
  void cleanupTestCase();

protected:
  TestHelper* mHelper;
  QueryResult result;
};

#endif // TESTBASE_H
