#include "testglobalfunctionqueries.h"

#include <QTest>

using namespace KDevelop;

QTEST_MAIN(TestGlobalFunctionQueries)

void TestGlobalFunctionQueries::initTestCase()
{
  mHelper->loadDatabase("new_test_queries.pd");
}

void TestGlobalFunctionQueries::testZeroArugments()
{
  QueryResult result("", IndexedString());
  //EXEC_CLASS_QUERY(mHelper, "test_global_function_hits.cpp", "test-global-template-1-func-test0", 0);
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "test0()", 1);

  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 8, 2, 8, 9);
  VERIFY_RANGE(use->functionIdentifierRange(), 8, 2, 8, 7);

  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "*()", 1);
}

void TestGlobalFunctionQueries::testOneArgument()
{
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "*(char*)", 2);
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "*(int)", 2);

  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "test1(char*)", 2);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 10, 2, 10, 21);
  VERIFY_RANGE(use->functionIdentifierRange(), 10, 2, 10, 7);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 10, 8, 10, 20);

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 11, 2, 11, 20);
  VERIFY_RANGE(use->functionIdentifierRange(), 11, 2, 11, 7);
  VERIFY_RANGE(use->argument(0)->range(), 11, 8, 11, 19);

  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "test1(int)", 2);
  use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 12, 2, 12, 12);
  VERIFY_RANGE(use->functionIdentifierRange(), 12, 2, 12, 7);
  VERIFY_RANGE(use->argument(0)->range(), 12, 8, 12, 11);

  ///TODO: what to do with the func call in macro use?
  use = result.hits<FunctionCall>().at(1);
  QVERIFY(use->inMacro());
  VERIFY_RANGE(use->range(), 34, 12, 34, 12);
  VERIFY_RANGE(use->functionIdentifierRange(), 34, 12, 34, 12);
  QEXPECT_FAIL("", "we don't get an AST node here, hence cannot see how many arguments got used...", Abort);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 34, 12, 34, 12);
}

void TestGlobalFunctionQueries::testTwoArguments()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "test2(int, char const*)", 3);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 16, 2, 16, 13);
  VERIFY_RANGE(use->functionIdentifierRange(), 16, 2, 16, 7);
  VERIFY_RANGE(use->argument(0)->range(), 16, 8, 16, 9);
  VERIFY_RANGE(use->argument(1)->range(), 16, 11, 16, 12);

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 17, 2, 17, 35);
  VERIFY_RANGE(use->functionIdentifierRange(), 17, 2, 17, 7);
  VERIFY_RANGE(use->argument(0)->range(), 17, 8, 17, 12);
  VERIFY_RANGE(use->argument(1)->range(), 17, 14, 17, 34);

  use = result.hits<FunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 18, 2, 18, 29);
  VERIFY_RANGE(use->functionIdentifierRange(), 18, 2, 18, 7);
  VERIFY_RANGE(use->argument(0)->range(), 18, 8, 18, 14);
  VERIFY_RANGE(use->argument(1)->range(), 18, 16, 18, 28);
}

void TestGlobalFunctionQueries::testTwoArgumentsRestricted()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp", "test2(int, char const*) : res", 1);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 18, 2, 18, 29);
  VERIFY_RANGE(use->functionIdentifierRange(), 18, 2, 18, 7);
  VERIFY_RANGE(use->argument(0)->range(), 18, 8, 18, 14);
  VERIFY_RANGE(use->argument(1)->range(), 18, 16, 18, 28);
}

void TestGlobalFunctionQueries::testThreeArguments()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper,
                      "test_global_function_hits.cpp",
                      "test3(int, char const*, char const*)", 6);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 20, 2, 20, 16);
  VERIFY_RANGE(use->functionIdentifierRange(), 20, 2, 20, 7);
  VERIFY_RANGE(use->argument(0)->range(), 20, 8, 20, 9);
  VERIFY_RANGE(use->argument(1)->range(), 20, 11, 20, 12);
  VERIFY_RANGE(use->argument(2)->range(), 20, 14, 20, 15);

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 21, 2, 21, 23);
  VERIFY_RANGE(use->functionIdentifierRange(), 21, 2, 21, 7);
  VERIFY_RANGE(use->argument(0)->range(), 21, 8, 21, 9);
  VERIFY_RANGE(use->argument(1)->range(), 21, 11, 21, 12);
  VERIFY_RANGE(use->argument(2)->range(), 21, 14, 21, 22);

  use = result.hits<FunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 22, 2, 22, 36);
  VERIFY_RANGE(use->functionIdentifierRange(), 22, 2, 22, 7);
  VERIFY_RANGE(use->argument(0)->range(), 22, 8, 22, 18);
  VERIFY_RANGE(use->argument(1)->range(), 22, 20, 22, 32);
  VERIFY_RANGE(use->argument(2)->range(), 22, 34, 22, 35);

  use = result.hits<FunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 23, 2, 23, 42);
  VERIFY_RANGE(use->functionIdentifierRange(), 23, 2, 23, 7);
  VERIFY_RANGE(use->argument(0)->range(), 23, 8, 23, 12);
  VERIFY_RANGE(use->argument(1)->range(), 23, 14, 23, 27);
  VERIFY_RANGE(use->argument(2)->range(), 23, 29, 23, 41);

  use = result.hits<FunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 24, 2, 24, 26);
  VERIFY_RANGE(use->functionIdentifierRange(), 24, 2, 24, 7);
  VERIFY_RANGE(use->argument(0)->range(), 24, 8, 24, 15);
  VERIFY_RANGE(use->argument(1)->range(), 24, 17, 24, 25);
   // third arg uses the default one and should  therefore be invalid.
  QCOMPARE(use->argumentCount(), 2);

  use = result.hits<FunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 25, 2, 25, 46);
  VERIFY_RANGE(use->functionIdentifierRange(), 25, 2, 25, 7);
  VERIFY_RANGE(use->argument(0)->range(), 25, 8, 25, 15);
  VERIFY_RANGE(use->argument(1)->range(), 25, 17, 25, 25);
  VERIFY_RANGE(use->argument(2)->range(), 25, 27, 25, 45);
}

void TestGlobalFunctionQueries::testThreeArgumentsRestricted()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp",
                      "test3(int, char const*, char const*) : res1", 1);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 22, 2, 22, 36);
  VERIFY_RANGE(use->functionIdentifierRange(), 22, 2, 22, 7);
  VERIFY_RANGE(use->argument(0)->range(), 22, 8, 22, 18);
  VERIFY_RANGE(use->argument(1)->range(), 22, 20, 22, 32);
  VERIFY_RANGE(use->argument(2)->range(), 22, 34, 22, 35);

  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp",
                      "test3(int, char const*, char const*) : res2", 1);
  use = result.hits<FunctionCall>().at(0);

  VERIFY_RANGE(use->range(), 23, 2, 23, 42);
  VERIFY_RANGE(use->functionIdentifierRange(), 23, 2, 23, 7);
  VERIFY_RANGE(use->argument(0)->range(), 23, 8, 23, 12);
  VERIFY_RANGE(use->argument(1)->range(), 23, 14, 23, 27);
  VERIFY_RANGE(use->argument(2)->range(), 23, 29, 23, 41);
}

void TestGlobalFunctionQueries::testOneTemplateArugment()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0()", 0);
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<T>()", 3);

  int offset = 12; // Makes it easier to add code before the uses.
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), offset, 2, offset, 15);
  VERIFY_RANGE(use->functionIdentifierRange(), offset, 2, offset, 7);
  VERIFY_RANGE(use->templateArgumentRange(0), offset, 8, offset, 12);

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), offset + 1, 2, offset + 1, 19);
  VERIFY_RANGE(use->functionIdentifierRange(), offset + 1, 2, offset + 1, 7);
  VERIFY_RANGE(use->templateArgumentRange(0), offset + 1, 8, offset + 1, 16);

  use = result.hits<FunctionCall>().at(2);
  VERIFY_RANGE(use->range(), offset + 2, 2, offset + 2, 16);
  VERIFY_RANGE(use->functionIdentifierRange(), offset + 2, 2, offset + 2, 7);
  VERIFY_RANGE(use->templateArgumentRange(0), offset + 2, 8, offset + 2, 13);

  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<char>()", 1);
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<float>()", 1);
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<TestType>()", 1);
}

void TestGlobalFunctionQueries::testTwoTemplateArguments()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<T,U>()", 1);

  int offset = 12; // Makes it easier to add code before the uses.
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), offset + 4, 2, offset + 4, 22);
  VERIFY_RANGE(use->functionIdentifierRange(), offset + 4, 2, offset + 4, 7);
  VERIFY_RANGE(use->templateArgumentRange(0), offset + 4, 8, offset + 4, 12);
  VERIFY_RANGE(use->templateArgumentRange(1), offset + 4, 14, offset + 4, 19);
}

void TestGlobalFunctionQueries::testThreeTemplateArguments()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_template_function_hits.cpp",
                      "test0<T,U,V>()", 1);

  int offset = 12; // Makes it easier to add code before the uses.
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), offset + 5, 2, offset + 5, 32);
  VERIFY_RANGE(use->functionIdentifierRange(), offset + 5, 2, offset + 5, 7);
  VERIFY_RANGE(use->templateArgumentRange(0), offset + 5, 8, offset + 5, 16);
  VERIFY_RANGE(use->templateArgumentRange(1), offset + 5, 18, offset + 5, 22);
  VERIFY_RANGE(use->templateArgumentRange(2), offset + 5, 24, offset + 5, 29);
}

void TestGlobalFunctionQueries::testDefaultArgumentRestriction()
{
  QueryResult result("", IndexedString());
  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp",
                      "test4(Foo)", 2);
  mHelper->dumpResult(result);
  FunctionCall::Ptr use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 27, 2, 27, 9);
  VERIFY_RANGE(use->functionIdentifierRange(), 27, 2, 27, 7);
  // default arg
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 28, 2, 28, 12);
  VERIFY_RANGE(use->functionIdentifierRange(), 28, 2, 28, 7);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 28, 8, 28, 11); // explicitly set

  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp",
                      "test4(Bar)", 2);
  mHelper->dumpResult(result);
  use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 29, 2, 29, 12);
  VERIFY_RANGE(use->functionIdentifierRange(), 29, 2, 29, 7);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 29, 8, 29, 11);
  //TODO: what should we do with this case?
  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 32, 11, 32, 11);
  VERIFY_RANGE(use->functionIdentifierRange(), 32, 11, 32, 11);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 32, 7, 32, 10);

  EXEC_FUNCTION_QUERY(mHelper, "test_global_function_hits.cpp",
                      "test3(int, char const*, char const*) : res3", 2);
  mHelper->dumpResult(result);

  use = result.hits<FunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 24, 2, 24, 26);
  VERIFY_RANGE(use->functionIdentifierRange(), 24, 2, 24, 7);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 24, 8, 24, 15);
  VERIFY_RANGE(use->argument(1)->range(), 24, 17, 24, 25);
  // 3rd. is default arg

  use = result.hits<FunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 25, 2, 25, 46);
  VERIFY_RANGE(use->functionIdentifierRange(), 25, 2, 25, 7);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 25, 8, 25, 15);
  VERIFY_RANGE(use->argument(1)->range(), 25, 17, 25, 25);
  VERIFY_RANGE(use->argument(2)->range(), 25, 27, 25, 45); // explicitly set
}
