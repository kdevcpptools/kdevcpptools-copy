/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "testqtooltip.h"

#include <QTest>

QTEST_MAIN(TestQToolTip)

void TestQToolTip::initTestCase()
{
  mHelper->loadDatabase("qt3-to-qt4/qtooltip.pd");
}

void TestQToolTip::add()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qtooltip-test.cpp", "QToolTip::add(QWidget *, const QString &)", 3);
  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 4, 2, 4, 39);
  VERIFY_RANGE(use->functionIdentifierRange(), 4, 12, 4, 15);
  VERIFY_RANGE(use->object()->range(), 4, 2, 4, 10);
  VERIFY_RANGE(use->accessorRange(), 4, 10, 4, 12);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 4, 16, 4, 24);
  VERIFY_RANGE(use->argument(1)->range(), 4, 26, 4, 38);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 5, 2, 5, 46);
  VERIFY_RANGE(use->functionIdentifierRange(), 5, 12, 5, 15);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 5, 16, 5, 24);
  VERIFY_RANGE(use->argument(1)->range(), 5, 26, 5, 45);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 7, 2, 7, 34);
  VERIFY_RANGE(use->functionIdentifierRange(), 7, 12, 7, 15);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 7, 16, 7, 24);
  VERIFY_RANGE(use->argument(1)->range(), 7, 26, 7, 33);
}
