/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#include "qobjecttest.h"

#include <QTest>

QTEST_MAIN(QObjectTest)

void QObjectTest::initTestCase()
{
  mHelper->loadDatabase("qt3-to-qt4/qobject.pd");
  QCOMPARE(mHelper->database()->transforms().size(), 5);
}

void QObjectTest::child()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::child (all)", 7);
  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 4, 2, 4, 19);
  VERIFY_RANGE(use->functionIdentifierRange(), 4, 7, 4, 12);
  VERIFY_RANGE(use->object()->range(), 4, 2, 4, 5);
  VERIFY_RANGE(use->accessorRange(), 4, 5, 4, 7);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 4, 13, 4, 18);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 5, 2, 5, 31);
  VERIFY_RANGE(use->functionIdentifierRange(), 5, 7, 5, 12);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 5, 13, 5, 18);
  VERIFY_RANGE(use->argument(1)->range(), 5, 20, 5, 30);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 6, 2, 6, 22);
  VERIFY_RANGE(use->functionIdentifierRange(), 6, 7, 6, 12);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 6, 13, 6, 18);
  VERIFY_RANGE(use->argument(1)->range(), 6, 20, 6, 21);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 7, 2, 7, 28);
  VERIFY_RANGE(use->functionIdentifierRange(), 7, 7, 7, 12);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 7, 13, 7, 18);
  VERIFY_RANGE(use->argument(1)->range(), 7, 20, 7, 21);
  VERIFY_RANGE(use->argument(2)->range(), 7, 23, 7, 27);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 8, 2, 8, 36);
  VERIFY_RANGE(use->functionIdentifierRange(), 8, 7, 8, 12);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 8, 13, 8, 18);
  VERIFY_RANGE(use->argument(1)->range(), 8, 20, 8, 29);
  VERIFY_RANGE(use->argument(2)->range(), 8, 31, 8, 35);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 9, 2, 9, 37);
  VERIFY_RANGE(use->functionIdentifierRange(), 9, 7, 9, 12);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 9, 13, 9, 18);
  VERIFY_RANGE(use->argument(1)->range(), 9, 20, 9, 29);
  VERIFY_RANGE(use->argument(2)->range(), 9, 31, 9, 36);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 10, 2, 10, 29);
  VERIFY_RANGE(use->functionIdentifierRange(), 10, 7, 10, 12);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 10, 13, 10, 18);
  VERIFY_RANGE(use->argument(1)->range(), 10, 20, 10, 21);
  VERIFY_RANGE(use->argument(2)->range(), 10, 23, 10, 28);

  lock.unlock();
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::child (non recursive)", 2);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::child (recursive)", 5);
}

void QObjectTest::queryList()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::queryList (all)", 11);
  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 17, 26, 17, 42);
  VERIFY_RANGE(use->object()->range(), 17, 26, 17, 29);
  VERIFY_RANGE(use->accessorRange(), 17, 29, 17, 31);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  VERIFY_RANGE(use->functionIdentifierRange(), 17, 31, 17, 40);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 18, 26, 18, 54);
  VERIFY_RANGE(use->functionIdentifierRange(), 18, 31, 18, 40);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 18, 41, 18, 53);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 19, 26, 19, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 19, 31, 19, 40);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 19, 41, 19, 53);
  VERIFY_RANGE(use->argument(1)->range(), 19, 55, 19, 68);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 20, 26, 20, 75);
  VERIFY_RANGE(use->functionIdentifierRange(), 20, 31, 20, 40);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 20, 41, 20, 53);
  VERIFY_RANGE(use->argument(1)->range(), 20, 55, 20, 68);
  VERIFY_RANGE(use->argument(2)->range(), 20, 70, 20, 74);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 21, 13, 21, 63);
  VERIFY_RANGE(use->functionIdentifierRange(), 21, 18, 21, 27);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 21, 28, 21, 40);
  VERIFY_RANGE(use->argument(1)->range(), 21, 42, 21, 55);
  VERIFY_RANGE(use->argument(2)->range(), 21, 57, 21, 62);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 22, 26, 22, 81);
  VERIFY_RANGE(use->functionIdentifierRange(), 22, 31, 22, 40);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 22, 41, 22, 53);
  VERIFY_RANGE(use->argument(1)->range(), 22, 55, 22, 68);
  VERIFY_RANGE(use->argument(2)->range(), 22, 70, 22, 74);
  VERIFY_RANGE(use->argument(3)->range(), 22, 76, 22, 80);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 23, 13, 23, 65);
  VERIFY_RANGE(use->functionIdentifierRange(), 23, 18, 23, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 23, 28, 23, 40);
  VERIFY_RANGE(use->argument(1)->range(), 23, 42, 23, 55);
  VERIFY_RANGE(use->argument(2)->range(), 23, 57, 23, 61);
  VERIFY_RANGE(use->argument(3)->range(), 23, 63, 23, 64);

  use = result.hits<ClassFunctionCall>().at(7);
  VERIFY_RANGE(use->range(), 24, 13, 24, 68);
  VERIFY_RANGE(use->functionIdentifierRange(), 24, 18, 24, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 24, 28, 24, 40);
  VERIFY_RANGE(use->argument(1)->range(), 24, 42, 24, 55);
  VERIFY_RANGE(use->argument(2)->range(), 24, 57, 24, 61);
  VERIFY_RANGE(use->argument(3)->range(), 24, 63, 24, 67);

  use = result.hits<ClassFunctionCall>().at(8);
  VERIFY_RANGE(use->range(), 25, 13, 25, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 25, 18, 25, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 25, 28, 25, 40);
  VERIFY_RANGE(use->argument(1)->range(), 25, 42, 25, 55);
  VERIFY_RANGE(use->argument(2)->range(), 25, 57, 25, 61);
  VERIFY_RANGE(use->argument(3)->range(), 25, 63, 25, 68);

  use = result.hits<ClassFunctionCall>().at(9);
  VERIFY_RANGE(use->range(), 26, 13, 26, 65);
  VERIFY_RANGE(use->functionIdentifierRange(), 26, 18, 26, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 26, 28, 26, 40);
  VERIFY_RANGE(use->argument(1)->range(), 26, 42, 26, 55);
  VERIFY_RANGE(use->argument(2)->range(), 26, 57, 26, 61);
  VERIFY_RANGE(use->argument(3)->range(), 26, 63, 26, 64);

  use = result.hits<ClassFunctionCall>().at(10);
  VERIFY_RANGE(use->range(), 27, 13, 27, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 27, 18, 27, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 27, 28, 27, 40);
  VERIFY_RANGE(use->argument(1)->range(), 27, 42, 27, 55);
  VERIFY_RANGE(use->argument(2)->range(), 27, 57, 27, 61);
  VERIFY_RANGE(use->argument(3)->range(), 27, 63, 27, 68);

  lock.unlock();

  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::queryList (qid non-recursive)", 1);
  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 25, 13, 25, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 25, 18, 25, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 25, 28, 25, 40);
  VERIFY_RANGE(use->argument(1)->range(), 25, 42, 25, 55);
  VERIFY_RANGE(use->argument(2)->range(), 25, 57, 25, 61);
  VERIFY_RANGE(use->argument(3)->range(), 25, 63, 25, 68);

  EXEC_CLASSFUNCTION_QUERY(mHelper, "qt3-to-qt4/qobject-test.cpp", "QObject::queryList (literal non-recursive)", 3);
  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 25, 13, 25, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 25, 18, 25, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 25, 28, 25, 40);
  VERIFY_RANGE(use->argument(1)->range(), 25, 42, 25, 55);
  VERIFY_RANGE(use->argument(2)->range(), 25, 57, 25, 61);
  VERIFY_RANGE(use->argument(3)->range(), 25, 63, 25, 68);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 26, 13, 26, 65);
  VERIFY_RANGE(use->functionIdentifierRange(), 26, 18, 26, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 26, 28, 26, 40);
  VERIFY_RANGE(use->argument(1)->range(), 26, 42, 26, 55);
  VERIFY_RANGE(use->argument(2)->range(), 26, 57, 26, 61);
  VERIFY_RANGE(use->argument(3)->range(), 26, 63, 26, 64);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 27, 13, 27, 69);
  VERIFY_RANGE(use->functionIdentifierRange(), 27, 18, 27, 27);
  QCOMPARE(use->argumentCount(), 4);
  VERIFY_RANGE(use->argument(0)->range(), 27, 28, 27, 40);
  VERIFY_RANGE(use->argument(1)->range(), 27, 42, 27, 55);
  VERIFY_RANGE(use->argument(2)->range(), 27, 57, 27, 61);
  VERIFY_RANGE(use->argument(3)->range(), 27, 63, 27, 68);
}
