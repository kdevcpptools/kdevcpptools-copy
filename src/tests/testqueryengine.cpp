/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "testqueryengine.h"

#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtTest/QTest>

#include <languages/cpp/cppduchain/declarationbuilder.h>
#include <languages/cpp/cppduchain/usebuilder.h>
#include <languages/cpp/parser/parser.h>
#include <languages/cpp/parser/parsesession.h>
#include <languages/cpp/parser/rpp/preprocessor.h>
#include <languages/cpp/parser/rpp/pp-engine.h>

#include <libportdb/xmlparser.h>

#include "libquery/cppqueryengine.h"

using namespace KDevelop;

QTEST_MAIN(TestQueryEngine)

/// NOTE: Update this number every time you add a new query to the test set.
///       I added this to have some kind of check that we "know" which queries
///       are in the db and that it doesn't contain unexpected queries.
int sQueryCount = 24;

static QString sQueryDatabaseFile = QString("testqueries.pd");

/// helper Methods.

void qDebugRange(SimpleRange const &range)
{
  qDebug() << '(' + QString::number(range.start.line) + ',' + QString::number(range.start.column)
    + "):(" + QString::number(range.end.line) + ',' + QString::number(range.end.column) + ')';
}

bool lessThan(DeclarationUse const &lhs, DeclarationUse const &rhs)
{
  return lhs.range() < rhs.range();
}

QString testFileDirectoryPath()
{
  QFileInfo info;
  info.setFile(QDir::currentPath());
  return info.absoluteFilePath() + "/test-files";
}

QString testFileFullPath(QString const &file)
{
  return testFileDirectoryPath() + '/' + file;
}

#define verifyRange(range, sLine, sColumn, eLine, eColumn) \
  QVERIFY(range.isValid()); \
  QCOMPARE(range.start.line, sLine); \
  QCOMPARE(range.start.column, sColumn); \
  QCOMPARE(range.end.line, eLine); \
  QCOMPARE(range.end.column, eColumn); \

/// TestQueryEngine methods

TestQueryEngine::TestQueryEngine() : mQueryResult("test-id")
{}

void TestQueryEngine::execQuery(QString const &file, QString const &queryId, int expectedUseCount, bool expectFail)
{
  DUChainWriteLocker lock(DUChain::lock());

  TopDUContext *context = mLastContext.second;
  if (mLastContext.first != file) {
    QFileInfo info(testFileFullPath(file));
    QVERIFY(info.exists());
    context = parse(testFileFullPath(file));
    mLastContext = QPair<QString, TopDUContext*>(file, context);
  }

  QVERIFY(context->ast());
  QVERIFY(mDatabase.containsQuery(queryId));

  CppQueryEngine engine;
  mQueryResult = engine.execQuery(ReferencedTopDUContext(context), mDatabase.classQuery(queryId));

  qSort(mQueryResult.locations.begin(), mQueryResult.locations.end(), lessThan);

  if (expectFail) {
    QEXPECT_FAIL("", "count differs", Continue);
    QCOMPARE(mQueryResult.locations.size(), expectedUseCount);
  } else {
    QCOMPARE(mQueryResult.locations.size(), expectedUseCount);
  }

  if (expectedUseCount > 0)
    QCOMPARE(mQueryResult.status, UseQueryResult::Match);
  else
    QCOMPARE(mQueryResult.status, UseQueryResult::NoMatch);
}

TopDUContext *TestQueryEngine::parse(QString const &fileName)
{
  QFile file(fileName);
  Q_ASSERT(file.exists());
  Q_ASSERT(file.open(QIODevice::ReadOnly));
  QByteArray unit = file.readAll();
  file.close();

  //If the AST flag is set, then the parse session needs to be owned by a shared pointer
  ParseSession::Ptr session(new ParseSession());

  rpp::Preprocessor preprocessor;
  rpp::pp pp(&preprocessor);

  session->setContentsAndGenerateLocationTable(pp.processFile(fileName, unit));

  Parser parser(&mControl);
  TranslationUnitAST* ast = parser.parse(session.data());
  ast->session = session.data();

  session->setASTNodeParents();

  IndexedString url(fileName);

  DeclarationBuilder definitionBuilder(session.data());
  Cpp::EnvironmentFilePointer envFile( new Cpp::EnvironmentFile( url, 0 ) );

  TopDUContext* top = definitionBuilder.buildDeclarations(envFile, ast, 0);
  top->setAst(IAstContainer::Ptr(session.data()));

  UseBuilder useBuilder(session.data());
  useBuilder.setMapAst(true);
  useBuilder.buildUses(ast);

  Q_ASSERT(top);
  return top;
}

/// Init/cleanup

void TestQueryEngine::initTestCase()
{
  XmlParser parser;
  XmlParser::ParseResult result = parser.parse(testFileDirectoryPath() + '/' + sQueryDatabaseFile, &mDatabase);
  QVERIFY(result == XmlParser::Ok);
  QCOMPARE(mDatabase.queries(CppPortingDatabase::QueriesOnly).size(), sQueryCount);
}

void TestQueryEngine::cleanupTestCase()
{ }

/// Actual Tests

void TestQueryEngine::testGlobalFunctionUses()
{
  execQuery("test_global_function_uses.cpp", "test-global-template-1-func-test0", 0);
  execQuery("test_global_function_uses.cpp", "test-global-func-test0", 1);
  DeclarationUse use = mQueryResult.locations[0];
  verifyRange(use.range(), 8, 2, 8, 9);
  verifyRange(use.range(DeclarationUse::FunctionId), 8, 2, 8, 7);

  execQuery("test_global_function_uses.cpp", "test-global-func-test1-char", 2);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 10, 2, 10, 21);
  verifyRange(use.range(DeclarationUse::FunctionId), 10, 2, 10, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 10, 8, 10, 20);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 11, 2, 11, 20);
  verifyRange(use.range(DeclarationUse::FunctionId), 11, 2, 11, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 11, 8, 11, 19);

  execQuery("test_global_function_uses.cpp", "test-global-func-test1-int", 1);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 12, 2, 12, 12);
  verifyRange(use.range(DeclarationUse::FunctionId), 12, 2, 12, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 12, 8, 12, 11);

  execQuery("test_global_function_uses.cpp", "test-global-func-test2", 3);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 16, 2, 16, 13);
  verifyRange(use.range(DeclarationUse::FunctionId), 16, 2, 16, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 16, 8, 16, 9);
  verifyRange(use.range(DeclarationUse::Arg, 1), 16, 11, 16, 12);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 17, 2, 17, 35);
  verifyRange(use.range(DeclarationUse::FunctionId), 17, 2, 17, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 17, 8, 17, 12);
  verifyRange(use.range(DeclarationUse::Arg, 1), 17, 14, 17, 34);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 18, 2, 18, 29);
  verifyRange(use.range(DeclarationUse::FunctionId), 18, 2, 18, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 18, 8, 18, 14);
  verifyRange(use.range(DeclarationUse::Arg, 1), 18, 16, 18, 28);

  execQuery("test_global_function_uses.cpp", "test-global-func-test2-restricted", 1);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 18, 2, 18, 29);
  verifyRange(use.range(DeclarationUse::FunctionId), 18, 2, 18, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 18, 8, 18, 14);
  verifyRange(use.range(DeclarationUse::Arg, 1), 18, 16, 18, 28);

  execQuery("test_global_function_uses.cpp", "test-global-func-test3", 5);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 20, 2, 20, 16);
  verifyRange(use.range(DeclarationUse::FunctionId), 20, 2, 20, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 20, 8, 20, 9);
  verifyRange(use.range(DeclarationUse::Arg, 1), 20, 11, 20, 12);
  verifyRange(use.range(DeclarationUse::Arg, 2), 20, 14, 20, 15);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 21, 2, 21, 23);
  verifyRange(use.range(DeclarationUse::FunctionId), 21, 2, 21, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 21, 8, 21, 9);
  verifyRange(use.range(DeclarationUse::Arg, 1), 21, 11, 21, 12);
  verifyRange(use.range(DeclarationUse::Arg, 2), 21, 14, 21, 22);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 22, 2, 22, 36);
  verifyRange(use.range(DeclarationUse::FunctionId), 22, 2, 22, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 22, 8, 22, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 22, 20, 22, 32);
  verifyRange(use.range(DeclarationUse::Arg, 2), 22, 34, 22, 35);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), 23, 2, 23, 42);
  verifyRange(use.range(DeclarationUse::FunctionId), 23, 2, 23, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 23, 8, 23, 12);
  verifyRange(use.range(DeclarationUse::Arg, 1), 23, 14, 23, 27);
  verifyRange(use.range(DeclarationUse::Arg, 2), 23, 29, 23, 41);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), 24, 2, 24, 26);
  verifyRange(use.range(DeclarationUse::FunctionId), 24, 2, 24, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 24, 8, 24, 15);
  verifyRange(use.range(DeclarationUse::Arg, 1), 24, 17, 24, 25);
  QCOMPARE(use.range(DeclarationUse::Arg, 2).isValid(), false); // Uses the default one and should therefore be invalid.

  execQuery("test_global_function_uses.cpp", "test-global-func-test3-restricted-1", 1);
  use = mQueryResult.locations[0];
  verifyRange(use.range(), 22, 2, 22, 36);
  verifyRange(use.range(DeclarationUse::FunctionId), 22, 2, 22, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 22, 8, 22, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 22, 20, 22, 32);
  verifyRange(use.range(DeclarationUse::Arg, 2), 22, 34, 22, 35);

  execQuery("test_global_function_uses.cpp", "test-global-func-test3-restricted-2", 1);
  use = mQueryResult.locations[0];

  verifyRange(use.range(), 23, 2, 23, 42);
  verifyRange(use.range(DeclarationUse::FunctionId), 23, 2, 23, 7);
  verifyRange(use.range(DeclarationUse::Arg, 0), 23, 8, 23, 12);
  verifyRange(use.range(DeclarationUse::Arg, 1), 23, 14, 23, 27);
  verifyRange(use.range(DeclarationUse::Arg, 2), 23, 29, 23, 41);
}

void TestQueryEngine::testGlobalTemplateFunctionUses()
{
  execQuery("test_global_template_function_uses.cpp",
            "test-global-func-test0", 0);

  execQuery("test_global_template_function_uses.cpp",
            "test-global-template-1-func-test0", 3);

  int offset = 12; // Makes it easier to add code before the uses.
  DeclarationUse use = mQueryResult.locations[0];
  verifyRange(use.range(), offset, 2, offset, 15);
  verifyRange(use.range(DeclarationUse::FunctionId), offset, 2, offset, 7);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 0), offset, 8, offset, 12);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), offset + 1, 2, offset + 1, 19);
  verifyRange(use.range(DeclarationUse::FunctionId), offset + 1, 2, offset + 1, 7);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 0), offset + 1, 8, offset + 1, 16);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), offset + 2, 2, offset + 2, 16);
  verifyRange(use.range(DeclarationUse::FunctionId), offset + 2, 2, offset + 2, 7);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 0), offset + 2, 8, offset + 2, 13);

  execQuery("test_global_template_function_uses.cpp",
            "test-global-template-2-func-test0", 1);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), offset + 4, 2, offset + 4, 22);
  verifyRange(use.range(DeclarationUse::FunctionId), offset + 4, 2, offset + 4, 7);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 0), offset + 4, 8, offset + 4, 12);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 1), offset + 4, 14, offset + 4, 19);

  execQuery("test_global_template_function_uses.cpp",
            "test-global-template-3-func-test0", 1);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), offset + 5, 2, offset + 5, 32);
  verifyRange(use.range(DeclarationUse::FunctionId), offset + 5, 2, offset + 5, 7);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 0), offset + 5, 8, offset + 5, 16);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 1), offset + 5, 18, offset + 5, 22);
  verifyRange(use.range(DeclarationUse::FunctionTemplateArg, 2), offset + 5, 24, offset + 5, 29);
}

void TestQueryEngine::testCtorUses()
{
  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test", 2);

  DeclarationUse use = mQueryResult.locations[0];
  verifyRange(use.range(), 24, 16, 24, 18);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 52, 22, 52, 24);

  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-copy", 6, true);
  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-copy", 5);

  // The first use is at line 15 in the initializer list of the ctor of SubTest
  //use = mQueryResult.locations[0];
  //verifyRange(use.range(), 14 ,38, 14, 45);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), 25 ,9, 25, 13);
  verifyRange(use.range(DeclarationUse::Arg, 0), 25, 10, 25, 12);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 26 ,16, 26, 20);
  verifyRange(use.range(DeclarationUse::Arg, 0), 26, 17, 26, 19);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 27 ,16, 27, 29);
  verifyRange(use.range(DeclarationUse::Arg, 0), 27, 17, 27, 28);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), 53 ,22, 53, 26);
  verifyRange(use.range(DeclarationUse::Arg, 0), 53, 23, 53, 25);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), 54 ,22, 54, 35);
  verifyRange(use.range(DeclarationUse::Arg, 0), 54, 23, 54, 34);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-int", 6, true);
  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-int", 5);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), 27 ,21, 27, 28);
  verifyRange(use.range(DeclarationUse::Arg, 0), 27, 22, 27, 27);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 30 , 9, 30, 12);
  verifyRange(use.range(DeclarationUse::Arg, 0), 30, 10, 30, 11);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 31 , 9, 31, 14);
  verifyRange(use.range(DeclarationUse::Arg, 0), 31, 10, 31, 13);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), 32 , 9, 32, 18);
  verifyRange(use.range(DeclarationUse::Arg, 0), 32, 10, 32, 17);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), 54 , 27, 54, 34);
  verifyRange(use.range(DeclarationUse::Arg, 0), 54, 28, 54, 33);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-char-char", 13, true); // [0] at line 17
  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-char-char", 12);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), 36 ,9, 36, 35);
  verifyRange(use.range(DeclarationUse::Arg, 0), 36, 10, 36, 21);
  verifyRange(use.range(DeclarationUse::Arg, 1), 36, 23, 36, 34);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 37 , 10, 37, 32);
  verifyRange(use.range(DeclarationUse::Arg, 0), 37, 11, 37, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 37, 20, 37, 31);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 38, 10, 38, 32);
  verifyRange(use.range(DeclarationUse::Arg, 0), 38, 11, 38, 22);
  verifyRange(use.range(DeclarationUse::Arg, 1), 38, 24, 38, 31);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), 39, 10, 39, 28);
  verifyRange(use.range(DeclarationUse::Arg, 0), 39, 11, 39, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 39, 20, 39, 27);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), 41 ,17, 41, 43);
  verifyRange(use.range(DeclarationUse::Arg, 0), 41, 18, 41, 29);
  verifyRange(use.range(DeclarationUse::Arg, 1), 41, 31, 41, 42);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[5];
  verifyRange(use.range(), 42 , 17, 42, 39);
  verifyRange(use.range(DeclarationUse::Arg, 0), 42, 18, 42, 25);
  verifyRange(use.range(DeclarationUse::Arg, 1), 42, 27, 42, 38);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[6];
  verifyRange(use.range(), 43, 17, 43, 39);
  verifyRange(use.range(DeclarationUse::Arg, 0), 43, 18, 43, 29);
  verifyRange(use.range(DeclarationUse::Arg, 1), 43, 31, 43, 38);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[7];
  verifyRange(use.range(), 44, 17, 44, 35);
  verifyRange(use.range(DeclarationUse::Arg, 0), 44, 18, 44, 25);
  verifyRange(use.range(DeclarationUse::Arg, 1), 44, 27, 44, 34);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[8];
  verifyRange(use.range(), 56, 22, 56, 48);
  verifyRange(use.range(DeclarationUse::Arg, 0), 56, 23, 56, 34);
  verifyRange(use.range(DeclarationUse::Arg, 1), 56, 36, 56, 47);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[9];
  verifyRange(use.range(), 57, 22, 57, 44);
  verifyRange(use.range(DeclarationUse::Arg, 0), 57, 23, 57, 30);
  verifyRange(use.range(DeclarationUse::Arg, 1), 57, 32, 57, 43);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[10];
  verifyRange(use.range(), 58, 22, 58, 44);
  verifyRange(use.range(DeclarationUse::Arg, 0), 58, 23, 58, 34);
  verifyRange(use.range(DeclarationUse::Arg, 1), 58, 36, 58, 43);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[11];
  verifyRange(use.range(), 59, 22, 59, 40);
  verifyRange(use.range(DeclarationUse::Arg, 0), 59, 23, 59, 30);
  verifyRange(use.range(DeclarationUse::Arg, 1), 59, 32, 59, 39);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  execQuery("test_ctor_uses.cpp",
            "test-ctor-Test-Test-char-char-int", 8);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), 46 ,10, 46, 38);
  verifyRange(use.range(DeclarationUse::Arg, 0), 46, 11, 46, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 46, 20, 46, 26);
  verifyRange(use.range(DeclarationUse::Arg, 2), 46, 28, 46, 37);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), 47 , 10, 47, 42);
  verifyRange(use.range(DeclarationUse::Arg, 0), 47, 11, 47, 28);
  verifyRange(use.range(DeclarationUse::Arg, 1), 47, 30, 47, 36);
  verifyRange(use.range(DeclarationUse::Arg, 2), 47, 38, 47, 41);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), 48, 10, 48, 33);
  verifyRange(use.range(DeclarationUse::Arg, 0), 48, 11, 48, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 48, 20, 48, 27);
  verifyRange(use.range(DeclarationUse::Arg, 2), 48, 29, 48, 32);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), 49, 10, 49, 38);
  verifyRange(use.range(DeclarationUse::Arg, 0), 49, 11, 49, 18);
  verifyRange(use.range(DeclarationUse::Arg, 1), 49, 20, 49, 27);
  verifyRange(use.range(DeclarationUse::Arg, 2), 49, 29, 49, 37);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), true);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), 61, 22, 61, 50);
  verifyRange(use.range(DeclarationUse::Arg, 0), 61, 23, 61, 30);
  verifyRange(use.range(DeclarationUse::Arg, 1), 61, 32, 61, 38);
  verifyRange(use.range(DeclarationUse::Arg, 2), 61, 40, 61, 49);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[5];
  verifyRange(use.range(), 62, 22, 62, 54);
  verifyRange(use.range(DeclarationUse::Arg, 0), 62, 23, 62, 40);
  verifyRange(use.range(DeclarationUse::Arg, 1), 62, 42, 62, 48);
  verifyRange(use.range(DeclarationUse::Arg, 2), 62, 50, 62, 53);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[6];
  verifyRange(use.range(), 63, 22, 63, 45);
  verifyRange(use.range(DeclarationUse::Arg, 0), 63, 23, 63, 30);
  verifyRange(use.range(DeclarationUse::Arg, 1), 63, 32, 63, 39);
  verifyRange(use.range(DeclarationUse::Arg, 2), 63, 41, 63, 44);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);

  use = mQueryResult.locations[7];
  verifyRange(use.range(), 64, 22, 64, 50);
  verifyRange(use.range(DeclarationUse::Arg, 0), 64, 23, 64, 30);
  verifyRange(use.range(DeclarationUse::Arg, 1), 64, 32, 64, 39);
  verifyRange(use.range(DeclarationUse::Arg, 2), 64, 41, 64, 49);
  QVERIFY(use.hasProperty(DeclarationUse::ImplicitCtorCall));
  QCOMPARE(use.value(DeclarationUse::ImplicitCtorCall).toBool(), false);
}

void TestQueryEngine::testMemberFunctionUses()
{
  execQuery("test_member_function_uses.cpp",
            "test-member-Test-foo", 4);

  int offset = 7;

  DeclarationUse use = mQueryResult.locations[0];
  verifyRange(use.range(), offset, 4, offset, 9);
  QVERIFY(! use.range(DeclarationUse::ObjectId).isValid());
  QVERIFY(! use.range(DeclarationUse::Accessor).isValid());
  verifyRange(use.range(DeclarationUse::MemberId), offset, 4, offset, 7);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), offset + 1, 4, offset + 1, 15);
  verifyRange(use.range(DeclarationUse::ObjectId), offset + 1, 4, offset + 1, 8);
  verifyRange(use.range(DeclarationUse::Accessor), offset + 1, 8, offset + 1, 10);
  verifyRange(use.range(DeclarationUse::MemberId), offset + 1, 10, offset + 1, 13);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), offset + 14, 2, offset + 14, 9);
  verifyRange(use.range(DeclarationUse::ObjectId), offset + 14, 2, offset + 14, 3);
  verifyRange(use.range(DeclarationUse::Accessor), offset + 14, 3, offset + 14, 4);
  verifyRange(use.range(DeclarationUse::MemberId), offset + 14, 4, offset + 14, 7);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), offset + 16, 2, offset + 16, 13);
  verifyRange(use.range(DeclarationUse::ObjectId), offset + 16, 2, offset + 16, 6);
  verifyRange(use.range(DeclarationUse::Accessor), offset + 16, 6, offset + 16, 8);
  verifyRange(use.range(DeclarationUse::MemberId), offset + 16, 8, offset + 16, 11);
}

void TestQueryEngine::testTemplateTypeMemberFunctionUses()
{
  // This query should return 0 uses becauses it doesn't have a template-argument
  // restriction.
  execQuery("template_type_member_function_uses.cpp",
            "test-member-Test-foo", 0);

  execQuery("template_type_member_function_uses.cpp",
            "Test<T>::foo()", 4);
}

void TestQueryEngine::testQWidgetSubclasses()
{
  execQuery("test_qwidget_enum.cpp",
            "test-QWidget-Subclass-Uses", 12);

  int offset = 33;
  DeclarationUse use;

  use = mQueryResult.locations[0];
  verifyRange(use.range(), offset, 35, offset, 48);
  verifyRange(use.range(DeclarationUse::TypeId), offset, 35, offset, 48);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), offset + 3, 68, offset + 3, 81);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 3, 68, offset + 3, 81);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), offset + 8, 2, offset + 8, 10);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 8, 2, offset + 8, 10);

  use = mQueryResult.locations[3];
  verifyRange(use.range(), offset + 8, 21, offset + 8, 29);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 8, 21, offset + 8, 29);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), offset + 9, 2, offset + 9, 15);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 9, 2, offset + 9, 15);

  use = mQueryResult.locations[5];
  verifyRange(use.range(), offset + 9, 26, offset + 9, 39);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 9, 26, offset + 9, 39);

  use = mQueryResult.locations[6];
  verifyRange(use.range(), offset + 10, 2, offset + 10, 15);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 10, 2, offset + 10, 15);

  use = mQueryResult.locations[7];
  verifyRange(use.range(), offset + 10, 26, offset + 10, 39);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 10, 26, offset + 10, 39);

  use = mQueryResult.locations[8];
  verifyRange(use.range(), offset + 11, 2, offset + 11, 21);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 11, 2, offset + 11, 21);

  use = mQueryResult.locations[9];
  verifyRange(use.range(), offset + 11, 32, offset + 11, 51);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 11, 32, offset + 11, 51);

  use = mQueryResult.locations[10];
  verifyRange(use.range(), offset + 13, 2, offset + 13, 10);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 13, 2, offset + 13, 10);

  use = mQueryResult.locations[11];
  verifyRange(use.range(), offset + 13, 26, offset + 13, 34);
  verifyRange(use.range(DeclarationUse::TypeId), offset + 13, 26, offset + 13, 34);
}

void TestQueryEngine::testQWidgetSubclassCtorUses()
{
  execQuery("test_qwidget_enum.cpp",
            "test-QWidget-Subclass-Ctor-Uses", 6, true); // At line 37 there's a use of AnotherWidget ctor
  execQuery("test_qwidget_enum.cpp",
            "test-QWidget-Subclass-Ctor-Uses", 5);

  int offset = 36;

  DeclarationUse use = mQueryResult.locations[0];
//   verifyRange(use.range(), offset, 29, offset, 55);
//   verifyRange(use.range(DeclarationUse::Arg, 0), offset, 30, offset, 31);
//   verifyRange(use.range(DeclarationUse::Arg, 1), offset, 33, offset, 54);

  use = mQueryResult.locations[0];
  verifyRange(use.range(), offset + 5, 29, offset + 5, 55);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 5, 30, offset + 5, 31);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 5, 33, offset + 5, 54);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), offset + 6, 39, offset + 6, 65);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 6, 40, offset + 6, 41);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 6, 43, offset + 6, 64);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), offset + 7, 39, offset + 7, 42);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 7, 40, offset + 7, 41);
  QVERIFY(!use.range(DeclarationUse::Arg, 1).isValid());

  use = mQueryResult.locations[3];
  verifyRange(use.range(), offset + 8, 51, offset + 8, 77);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 8, 52, offset + 8, 53);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 8, 55, offset + 8, 76);

  use = mQueryResult.locations[4];
  verifyRange(use.range(), offset + 10, 34, offset + 10, 60);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 10, 35, offset + 10, 36);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 10, 38, offset + 10, 59);
}

void TestQueryEngine::testQWidgetSubclassCtorUsesRestricted()
{
  execQuery("test_qwidget_enum.cpp",
            "test-QWidget-Subclass-Ctor-Uses-Restricted", 3);

  int offset = 36;

  DeclarationUse use = mQueryResult.locations[0];
  verifyRange(use.range(), offset + 5, 29, offset + 5, 55);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 5, 30, offset + 5, 31);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 5, 33, offset + 5, 54);

  use = mQueryResult.locations[1];
  verifyRange(use.range(), offset + 6, 39, offset + 6, 65);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 6, 40, offset + 6, 41);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 6, 43, offset + 6, 64);

  use = mQueryResult.locations[2];
  verifyRange(use.range(), offset + 8, 51, offset + 8, 77);
  verifyRange(use.range(DeclarationUse::Arg, 0), offset + 8, 52, offset + 8, 53);
  verifyRange(use.range(DeclarationUse::Arg, 1), offset + 8, 55, offset + 8, 76);
}

#include "testqueryengine.moc"
