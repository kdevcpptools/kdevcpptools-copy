#include "testclassqueries.h"

#include <QTest>

QTEST_MAIN(TestTypeQueries)

void TestTypeQueries::initTestCase()
{
  mHelper->loadDatabase("new_test_queries.pd");
}

void TestTypeQueries::testClassQueries()
{
  EXEC_CLASS_QUERY(mHelper, "test_type_hits.cpp", "Test<T>", 0);
  EXEC_CLASS_QUERY(mHelper, "test_type_hits.cpp", "Test", 6);

  ClassUse::Ptr use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 2, 0, 2, 4);
  VERIFY_RANGE(use->identifierRange(), 2, 0, 2, 4);

  use = result.hits<ClassUse>()[1];
  VERIFY_RANGE(use->range(), 2, 9, 2, 13);
  VERIFY_RANGE(use->identifierRange(), 2, 9, 2, 13);

  use = result.hits<ClassUse>()[2];
  VERIFY_RANGE(use->range(), 6, 2, 6, 6);
  VERIFY_RANGE(use->identifierRange(), 6, 2, 6, 6);

  use = result.hits<ClassUse>()[3];
  VERIFY_RANGE(use->range(), 7, 2, 7, 6);
  VERIFY_RANGE(use->identifierRange(), 7, 2, 7, 6);

  use = result.hits<ClassUse>()[4];
  VERIFY_RANGE(use->range(), 8, 2, 8, 6);
  VERIFY_RANGE(use->identifierRange(), 8, 2, 8, 6);

  use = result.hits<ClassUse>()[5];
  VERIFY_RANGE(use->range(), 8, 18, 8, 22);
  VERIFY_RANGE(use->identifierRange(), 8, 18, 8, 22);

  EXEC_CLASS_QUERY(mHelper, "test_type_hits.cpp", "*<T>", 0);
  EXEC_CLASS_QUERY(mHelper, "test_type_hits.cpp", "*", 6);
}

void TestTypeQueries::testSingleTemplateArgClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test", 0);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test<T>", 8);

  ClassUse::Ptr use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 4, 0, 4, 9);
  VERIFY_RANGE(use->identifierRange(), 4, 0, 4, 4);
  VERIFY_RANGE(use->templateArgumentRange(0), 4, 5, 4, 8);

  use = result.hits<ClassUse>()[1];
  VERIFY_RANGE(use->range(), 4, 14, 4, 23);
  VERIFY_RANGE(use->identifierRange(), 4, 14, 4, 18);
  VERIFY_RANGE(use->templateArgumentRange(0), 4, 19, 4, 22);

  use = result.hits<ClassUse>()[2];
  VERIFY_RANGE(use->range(), 8, 2, 8, 14);
  VERIFY_RANGE(use->identifierRange(), 8, 2, 8, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 8, 7, 8, 13);

  use = result.hits<ClassUse>()[3];
  VERIFY_RANGE(use->range(), 9, 2, 9, 12);
  VERIFY_RANGE(use->identifierRange(), 9, 2, 9, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 9, 7, 9, 11);

  use = result.hits<ClassUse>()[4];
  VERIFY_RANGE(use->range(), 10, 2, 10, 16);
  VERIFY_RANGE(use->identifierRange(), 10, 2, 10, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 10, 7, 10, 15);

  use = result.hits<ClassUse>()[5];
  VERIFY_RANGE(use->range(), 10, 28, 10, 42);
  VERIFY_RANGE(use->identifierRange(), 10, 28, 10, 32);
  VERIFY_RANGE(use->templateArgumentRange(0), 10, 33, 10, 41);

  use = result.hits<ClassUse>()[6];
  VERIFY_RANGE(use->range(), 19, 26, 19, 37);
  VERIFY_RANGE(use->identifierRange(), 19, 26, 19, 30);
  VERIFY_RANGE(use->templateArgumentRange(0), 19, 31, 19, 36);

  use = result.hits<ClassUse>()[7];
  VERIFY_RANGE(use->range(), 19, 75, 19, 86);
  VERIFY_RANGE(use->identifierRange(), 19, 75, 19, 79);
  VERIFY_RANGE(use->templateArgumentRange(0), 19, 80, 19, 85);

  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "*", 0);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "*<T>", 8);
}

void TestTypeQueries::testSingleTemplateArgRestrictedClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test<int>", 2);

  ClassUse::Ptr use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 4, 0, 4, 9);
  VERIFY_RANGE(use->identifierRange(), 4, 0, 4, 4);
  VERIFY_RANGE(use->templateArgumentRange(0), 4, 5, 4, 8);

  use = result.hits<ClassUse>()[1];
  VERIFY_RANGE(use->range(), 4, 14, 4, 23);
  VERIFY_RANGE(use->identifierRange(), 4, 14, 4, 18);
  VERIFY_RANGE(use->templateArgumentRange(0), 4, 19, 4, 22);

  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test<double>", 1);
  use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 8, 2, 8, 14);
  VERIFY_RANGE(use->identifierRange(), 8, 2, 8, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 8, 7, 8, 13);

  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test<char>", 1);
  use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 9, 2, 9, 12);
  VERIFY_RANGE(use->identifierRange(), 9, 2, 9, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 9, 7, 9, 11);

  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test<unsigned>", 2);
  use = result.hits<ClassUse>()[0];
  VERIFY_RANGE(use->range(), 10, 2, 10, 16);
  VERIFY_RANGE(use->identifierRange(), 10, 2, 10, 6);
  VERIFY_RANGE(use->templateArgumentRange(0), 10, 7, 10, 15);

  use = result.hits<ClassUse>()[1];
  VERIFY_RANGE(use->range(), 10, 28, 10, 42);
  VERIFY_RANGE(use->identifierRange(), 10, 28, 10, 32);
  VERIFY_RANGE(use->templateArgumentRange(0), 10, 33, 10, 41);
}

void TestTypeQueries::testTwoTemplateArgsClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test2<T, T>", 5);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "*<T, T>", 5);
}

void TestTypeQueries::testTwoTemplateArgsRestrictedClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test2<int, char>", 2);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test2<int, T>", 2);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test2<unsigned, double>", 3);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test2<T, double>", 3);
}

void TestTypeQueries::testThreeTemplateArgsClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test3<T, T, T>", 5);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "*<T, T, T>", 5);
}

void TestTypeQueries::testThreeTemplateArgsRestrictedClassQuery()
{
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test3<int, char, double>", 2);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test3<int, float, long int>", 1);
  EXEC_CLASS_QUERY(mHelper, "test_template_type_hits.cpp", "Test3<unsigned, double, Test<float> >", 2);
}

void TestTypeQueries::testMemberQueriesZeroArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::foo()", 7);
  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 7, 4, 7, 9);
  VERIFY_RANGE(use->functionIdentifierRange(), 7, 4, 7, 7);
  QVERIFY(use->accessorRange().isEmpty());
  QVERIFY(use->object()->range().isEmpty());
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), true);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 8, 4, 8, 15);
  VERIFY_RANGE(use->functionIdentifierRange(), 8, 10, 8, 13);
  VERIFY_RANGE(use->object()->range(), 8, 4, 8, 8);
  VERIFY_RANGE(use->accessorRange(), 8, 8, 8, 10);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 24, 2, 24, 9);
  VERIFY_RANGE(use->functionIdentifierRange(), 24, 4, 24, 7);
  VERIFY_RANGE(use->object()->range(), 24, 2, 24, 3);
  VERIFY_RANGE(use->accessorRange(), 24, 3, 24, 4);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 26, 2, 26, 13);
  VERIFY_RANGE(use->functionIdentifierRange(), 26, 8, 26, 11);
  VERIFY_RANGE(use->object()->range(), 26, 2, 26, 6);
  VERIFY_RANGE(use->accessorRange(), 26, 6, 26, 8);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 28, 2, 28, 17);
  VERIFY_RANGE(use->functionIdentifierRange(), 28, 12, 28, 15);
  VERIFY_RANGE(use->object()->range(), 28, 2, 28, 10);
  VERIFY_RANGE(use->accessorRange(), 28, 10, 28, 12);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 29, 2, 29, 16);
  VERIFY_RANGE(use->functionIdentifierRange(), 29, 11, 29, 14);
  VERIFY_RANGE(use->object()->range(), 29, 2, 29, 10);
  VERIFY_RANGE(use->accessorRange(), 29, 10, 29, 11);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 53, 2, 53, 32);
  VERIFY_RANGE(use->functionIdentifierRange(), 53, 27, 53, 30);
  VERIFY_RANGE(use->object()->range(), 53, 2, 53, 25);
  VERIFY_RANGE(use->accessorRange(), 53, 25, 53, 27);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 0);
}

void TestTypeQueries::testMemberQueriesOneArg()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test1()", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test1(int)", 3);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 36, 2, 36, 18);
  VERIFY_RANGE(use->functionIdentifierRange(), 36, 4, 36, 9);
  VERIFY_RANGE(use->object()->range(), 36, 2, 36, 3);
  VERIFY_RANGE(use->accessorRange(), 36, 3, 36, 4);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 36, 10, 36, 17);
  QCOMPARE(use->templateArgumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 37, 2, 37, 17);
  VERIFY_RANGE(use->functionIdentifierRange(), 37, 4, 37, 9);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 37, 10, 37, 16);
  QCOMPARE(use->templateArgumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(2);
  QVERIFY(use->inMacro());
  VERIFY_RANGE(use->range(), 50, 12, 50, 12);
  VERIFY_RANGE(use->functionIdentifierRange(), 50, 12, 50, 12);
  QEXPECT_FAIL("", "we don't get an AST for this call, hence no way to find the number/ranges of arguments", Abort);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 50, 12, 50, 12);
  QCOMPARE(use->templateArgumentCount(), 0);
}

void TestTypeQueries::testMemberQueriesTwoArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test2()", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test2(int)", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test2(int, char const*)", 3);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 38, 2, 38, 20);
  VERIFY_RANGE(use->functionIdentifierRange(), 38, 4, 38, 9);
  VERIFY_RANGE(use->object()->range(), 38, 2, 38, 3);
  VERIFY_RANGE(use->accessorRange(), 38, 3, 38, 4);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 38, 10, 38, 16);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(1)->range(), 38, 18, 38, 19);
  QVERIFY(!use->templateArgumentRange(1).isValid());

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 39, 2, 39, 28);
  VERIFY_RANGE(use->functionIdentifierRange(), 39, 4, 39, 9);
  VERIFY_RANGE(use->object()->range(), 39, 2, 39, 3);
  VERIFY_RANGE(use->accessorRange(), 39, 3, 39, 4);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 39, 10, 39, 16);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(1)->range(), 39, 18, 39, 27);
  QVERIFY(!use->templateArgumentRange(1).isValid());

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 40, 2, 40, 25);
  VERIFY_RANGE(use->functionIdentifierRange(), 40, 4, 40, 9);
  VERIFY_RANGE(use->object()->range(), 40, 2, 40, 3);
  VERIFY_RANGE(use->accessorRange(), 40, 3, 40, 4);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 40, 10, 40, 13);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(1)->range(), 40, 15, 40, 24);
  QVERIFY(!use->templateArgumentRange(1).isValid());
}

void TestTypeQueries::testMemberQueriesThreeArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test3()", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test3(int)", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test3(int, char const*)", 0);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_function_hits.cpp", "Test::test3(int, char const*, float)", 3);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 43, 2, 43, 29);
  VERIFY_RANGE(use->functionIdentifierRange(), 43, 4, 43, 9);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 43, 10, 43, 16);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(1)->range(), 43, 18, 43, 19);
  QVERIFY(!use->templateArgumentRange(1).isValid());
  VERIFY_RANGE(use->argument(2)->range(), 43, 21, 43, 28);
  QVERIFY(!use->templateArgumentRange(2).isValid());

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 45, 2, 45, 53);
  VERIFY_RANGE(use->functionIdentifierRange(), 45, 28, 45, 33);
  VERIFY_RANGE(use->object()->range(), 45, 2, 45, 27);
  VERIFY_RANGE(use->accessorRange(), 45, 27, 45, 28);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 45, 34, 45, 37);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(1)->range(), 45, 39, 45, 47);
  QVERIFY(!use->templateArgumentRange(1).isValid());
  VERIFY_RANGE(use->argument(2)->range(), 45, 49, 45, 52);
  QVERIFY(!use->templateArgumentRange(2).isValid());

  ///TODO: what to do here? (use in macro)
  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 48, 27, 48, 27);
  VERIFY_RANGE(use->functionIdentifierRange(), 48, 27, 48, 27);
  QCOMPARE(use->argumentCount(), 3);
  QCOMPARE(use->templateArgumentCount(), 0);
  VERIFY_RANGE(use->argument(0)->range(), 48, 6, 48, 9);
  QVERIFY(!use->templateArgumentRange(0).isValid());
  VERIFY_RANGE(use->argument(1)->range(), 48, 11, 48, 17);
  QVERIFY(!use->templateArgumentRange(1).isValid());
  VERIFY_RANGE(use->argument(2)->range(), 48, 19, 48, 26);
  QVERIFY(!use->templateArgumentRange(2).isValid());
}

void TestTypeQueries::testMemberQueriesSingleTemplate()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0<T>()", 2);

  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 14, 2, 14, 15);
  VERIFY_RANGE(use->functionIdentifierRange(), 14, 4, 14, 8);
  QCOMPARE(use->argumentCount(), 0);
  QCOMPARE(use->templateArgumentCount(), 1);
  VERIFY_RANGE(use->templateArgumentRange(0), 14, 9, 14, 12);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 15, 2, 15, 16);
  VERIFY_RANGE(use->functionIdentifierRange(), 15, 4, 15, 8);
  QCOMPARE(use->argumentCount(), 0);
  QCOMPARE(use->templateArgumentCount(), 1);
  VERIFY_RANGE(use->templateArgumentRange(0), 15, 9, 15, 13);

  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0<char>()", 1);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0<int>()", 1);

  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0_static<char>(char)", 2);
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0_static<int>(int)", 4);

  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t1_0_static<T>(T)", 6);

  KDevelop::DUChainReadLocker lock;
  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 17, 2, 17, 23);
  VERIFY_RANGE(use->functionIdentifierRange(), 17, 4, 17, 15);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 17, 21, 17, 22);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 18, 2, 18, 18);
  VERIFY_RANGE(use->functionIdentifierRange(), 18, 4, 18, 15);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 18, 16, 18, 17);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 19, 2, 19, 27);
  VERIFY_RANGE(use->functionIdentifierRange(), 19, 8, 19, 19);
  VERIFY_RANGE(use->functionIdentifierRange(), 19, 8, 19, 19);
  VERIFY_RANGE(use->object()->range(), 19, 2, 19, 6);
  VERIFY_RANGE(use->accessorRange(), 19, 6, 19, 8);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 19, 25, 19, 26);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 20, 2, 20, 22);
  VERIFY_RANGE(use->functionIdentifierRange(), 20, 8, 20, 19);
  VERIFY_RANGE(use->object()->range(), 20, 2, 20, 6);
  VERIFY_RANGE(use->accessorRange(), 20, 6, 20, 8);
  QCOMPARE(use->implicitCtorCall(), false);
  QCOMPARE(use->implicitOnThis(), false);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 20, 20, 20, 21);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 22, 2, 22, 18);
  VERIFY_RANGE(use->functionIdentifierRange(), 22, 4, 22, 15);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 22, 16, 22, 17);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 23, 2, 23, 22);
  VERIFY_RANGE(use->functionIdentifierRange(), 23, 8, 23, 19);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 23, 20, 23, 21);
}

void TestTypeQueries::testMemberQueriesTwoTemplate()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_member_template_function_hits.cpp", "Test::t2_static<T, U>(T, U)", 3);
  mHelper->dumpResult(result);
}

void TestTypeQueries::testCtorQueryZeroArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_ctor_hits.cpp", "Test::Test()", 4);
  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 13, 14, 13, 20);
  VERIFY_RANGE(use->functionIdentifierRange(), 13, 14, 13, 18);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 24, 12, 24, 18);
  VERIFY_RANGE(use->functionIdentifierRange(), 24, 12, 24, 16);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 53, 18, 53, 24);
  VERIFY_RANGE(use->functionIdentifierRange(), 53, 18, 53, 22);
  QCOMPARE(use->argumentCount(), 0);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 67, 7, 67, 12);
  VERIFY_RANGE(use->object()->range(), 67, 7, 67, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 0);

  ///TODO: implicit ctor calls are not reported by KDevelop
}

void TestTypeQueries::testCtorQueryOneArg()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_ctor_hits.cpp", "Test::Test(Test const &)", 6);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 14, 34, 14, 45);
  VERIFY_RANGE(use->functionIdentifierRange(), 14, 34, 14, 38);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 14, 39, 14, 44);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 25, 7, 25, 13);
  VERIFY_RANGE(use->object()->range(), 25, 7, 25, 9);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 25, 10, 25, 12);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 26, 12, 26, 20);
  VERIFY_RANGE(use->functionIdentifierRange(), 26, 12, 26, 16);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 26, 17, 26, 19);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 27, 12, 27, 29);
  VERIFY_RANGE(use->functionIdentifierRange(), 27, 12, 27, 16);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 27, 17, 27, 28);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 54, 18, 54, 26);
  VERIFY_RANGE(use->functionIdentifierRange(), 54, 18, 54, 22);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 54, 23, 54, 25);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 55, 18, 55, 35);
  VERIFY_RANGE(use->functionIdentifierRange(), 55, 18, 55, 22);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 55, 23, 55, 34);

  lock.unlock();

  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_ctor_hits.cpp", "Test::Test(int)", 7);

  lock.lock();

  use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 15, 19, 15, 26);
  VERIFY_RANGE(use->functionIdentifierRange(), 15, 19, 15, 23);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 15, 24, 15, 25);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 18, 58, 18, 68);
  VERIFY_RANGE(use->functionIdentifierRange(), 18, 58, 18, 62);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 18, 63, 18, 67);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 27, 17, 27, 28);
  VERIFY_RANGE(use->functionIdentifierRange(), 27, 17, 27, 21);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 27, 22, 27, 27);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 30, 7, 30, 12);
  VERIFY_RANGE(use->object()->range(), 30, 7, 30, 9);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 30, 10, 30, 11);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 31, 7, 31, 14);
  VERIFY_RANGE(use->object()->range(), 31, 7, 31, 9);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 31, 10, 31, 13);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 32, 7, 32, 18);
  VERIFY_RANGE(use->object()->range(), 32, 7, 32, 9);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 32, 10, 32, 17);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 55, 23, 55, 34);
  VERIFY_RANGE(use->functionIdentifierRange(), 55, 23, 55, 27);
  QCOMPARE(use->argumentCount(), 1);
  VERIFY_RANGE(use->argument(0)->range(), 55, 28, 55, 33);
}

void TestTypeQueries::testCtorQueryTwoArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_ctor_hits.cpp", "Test::Test(char const*, char const*)", 14);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 16, 48, 16, 64);
  VERIFY_RANGE(use->functionIdentifierRange(), 16, 48, 16, 52);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 16, 53, 16, 57);
  VERIFY_RANGE(use->argument(1)->range(), 16, 59, 16, 63);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 17, 58, 17, 74);
  VERIFY_RANGE(use->functionIdentifierRange(), 17, 58, 17, 62);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 17, 63, 17, 67);
  VERIFY_RANGE(use->argument(1)->range(), 17, 69, 17, 73);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 36, 7, 36, 35);
  VERIFY_RANGE(use->object()->range(), 36, 7, 36, 9);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 36, 10, 36, 21);
  VERIFY_RANGE(use->argument(1)->range(), 36, 23, 36, 34);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 37, 7, 37, 32);
  VERIFY_RANGE(use->object()->range(), 37, 7, 37, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 37, 11, 37, 18);
  VERIFY_RANGE(use->argument(1)->range(), 37, 20, 37, 31);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 38, 7, 38, 32);
  VERIFY_RANGE(use->object()->range(), 38, 7, 38, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 38, 11, 38, 22);
  VERIFY_RANGE(use->argument(1)->range(), 38, 24, 38, 31);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 39, 7, 39, 28);
  VERIFY_RANGE(use->object()->range(), 39, 7, 39, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 39, 11, 39, 18);
  VERIFY_RANGE(use->argument(1)->range(), 39, 20, 39, 27);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 41, 13, 41, 43);
  VERIFY_RANGE(use->functionIdentifierRange(), 41, 13, 41, 17);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 41, 18, 41, 29);
  VERIFY_RANGE(use->argument(1)->range(), 41, 31, 41, 42);

  use = result.hits<ClassFunctionCall>().at(7);
  VERIFY_RANGE(use->range(), 42, 13, 42, 39);
  VERIFY_RANGE(use->functionIdentifierRange(), 42, 13, 42, 17);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 42, 18, 42, 25);
  VERIFY_RANGE(use->argument(1)->range(), 42, 27, 42, 38);

  use = result.hits<ClassFunctionCall>().at(8);
  VERIFY_RANGE(use->range(), 43, 13, 43, 39);
  VERIFY_RANGE(use->functionIdentifierRange(), 43, 13, 43, 17);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 43, 18, 43, 29);
  VERIFY_RANGE(use->argument(1)->range(), 43, 31, 43, 38);

  use = result.hits<ClassFunctionCall>().at(9);
  VERIFY_RANGE(use->range(), 44, 13, 44, 35);
  VERIFY_RANGE(use->functionIdentifierRange(), 44, 13, 44, 17);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 44, 18, 44, 25);
  VERIFY_RANGE(use->argument(1)->range(), 44, 27, 44, 34);

  use = result.hits<ClassFunctionCall>().at(10);
  VERIFY_RANGE(use->range(), 57, 18, 57, 48);
  VERIFY_RANGE(use->functionIdentifierRange(), 57, 18, 57, 22);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 57, 23, 57, 34);
  VERIFY_RANGE(use->argument(1)->range(), 57, 36, 57, 47);

  use = result.hits<ClassFunctionCall>().at(11);
  VERIFY_RANGE(use->range(), 58, 18, 58, 44);
  VERIFY_RANGE(use->functionIdentifierRange(), 58, 18, 58, 22);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 58, 23, 58, 30);
  VERIFY_RANGE(use->argument(1)->range(), 58, 32, 58, 43);

  use = result.hits<ClassFunctionCall>().at(12);
  VERIFY_RANGE(use->range(), 59, 18, 59, 44);
  VERIFY_RANGE(use->functionIdentifierRange(), 59, 18, 59, 22);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 59, 23, 59, 34);
  VERIFY_RANGE(use->argument(1)->range(), 59, 36, 59, 43);

  use = result.hits<ClassFunctionCall>().at(13);
  VERIFY_RANGE(use->range(), 60, 18, 60, 40);
  VERIFY_RANGE(use->functionIdentifierRange(), 60, 18, 60, 22);
  QCOMPARE(use->argumentCount(), 2);
  VERIFY_RANGE(use->argument(0)->range(), 60, 23, 60, 30);
  VERIFY_RANGE(use->argument(1)->range(), 60, 32, 60, 39);
}

void TestTypeQueries::testCtorQueryThreeArgs()
{
  EXEC_CLASSFUNCTION_QUERY(mHelper, "test_ctor_hits.cpp", "Test::Test(char const*, char const*, int)", 8);

  KDevelop::DUChainReadLocker lock;
  ClassFunctionCall::Ptr use = result.hits<ClassFunctionCall>().at(0);
  VERIFY_RANGE(use->range(), 46, 7, 46, 38);
  VERIFY_RANGE(use->object()->range(), 46, 7, 46, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 46, 11, 46, 18);
  VERIFY_RANGE(use->argument(1)->range(), 46, 20, 46, 26);
  VERIFY_RANGE(use->argument(2)->range(), 46, 28, 46, 37);

  use = result.hits<ClassFunctionCall>().at(1);
  VERIFY_RANGE(use->range(), 47, 7, 47, 42);
  VERIFY_RANGE(use->object()->range(), 47, 7, 47, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 47, 11, 47, 28);
  VERIFY_RANGE(use->argument(1)->range(), 47, 30, 47, 36);
  VERIFY_RANGE(use->argument(2)->range(), 47, 38, 47, 41);

  use = result.hits<ClassFunctionCall>().at(2);
  VERIFY_RANGE(use->range(), 48, 7, 48, 33);
  VERIFY_RANGE(use->object()->range(), 48, 7, 48, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 48, 11, 48, 18);
  VERIFY_RANGE(use->argument(1)->range(), 48, 20, 48, 27);
  VERIFY_RANGE(use->argument(2)->range(), 48, 29, 48, 32);

  use = result.hits<ClassFunctionCall>().at(3);
  VERIFY_RANGE(use->range(), 49, 7, 49, 38);
  VERIFY_RANGE(use->object()->range(), 49, 7, 49, 10);
  QVERIFY(!use->functionIdentifierRange().isValid());
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 49, 11, 49, 18);
  VERIFY_RANGE(use->argument(1)->range(), 49, 20, 49, 27);
  VERIFY_RANGE(use->argument(2)->range(), 49, 29, 49, 37);

  use = result.hits<ClassFunctionCall>().at(4);
  VERIFY_RANGE(use->range(), 62, 18, 62, 50);
  VERIFY_RANGE(use->functionIdentifierRange(), 62, 18, 62, 22);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 62, 23, 62, 30);
  VERIFY_RANGE(use->argument(1)->range(), 62, 32, 62, 38);
  VERIFY_RANGE(use->argument(2)->range(), 62, 40, 62, 49);

  use = result.hits<ClassFunctionCall>().at(5);
  VERIFY_RANGE(use->range(), 63, 18, 63, 54);
  VERIFY_RANGE(use->functionIdentifierRange(), 63, 18, 63, 22);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 63, 23, 63, 40);
  VERIFY_RANGE(use->argument(1)->range(), 63, 42, 63, 48);
  VERIFY_RANGE(use->argument(2)->range(), 63, 50, 63, 53);

  use = result.hits<ClassFunctionCall>().at(6);
  VERIFY_RANGE(use->range(), 64, 18, 64, 45);
  VERIFY_RANGE(use->functionIdentifierRange(), 64, 18, 64, 22);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 64, 23, 64, 30);
  VERIFY_RANGE(use->argument(1)->range(), 64, 32, 64, 39);
  VERIFY_RANGE(use->argument(2)->range(), 64, 41, 64, 44);

  use = result.hits<ClassFunctionCall>().at(7);
  VERIFY_RANGE(use->range(), 65, 18, 65, 50);
  VERIFY_RANGE(use->functionIdentifierRange(), 65, 18, 65, 22);
  QCOMPARE(use->argumentCount(), 3);
  VERIFY_RANGE(use->argument(0)->range(), 65, 23, 65, 30);
  VERIFY_RANGE(use->argument(1)->range(), 65, 32, 65, 39);
  VERIFY_RANGE(use->argument(2)->range(), 65, 41, 65, 49);
}

void TestTypeQueries::testCtorQuerySingleTemplate()
{
  EXEC_QUERY(mHelper, "test_ctor_template_hits.cpp", "Test<T>::Test()", 0, ClassFunctionQuery, ClassUse);
  QEXPECT_FAIL("", "sadly no hits get reported for ctors of template classes, have to fix that in cpp lang support", Continue);
  QCOMPARE(result.hits<ClassFunctionCall>().size(), 2);

  mHelper->dumpResult(result);
}

void TestTypeQueries::testSubclassOf()
{
  EXEC_CLASS_QUERY(mHelper, "test_subclassof_hits.cpp", "children of Base", 5);
  EXEC_CLASS_QUERY(mHelper, "test_subclassof_hits.cpp", "children of Child", 2);
  EXEC_CLASS_QUERY(mHelper, "test_subclassof_hits.cpp", "children of SubChild", 1);
}
