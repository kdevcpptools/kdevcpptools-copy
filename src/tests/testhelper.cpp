#include "testhelper.h"

#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtTest/QTest>

#include <languages/cpp/cppduchain/declarationbuilder.h>
#include <languages/cpp/cppduchain/usebuilder.h>
#include <languages/cpp/parser/control.h>
#include <languages/cpp/parser/parser.h>
#include <languages/cpp/parser/parsesession.h>
#include <languages/cpp/parser/rpp/preprocessor.h>
#include <languages/cpp/parser/rpp/pp-engine.h>

#include "libportdb/xmlparser.h"
#include "libquery/cppqueryengine.h"

#include "libquery/queryuses/classfunctioncall.h"
#include "libquery/queryuses/classuse.h"
#include "libquery/queryuses/functioncall.h"
#include "libquery/queryuses/enumuse.h"

#include <languages/cpp/cppduchain/dumpchain.h>

#include <tests/autotestshell.h>
#include <tests/testcore.h>
#include <interfaces/ilanguagecontroller.h>
#include <language/backgroundparser/backgroundparser.h>
#include <tests/testproject.h>
#include <interfaces/ilanguage.h>

using namespace KDevelop;

namespace { /// Anonymous helper functions;
  QString testFileFullPath(QString const &file)
  {
    #ifdef KDESRCDIR
    return QString(KDESRCDIR) + "/test-files/" + file;
    #else
    return file;
    #endif
  }
}

class TestFile : public QObject
{
    Q_OBJECT
public:
    TestFile(const QString& file, TestProject *project) : m_file(file), m_ready(false) {
        project->addToFileSet(m_file);
    }

    ~TestFile()
    {
    }


    void parse(TopDUContext::Features features)
    {
        DUChain::self()->updateContextForUrl(m_file, features, this);
    }

    void waitForParsed()
    {
        QTime t;
        t.start();
        while (!m_ready) {
            Q_ASSERT(t.elapsed() < 60000);
            QTest::qWait(10);
        }
    }

    ReferencedTopDUContext topContext()
    {
        waitForParsed();
        return m_topContext;
    }

public slots:
    ///NOTE: leave the KDevelop:: in the signature, else it will never get called :-S
    void updateReady(KDevelop::IndexedString url, KDevelop::ReferencedTopDUContext topContext)
    {
        Q_ASSERT(url.toUrl() == m_file.toUrl());

        DUChainWriteLocker lock(DUChain::lock());
        if (topContext->parsingEnvironmentFile() && topContext->parsingEnvironmentFile()->isProxyContext()) {
          Q_ASSERT(!topContext->importedParentContexts().isEmpty());
          m_topContext = ReferencedTopDUContext(dynamic_cast<TopDUContext*>(topContext->importedParentContexts().first().context(0)));
        } else {
          m_topContext = topContext;
        }

        Q_ASSERT(m_topContext->ast());
        m_ready = true;
    }

private:
    IndexedString m_file;
    bool m_ready;
    ReferencedTopDUContext m_topContext;
};

TestHelper::TestHelper() : mDatabase(new CppPortingDatabase)
{
    AutoTestShell::init();
    m_core = new TestCore();
    m_core->initialize(Core::NoUi);
    m_projectController = new TestProjectController(m_core);
    m_core->setProjectController(m_projectController);
}

TestHelper::~TestHelper()
{
  m_core->cleanup();
  delete m_core;
  delete mDatabase;
}

CppPortingDatabase *TestHelper::database() const
{
  return mDatabase;
}

void TestHelper::loadDatabase(QString const &databaseFileName)
{
  // Comment out for now. Its too much of an hasle to keep the format in sync
  QString fileName = testFileFullPath(databaseFileName);
  QFileInfo info(fileName);
  Q_ASSERT_X(info.exists(), "TestHelper::loadDatabase", QString("could not find database %1").arg(fileName).toAscii());

  XmlParser parser;
  XmlParser::ParseResult result = parser.parse(fileName, mDatabase);
  Q_ASSERT(result == XmlParser::Ok);
}

ReferencedTopDUContext TestHelper::parse(QString const &fileName) const
{
    TestProject* project = new TestProject;
    m_projectController->clearProjects();
    m_projectController->addProject(project);
    QList< ILanguage* > languages = m_core->languageController()->languagesForUrl(KUrl(fileName));
    if (languages.count() != 1 || languages.first()->name() != "C++") {
      return ReferencedTopDUContext(0);
    }

    TestFile f2(testFileFullPath(fileName), project);
    f2.parse(TopDUContext::AllDeclarationsContextsUsesAndAST);

    return f2.topContext();
}

void TestHelper::dumpQueries() const
{
  qDebug() << "-- total queries:" << mDatabase->queries().count();
  foreach ( Query::Ptr const query, mDatabase->queries() ) {
    qDebug() << query->id();
  }
  qDebug() << "/--";
}

void TestHelper::dumpResult(QueryResult const &result) const
{
  qDebug() << QString("DUMPING: result has %1 uses").arg(result.hits().size());
  for ( int i = 0; i < result.hits().size(); ++i ) {
    QueryHit::Ptr use = result.hits().at(i);
    if ( FunctionCall::Ptr c = use.dynamicCast<FunctionCall>() ) {
      qDebug() << i << c->range().castToSimpleRange().textRange() << c->functionIdentifierRange().castToSimpleRange().textRange()
                << '(' << c->argumentCount() << "arguments, " << c->templateArgumentCount() << "template arguments )";
      for ( int j = 0; j < c->argumentCount(); ++j ) {
        qDebug() << "\t" << "argument" << j << c->argument(j)->range().castToSimpleRange().textRange();
      }
      for ( int j = 0; j < c->templateArgumentCount(); ++j ) {
        qDebug() << "\t" << "template argument" << j << c->templateArgumentRange(j).castToSimpleRange().textRange();
      }
    } else if ( ClassUse::Ptr u = use.dynamicCast<ClassUse>() ) {
      qDebug() << i << u->range().castToSimpleRange().textRange() << u->identifierRange().castToSimpleRange().textRange() << '(' << u->templateArgumentCount() << "template arguments )";
      for ( int j = 0; j < u->templateArgumentCount(); ++j ) {
        qDebug() << "\t" << "template argument" << j << u->templateArgumentRange(j).castToSimpleRange().textRange();
      }
    } else if ( EnumUse::Ptr e = use.dynamicCast<EnumUse>() ) {
      qDebug() << i << e->range().castToSimpleRange().textRange();
    } else {
      Q_ASSERT_X(false, kBacktrace().toLocal8Bit().constData(), "Use has unhandled type, cannot be dumped.");
    }
  }
}

#include "testhelper.moc"
#include "moc_testhelper.cpp"
