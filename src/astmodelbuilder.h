/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ASTMODEL_H
#define ASTMODEL_H

#include <QtCore/QStack>
#include <QtGui/QStandardItemModel>

#include <default_visitor.h>
#include <parsesession.h>
#include "../cppduchain/cppeditorintegrator.h"

class ASTModelBuilder : DefaultVisitor
{
  public:
    ///NOTE: The caller takes ownership of the returned Model.
    QStandardItemModel* createModel(ParseSession *session);

  private:
    QStack<QStandardItem*> mCurrentParent;
    QStandardItemModel    *mModel;
    CppEditorIntegrator   *mEditor;

  protected:
    void createAppendAndPushChild(AST *node);
    virtual void visit(AST* node);
};

#endif // ASTMODEL_H
