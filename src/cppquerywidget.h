/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CPPQUERYWIDGET_H
#define CPPQUERYWIDGET_H

#include <QtCore/QMap>
#include <QtCore/QSet>
#include <QtGui/QWidget>

#include <KUrl>

#include <libtransform/cpptransformengine.h>
#include <libtransform/transform.h>
#include <libportdb/cppportingdatabase.h>
#include <libquery/query.h>

#include "models-views/bardelegate.h"
#include "models-views/resultdatamodel.h"

class QComboBox;
class QTextEdit;
class QTreeWidgetItem;

namespace KDevelop
{
class IDocument;
class IndexedString;
class IProject;
class ParseJob;
class ReferencedTopDUContext;
}

namespace Ui
{
class CppQueryEngineWidget;
}

class FileImpactWidget;
class KJob;
class QueryJob;
class ResultTreeWidget;

class CppQueryWidget : public QWidget
{
    Q_OBJECT

  public:
    CppQueryWidget(QWidget *parent /*, m_plugin */);

    virtual ~CppQueryWidget();

  public slots:
    void handleJobResult(KJob*);
    void handleJobDeletion(QObject*);
    void handleResult(const QueryResult &result);
    void duchainChanged(KDevelop::DUContextPointer);

  private: /// Methods
    QList<Query::Ptr> currentQueries() const;
    QString removeProjectPath(KDevelop::IndexedString const &url);

  private slots:
    /// Tries to apply all transforms that apply for the current selected file.
    void applyAll();
    /// Tries to apply transforms for all hits of @param query that apply for the current selected file.
    void applyAllForQuery(const QString &queryId);
    /// Tries to apply the transform for @param queryId at @oaram range
    void applyQueryForRange(const QString &queryId, const KDevelop::SimpleRange &range);
    void configureColors();

    void handleFileSelectionChange(const QString &file);
    void handleFileDoubleClick(const QString &file);
    void handleQuerySelectionChange(const QString &query);
    void fillQueryCombo();
    void openDatabase(KUrl const &fileName);
    void projectActivated(QString const &project); /// Handles combobox changes
    void projectClosed(KDevelop::IProject*);
    void projectOpened(KDevelop::IProject*);
    void setCorrectQueryControlWidgetsStatus();
    void showAST();

    void fileQueryButtonClicked();
    void startFileQuery();
    void projectQueryButtonClicked();
    void startProjectQuery();

    void setCurrentDocument(KDevelop::IDocument*);
    void currentDocumentClosed();

  private:
    KDevelop::IProject*           mCurrentProject;
    CppPortingDatabase            mDatabase;
    // for the whole project
    FileImpactWidget             *mFileImpactWidget;
    ResultDataModel::Ptr          mResultModel;
    ResultTreeWidget             *mResultTreeWidget;
    // for the current file
    FileImpactWidget             *mCurFileImpactWidget;
    ResultDataModel::Ptr          mCurFileResultModel;

    QueryJob                     *mJob;

    CppTransformEngine::Ptr       mTransformEngine;
    Ui::CppQueryEngineWidget*     mUi;
    KDevelop::IndexedString       mCurrentFile;

    bool                          mStartingQuery;
};

#endif // CPPQUERYWIDGET_H
