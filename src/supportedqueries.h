/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef SUPPORTEDQUERIES_H
#define SUPPORTEDQUERIES_H

#include <QtCore/QList>

#include <cpptransformengine.h>
#include <transform.h>

/**
 * Temporary class to store queries/transforms that are already implemented. This class must
 * be removed as soon as there is a more convenient way to store queries.
 */
class SupportedQueries
{
  public:    
    /// Initialize the engine with supported queries.
    static void init(CppTransformEngine::Ptr const &engine);
    
    // QString queries/transforms
    static void ctorQStringConstCharPtr(CppTransformEngine::Ptr const &engine);
    static void ctorQStringConstStdStringRef(CppTransformEngine::Ptr const &engine);
    static void typeQPtrList(CppTransformEngine::Ptr const &engine);
};

#endif // SUPPORTEDQUERIES_H
