#ifndef FUNCTIONQUERY_H
#define FUNCTIONQUERY_H

#include <QtCore/QString>

#include "query.h"
#include <QMap>

namespace KDevelop {
class QualifiedIdentifier;
}

class FunctionQueryPrivate;

/**
 * Searches for function calls to specified function taking in account optional
 * restrictions on the arguments passed the call.
 */
class CPPQUERYENGINE_EXPORT FunctionQuery : public Query
{
public:
  typedef QSharedPointer<FunctionQuery> Ptr;
  typedef QMap<int, QString> Restrictions;

  enum ArgumentRestriction {
    LiteralRestriction, ///> Can be used to see if a passed literal (char, strings, bools, ...) matches given regexp
    QidRestriction          ///> Can be used to see if a passed argument equals given qid (e.g. enum values)
  };

public:
    FunctionQuery(QString const &queryId,
                  KDevelop::QualifiedIdentifier const &functionIdentifier);

    virtual ~FunctionQuery();

    /**
     * Append a valid type expression for each argument of the function which is
     * looked up. If no types appended, only functions with no arguments are
     * considered.
     */
    void appendArgumentType(QString const &argumentType);

    QStringList argumentTypes() const;

    /**
     * Gives possibility to restrict the search to find only calls of the specified
     * function that have passed values to the arguments that adhere to given
     * restrictions.
     */
    void addArgumentRestriction(ArgumentRestriction kind, int argument, QString const &restriction);

    /**
     * Retrieve restrictions per kind.
     */
    Restrictions argumentRestrictions(ArgumentRestriction kind) const;

    /**
     * Append restrictions if you're looking for a template function, e.g:
     * T qobject_cast(QObject *). If no restriction is set, only non template
     * functions are looked up. To find functions with a specified number of
     * template arguments, append as many restrictions as needed. To find only
     * template functions with a specified template argument set the restriction to
     * that argument.
     *
     * Note: restriction can be any valid type expression.
     *       e.g. "QString", "QString *", "QString &", "QString const *", etc.
     */
    void appendTemplateRestriction(QString const &restriction = QString());

    KDevelop::QualifiedIdentifier functionIdentifier() const;

    QStringList templateRestrictions() const;

private:
  FunctionQueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(FunctionQuery)
};

#endif // FUNCTIONQUERY_H
