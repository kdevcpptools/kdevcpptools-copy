#include "classfunctionquery.h"

#include "language/duchain/identifier.h"

using namespace KDevelop;

///BEGIN: ClassFunctionQueryPrivate

class ClassFunctionQueryPrivate
{
public:
  ClassFunctionQueryPrivate()
    : constRestriction(ClassFunctionQuery::NoConstRestriction)
  { }

  ClassFunctionQuery::ConstnessRestriction constRestriction;
};

///END: ClassFunctionQueryPrivate

///BEGIN: ClassFunctionQuery

ClassFunctionQuery::ClassFunctionQuery(const QString& queryId, const KDevelop::QualifiedIdentifier& functionIdentifier)
  : FunctionQuery(queryId, functionIdentifier), d_ptr(new ClassFunctionQueryPrivate())
{ }

ClassFunctionQuery::~ClassFunctionQuery()
{
  delete d_ptr;
}

ClassFunctionQuery::ConstnessRestriction ClassFunctionQuery::constnessRestriction() const
{
  return d_ptr->constRestriction;
}

void ClassFunctionQuery::setConstnessRestriction(const ClassFunctionQuery::ConstnessRestriction restriction)
{
  d_ptr->constRestriction = restriction;
}

///END: ClassFunctionQuery