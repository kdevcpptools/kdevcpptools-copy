/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ENUMQUERY_H
#define ENUMQUERY_H

#include "query.h"

namespace KDevelop {
class QualifiedIdentifier;
}

class EnumQueryPrivate;

class CPPQUERYENGINE_EXPORT EnumQuery : public Query
{
public:
  typedef QSharedPointer<EnumQuery> Ptr;

public:
  EnumQuery(QString const &queryId,
            KDevelop::QualifiedIdentifier const &identifier);
  virtual ~EnumQuery();

  KDevelop::QualifiedIdentifier qualifiedIdentifier() const;

private:
  EnumQueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(EnumQuery)
};

#endif // ENUMQUERY_H
