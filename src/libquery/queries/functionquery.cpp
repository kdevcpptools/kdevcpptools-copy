#include "functionquery.h"

#include <language/duchain/identifier.h>

using namespace KDevelop;

class FunctionQueryPrivate
{
public: /// Members
  QStringList         mArgumentTypes;
  QualifiedIdentifier mFunctionIdentifier;
  QMap<FunctionQuery::ArgumentRestriction, FunctionQuery::Restrictions> mRestrictionsPerKind;
  QStringList         mTemplateRestrictions;

public: /// Functions
  FunctionQueryPrivate(QualifiedIdentifier const &functionIdentifier);
};

FunctionQueryPrivate::FunctionQueryPrivate(QualifiedIdentifier const &functionIdentifier)
  :mFunctionIdentifier(functionIdentifier)
{ }

FunctionQuery::FunctionQuery(QString const &queryId,
                             QualifiedIdentifier const &functionIdentifier)
  : Query(queryId), d_ptr(new FunctionQueryPrivate(functionIdentifier))
{ }

FunctionQuery::~FunctionQuery()
{
  delete d_ptr;
}

void FunctionQuery::appendArgumentType(QString const &argumentType)
{
  d_ptr->mArgumentTypes << argumentType;
}

QStringList FunctionQuery::argumentTypes() const
{
  return d_ptr->mArgumentTypes;
}

void FunctionQuery::addArgumentRestriction(ArgumentRestriction kind, int argument, QString const &restriction)
{
  d_ptr->mRestrictionsPerKind[kind].insert(argument, restriction);
}

FunctionQuery::Restrictions FunctionQuery::argumentRestrictions(ArgumentRestriction kind) const
{
  return d_ptr->mRestrictionsPerKind[kind];
}

void FunctionQuery::appendTemplateRestriction(QString const &restriction)
{
  d_ptr->mTemplateRestrictions << restriction;
}

KDevelop::QualifiedIdentifier FunctionQuery::functionIdentifier() const
{
  return d_ptr->mFunctionIdentifier;
}

QStringList FunctionQuery::templateRestrictions() const
{
  return d_ptr->mTemplateRestrictions;
}
