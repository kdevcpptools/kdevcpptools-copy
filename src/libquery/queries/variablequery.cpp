#include "variablequery.h"

#include <QtCore/QList>

#include <language/duchain/identifier.h>

using namespace KDevelop;

class VariableQueryPrivate
{
public:
  QList<QString>                     mTemplateRestrictions;
  QualifiedIdentifier                mTypeIdentifier;
  QList<VariableQuery::TypeModifier> mTypeSpeficiers;
};

VariableQuery::VariableQuery(QString const &queryId,
                             QualifiedIdentifier const &qid)
  : Query(queryId), d_ptr(new VariableQueryPrivate)
{ }

void VariableQuery::appendTemplateRestriction(QString const &restriction)
{
  d_func()->mTemplateRestrictions << restriction;
}

void VariableQuery::appendTypeModifier(TypeModifier specifier)
{
  d_func()->mTypeSpeficiers << specifier;
}
