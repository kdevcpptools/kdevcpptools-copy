#include "classquery.h"

#include <language/duchain/identifier.h>

using namespace KDevelop;

class ClassQueryPrivate
{
public: /// members
  QualifiedIdentifier mQid;
  QStringList         mTemplateTypeIdentifiers;
  QualifiedIdentifier mSubclassOf;

public: /// Functions
  ClassQueryPrivate(QualifiedIdentifier const &qid);
};

ClassQueryPrivate::ClassQueryPrivate(QualifiedIdentifier const &qid)
    : mQid(qid)
{ }

ClassQuery::ClassQuery(QString const &queryId, QualifiedIdentifier const &qid)
  : Query(queryId)
  , d_ptr(new ClassQueryPrivate(qid))
{ }

ClassQuery::~ClassQuery()
{
  delete d_ptr;
}

void ClassQuery::appendTemplateRestriction(QString const &restriction)
{
  d_ptr->mTemplateTypeIdentifiers << restriction;
}

QualifiedIdentifier ClassQuery::qualifiedIdentifier() const
{
  return d_ptr->mQid;
}

QStringList ClassQuery::templateRestrictions() const
{
  return d_ptr->mTemplateTypeIdentifiers;
}

void ClassQuery::setSubclassOf(const KDevelop::QualifiedIdentifier& pid)
{
  d_ptr->mSubclassOf = pid;
}

KDevelop::QualifiedIdentifier ClassQuery::subclassOf() const
{
  return d_ptr->mSubclassOf;
}
