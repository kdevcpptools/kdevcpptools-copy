/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "enumquery.h"

#include <language/duchain/identifier.h>

using namespace KDevelop;

//BEGIN EnumQueryPrivate
class EnumQueryPrivate {
public:
  EnumQueryPrivate(const QualifiedIdentifier& identifier);
  QualifiedIdentifier mQid;
};

EnumQueryPrivate::EnumQueryPrivate(const QualifiedIdentifier& identifier)
  : mQid(identifier)
{

}

//END EnumQueryPrivate

//BEGIN EnumQuery

EnumQuery::EnumQuery(const QString& queryId, const QualifiedIdentifier& identifier)
  : Query(queryId), d_ptr(new EnumQueryPrivate(identifier))
{
}

EnumQuery::~EnumQuery()
{
  delete d_ptr;
}

QualifiedIdentifier EnumQuery::qualifiedIdentifier() const
{
  return d_ptr->mQid;
}

//END EnumQuery
