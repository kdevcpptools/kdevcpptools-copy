#ifndef VARIABLEQUERY_H
#define VARIABLEQUERY_H

#include "query.h"

#include <QtCore/QSharedPointer>
#include <QtCore/QString>

class VariableQueryPrivate;

class VariableQuery : public Query
{
public:
  enum TypeModifier {
    Const,
    Pointer,
    Reference
  };

  typedef QSharedPointer<VariableQuery> Ptr;

public:
  VariableQuery(QString const &queryId);

  /**
   * Specifies the precise type of the variable. Note: this works from right to left.
   * e.g.
   *
   * VariableQuery vq("test-id", "Test");
   * vq.appendTypeSpecifier( VariableQuery::Const );
   * vq.appendTypeSpecifier( VariableQuery::Pointer );
   *
   * finds variables of pointers to const Test objects.
   *
   * and
   *
   * VariableQuery vq("test-id", "Test");
   * vq.appendTypeSpecifier( VariableQuery::Pointer );
   * vq.appendTypeSpecifier( VariableQuery::Const );
   *
   * finds variables of const pointers to Test objects.
   */
  void appendTypeModifier(TypeModifier specifier);

private:
  VariableQueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(VariableQuery)
};

#endif // VARIABLEQUERY_H
