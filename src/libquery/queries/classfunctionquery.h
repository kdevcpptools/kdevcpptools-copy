#ifndef CLASSFUNCTIONQUERY_H
#define CLASSFUNCTIONQUERY_H

#include "functionquery.h"

class ClassFunctionQueryPrivate;

/**
 * Query to find uses of a specified method.
 *
 * To find some method use e.g.:
 * \code
 * ClassFunctionQuery query("QString::append(std::string const &)", QualifiedIdentifier("QString::append"));
 * query->appendArgumentType("std::string const &");
 * \endcode
 *
 * To find a ctor of a class use e.g.:
 * \code
 * ClassFunctionQuery ctorQuery("QString::QString(std::string const &)", QualifiedIdentifier("QString::QString"));
 * ctorQuery->setCtorRestriction(RestrictCtor);
 * ctorQuery->appendArgumentType("std::string const &");
 * \endcode
 *
 * To find all non-const non-ctor of a class use e.g.:
 * \code
 * ClassFunctionQuery query("QString::*()", QualifiedIdentifier("QString::*"));
 * query->setConstnessRestriction(RestrictNonConst);
 * query->setCtorRestriction(RestrictNonCtor);
 * \endcode
 *
 *
 * TODO: Optionally add a parent ClassQuery that pre-filters the list of classes we search in.
 * TODO: add more restrictions, @see KDevelop::ClassMemberDeclaration & ClassFunctionDeclaration
 */
class CPPQUERYENGINE_EXPORT ClassFunctionQuery : public FunctionQuery
{
public:
  typedef QSharedPointer<ClassFunctionQuery> Ptr;

public:
  ClassFunctionQuery(QString const &queryId,
                     KDevelop::QualifiedIdentifier const &functionIdentifier);
  virtual ~ClassFunctionQuery();

  enum ConstnessRestriction {
    /// Query will return all matching functions.
    NoConstRestriction,
    /// Query will only return constant functions
    RestrictConst,
    /// Query will only return functions that are not constant
    RestrictNonConst
  };
  ConstnessRestriction constnessRestriction() const;
  void setConstnessRestriction(ConstnessRestriction const restriction);

protected:
  ClassFunctionQueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(ClassFunctionQuery)
};

#endif // CLASSFUNCTIONQUERY_H
