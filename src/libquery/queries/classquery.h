#ifndef TYPEQEURY_H
#define TYPEQEURY_H

#include <QtCore/QSharedPointer>
#include <QtCore/QString>

#include "query.h"

namespace KDevelop {
class QualifiedIdentifier;
}

class ClassQueryPrivate;

class CPPQUERYENGINE_EXPORT ClassQuery : public Query
{
public:
  typedef QSharedPointer<ClassQuery> Ptr;

public:
  ClassQuery(QString const &queryId, KDevelop::QualifiedIdentifier const &qid);
  virtual ~ClassQuery();

  /**
   * Append restrictions if you're looking for a template type, e.g QList<T>. If
   * no restriction is set, only non template types are looked up. To
   * find all types with a specified number of template argument append as many
   * restrictions as needed. To find only template types with a specified template
   * argument set the restriction to that argument.
   *
   * e.g.:
   *
   * ClassQuery cq1("Test<T>", "Test")
   * cq.appendTemplateRestriction()          : finds uses of type Test<T> for any T

   * ClassQuery cq2("Test<QString>", "Test")
   * cq.appendTemplateRestriction("QString") : finds only uses of type Test<QString>
   *
   * Note: restriction can be any valid type expression.
   *       e.g. "QString", "QString *", "QString &", "QString const *", etc.
   */
  void appendTemplateRestriction(QString const &restriction = QString());

  /// Returns the qualified identifier of the class or struct that is queried for.
  KDevelop::QualifiedIdentifier qualifiedIdentifier() const;

  /// Returns the list of template restrictions.
  QStringList templateRestrictions() const;

  /// Add @p pid as parent class filter for this query.
  void setSubclassOf(const KDevelop::QualifiedIdentifier& pid);

  /// Returns the QualifiedIdentifier of a parent class that this query shall match.
  KDevelop::QualifiedIdentifier subclassOf() const;

private:
  ClassQueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(ClassQuery)
};

#endif // TYPEQEURY_H
