/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "classfunctioncall.h"

#include <language/duchain/classfunctiondeclaration.h>
#include <languages/cpp/cppduchain/expressionevaluationresult.h>
#include <languages/cpp/cppduchain/expressionparser.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>

#include "functioncall_p.h"
#include "util.h"
#include "variable_p.h"

using namespace KDevelop;

//BEGIN: ClassFunctionCallPrivate

class ClassFunctionCallPrivate : public FunctionCallPrivate {
public:
  ClassFunctionCallPrivate(ReferencedTopDUContext const &context);
  ClassFunctionCallPrivate(ClassFunctionCallPrivate const &other);

  ClassFunctionCallPrivate &operator=(ClassFunctionCallPrivate const &other);

  void extract(const RangeInRevision& range, const ClassFunctionDeclaration* cdecl);

  QString stringForNode(AST *node) const
  {
    CppEditorIntegrator editor(mParseSession);
    return editor.tokensToStrings(node->start_token, node->end_token);
  }

public:
  RangeInRevision mObjectRange;
  RangeInRevision mAccessorRange;
  bool mImplicitCtorCall;
  bool mImplicitOnThis;
  QString mObjectString;
};

ClassFunctionCallPrivate::ClassFunctionCallPrivate(const KDevelop::ReferencedTopDUContext& context)
  : FunctionCallPrivate(context),
    mObjectRange(RangeInRevision::invalid()), mAccessorRange(RangeInRevision::invalid()),
    mImplicitCtorCall(false), mImplicitOnThis(false)
{
}

ClassFunctionCallPrivate::ClassFunctionCallPrivate(const ClassFunctionCallPrivate& other)
  : FunctionCallPrivate(other),
    mObjectRange(other.mObjectRange), mAccessorRange(other.mAccessorRange),
    mImplicitCtorCall(other.mImplicitCtorCall), mImplicitOnThis(other.mImplicitOnThis)
{
}

ClassFunctionCallPrivate& ClassFunctionCallPrivate::operator=(const ClassFunctionCallPrivate& other)
{
  if ( this != &other ) {
    mObjectRange = other.mObjectRange;
    mAccessorRange = other.mAccessorRange;
    mImplicitCtorCall = other.mImplicitCtorCall;
    mImplicitOnThis = other.mImplicitOnThis;
    FunctionCallPrivate::operator=(other);
  }
  return *this;
}

void ClassFunctionCallPrivate::extract( const KDevelop::RangeInRevision& range,
                                        const KDevelop::ClassFunctionDeclaration *cdecl )
{
  Q_ASSERT( range.isValid() );
  Q_ASSERT( cdecl );

  mInsideMacro = range.isEmpty();
  AST *node = nodeForRange(range); // UnqualifiedNameAst
  Q_ASSERT(node || mInsideMacro);
  if (mInsideMacro && !node) {
    mRange = range;
    mFunctionIdRange = range;
    return;
  }

  mRange = RangeInRevision::invalid();

  // for some strange reasons (aka parse errors in the C++ parser) it can happen
  // that node is not set here, do not crash in this case
  if ( !node )
    return;

  // the start node for this call
  AST* start = 0;
  // the node of the function call or initializer list
  AST* call = 0;

  if ( node->kind == AST::Kind_FunctionCall ) {
    // uses of ctors have only the '(' marked as use
    call = node;
    // get identifier
    AST* postFixParent = ancestorNode(node, mParseSession, 1);
    Q_ASSERT(postFixParent);
    Q_ASSERT(postFixParent->kind == AST::Kind_PostfixExpression);
    PostfixExpressionAST *peParent = reinterpret_cast<PostfixExpressionAST*>(postFixParent);
    Q_ASSERT(peParent);
    // should be the identifier (PrimaryExpression -> Name -> UnqualifiedName)
    start = peParent->expression;
    ///TODO: template arguments must not be included in this range!
    mFunctionIdRange = rangeForNode(start);
  } else if ( node->kind == AST::Kind_NewExpression ) {
    // new foo(...) style ctor
    NewExpressionAST* exp = reinterpret_cast<NewExpressionAST*>(node);
    Q_ASSERT(exp);
    Q_ASSERT(exp->new_type_id);
    Q_ASSERT(exp->new_type_id->type_specifier);
    start = exp->new_type_id;
    ///TODO: template arguments must not be included in this range!
    mFunctionIdRange = rangeForNode(start);
    call = exp->new_initializer; // if null we have something like Test a = Test;
  } else if ( node->kind == AST::Kind_MemInitializer ) {
    // ChildCTor() : BaseCTor() ...
    MemInitializerAST* exp = reinterpret_cast<MemInitializerAST*>(node);
    Q_ASSERT(exp);
    Q_ASSERT(exp->initializer_id);
    start = exp->initializer_id;
    ///TODO: template arguments must not be included in this range!
    mFunctionIdRange = rangeForNode(start);
    call = node;
  } else if ( node->kind == AST::Kind_InitDeclarator ) {
    // ClassName varName(arg);
    InitDeclaratorAST* exp = reinterpret_cast<InitDeclaratorAST*>(node);
    Q_ASSERT(exp);
    Q_ASSERT(exp->declarator);
    start = exp->declarator->id;
    ///TODO: template arguments must not be included in this range!
    mObjectRange = rangeForNode(start);
    mObjectString = stringForNode(start);
    if ( exp->declarator->parameter_declaration_clause ) {
      mImplicitCtorCall = true;
      call = exp->declarator->parameter_declaration_clause;
    } else {
      call = exp->initializer;
    }
    // include closing paren
    mRange = rangeForNode(exp);
  } else {
    // otherwise it's the identifier that's marked as use
    Q_ASSERT(node->kind == AST::Kind_UnqualifiedName);
    UnqualifiedNameAST* name = static_cast<UnqualifiedNameAST*>(node);
    start = ancestorNode(node, mParseSession, 3);
    if (start->start_token == node->start_token) {
      mImplicitOnThis = true;
    } else {
      AST* parent_2 = ancestorNode(node, mParseSession, 2);
      CppEditorIntegrator editor(mParseSession);
      if (parent_2->kind == AST::Kind_ClassMemberAccess) {
        // e.g.: foo.bar() or foo->bar()
        mObjectRange = editor.findRange(start->start_token, parent_2->start_token);
        mObjectString = editor.tokensToStrings(start->start_token, parent_2->start_token);
        mAccessorRange = editor.findRange(parent_2->start_token, node->start_token);
      } else {
        Q_ASSERT(parent_2->kind == AST::Kind_PrimaryExpression);
        // e.g. Foo::bar()
        PrimaryExpressionAST* peAst = static_cast<PrimaryExpressionAST*>(parent_2);
        Q_ASSERT(peAst->name->unqualified_name == name);
        // Foo
        const ListNode< UnqualifiedNameAST* >* it = peAst->name->qualified_names->toBack();
        mObjectRange = editor.findRange(start->start_token, it->element->end_token);
        mObjectString = editor.tokensToStrings(start->start_token, it->element->end_token);
        // ::
        mAccessorRange = editor.findRange(it->element->end_token, name->start_token);
      }
      Q_ASSERT(mObjectRange.isValid());
      Q_ASSERT(mAccessorRange.isValid());
    }
    Q_ASSERT(start->kind == AST::Kind_PostfixExpression || start->kind == AST::Kind_CppCastExpression);
    mFunctionIdRange = range;
    call = findFunctionCall(name, mParseSession);
  }

  Q_ASSERT(mFunctionIdRange.isValid() || mObjectRange.isValid());
  Q_ASSERT(start);
  if ( !mRange.isValid() ) {
    mRange = rangeForNode(start, call);
  }

  extractArgumentRanges(call);

  if (Cpp::isTemplateDeclaration(cdecl))
    mTemplateArgumentRanges = extractTemplateArgumentRanges(range);
}

//END: ClassFunctionCallPrivate

//BEGIN: ClassFunctionCall

ClassFunctionCall::ClassFunctionCall(const ClassFunctionDeclaration* cdecl,
                                     const ReferencedTopDUContext& context,
                                     const RangeInRevision& range)
  : FunctionCall(new ClassFunctionCallPrivate(context)),
    d_ptr(static_cast<ClassFunctionCallPrivate*>(FunctionCall::d_ptr))
{
  Q_ASSERT( d_ptr == FunctionCall::d_ptr );
  Q_ASSERT( d_ptr == QueryHit::d_ptr );
  Q_ASSERT( range.isValid() );
  d_ptr->extract( range, cdecl );
}

ClassFunctionCall::ClassFunctionCall(const ClassFunctionCall& other)
  : FunctionCall(new ClassFunctionCallPrivate(*other.d_ptr)),
    d_ptr(static_cast<ClassFunctionCallPrivate*>(FunctionCall::d_ptr))
{
}

ClassFunctionCall::~ClassFunctionCall()
{
  // class_function_call_d_ptr gets deleted in QueryUse
}

ClassFunctionCall& ClassFunctionCall::operator=(const ClassFunctionCall& other)
{
  if (this != &other)
    *this->d_ptr = *other.d_ptr;

  FunctionCall::operator=(other);

  return *this;
}

RangeInRevision ClassFunctionCall::accessorRange() const
{
  return d_ptr->mAccessorRange;
}

Variable::Ptr ClassFunctionCall::object() const
{
  Variable::Ptr variable(new Variable(d_ptr->mObjectRange));

  const RangeInRevision range = d_ptr->mRange;

  DUContext *context = d_ptr->mContext->findContextAt(CursorInRevision(range.start.line, range.start.column));

  QString objectTypeString = d_ptr->mObjectString;
  if (d_ptr->mImplicitOnThis)
    objectTypeString = QLatin1String("this");

  variable->d_ptr->mContext = context;
  variable->d_ptr->mTypeString = objectTypeString;

  return variable;
}

bool ClassFunctionCall::implicitCtorCall() const
{
  return d_ptr->mImplicitCtorCall;
}

bool ClassFunctionCall::implicitOnThis() const
{
  return d_ptr->mImplicitOnThis;
}

RangeInRevision ClassFunctionCall::rangeForItem(QueryHit::Item item) const
{
  if (item == ObjectId) {
    return object()->range();
  } else if (item == Accessor) {
    return accessorRange();
  } else {
    return FunctionCall::rangeForItem(item);
  }
}

//END: ClassFunctionCall
