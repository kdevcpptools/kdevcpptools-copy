/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#ifndef FUNCTIONCALL_H
#define FUNCTIONCALL_H

#include "queryhit.h"

#include "variable.h"

namespace KDevelop {
class FunctionDeclaration;
class ReferencedTopDUContext;
}

class AST;
class FunctionCallPrivate;

class CPPQUERYENGINE_EXPORT FunctionCall : public QueryHit
{
public:
  typedef QSharedPointer<FunctionCall> Ptr;

  FunctionCall(KDevelop::FunctionDeclaration const *cdecl,
               KDevelop::ReferencedTopDUContext const &context,
               KDevelop::RangeInRevision const &range);

  FunctionCall(FunctionCall const &other);

  virtual ~FunctionCall();

  FunctionCall &operator=(FunctionCall const &other);

  /// @return the range of the function identifier
  KDevelop::RangeInRevision functionIdentifierRange() const;

  /// @return the number of arguments in this function call.
  int argumentCount() const;
  /// @return the argument at the given position.
  Variable::Ptr argument(int argument) const;

  /// @return the number of template arguments in this function call.
  int templateArgumentCount() const;
  /// @return the range for the template argument at the given position.
  KDevelop::RangeInRevision templateArgumentRange(int argument) const;

  virtual KDevelop::RangeInRevision rangeForItem(Item item) const;
  virtual KDevelop::RangeInRevision rangeForOffsetItem(ItemWithOffset , unsigned int offset) const;

protected:
  FunctionCall(FunctionCallPrivate* data);

  FunctionCallPrivate * const d_ptr;
};

#endif // FUNCTIONCALL_H
