/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "queryhit.h"
#include "queryhit_p.h"

QueryHit::QueryHit(QueryHitPrivate* data)
  : d_ptr(data)
{
  Q_ASSERT(data);
}

QueryHit::~QueryHit()
{
  delete d_ptr;
  d_ptr = 0;
}

RangeInRevision QueryHit::range() const
{
  return d_ptr->mRange;
}

IndexedString QueryHit::file() const
{
  return d_ptr->mContext->url();
}

RangeInRevision QueryHit::rangeForItem(QueryHit::Item item) const
{
  if (item == All) {
    return range();
  } else {
    return RangeInRevision::invalid();
  }
}

RangeInRevision QueryHit::rangeForOffsetItem(QueryHit::ItemWithOffset, unsigned int) const
{
  return RangeInRevision::invalid();
}

bool QueryHit::inMacro() const
{
  return d_ptr->mInsideMacro;
}
