/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#ifndef CLASSFUNCTIONCALL_H
#define CLASSFUNCTIONCALL_H

#include "functioncall.h"

namespace KDevelop {
class ClassFunctionDeclaration;
class Declaration;
class ReferencedTopDUContext;
}

class ClassFunctionCallPrivate;

class CPPQUERYENGINE_EXPORT ClassFunctionCall : public FunctionCall
{
public:
  typedef QSharedPointer<ClassFunctionCall> Ptr;

  ClassFunctionCall(KDevelop::ClassFunctionDeclaration const *cdecl,
               KDevelop::ReferencedTopDUContext const &context,
               KDevelop::RangeInRevision const &range);
  ClassFunctionCall(ClassFunctionCall const &other);

  virtual ~ClassFunctionCall();

  ClassFunctionCall &operator=(ClassFunctionCall const &other);

  /// @return the range of the accessor, i.e. either ->, . or ::
  KDevelop::RangeInRevision accessorRange() const;

  /// @return the variable of the calling object, i.e. either someVar or NameSpace::ClassName or foo()->bar()
  Variable::Ptr object() const;

  /// @return whether this call is an implicit ctor call
  bool implicitCtorCall() const;
  /// @return whether this call is an implicit call on the this-pointer.
  bool implicitOnThis() const;

  virtual KDevelop::RangeInRevision rangeForItem(Item item) const;

private:
  ClassFunctionCallPrivate * const d_ptr;
};

#endif // CLASSFUNCTIONCALL_H
