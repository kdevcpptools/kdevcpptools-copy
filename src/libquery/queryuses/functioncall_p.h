/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#ifndef FUNCTIONCALL_P_H
#define FUNCTIONCALL_P_H

#include "queryhit_p.h"

#include "variable.h"

namespace KDevelop {
  class FunctionDeclaration;
}

class FunctionCallPrivate : public QueryHitPrivate
{
public: /// Members
  QList<Variable::Ptr> mArguments;
  RangeInRevision      mFunctionIdRange;
  QList<RangeInRevision> mTemplateArgumentRanges;

public: /// Functions
  FunctionCallPrivate(ReferencedTopDUContext const &context);
  FunctionCallPrivate(FunctionCallPrivate const &other);

  void extract(const RangeInRevision& range, const FunctionDeclaration* cdecl);
  void extractArgumentRanges(AST *node);

  FunctionCallPrivate &operator=(FunctionCallPrivate const &other);
};

#endif // FUNCTIONCALL_P_H
