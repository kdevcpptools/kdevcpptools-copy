#include "functioncall.h"

#include <language/duchain/functiondeclaration.h>

#include "functioncall_p.h"

using namespace KDevelop;

FunctionCall::FunctionCall(FunctionDeclaration const *fdecl,
                           ReferencedTopDUContext const &context,
                           RangeInRevision const &range)
  : QueryHit(new FunctionCallPrivate(context)), d_ptr(static_cast<FunctionCallPrivate*>(QueryHit::d_ptr))
{
  Q_ASSERT(d_ptr == QueryHit::d_ptr);
  d_ptr->extract(range, fdecl);
}

FunctionCall::FunctionCall(FunctionCallPrivate* data)
  : QueryHit(data), d_ptr(data)
{
  Q_ASSERT(d_ptr == data);
  Q_ASSERT(d_ptr == QueryHit::d_ptr);
}

FunctionCall::FunctionCall(FunctionCall const &other)
  : QueryHit(new FunctionCallPrivate(*other.d_ptr)), d_ptr(static_cast<FunctionCallPrivate*>(QueryHit::d_ptr))
{
  Q_ASSERT(d_ptr == QueryHit::d_ptr);
}

FunctionCall::~FunctionCall()
{
  // function_call_d_ptr gets deleted in QueryUse
}

FunctionCall &FunctionCall::operator=(FunctionCall const &other)
{
  if (this != &other)
    *this->d_ptr = *other.d_ptr;

  QueryHit::operator=(other);

  return *this;
}

int FunctionCall::argumentCount() const
{
  return d_ptr->mArguments.size();
}

Variable::Ptr FunctionCall::argument(int argument) const
{
  Q_ASSERT(argument >= 0 && argument < d_ptr->mArguments.size());

  return d_ptr->mArguments.at(argument);
}

RangeInRevision FunctionCall::functionIdentifierRange() const
{
  return d_ptr->mFunctionIdRange;
}

int FunctionCall::templateArgumentCount() const
{
  return d_ptr->mTemplateArgumentRanges.size();
}

RangeInRevision FunctionCall::templateArgumentRange(int argument) const
{
  if (argument < d_ptr->mTemplateArgumentRanges.size())
    return d_ptr->mTemplateArgumentRanges.at(argument);

  return RangeInRevision::invalid();
}

KDevelop::RangeInRevision FunctionCall::rangeForItem(QueryHit::Item item) const
{
  if (item == Identifier) {
    return functionIdentifierRange();
  } else {
    return QueryHit::rangeForItem(item);
  }
}

KDevelop::RangeInRevision FunctionCall::rangeForOffsetItem(QueryHit::ItemWithOffset item, unsigned int offset) const
{
  switch (item) {
    case Arg:
      return argument(offset)->range();
    case TemplateArg:
      return templateArgumentRange(offset);
  }

  return RangeInRevision::invalid();
}
