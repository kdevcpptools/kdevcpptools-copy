/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef VARIABLE_H
#define VARIABLE_H

#include <QtCore/QSharedPointer>

#include <language/duchain/declaration.h>
#include <language/duchain/types/abstracttype.h>

#include "cppqueryengine_export.h"

namespace KDevelop {
  class DUContext;
  class RangeInRevision;
  class TopDUContext;
}

class VariablePrivate;

class CPPQUERYENGINE_EXPORT Variable {
public:
  typedef QSharedPointer<Variable> Ptr;
  typedef QList<Variable::Ptr> List;

  Variable(const KDevelop::RangeInRevision &range);
  ~Variable();

  /// @return the type of the function argument
  KDevelop::AbstractType::Ptr type() const;

  /// @return the declaration of the function argument or a null pointer if none exists
  KDevelop::Declaration* declaration() const;

  /// @return the range of the function argument
  KDevelop::RangeInRevision range() const;

  /// @return the literal content of the function argument
  QString content() const;

private:
  friend class ClassFunctionCall;
  friend class FunctionCallPrivate;
  friend class FunctionCallVisitor;
  friend class FunctionCallVisitorPrivate;

  VariablePrivate * const d_ptr;
  Q_DECLARE_PRIVATE(Variable)
};

#endif // VARIABLE_H
