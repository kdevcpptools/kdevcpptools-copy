/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef QUERYUSE_H
#define QUERYUSE_H

#include <QtCore/QSharedPointer>

#include "cppqueryengine_export.h"

namespace KDevelop {
  class IndexedString;
  class RangeInRevision;
}

class QueryHitPrivate;

/**
 * Not to be used directly, only for inheritance.
 */
class CPPQUERYENGINE_EXPORT QueryHit {
public:
  typedef QSharedPointer<QueryHit> Ptr;

  virtual ~QueryHit();

  /// @return The file in which this hit occured.
  KDevelop::IndexedString file() const;

  /// @return The whole range for this hit.
  KDevelop::RangeInRevision range() const;

  enum Item {
    All,
    Identifier,
    //BEGIN ClassFunction
    ObjectId,
    Accessor
  };
  KDevelop::RangeInRevision virtual rangeForItem(Item item) const;

  enum ItemWithOffset {
    Arg,
    TemplateArg
  };
  KDevelop::RangeInRevision virtual rangeForOffsetItem(QueryHit::ItemWithOffset, unsigned int) const;

  /// @return True when this hit is inside a macro expansion, false otherwise.
  bool inMacro() const;

protected:
  explicit QueryHit(QueryHitPrivate * data);

  QueryHitPrivate* d_ptr;
};

#endif // QUERYUSE_H
