/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "queryhit_p.h"

QueryHitPrivate::QueryHitPrivate(ReferencedTopDUContext const &context)
  : mContext(context)
  , mParseSession(dynamic_cast<ParseSession *>(mContext->ast().data()))
  , mRange(RangeInRevision::invalid()), mInsideMacro(false)
{ }

QueryHitPrivate::QueryHitPrivate(const QueryHitPrivate& other)
  :mContext(other.mContext), mParseSession(other.mParseSession), mRange(other.mRange), mInsideMacro(other.mInsideMacro)
{ }

QueryHitPrivate& QueryHitPrivate::operator=(const QueryHitPrivate& other)
{
  if (this != &other) {
    mRange = other.mRange;
    mContext = other.mContext;
    mParseSession = other.mParseSession;
    mInsideMacro = other.mInsideMacro;
  }
  return *this;
}

QList<RangeInRevision> QueryHitPrivate::extractTemplateArgumentRanges(RangeInRevision const &range) const
{
  QList<RangeInRevision> result;

  AST *node = nodeForRange(range);
  if (!node) {
    qDebug() << Q_FUNC_INFO << "AST node not mapped";
    return result;
  }

  if (node->kind == AST::Kind_UnqualifiedName) {
    CppEditorIntegrator editor(mParseSession);

    UnqualifiedNameAST *name = static_cast<UnqualifiedNameAST*>(node);
    if (!name->template_arguments) {
      ///TODO: what should we do here, e.g.: someTplFunc(5, 3) == someTplFunc<int, int>(5, 3);
      return result;
    }

    ListNode<TemplateArgumentAST*> const *it = name->template_arguments->toFront();
    ListNode<TemplateArgumentAST*> const *end = it;
    do {
      TemplateArgumentAST *templateArgument = it->element;
      result << editor.findRange(templateArgument);
      it = it->next;
    } while (it != end);

  } else {
    qDebug() << Q_FUNC_INFO << "Unsupported node kind:" << node->kind;
  }

  return result;
}

AST *QueryHitPrivate::nodeForRange(RangeInRevision const &range) const
{
  if (!range.isValid()) {
    qDebug() << "[QueryHitPrivate::nodeForRange] Invalid range";
    return 0;
  }

  DUContext* ductx = mContext->findContextAt(CursorInRevision(range.start.line, range.start.column));
  if (!ductx) {
    qDebug() << "[QueryHitPrivate::nodeForRange] Context not found.";
    return 0;
  }

  DUContextPointer ptr(ductx);
  return mParseSession->astNodeFromUse(qMakePair<DUContextPointer, RangeInRevision>(ptr, range));
}

RangeInRevision QueryHitPrivate::rangeForNode(AST *start, AST *end) const
{
  Q_ASSERT(start);

  CppEditorIntegrator editor(mParseSession);
  if (end) {
    return editor.findRange(start, end);
  } else {
    return editor.findRange(start);
  }
}
