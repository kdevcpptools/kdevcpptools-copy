/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef VARIABLE_P_H
#define VARIABLE_P_H

#include <language/duchain/declarationid.h>
#include <language/duchain/types/abstracttype.h>
#include <language/editor/rangeinrevision.h>

#include <languages/cpp/cppduchain/expressionevaluationresult.h>
#include <languages/cpp/cppduchain/expressionparser.h>
#include <languages/cpp/parser/ast.h>

#include <QtCore/QString>

class VariablePrivate
{
public:
  VariablePrivate();

  void resolveTypeDeclaration();

  AST* mNode;
  QString mTypeString;
  KDevelop::DUContext *mContext;
  KDevelop::AbstractType::Ptr mType;
  KDevelop::Declaration *mDeclaration;
  KDevelop::RangeInRevision mRange;
};

#endif // VARIABLE_P_H
