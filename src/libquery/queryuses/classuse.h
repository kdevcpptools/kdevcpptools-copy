#ifndef CLASSUSE_H
#define CLASSUSE_H

#include "queryhit.h"

namespace KDevelop {
class ClassDeclaration;
class RangeInRevision;
class ReferencedTopDUContext;
}

class ClassUsePrivate;

/// Holds info with respect to the use of a class or struct
class CPPQUERYENGINE_EXPORT ClassUse : public QueryHit
{
public:
  typedef QSharedPointer<ClassUse> Ptr;

  /// Extracts the needed info for given class decl at the range as reported
  /// by DUCHain.
  ClassUse(KDevelop::ClassDeclaration const *cdecl,
           KDevelop::ReferencedTopDUContext const &context,
           KDevelop::RangeInRevision range);

  ClassUse(ClassUse const &other);

  ~ClassUse();

  ClassUse &operator=(ClassUse const &other);

  /// Returns the range of the Identifier
  KDevelop::RangeInRevision identifierRange() const;

  /// Returns the total number of template arguments
  int templateArgumentCount() const;

  /// Returns the range of the template argument at offset
  KDevelop::RangeInRevision templateArgumentRange(int offset) const;

  virtual KDevelop::RangeInRevision rangeForItem(Item item) const;
  virtual KDevelop::RangeInRevision rangeForOffsetItem(ItemWithOffset item, unsigned int offset) const;

private:
  ClassUsePrivate * const class_use_d_ptr;
};

#endif // CLASSUSE_H
