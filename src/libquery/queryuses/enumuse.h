/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ENUMUSE_H
#define ENUMUSE_H

#include <queryuses/queryhit.h>

namespace KDevelop {
class Declaration;
class ReferencedTopDUContext;
}

class CPPQUERYENGINE_EXPORT EnumUse : public QueryHit
{
public:
  typedef QSharedPointer<EnumUse> Ptr;

  /// Extracts the needed info for given enum decl at the range as reported
  /// by DUCHain.
  EnumUse(KDevelop::Declaration const *decl,
           KDevelop::ReferencedTopDUContext const &context,
           KDevelop::RangeInRevision range);

  EnumUse(EnumUse const &other);

  ~EnumUse();

  EnumUse &operator=(EnumUse const &other);
};

#endif // ENUMUSE_H
