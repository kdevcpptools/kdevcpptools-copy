#include "classuse.h"

#include <QtCore/QList>

#include <language/duchain/classdeclaration.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>
#include <languages/cpp/parser/ast.h>

#include "queryhit_p.h"

using namespace Cpp;
using namespace KDevelop;

class ClassUsePrivate : public QueryHitPrivate
{
public:
  RangeInRevision        mIdentifierRange;
  QList<RangeInRevision> mTemplateArgumentRanges;

public:
  ClassUsePrivate(ReferencedTopDUContext const &context);
  ClassUsePrivate(ClassUsePrivate const &other);
  ClassUsePrivate &operator=(ClassUsePrivate const &other);
  void extract(RangeInRevision const &range, ClassDeclaration const *cdecl);
};

ClassUsePrivate::ClassUsePrivate(ReferencedTopDUContext const &context)
  : QueryHitPrivate(context)
{ }

ClassUsePrivate::ClassUsePrivate(ClassUsePrivate const &other)
  : QueryHitPrivate(other)
{
  mIdentifierRange = other.mIdentifierRange;
  mTemplateArgumentRanges = other.mTemplateArgumentRanges;
}

ClassUsePrivate &ClassUsePrivate::operator=(ClassUsePrivate const &other)
{
  if (this != &other) {
    mIdentifierRange = other.mIdentifierRange;
    mTemplateArgumentRanges = other.mTemplateArgumentRanges;
    QueryHitPrivate::operator=(other);
  }
  return *this;
}

void ClassUsePrivate::extract(RangeInRevision const &range, ClassDeclaration const *cdecl)
{
  mIdentifierRange = range;

  AST *node = nodeForRange(range);
  if (node) {
    CppEditorIntegrator editor(mParseSession);
    mRange = editor.findRange(node);
  } else
    qDebug() << "[ClassUsePrivate::extract(ClassDeclaration)] AST node not mapped";

  if (isTemplateDeclaration(cdecl))
    mTemplateArgumentRanges = extractTemplateArgumentRanges(range);
}

ClassUse::ClassUse(ClassDeclaration const *cdecl,
                   ReferencedTopDUContext const &context,
                   RangeInRevision range)
  : QueryHit(new ClassUsePrivate(context)),
    class_use_d_ptr(static_cast<ClassUsePrivate*>(d_ptr))
{
  class_use_d_ptr->extract(range, cdecl);
}

ClassUse::ClassUse(ClassUse const &other)
  : QueryHit(new ClassUsePrivate(*other.class_use_d_ptr)),
    class_use_d_ptr(static_cast<ClassUsePrivate*>(d_ptr))
{ }

ClassUse::~ClassUse()
{
  // NOTE: do don't delete the pointer, this is done in QueryHit
  // delete class_use_d_ptr;
}

ClassUse &ClassUse::operator=(ClassUse const &other)
{
  if (&other != this) {
    *this->class_use_d_ptr = *(other.class_use_d_ptr);
  }

  QueryHit::operator=(other);

  return *this;
}

RangeInRevision ClassUse::identifierRange() const
{
  return class_use_d_ptr->mIdentifierRange;
}

int ClassUse::templateArgumentCount() const
{
  return class_use_d_ptr->mTemplateArgumentRanges.count();
}

RangeInRevision ClassUse::templateArgumentRange(int offset) const
{
  return class_use_d_ptr->mTemplateArgumentRanges[offset];
}

KDevelop::RangeInRevision ClassUse::rangeForItem(QueryHit::Item item) const
{
  if (item == Identifier) {
    return identifierRange();
  } else {
    return QueryHit::rangeForItem(item);
  }
}

KDevelop::RangeInRevision ClassUse::rangeForOffsetItem(QueryHit::ItemWithOffset item, unsigned int offset) const
{
  if (item == TemplateArg) {
    return templateArgumentRange(offset);
  } else {
    return QueryHit::rangeForOffsetItem(item, offset);
  }
}
