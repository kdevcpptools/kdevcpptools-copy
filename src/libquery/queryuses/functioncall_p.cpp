/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "functioncall_p.h"

#include <language/duchain/functiondeclaration.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>

#include "functioncallvisitor.h"
#include "util.h"
#include "variable_p.h"

FunctionCallPrivate::FunctionCallPrivate(ReferencedTopDUContext const &context)
  : QueryHitPrivate(context), mFunctionIdRange(RangeInRevision::invalid())
{ }

FunctionCallPrivate::FunctionCallPrivate(FunctionCallPrivate const &other)
  : QueryHitPrivate(other)
  , mArguments(other.mArguments)
  , mFunctionIdRange(other.mFunctionIdRange)
  , mTemplateArgumentRanges(other.mTemplateArgumentRanges)
{ }

FunctionCallPrivate &FunctionCallPrivate::operator=(FunctionCallPrivate const &other)
{
  if (this != &other) {
    mArguments = other.mArguments;
    mFunctionIdRange = other.mFunctionIdRange;
    mTemplateArgumentRanges = other.mTemplateArgumentRanges;
    QueryHitPrivate::operator=(other);
  }

  return *this;
}

void FunctionCallPrivate::extract(RangeInRevision const &range,
                                  FunctionDeclaration const *cdecl)
{
  // function name
  mFunctionIdRange = range;
  mInsideMacro = range.isEmpty();
  AST *node = nodeForRange(range); // UnqualifiedNameAst
  // there are cases where nodeForRange cannot give us the proper node back when inside a macro expansion...
  Q_ASSERT(node || mInsideMacro);
  if (!node) {
    mRange = range;
    return;
  }
  Q_ASSERT(node->kind == AST::Kind_UnqualifiedName);

  // the node of the function call
  FunctionCallAST* call = findFunctionCall(static_cast<UnqualifiedNameAST*>(node), mParseSession);
  Q_ASSERT(call);

  // range for the full call including name and arguments
  mRange = rangeForNode(node, call);

  extractArgumentRanges(call);

  if (Cpp::isTemplateDeclaration(cdecl))
    mTemplateArgumentRanges = extractTemplateArgumentRanges(range);
}

void FunctionCallPrivate::extractArgumentRanges(AST *node)
{
  FunctionCallVisitor visitor(mParseSession);
  mArguments = visitor.arguments(node);

  DUContext *context = mContext->findContextAt(CursorInRevision(mRange.start.line, mRange.start.column));
  foreach (const Variable::Ptr &var, mArguments)
    var->d_ptr->mContext = context;
}
