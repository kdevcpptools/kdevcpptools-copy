/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#ifndef QUERYHIT_P_H
#define QUERYHIT_P_H

#include <language/editor/simplerange.h>
#include <language/duchain/topducontext.h>
#include <languages/cpp/cppduchain/cppeditorintegrator.h>
#include <languages/cpp/parser/parsesession.h>
#include <languages/cpp/parser/ast.h>

#include "templatehelper.h"

using namespace KDevelop;

class QueryHitPrivate
{
public:
  ReferencedTopDUContext mContext;
  ParseSession          *mParseSession; /// Required for several of the extract functions.
  RangeInRevision        mRange; ///< Range for the whole use of this query.
  bool                   mInsideMacro; ///< True when the use is inside a macro expansion, false otherwise.

public:
  QueryHitPrivate(ReferencedTopDUContext const &context);
  QueryHitPrivate(QueryHitPrivate const &other);

  QList<RangeInRevision> extractTemplateArgumentRanges(RangeInRevision const &range) const;

  AST *nodeForRange(RangeInRevision const &range) const;

  /// Extract range for a given node @p start. If @p end is given, return the range from @p start to @p end.
  RangeInRevision rangeForNode(AST *start, AST *end = 0) const;

  QueryHitPrivate &operator=(QueryHitPrivate const &other);
};

#endif // QUERYHIT_P_H
