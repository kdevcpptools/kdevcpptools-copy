/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "variable.h"

#include "variable_p.h"

#include <language/duchain/ducontext.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>

VariablePrivate::VariablePrivate()
  : mDeclaration( 0 )
{
}

void VariablePrivate::resolveTypeDeclaration()
{
  KDevelop::DUContextPointer contextPtr(mContext);
  KDevelop::TopDUContext *topContext = contextPtr->topContext();

  Cpp::ExpressionParser parser(true);
  Cpp::ExpressionEvaluationResult result = parser.evaluateType(mTypeString.toUtf8(), contextPtr, topContext, true);

  if (!result.isValid()) {
    qDebug() << "[Variable::resolve] Invalid type expression:" << mTypeString << "at" << topContext->url().str();
    mType = KDevelop::AbstractType::Ptr();
    mDeclaration = 0;
  } else {
    const KDevelop::DeclarationId declarationId = result.instanceDeclaration;
    mDeclaration = declarationId.getDeclaration(topContext);
    if (mDeclaration) // that's the case if function was called like: func( var1, var2 )
      mType = mDeclaration->abstractType();
    else // that's the case if function was called like: func( true, "hello" )
      mType = result.type.abstractType();

    // try to resolve a delayed type
    if ( mType->whichType() == KDevelop::AbstractType::TypeDelayed )
      mType = Cpp::resolveDelayedTypes( mType, mContext, topContext );
  }
}

Variable::Variable(const KDevelop::RangeInRevision &range)
  : d_ptr(new VariablePrivate)
{
  d_ptr->mRange = range;
}

Variable::~Variable()
{
  delete d_ptr;
}

KDevelop::AbstractType::Ptr Variable::type() const
{
  if ( !d_ptr->mType )
    d_ptr->resolveTypeDeclaration();

  return d_ptr->mType;
}

KDevelop::Declaration* Variable::declaration() const
{
  if ( !d_ptr->mDeclaration )
    d_ptr->resolveTypeDeclaration();

  return d_ptr->mDeclaration;
}

KDevelop::RangeInRevision Variable::range() const
{
  return d_ptr->mRange;
}

QString Variable::content() const
{
  return d_ptr->mTypeString;
}
