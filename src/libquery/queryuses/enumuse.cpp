/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "enumuse.h"

#include "queryhit_p.h"

#include <language/duchain/declaration.h>
#include <language/duchain/types/enumerationtype.h>
#include <language/duchain/types/enumeratortype.h>

EnumUse::EnumUse(const KDevelop::Declaration* decl, const KDevelop::ReferencedTopDUContext& context, RangeInRevision range)
  : QueryHit(new QueryHitPrivate(context))
{
  Q_ASSERT(range.isValid());
  Q_ASSERT(decl->abstractType().cast<EnumerationType>() || decl->abstractType().cast<EnumeratorType>());

  d_ptr->mRange = RangeInRevision::invalid();

  AST *node = d_ptr->nodeForRange( range );
  if ( node ) {
    AST *parent = d_ptr->mParseSession->parentAstNode( node );
    if ( parent && parent->kind == AST::Kind_Name )
      range = d_ptr->rangeForNode( parent );
  }

  if ( !d_ptr->mRange.isValid() )
    d_ptr->mRange = range;

  d_ptr->mInsideMacro = range.isEmpty();
}

EnumUse::EnumUse(EnumUse const &other)
  : QueryHit(new QueryHitPrivate(*other.d_ptr))
{
}

EnumUse::~EnumUse()
{
}

EnumUse &EnumUse::operator=(EnumUse const &other)
{
  QueryHit::operator=(other);

  return *this;
}
