/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CPPQUERYENGINE_H
#define CPPQUERYENGINE_H

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QPair>

#include "queries/classquery.h"
#include "queries/functionquery.h"
#include "queries/classfunctionquery.h"
#include "queries/enumquery.h"


namespace KDevelop {
class ReferencedTopDUContext;
}

class QueryResult;
class QueriesResult;

typedef QMap<uint, QString> Restrictions;

class CPPQUERYENGINE_EXPORT CppQueryEngine
{
  public:
    CppQueryEngine();
    ~CppQueryEngine();

    /// Return the declaration of the specified type.
    QueryResult query( KDevelop::ReferencedTopDUContext const &context, Query::Ptr const &query );

    /// Convenience function, performs all queries and returns the result.
    QueriesResult query( const KDevelop::ReferencedTopDUContext &context,
                         const QList<Query::Ptr> query );

  private:
    class CppQueryEnginePrivate * const d;
};

#endif // CPPQUERYENGINE_H
