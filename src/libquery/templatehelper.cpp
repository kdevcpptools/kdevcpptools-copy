/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "templatehelper.h"

#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/declaration.h>

#include <languages/cpp/cppduchain/cppeditorintegrator.h>
#include <languages/cpp/cppduchain/cpptypes.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>
#include <languages/cpp/parser/ast.h>
#include <languages/cpp/parser/parsesession.h>

using namespace Cpp;
using namespace KDevelop;

/// TemplateArgumentInfoExtractorPrivate

class TemplateHelperPrivate
{
  public:
    ParseSession   *mSession;

    TemplateHelperPrivate(ParseSession *session);
};

TemplateHelperPrivate::TemplateHelperPrivate(ParseSession *session)
  : mSession(session)
{ }

/// TemplateArgumentInfoExtractor

TemplateHelper::TemplateHelper(ParseSession *session)
  : d(new TemplateHelperPrivate(session))
{ }

TemplateHelper::~TemplateHelper()
{
  delete d;
}

/// TemplateArgumentInfoExtractor - static methods

QString TemplateHelper::templateArgumentIdentifier(TemplateDeclaration *templateArgsDecl, int argOffset)
{
  Q_ASSERT(templateArgsDecl);

  DUContext *templateArgContext = templateArgsDecl->templateParameterContext();
  Q_ASSERT(templateArgContext);
  QVector<Declaration *> templateArgDeclarations = templateArgContext->localDeclarations();
  Q_ASSERT(templateArgDeclarations.size() > argOffset);

  Declaration* d = templateArgDeclarations.at(argOffset);
  Q_ASSERT(d);

  CppTemplateParameterType::Ptr t = d->abstractType().cast<CppTemplateParameterType>();
  Q_ASSERT(t);
  return t->qualifiedIdentifier().last().toString();
}
