#include "query.h"

#include <QtCore/QString>

class QueryPrivate
{
public:
  QueryPrivate(QString const id) : mId(id)
  { }

  QString mId;
};

Query::Query(QString const &queryId)
  : d_ptr(new QueryPrivate(queryId))
{ }

Query::~Query()
{
  delete d_ptr;
}

QString Query::id() const
{
  return d_func()->mId;
}
