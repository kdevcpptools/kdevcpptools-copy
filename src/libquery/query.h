#ifndef QUERY_H
#define QUERY_H

#include <QtCore/QMetaType>
#include <QtCore/QtGlobal>
#include <QtCore/QSharedPointer>

#include "cppqueryengine_export.h"

class QString;
class QueryPrivate;

class CPPQUERYENGINE_EXPORT Query
{
public:
  typedef QSharedPointer<Query> Ptr;

public:
  Query(QString const &queryId);
  virtual ~Query();

  QString id() const;

private:
  QueryPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(Query)
};

Q_DECLARE_METATYPE(Query::Ptr)

#endif // QUERY_H
