/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "cppqueryengine.h"

#include <language/codegen/coderepresentation.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/declaration.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/ducontext.h>
#include <language/duchain/functiondeclaration.h>
#include <language/duchain/topducontext.h>
#include <language/duchain/duchainutils.h>
#include <language/interfaces/iastcontainer.h>
#include <language/duchain/declarationid.h>

#include <languages/cpp/cppduchain/expressionevaluationresult.h>
#include <languages/cpp/cppduchain/expressionparser.h>
#include <languages/cpp/cppduchain/templatedeclaration.h>
#include <languages/cpp/parser/parsesession.h>
#include <languages/cpp/parser/ast.h>

#include "functioncallvisitor.h"
#include "templatehelper.h"
#include "util.h"
#include "queryresult.h"
#include "queriesresult.h"
#include "queryuses/classuse.h"
#include "queryuses/functioncall.h"
#include "queryuses/classfunctioncall.h"
#include "queryuses/enumuse.h"

#define ifDebug(x)

using namespace Cpp;
using namespace KDevelop;

/// Helper function to access all declarations in @p context for a given @p identifier.
QList<Declaration*> findDeclarations(ReferencedTopDUContext context, QualifiedIdentifier const &identifier)
{
  if ( identifier == QualifiedIdentifier("*") ) {
    QList<Declaration*> decls;
    typedef QPair<Declaration*, int> DeclarationPair;
    QList<DeclarationPair> allDeclarations = context->allDeclarations(CursorInRevision::invalid(), context->topContext());
    foreach (const DeclarationPair& pair, allDeclarations) {
      decls << pair.first;
    }
    return decls;
  } else {
    return context->findDeclarations(identifier, CursorInRevision::invalid(), AbstractType::Ptr(), 0, TopDUContext::DirectQualifiedLookup);
  }
}

/// CppQueryEnginePrivate

typedef QList<RangeInRevision> RangeInRevisionList;
typedef QList<FunctionDeclaration*> FunctionDeclarationList;
typedef QMap<FunctionDeclaration*, QList<RangeInRevision> > FunctionDeclarationUsesMap;
typedef QList<ClassFunctionDeclaration*> ClassFunctionDeclarationList;
typedef QMap<ClassFunctionDeclaration*, QList<RangeInRevision> > ClassFunctionDeclarationUsesMap;
typedef QList<Declaration*> EnumDeclarationList;
typedef QMap<Declaration*, QList<RangeInRevision> > EnumDeclarationUsesMap;

class CppQueryEnginePrivate
{
  public: /// Members
    ReferencedTopDUContext mContext;

  public: /// Functions. It is asusumed that the DUCHain is locked.
    CppQueryEnginePrivate();

    QPair<ParseSession, AST*> sessionAndNodeFor(RangeInRevision const &range) const;

    //BEGIN ClassQuery handling
    QueryResult execQuery(ClassQuery::Ptr const &query, ReferencedTopDUContext const &context);

    QList<ClassDeclaration *> findClassDeclaration(ClassQuery::Ptr const &query) const;

    /// Finds all classes inheritting directly or inderectly from @param classDecl
    /// upto @param maxDepth.
    QList<Declaration*> findSubClasses(ClassDeclaration *classDecl, unsigned maxDepth) const;

    QList<RangeInRevision> findTemplateClassDeclarationUses(TemplateDeclaration const *classDecl,
                                                        QStringList const &restirctions) const;
    //END ClassQuery handling

    //BEGIN ClassFunctionQuery handling

    QueryResult execQuery(ClassFunctionQuery::Ptr const &query, ReferencedTopDUContext const &context);

    /// Returns list of class method declarations matching the query
    /// and the declarations of all matching instantatiations in case of a
    /// template function.
    ClassFunctionDeclarationList findClassFunctionDeclarations(ClassFunctionQuery::Ptr const &query) const;

    ClassFunctionDeclarationUsesMap findClassFunctionDeclarationUses(ClassFunctionDeclarationList const &decls,
                                                           ClassFunctionQuery::Ptr const &query) const;

    //END ClassFunctionQuery handling

    //BEGIN FunctionQuery handling

    QueryResult execQuery(FunctionQuery::Ptr const &query, ReferencedTopDUContext const &context);

    FunctionDeclarationUsesMap findFunctionDeclarationUses(FunctionDeclarationList const &decls,
                                                           FunctionQuery::Ptr const &query) const;

    /// Returns the declarations of given the global function matching the query
    /// and the declarations of all matching instantatiations in case of a
    /// template function.
    FunctionDeclarationList findFunctionDeclarations(FunctionQuery::Ptr const &query) const;


    /// Looks up the FunctionCallAST node in the AST belonging to @param use and
    /// then checks if the functionall matches all of the set restrictions in
    ///  @param query.
    bool useMatchesRestrictions(AbstractFunctionDeclaration* funDec, RangeInRevision const &use, FunctionQuery::Ptr const &query) const;
    //END FunctionQuery handling

    //BEGIN EnumQuery handling

    QueryResult execQuery(EnumQuery::Ptr const &query, ReferencedTopDUContext const &context);

    /// Returns the enum declarations matching the query
    EnumDeclarationList findEnumDeclarations(EnumQuery::Ptr const &query) const;

    /// Returns all uses of the given enum declaration that match the query.
    EnumDeclarationUsesMap findEnumDeclarationUses(EnumDeclarationList const &decls,
                                                    EnumQuery::Ptr const &query) const;

    //END EnumQuery handling

    QList<Declaration*> findInstantiations(TemplateDeclaration *decl, QStringList const &restrictions, DUContextPointer context) const;

    Declaration *findTypeDeclaration(QualifiedIdentifier const &qid) const;

    bool matchesArgumentTypes(Declaration *memberDecl,
                              QStringList const &argumentTypes) const;

    IndexedType resolveType(QString const &typeStr,
                            DUContextPointer const &ctx,
                            Declaration* decl) const;

    QList<RangeInRevision> usesForCurrentContext(Declaration *decl) const;
};

CppQueryEnginePrivate::CppQueryEnginePrivate() : mContext(0)
{ }

//BEGIN ClassQuery handling

QueryResult CppQueryEnginePrivate::execQuery(ClassQuery::Ptr const &query,
                                             ReferencedTopDUContext const &context)
{
  ifDebug(qDebug() << "executing class query" << query->id();)

  Q_ASSERT(context);
  mContext = context;

  DUChainReadLocker lock(DUChain::lock());

  QueryResult result(query->id(), context->url());
  result.setStatus(QueryResult::NoMatch);

  const bool hasTemplateRestrictions = !query->templateRestrictions().isEmpty();
  ifDebug(qDebug() << "has template restrictions:" << hasTemplateRestrictions);

  QList<ClassDeclaration *> declarations = findClassDeclaration(query);
  ifDebug(qDebug() << "found" << declarations.size() << "unfiltered class declarations";)

  if (declarations.isEmpty())
    return result;

  foreach (ClassDeclaration *classDecl, declarations) {
    Q_ASSERT(classDecl);
    ifDebug(qDebug() << "checking class declaration" << classDecl->toString();)

    QList<RangeInRevision> uses;

    if (isTemplateDeclaration(classDecl) && hasTemplateRestrictions) {
      // A template class found and restrictions set.
      uses = findTemplateClassDeclarationUses(dynamic_cast<TemplateDeclaration*>(classDecl),
            query->templateRestrictions());
    } else if (hasTemplateRestrictions) {
      // Template restrictions but not a template class, no match.
      continue;
    } else {
      // Not a template class and no template restrictions.
      uses = usesForCurrentContext(classDecl);
    }

    ifDebug(qDebug() << "found" << uses.size() << "uses for this declaration";)
    foreach (RangeInRevision const &useRange, uses)
      result.insert(ClassUse::Ptr(new ClassUse(classDecl, mContext, useRange)));
  }

  if (!result.hits().isEmpty())
    result.setStatus(QueryResult::Match);

  return result;
}

QList<ClassDeclaration *> CppQueryEnginePrivate::findClassDeclaration(ClassQuery::Ptr const &query) const
{
  Q_ASSERT(DUChain::lock()->currentThreadHasReadLock());
  QList<Declaration*> declarations = findDeclarations(mContext, query->qualifiedIdentifier());
  QList<ClassDeclaration *> matches;

  ClassDeclaration* parentClassFilter = 0;
  if (!query->subclassOf().isEmpty()) {
    QList<Declaration*> parentClasses = findDeclarations(mContext, query->subclassOf());
    ifDebug(qDebug() << "found" << parentClasses.size() << "declarations for parent class" << query->subclassOf().toString();)
    foreach(Declaration* dec, parentClasses) {
      if (ClassDeclaration* cdec = dynamic_cast<ClassDeclaration*>(dec)) {
        parentClassFilter = cdec;
        break;
      }
    }
    if (!parentClassFilter) {
      // parent not found, we can return early
      return matches;
    }
  }

  foreach ( Declaration* decl, declarations ) {
    if (ClassDeclaration *classDecl = dynamic_cast<ClassDeclaration*>(decl)) {
      if (parentClassFilter && !classDecl->isPublicBaseClass(parentClassFilter, mContext)) {
        // does not inherit from requested class
        continue;
      }

      bool const declIsTemplate = isTemplateDeclaration(decl);

      if (query->templateRestrictions().isEmpty() && !declIsTemplate) {
        matches << classDecl;
      } else if (!query->templateRestrictions().isEmpty() && declIsTemplate) {
        TemplateDeclaration *templateClassDecl = dynamic_cast<TemplateDeclaration*>(decl);
        int const cnt = templateClassDecl->templateParameterContext()->localDeclarations().size();
        int const expectedCnt = query->templateRestrictions().size();
        if (cnt != expectedCnt)
          continue;

        // Todo: Check the types of the template parameters.
        matches << classDecl;
      } // Either a template declaration with non matching
        // Or a non template declaration while looking for one
    } // Not a class or struct
  }

  // Either no match or ambiguos here.
  //  if (decls.isEmpty())
  //    result.status = UseQueryResult::NoMatch;
  //  else  {// decls.size() > 1
  //    result.error = UseQueryResult::AmbiguosTypeIdentifier;
  //    result.status = UseQueryResult::Invalid;
  //  }
  return matches;
}

QList<Declaration*> CppQueryEnginePrivate::findSubClasses(ClassDeclaration *classDecl, unsigned maxDepth) const
{
  unsigned depth = 2; // NOTE: It's not completely clear to me what this servers for.
  QList<Declaration *> subclasses = DUChainUtils::getInheriters(classDecl, depth, false);
  QList<Declaration *> result = subclasses;

  if (maxDepth > 0) {
    foreach (Declaration *subclass, subclasses) {
      depth = maxDepth - 1;
      ClassDeclaration *cdecl = dynamic_cast<ClassDeclaration *>(subclass);
      Q_ASSERT(cdecl);
      result << findSubClasses(cdecl, depth);
    }
  }

  return result;
}

QList<RangeInRevision> CppQueryEnginePrivate::findTemplateClassDeclarationUses(TemplateDeclaration const *templateDecl,
                                                                           QStringList const &restrictions) const
{
  Q_ASSERT(templateDecl);
  Q_ASSERT(dynamic_cast<ClassDeclaration const *>(templateDecl));
  Q_ASSERT(templateDecl->templateParameterContext()->localDeclarations().size() == restrictions.size());

  ifDebug(qDebug() << "find uses for template class dec with restrictions" << restrictions;)
  QList<RangeInRevision> result;

  TemplateDeclaration::InstantiationsHash instantiations = templateDecl->instantiations();
  foreach(TemplateDeclaration* instantationDecl, instantiations.values()) {
    if ( !instantationDecl->instantiatedWith().isValid() ) {
      continue;
    }
    ClassDeclaration* cdecl = dynamic_cast<ClassDeclaration*>(instantationDecl->id(true).getDeclaration(mContext));
    Q_ASSERT(cdecl);

    InstantiationInformation info = instantationDecl->instantiatedWith().information();

    ifDebug(qDebug() << "instantiation" << cdecl->toString() << "has" << info.templateParametersSize() << "template parameters";)
    Q_ASSERT(info.templateParametersSize() == static_cast<quint64>(restrictions.size()));

    ExpressionParser parser;
    ExpressionEvaluationResult evalResult;
    bool allRestrictionsMatch = true; // For now...

    for (int i = 0; i < restrictions.size(); ++i) {
      QString restriction = restrictions.at(i);
      if (restriction.isEmpty())
        continue;

      evalResult = parser.evaluateType(restriction.toUtf8(),
                                       DUContextPointer(cdecl->context()),
                                       mContext);
      Q_ASSERT(evalResult.isValid());

      IndexedType type = info.templateParameters()[i];
      if (type != evalResult.type) {
        allRestrictionsMatch = false;
        break;
      }
    }

    if (allRestrictionsMatch)
      result << usesForCurrentContext(cdecl);
  }

  return result;
}
//END ClassQuery handling

//BEGIN ClassFunctionQuery handling

QueryResult CppQueryEnginePrivate::execQuery(ClassFunctionQuery::Ptr const &query,
                                             KDevelop::ReferencedTopDUContext const &context)
{
  ifDebug(qDebug() << "executing class func query" << query->id();)

  mContext = context;

  DUChainReadLocker lock(DUChain::lock());

  QueryResult result(query->id(), context->url());
  result.setStatus(QueryResult::NoMatch);

  ClassFunctionDeclarationList functionDecls = findClassFunctionDeclarations(query);
  ifDebug(qDebug() << "found" << functionDecls.size() << "class functions for" << query->id();)

  ClassFunctionDeclarationUsesMap uses = findClassFunctionDeclarationUses(functionDecls, query);
  ifDebug(qDebug() << "found" << uses.size() << "uses for these declarations";)

  QMapIterator<ClassFunctionDeclaration*, RangeInRevisionList> i(uses);

  while (i.hasNext()) {
    i.next();
    foreach (RangeInRevision const &useRange, i.value())
      result.insert(ClassFunctionCall::Ptr(new ClassFunctionCall(i.key(), mContext, useRange)));
  }

  if (!result.hits().isEmpty())
    result.setStatus(QueryResult::Match);

  return result;
}

ClassFunctionDeclarationList CppQueryEnginePrivate::findClassFunctionDeclarations(const ClassFunctionQuery::Ptr& query) const
{
  unsigned const expectedTemplateArgs = query->templateRestrictions().size();
  ClassFunctionDeclarationList classFunctionDecls;
  QList<Declaration*> decls = findDeclarations(mContext, query->functionIdentifier());
  ifDebug(qDebug() << "found" << decls.size() << "unfiltered declarations for " << query->functionIdentifier().toString();)
  foreach (Declaration *decl, decls) {
    ifDebug(qDebug() << decl->toString();)
    if (!decl->isFunctionDeclaration())
      continue;

    ClassFunctionDeclaration* classDecl = dynamic_cast<ClassFunctionDeclaration*>(decl);
    if (!classDecl)
      continue;

    // apply ClassFunctionQuery specific restrictions
    switch (query->constnessRestriction()) {
      case ClassFunctionQuery::NoConstRestriction:
        break;
      case ClassFunctionQuery::RestrictConst:
        if (!classDecl->abstractType() || !(classDecl->abstractType()->modifiers() & AbstractType::ConstModifier)) {
          continue;
        }
        break;
      case ClassFunctionQuery::RestrictNonConst:
        if (!classDecl->abstractType() || classDecl->abstractType()->modifiers() & AbstractType::ConstModifier) {
          continue;
        }
        break;
    }

    ifDebug(qDebug() << "matches argument types:" << matchesArgumentTypes(decl, query->argumentTypes());)
    if (matchesArgumentTypes(decl, query->argumentTypes())) {
      TemplateDeclaration *templateDecl = dynamic_cast<TemplateDeclaration*>(decl);
      if (templateDecl) {
        QList<Declaration*> instantiations = findInstantiations(templateDecl, query->templateRestrictions(), DUContextPointer(decl->context()));
        ifDebug(qDebug() << "found" << instantiations.size() << "instantations of" << decl->toString();)
        foreach (Declaration *instantiation, instantiations) {
          Q_ASSERT(dynamic_cast<ClassFunctionDeclaration*>(instantiation));
          classFunctionDecls << static_cast<ClassFunctionDeclaration*>(instantiation);
        }
      } else if (expectedTemplateArgs == 0) {
        Q_ASSERT(dynamic_cast<ClassFunctionDeclaration*>(decl));
        classFunctionDecls << static_cast<ClassFunctionDeclaration*>(decl);
      }
    }
  }

  return classFunctionDecls;
}

ClassFunctionDeclarationUsesMap CppQueryEnginePrivate::findClassFunctionDeclarationUses(const ClassFunctionDeclarationList& decls,
                                                                                        const ClassFunctionQuery::Ptr& query) const
{
  ClassFunctionDeclarationUsesMap result;

  foreach (ClassFunctionDeclaration *decl, decls) {
    QList<RangeInRevision> uses = usesForCurrentContext(decl);
    ifDebug(qDebug() << uses.size() << "uses for class function" << decl->toString();)
    foreach (RangeInRevision const &useRange, uses) {
      ifDebug(qDebug() << "use at" << useRange.textRange() << "matches restrictions:" << useMatchesRestrictions(decl, useRange, query);)
      if (useMatchesRestrictions(decl, useRange, query))
        result[decl] << useRange;
    }
  }

  return result;
}
//END ClassfunctionQuery handling

//BEGIN FunctionQuery handling

QueryResult CppQueryEnginePrivate::execQuery(FunctionQuery::Ptr const &query,
                                      ReferencedTopDUContext const &context)
{
  mContext = context;

  DUChainReadLocker lock(DUChain::lock());

  QueryResult result(query->id(), context->url());
  result.setStatus(QueryResult::NoMatch);

  QList<FunctionDeclaration*> functionDecls = findFunctionDeclarations(query);
  FunctionDeclarationUsesMap uses = findFunctionDeclarationUses(functionDecls, query);
  QMapIterator<FunctionDeclaration*, RangeInRevisionList> i(uses);

  while (i.hasNext()) {
    i.next();
    foreach (RangeInRevision const &useRange, i.value())
      result.insert(FunctionCall::Ptr(new FunctionCall(i.key(), mContext, useRange)));
  }

  if (!result.hits().isEmpty())
    result.setStatus(QueryResult::Match);

  return result;
}

QList<FunctionDeclaration*> CppQueryEnginePrivate::findFunctionDeclarations(FunctionQuery::Ptr const &query) const
{
  unsigned const expectedTemplateArgs = query->templateRestrictions().size();
  QList<FunctionDeclaration*> functionDecls;
  QList<Declaration*> decls = findDeclarations(mContext, query->functionIdentifier());
  foreach (Declaration *decl, decls) {
    if (!decl->isFunctionDeclaration() || dynamic_cast<ClassFunctionDeclaration*>(decl))
      continue;

    if (matchesArgumentTypes(decl, query->argumentTypes())) {
      TemplateDeclaration *templateDecl = dynamic_cast<TemplateDeclaration*>(decl);
      if (templateDecl && expectedTemplateArgs != 0) {
        QList<Declaration*> instantiations = findInstantiations(templateDecl, query->templateRestrictions(), DUContextPointer(decl->context()));
        foreach (Declaration *instantiation, instantiations) {
          Q_ASSERT(dynamic_cast<FunctionDeclaration*>(instantiation));
          functionDecls << static_cast<FunctionDeclaration*>(instantiation);
        }
      } else if (expectedTemplateArgs == 0) {
        Q_ASSERT(dynamic_cast<FunctionDeclaration*>(decl));
        functionDecls << static_cast<FunctionDeclaration*>(decl);
      }
    }
  }

  return functionDecls;
}

FunctionDeclarationUsesMap CppQueryEnginePrivate::findFunctionDeclarationUses(FunctionDeclarationList const &decls,
                                                                      FunctionQuery::Ptr const &query) const
{
  FunctionDeclarationUsesMap result;

  foreach (FunctionDeclaration *decl, decls) {
    QList<RangeInRevision> uses = usesForCurrentContext(decl);

    foreach (RangeInRevision const &useRange, uses) {
      if (useMatchesRestrictions(decl, useRange, query))
        result[decl] << useRange;
    }
  }

  return result;
}

bool CppQueryEnginePrivate::matchesArgumentTypes(Declaration *memberDecl,
                                                 QStringList const &argumentTypes) const
{
  if (argumentTypes.size() > 0 && !memberDecl->isFunctionDeclaration())
    return false; // A non function declaration with arguments? I don't think so.

  if (argumentTypes.isEmpty() && !memberDecl->isFunctionDeclaration())
    return true; // NOTE: I'm not sure if this is safe.

  // At this point we must have a function declaration.
  AbstractFunctionDeclaration *fdecl = dynamic_cast<AbstractFunctionDeclaration*>(memberDecl);
  Q_ASSERT(fdecl);
  DUContextPointer fContext(fdecl->internalFunctionContext());
  QVector<Declaration*> argumentDecls = fContext->localDeclarations();

  if (argumentDecls.size() != argumentTypes.size())
    return false;

  for (int i = 0; i < argumentTypes.size(); ++i) {
    if ( argumentTypes.at(i).isEmpty() ) {
      continue;
    }

    IndexedType expectedArgumentType = resolveType(argumentTypes.at(i), fContext, memberDecl);
    if (!expectedArgumentType.isValid())
       return false;

    // Check if the argument is a template type in which case we have to resolve it.
    AbstractType::Ptr resolvedType = resolveDelayedTypes(argumentDecls.at(i)->abstractType(),
                                                         fContext.data(),
                                                         fContext->topContext());
    if (!resolvedType) {
      qDebug() << "[CppQueryEnginePrivate::matchesArgumentTypes] Could not resolve"
               << argumentDecls.at(i)->abstractType()->toString();
      return false;
    }
    ifDebug(qDebug() << "expected:" << expectedArgumentType.abstractType()->toString() << "found:" << resolvedType->toString();)
    if (CppTemplateParameterType::Ptr tplType = CppTemplateParameterType::Ptr::dynamicCast(resolvedType)) {
      continue;
    }
    if (resolvedType->indexed() != expectedArgumentType)
      return false;
  }

  return true;
}

bool CppQueryEnginePrivate::useMatchesRestrictions(AbstractFunctionDeclaration* funDec, const KDevelop::RangeInRevision& use, const FunctionQuery::Ptr& query) const
{
  if (!use.isValid())
    return false;

  if (query->argumentRestrictions(FunctionQuery::LiteralRestriction).isEmpty() &&
      query->argumentRestrictions(FunctionQuery::QidRestriction).isEmpty())
    return true; // No restrictions.

  DUContext* ductx = mContext->findContextAt(CursorInRevision(use.start.line, use.start.column));
  if (!ductx) {
    qDebug() << "[CppQueryEngine::useMatchesRestrictions] Context not found.";
    return false;
  }

  ParseSession* session = dynamic_cast<ParseSession*>(mContext->ast().data());
  if (!session) {
    qDebug() << "[CppQueryEngine::useMatchesRestrictions] No parse session found.";
    return false;
  }

  DUContextPointer ptr(ductx);
  AST *node = session->astNodeFromUse(qMakePair<DUContextPointer, RangeInRevision>(ptr, use));
  if (!node) {
    qDebug() << "[CppQueryEngine::useMatchesRestrictions] AST node not mapped";
    return false;
  }

  // The AST really sucks. We need this kind of checks to find the information we're looking
  // for, making sure that we cover each case.
  InitializerAST *iAST = 0;
  switch (node->kind) {
  case AST::Kind_InitDeclarator:
    iAST = reinterpret_cast<InitDeclaratorAST*>(node)->initializer;
    break;
  case AST::Kind_Initializer:
    iAST = reinterpret_cast<InitializerAST*>(node);
    break;
  case AST::Kind_FunctionCall:
    Q_ASSERT(false);
    break; // We're already where we want to be, the function call visitor at the end will take care.
  case AST::Kind_NewExpression:
  {
    NewExpressionAST *neAST = reinterpret_cast<NewExpressionAST*>(node);
    FunctionCallVisitor visitor(session);
    return visitor.matchesRestrictions(neAST, query, funDec);
  }
  case AST::Kind_UnqualifiedName:
    node = findFunctionCall(static_cast<UnqualifiedNameAST*>(node), session);
    break;
  default:
    AST* parent = ancestorNode(node, session, 3);
    Q_ASSERT(parent);
    Q_ASSERT(parent->kind == AST::Kind_Initializer);
    iAST = reinterpret_cast<InitializerAST*>(parent);
    //return false;
  };

  if (iAST) { // Handle initializer if needed
    if (!iAST->initializer_clause)
      return false;

    InitializerClauseAST *icAST = reinterpret_cast<InitializerClauseAST*>(iAST->initializer_clause);

    if (icAST->expression->kind == AST::Kind_PostfixExpression) {
      PostfixExpressionAST *pfeAST = reinterpret_cast<PostfixExpressionAST*>(icAST->expression);
      // pfeAST->expression // Should have a "PrimaryExpressionAST -> NameAST" subtree
      const ListNode<ExpressionAST*> *it = pfeAST->sub_expressions->toFront();
      const ListNode<ExpressionAST*> *end = it;
      do {
        if (it->next->element->kind == AST::Kind_FunctionCall) {
          Q_ASSERT(it->next);
          Q_ASSERT(it->next->element->kind == AST::Kind_FunctionCall);
          node = it->next->element;
          break;
        }
        it = it->next;
      } while (it != end);
    } else {
      qDebug() << Q_FUNC_INFO << "UNSUPPERTED KIND" << iAST->expression->kind;
      return false;
    }
  }

  FunctionCallVisitor visitor(session);
  Q_ASSERT(node->kind == AST::Kind_FunctionCall);
  return visitor.matchesRestrictions(static_cast<FunctionCallAST*>(node), query, funDec);
}
//END FunctionQuery handling

//BEGIN EnumQuery handling

QueryResult CppQueryEnginePrivate::execQuery(const EnumQuery::Ptr& query,
                                             const KDevelop::ReferencedTopDUContext& context)
{
  ifDebug(qDebug() << "executing EnumQuery" << query->id();)
  mContext = context;

  DUChainReadLocker lock(DUChain::lock());

  QueryResult result(query->id(), context->url());
  result.setStatus(QueryResult::NoMatch);

  EnumDeclarationList enumDecls = findEnumDeclarations(query);
  EnumDeclarationUsesMap uses = findEnumDeclarationUses(enumDecls, query);
  QMapIterator<Declaration*, RangeInRevisionList> i(uses);

  while (i.hasNext()) {
    i.next();
    foreach (RangeInRevision const &useRange, i.value())
      result.insert(EnumUse::Ptr(new EnumUse(i.key(), mContext, useRange)));
  }

  if (!result.hits().isEmpty())
    result.setStatus(QueryResult::Match);

  return result;
}

EnumDeclarationList CppQueryEnginePrivate::findEnumDeclarations(const EnumQuery::Ptr& query) const
{
  EnumDeclarationList enumDecls;
  QList<Declaration*> decls = findDeclarations(mContext, query->qualifiedIdentifier());
  ifDebug(qDebug() << "found" << decls.count() << "unfiltered declarations for identifier" << query->qualifiedIdentifier().toString();)
  foreach (Declaration *decl, decls) {
    if (decl->isFunctionDeclaration())
      continue;

    if (decl->abstractType().cast<EnumerationType>() || decl->abstractType().cast<EnumeratorType>()) {
      ifDebug(qDebug() << "declaration is enumeration or enumerator:" << decl->toString();)
      enumDecls << decl;
    }
  }

  return enumDecls;
}

EnumDeclarationUsesMap CppQueryEnginePrivate::findEnumDeclarationUses(const EnumDeclarationList &decls,
                                                                      const EnumQuery::Ptr &/*query*/) const
{
  EnumDeclarationUsesMap result;

  foreach (Declaration *decl, decls) {
    result[decl] = usesForCurrentContext(decl);
    ifDebug(qDebug() << "found" << result[decl].count() << "uses for" << decl->toString());
  }

  return result;
}

//END EnumQuery handling

//BEGIN generic handling

QList<Declaration*> CppQueryEnginePrivate::findInstantiations(TemplateDeclaration* decl,
                                                              QStringList const &restrictions, DUContextPointer context) const
{
  Q_ASSERT(decl);

  QList<Declaration*> result;
  QList<TemplateDeclaration*> instantiations = decl->instantiations().values();
  ifDebug(qDebug() << "found" << instantiations.size() << "instantatiations";)

  ExpressionParser parser;
  ExpressionEvaluationResult evalResult;

  TemplateDeclaration::InstantiationsHash::const_iterator it = decl->instantiations().constBegin();
  while (it != decl->instantiations().constEnd()) {
    ifDebug(qDebug() << "checking instantation" << it.value()->id(true).getDeclaration(mContext)->toString();)
    InstantiationInformation info = it.key().information();
    // a (non-template) method in a template class, pick the args of the class (covers probably 99%)
    if ( info.templateParametersSize() == 0 ) {
      info = info.previousInstantiationInformation.information();
    }
    if ( info.templateParametersSize() != uint( restrictions.size() ) ) {
      ++it;
      continue;
    }

    bool allRestrictionsMatch = true;
    for (int i = 0; i < restrictions.size(); ++i) {
      QString restriction = restrictions.at(i);
      if (restriction.isEmpty())
        continue;

      evalResult = parser.evaluateType(restriction.toUtf8(), context, mContext);
      Q_ASSERT(evalResult.isValid());

      IndexedType type = info.templateParameters()[i];
      if (type != evalResult.type) {
        allRestrictionsMatch = false;
        break;
      }
    }

    if (allRestrictionsMatch) {
      result << it.value()->id(true).getDeclaration(mContext);
    }

    ++it;
  }

  return result;
}

IndexedType CppQueryEnginePrivate::resolveType(QString const &typeStr,
                                               DUContextPointer const &ctx,
                                               Declaration* decl) const
{
  QString expectedArgType = typeStr;
  static const QRegExp exp("\\$\\{templateArg\\[(\\d+)\\]\\}");
  int index = exp.indexIn(typeStr);
  if (index != -1) {
    TemplateDeclaration* templateArgDecl = dynamic_cast<TemplateDeclaration*>(decl);
    if ( templateArgDecl ) {
      Q_ASSERT(templateArgDecl);
      if ( !templateArgDecl->templateParameterContext() ) {
        // a (non-template) method in a template class, pick the args of the class (covers probably 99%)
        templateArgDecl = 0;
        if ( decl->context() && decl->context()->owner() ) {
          templateArgDecl = dynamic_cast<TemplateDeclaration*>(decl->context()->owner());
        }
      }
    }
    if ( !templateArgDecl ) {
      qWarning() << "${templateArg} restriction on argument of declaration that is neither a template nor is in a template class" << decl->toString() << typeStr;
      return IndexedType();
    }
    QString qid = TemplateHelper::templateArgumentIdentifier(templateArgDecl, exp.cap(1).toInt());
    expectedArgType.replace(index, exp.matchedLength(), qid);
  }

  ExpressionParser parser(/* strict = */ true);
  ExpressionEvaluationResult result = parser.evaluateType(expectedArgType.toUtf8(), ctx, mContext);
  if (!result.isValid())
    qDebug() << "[CppQueryEngine::matchesArgumentTypes] Invalid type expression:" << expectedArgType;

  return result.type;
}

QList<RangeInRevision> CppQueryEnginePrivate::usesForCurrentContext(Declaration *decl) const
{
  Q_ASSERT(decl);
  Q_ASSERT(mContext);
  return decl->uses().value(mContext->url());
}

//END CppQueryEnginePrivate

//BEGIN CppQueryEngine

CppQueryEngine::CppQueryEngine() : d(new CppQueryEnginePrivate)
{ }

CppQueryEngine::~CppQueryEngine()
{
  delete d;
}

QueryResult CppQueryEngine::query(ReferencedTopDUContext const &context,
                                  Query::Ptr const &query)
{
  Q_ASSERT_X(context->ast(), "CppQueryEngine::query", "Context has no AST set, use AllDeclarationsContextsUsesAndAST flag to get a proper context.");
  if ( ClassQuery::Ptr classQuery = query.dynamicCast<ClassQuery>() ) {
    return d->execQuery(classQuery, context);
  } else if ( ClassFunctionQuery::Ptr cfuncQuery = query.dynamicCast<ClassFunctionQuery>() ) {
    return d->execQuery(cfuncQuery, context);
  } else if ( EnumQuery::Ptr enumQuery = query.dynamicCast<EnumQuery>() ) {
    return d->execQuery(enumQuery, context);
  } else {
    Q_ASSERT(query.dynamicCast<FunctionQuery>());
    return d->execQuery(query.dynamicCast<FunctionQuery>(), context);
  }
}

QueriesResult CppQueryEngine::query( const ReferencedTopDUContext &context,
                                     const QList<Query::Ptr> queries )
{
  Q_ASSERT_X(context->ast(), "CppQueryEngine::query",
             "Context has no AST set, use AllDeclarationsContextsUsesAndAST flag to get a proper context.");

  QueriesResult result;
  foreach ( const Query::Ptr &qry, queries )
    result.insertQueryResult( qry->id(), query( context, qry ) );

  return result;
}
