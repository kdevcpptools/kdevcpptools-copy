#include "queryresult.h"

#include <QtCore/QString>

using namespace KDevelop;

class QueryResultPrivate
{
public:
  IndexedString                     mUrl;
  QMap<RangeInRevision, QueryHit::Ptr>  mUses;
  QString                           mQueryId;
  QueryResult::Status               mStatus;

public: // Functions
  QueryResultPrivate( const QString &queryId, const IndexedString &url );
  QueryResultPrivate(const QueryResultPrivate& other);
  QueryResultPrivate &operator=(QueryResultPrivate const &other);
  ~QueryResultPrivate();
};

QueryResultPrivate::QueryResultPrivate( const QString &queryId,
                                        const IndexedString &url )
    : mUrl( url )
    , mQueryId( queryId )
    , mStatus( QueryResult::NoMatch )
{ }

QueryResultPrivate::QueryResultPrivate(const QueryResultPrivate& other)
{
  *this = other;
}

QueryResultPrivate &QueryResultPrivate::operator=(QueryResultPrivate const &other)
{
  if (&other == this)
    return *this;

  mUrl = other.mUrl;
  mUses = other.mUses;
  mQueryId = other.mQueryId;
  mStatus = other.mStatus;
  return *this;
}

QueryResultPrivate::~QueryResultPrivate()
{ }

QueryResult::QueryResult()
  : d_ptr( new QueryResultPrivate( QString(), IndexedString() ) )
{
  d_ptr->mStatus = Invalid;
}

QueryResult::QueryResult( QString const &queryId, const KDevelop::IndexedString &url )
    : d_ptr( new QueryResultPrivate( queryId, url ) )
{ }

QueryResult::~QueryResult()
{
  delete d_ptr;
}

QueryResult::QueryResult(const QueryResult& other)
  : d_ptr(new QueryResultPrivate(*other.d_ptr))
{ }

QueryResult &QueryResult::operator=(QueryResult const &other)
{
  if (&other != this)
    *d_ptr = *other.d_ptr;

  return *this;
}

IndexedString QueryResult::url() const
{
  Q_D( const QueryResult );
  return d->mUrl;
}

QString QueryResult::queryId() const
{
  return d_ptr->mQueryId;
}

void QueryResult::insert( const QueryHit::Ptr &hit )
{
  d_ptr->mUses.insert(hit->range(), hit);
}

QList<QueryHit::Ptr> QueryResult::hits() const
{
  return d_ptr->mUses.values();
}

QueryResult::Status QueryResult::status() const
{
  return d_ptr->mStatus;
}

void QueryResult::setStatus(Status status)
{
  d_ptr->mStatus = status;
}
