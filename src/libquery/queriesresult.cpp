/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "queriesresult.h"

#include <QtCore/QStringList>

#include "queryresult.h"

struct QueriesResultPrivate
{
  QMap<QString, QueryResult> mResults;
};

QueriesResult::QueriesResult()
  : d_ptr( new QueriesResultPrivate )
{ }

QueriesResult::~QueriesResult()
{
  delete d_ptr;
}

QueriesResult &QueriesResult::operator=( const QueriesResult &other )
{
  if ( &other == this )
    return *this;

  *d_ptr = *other.d_ptr;
  return *this;
}

void QueriesResult::insertQueryResult( const QString &queryId, const QueryResult &result )
{
  Q_D( QueriesResult );
  Q_ASSERT( !d->mResults.contains( queryId ) );

  if ( result.status() == QueryResult::NoMatch )
    return;

  d->mResults.insert( queryId, result );
}

QStringList QueriesResult::queryIds() const
{
  Q_D( const QueriesResult );
  return d->mResults.keys();
}

QueryResult QueriesResult::result( const QString &queryId ) const
{
  Q_D( const QueriesResult );
  Q_ASSERT( d->mResults.contains( queryId ) );
  return d->mResults.value( queryId );
}