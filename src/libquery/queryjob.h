/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef QUERYJOB_H
#define QUERYJOB_H

#include <KJob>

#include "query.h"

namespace KDevelop {
class IndexedString;
class IProject;
class ReferencedTopDUContext;
}

class QueryJobPrivate;
class QueryResult;

class CPPQUERYENGINE_EXPORT QueryJob : public KJob
{
  Q_OBJECT

public:
  /** Starts a Query for given url. */
  QueryJob( const KDevelop::IndexedString &url,
            const QList<Query::Ptr> &queries,
            QObject* parent = 0 );

  QueryJob( KDevelop::IProject* project,
            const QList<Query::Ptr> queries,
            QObject* parent = 0) ;

  ~QueryJob();

  virtual void start();

signals:
  void resultFound( const QueryResult &result );

protected:
  virtual bool doKill();

private slots:
  void updateReady( const KDevelop::IndexedString &url,
                    const KDevelop::ReferencedTopDUContext &topContext );

private:
  QueryJobPrivate * const d_ptr;
  Q_DECLARE_PRIVATE( QueryJob )
  Q_DISABLE_COPY( QueryJob )
};

#endif // QUERYJOB_H
