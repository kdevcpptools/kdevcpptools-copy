/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "queryjob.h"

#include <klocalizedstring.h>

#include <interfaces/icore.h>
#include <interfaces/ilanguage.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iproject.h>
#include <interfaces/iruncontroller.h>
#include <language/backgroundparser/backgroundparser.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/parsingenvironment.h>
#include <language/interfaces/iastcontainer.h>

#include "cppqueryengine.h"
#include "queryresult.h"

using namespace KDevelop;

/// QueryJobPrivate

class QueryJobPrivate
{
public:
  QueryJobPrivate( QueryJob *qq,
                   const KDevelop::IndexedString &url,
                   const QList<Query::Ptr> &queries );

  QueryJobPrivate( QueryJob *qq,
                   KDevelop::IProject* project,
                   const QList<Query::Ptr> &queries );

  static ReferencedTopDUContext contentFromProxy( const ReferencedTopDUContext &ctx );

  static bool isCppFile( const IndexedString &url);

  void updateUrl( const IndexedString &url );

public:
  QueryJob               *q_ptr;
  int                     mCurrentFileCount;
  int                     mFileCount;
  KDevelop::IProject     *mProject;
  QList<Query::Ptr>       mQueries;
  KDevelop::IndexedString mUrl;
};

QueryJobPrivate::QueryJobPrivate( QueryJob *qq,
                                  const KDevelop::IndexedString &url,
                                  const QList<Query::Ptr> &queries )
  : q_ptr( qq )
  , mCurrentFileCount( 0 )
  , mFileCount( 0 )
  , mProject( 0 )
  , mQueries( queries )
  , mUrl( url )
{ }

QueryJobPrivate::QueryJobPrivate( QueryJob *qq,
                                  KDevelop::IProject *project,
                                  const QList<Query::Ptr> &queries )
  : q_ptr( qq )
  , mCurrentFileCount( 0 )
  , mFileCount( 0 )
  , mProject( project )
  , mQueries( queries )
{ }

ReferencedTopDUContext QueryJobPrivate::contentFromProxy( const ReferencedTopDUContext &ctx )
{
  if ( ctx->parsingEnvironmentFile()
       && ctx->parsingEnvironmentFile()->isProxyContext() ) {

    if( ctx->importedParentContexts().isEmpty() ) {
      qDebug() << "proxy-context for" << ctx->url().str() << "has no imports!" << ctx->ownIndex();
      Q_ASSERT( false );
    }

    Q_ASSERT( !ctx->importedParentContexts().isEmpty() );
    return ReferencedTopDUContext(
      dynamic_cast<TopDUContext*>( ctx->importedParentContexts().first().context( 0 ) ) );
  } else
    return ctx;
}

bool QueryJobPrivate::isCppFile( const IndexedString &url )
{
  QList<ILanguage*> languages = ICore::self()->languageController()->languagesForUrl( url.toUrl() );
  foreach ( ILanguage* language, languages ) {
    if ( language->name() == "C++" )
      return true;
  }

  // Not a cpp file
  return false;
}

void QueryJobPrivate::updateUrl( const IndexedString &url )
{
  if ( !isCppFile( url ) )
    return;

  TopDUContext::Features features = TopDUContext::AllDeclarationsContextsAndUses;
  features = (TopDUContext::Features) (features | TopDUContext::AST | TopDUContext::ForceUpdate );

//   DUChainReadLocker lock( DUChain::lock() );
//   DUChain::self()->updateContextForUrl( url, features, q_ptr );
  KDevelop::ICore::self()->languageController()->backgroundParser()->addDocument( url.toUrl(), features, 10000, q_ptr );
}

/// QueryJob

QueryJob::QueryJob( const KDevelop::IndexedString &url,
                    const QList<Query::Ptr> &queries,
                    QObject* parent )
  : KJob( parent )
  , d_ptr( new QueryJobPrivate( this, url, queries ) )
{
  if ( d_ptr->isCppFile(url) ) {
    setCapabilities( Killable );
    if ( queries.isEmpty() )
      setObjectName( i18n( "Performing 0 queries on %1",
                            url.toUrl().url() ) );
    else
      setObjectName( i18np( "Perform 1 query on %2",
                            "Perform %1 queries on %2",
                            queries.size(),
                            url.toUrl().url() ) );

    ICore::self()->runController()->registerJob( this );
  } else
    deleteLater();
}

QueryJob::QueryJob( KDevelop::IProject* project,
                    const QList<Query::Ptr> queries,
                    QObject* parent )
  : KJob( parent )
  , d_ptr( new QueryJobPrivate( this, project, queries ) )
{
  connect( project, SIGNAL(destroyed(QObject*)), SLOT(kill()) );
  setCapabilities( Killable );
  if ( queries.isEmpty() )
    setObjectName( i18n( "Performing 0 queries on %1",
                          project->name() ) );
  else
    setObjectName( i18np( "Performing 1 query on project %2",
                          "Performing %1 queries on project %2",
                          queries.size(),
                          project->name() ) );

  ICore::self()->runController()->registerJob( this );
}

QueryJob::~QueryJob()
{
  KDevelop::ICore::self()->languageController()->backgroundParser()->revertAllRequests(this);

  if(ICore::self()->runController()->currentJobs().contains(this))
      ICore::self()->runController()->unregisterJob(this);

  delete d_ptr;
}

void QueryJob::start()
{
  Q_D(QueryJob);

  if ( d->mQueries.size() == 0 )
    return; // Nothing to do, so don't even iterate the project files.

  if ( d->mProject ) {
    foreach ( const IndexedString& url, d->mProject->fileSet() ) {
      ++d->mFileCount;
      d->updateUrl( url );
    }
  } else {
    ++d->mFileCount;
    d->updateUrl( d->mUrl );
  }
}

bool QueryJob::doKill()
{
  deleteLater();
  return true;
}

void QueryJob::updateReady( const KDevelop::IndexedString &url,
                            const KDevelop::ReferencedTopDUContext &topContext )
{
  Q_D( QueryJob );

  ++d->mCurrentFileCount;
  if ( d->mCurrentFileCount >= d->mFileCount )
    doKill();

  DUChainReadLocker lock( DUChain::lock() );
  if ( !topContext ) {
    qDebug() << "Updating context for" << url.str() << "failed";
    return;
  }

  ReferencedTopDUContext actualTopContext = topContext;
  if ( topContext->parsingEnvironmentFile()
       && topContext->parsingEnvironmentFile()->isProxyContext() )
    actualTopContext = d->contentFromProxy( topContext );

  if ( !actualTopContext->ast() ) {
    qDebug() << "No AST set for" << topContext->url().str();
    return;
  }

  CppQueryEngine engine;
  foreach ( Query::Ptr const &query, d->mQueries ) {
    Q_ASSERT( query );

    QueryResult result = engine.query( actualTopContext, query );
    if ( result.status() == QueryResult::Match ) {
      emit resultFound( result );
    }
  }
}
