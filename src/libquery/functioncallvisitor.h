/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef FUNCTIONCALLVISITOR_H
#define FUNCTIONCALLVISITOR_H

#include <QtCore/QMap>
#include <QtCore/QString>

#include "queries/functionquery.h"
#include "queryuses/variable.h"

class AST;
class FunctionCallAST;
class NewExpressionAST;
class ParseSession;

namespace KDevelop {
class DUContext;
class SimpleRange;
class AbstractFunctionDeclaration;
}

typedef QMap<uint, QString> Restrictions;

class FunctionCallVisitor
{
  public:
    FunctionCallVisitor(ParseSession *session);
    ~FunctionCallVisitor();

    /// Returns the variables of the argument expressions.
    /// @p node has to be either a FunctionCallAST or a NewInitiliazerAST.
    Variable::List arguments(AST* node);

    /// Returns wether or not the function call matches the resrictions set in @param query.
    bool matchesRestrictions(FunctionCallAST* fcall, const FunctionQuery::Ptr& query, KDevelop::AbstractFunctionDeclaration* funDec) const;

    /// Returns wether or not the ctor call matches the resrictions set in @param query.
    bool matchesRestrictions(NewExpressionAST *ctor, FunctionQuery::Ptr const &query, KDevelop::AbstractFunctionDeclaration* funDec) const;

  private:
    class FunctionCallVisitorPrivate * const d;
};

#endif // FUNCTIONCALLVISITOR_H
