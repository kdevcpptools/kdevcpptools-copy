/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "functioncallvisitor.h"

#include <languages/cpp/cppduchain/cppeditorintegrator.h>
#include <languages/cpp/parser/ast.h>
#include <languages/cpp/parser/lexer.h>
#include <languages/cpp/parser/parsesession.h>
#include <dumptree.h>

#include <language/duchain/declaration.h>
#include <language/duchain/abstractfunctiondeclaration.h>
#include <language/duchain/types/functiontype.h>

#include <languages/cpp/cppduchain/expressionparser.h>
#include <../cppduchain/expressionevaluationresult.h>

#include "queryuses/variable.h"
#include "queryuses/variable_p.h"

#define ifDebug(x)
#include <util.h>

using namespace KDevelop;

/// FunctionCallVisitorPrivate

class FunctionCallVisitorPrivate
{
  public: /// Functions
    FunctionCallVisitorPrivate(ParseSession *session);

    bool hasRestrictionForArgument(unsigned offset) const;
    bool hasRestrictionForArgument(FunctionQuery::ArgumentRestriction, unsigned offset) const;

    /// Returns wheter or not mCurrentArgument represented by @param node matches
    /// the restrictions set, if any.
    bool matches(PrimaryExpressionAST *node) const;

    /// Returns the variable for @param node.
    Variable::Ptr variableForNode(AST *node) const;

    /// Returns the string contents for @param node. Useful for debugging.
    QString stringForNode(AST *node) const;

    /// Make sure that we don't reuse settings of a previous visit.
    void reset();

    /// Returns the value for @param node.
    QString value(StringLiteralAST *node) const;

    bool argumentsMatchRestrictions(ExpressionAST* arguments, FunctionQuery::Ptr query, AbstractFunctionDeclaration* funDec);

    void visit(BinaryExpressionAST *argument);
    void visit(PrimaryExpressionAST *argument);
    void visit(ParameterDeclarationClauseAST *arguments);

  public: /// Members
    QList<Variable::Ptr> mArguments;
    unsigned           mCurrentArgument;
    FunctionQuery::Ptr mQuery;
    bool               mRestrictionsMatch;
    ParseSession      *mSession;
};

FunctionCallVisitorPrivate::FunctionCallVisitorPrivate(ParseSession *session)
  : mCurrentArgument(0)
  , mRestrictionsMatch(false)
  , mSession(session)
{ }

bool FunctionCallVisitorPrivate::hasRestrictionForArgument(unsigned offset) const
{
  if (!mQuery)
    return false;

  return mQuery->argumentRestrictions(FunctionQuery::LiteralRestriction).contains(offset)
    || mQuery->argumentRestrictions(FunctionQuery::QidRestriction).contains(offset);
}

bool FunctionCallVisitorPrivate::hasRestrictionForArgument(FunctionQuery::ArgumentRestriction kind,
                                                           unsigned offset) const
{
  if (!mQuery)
    return false;

  return mQuery->argumentRestrictions(kind).contains(offset);
}

bool FunctionCallVisitorPrivate::matches(PrimaryExpressionAST *node) const
{
  CppEditorIntegrator editor(mSession);
  if (hasRestrictionForArgument(FunctionQuery::QidRestriction, mCurrentArgument)
      && node->name ) {

    QStringList names = mQuery->argumentRestrictions(FunctionQuery::QidRestriction).value(mCurrentArgument).split("::");
    if (node->name->qualified_names && names.size() != node->name->qualified_names->count() + 1)
      return false;
    else if (node->name->unqualified_name && names.size() == 1) {
      UnqualifiedNameAST *una = node->name->unqualified_name;
      return editor.tokenToString(una->start_token) == names.first();
    }

    for (int i = 0; i < node->name->qualified_names->count(); ++i) {
      UnqualifiedNameAST *una = node->name->qualified_names->at(i)->element;
      if (editor.tokenToString(una->start_token) != names[i])
        return false;
    }

    return editor.tokenToString(node->name->unqualified_name->start_token) == names.last();
  }

  if (hasRestrictionForArgument(FunctionQuery::LiteralRestriction, mCurrentArgument)) {
    QRegExp exp(mQuery->argumentRestrictions(FunctionQuery::LiteralRestriction).value(mCurrentArgument).toUtf8());
    // At this point we assume that the regexp, weherever it comes from, is
    // checked for validity
    Q_ASSERT(exp.isValid());

    QString literal = stringForNode(node).trimmed();

    return (exp.indexIn(literal, 0) != - 1);
  }

  // No restrictions for current argument, so return true.
  return true;
}

Variable::Ptr FunctionCallVisitorPrivate::variableForNode(AST *node) const
{
  Q_ASSERT(node);

  CppEditorIntegrator editor(mSession);

  Variable::Ptr var(new Variable(editor.findRange(node)));
  var->d_ptr->mNode = node;
  var->d_ptr->mTypeString = stringForNode(node);

  return var;
}

QString FunctionCallVisitorPrivate::stringForNode(AST* node) const
{
  Q_ASSERT(node);
  CppEditorIntegrator editor(mSession);
  return editor.tokensToStrings(node->start_token, node->end_token);
}

void FunctionCallVisitorPrivate::reset()
{
  mArguments.clear();
  mCurrentArgument = 0;
  mRestrictionsMatch = true;
}

QString FunctionCallVisitorPrivate::value(StringLiteralAST *node) const
{
  QString literal;
  const ListNode<uint>* it = node->literals->toFront(), *end = it;
  do {
    const Token& t = mSession->token_stream->token(it->element);
    literal.append(t.symbolString());
    it = it->next;
  } while (it != end);

  return literal;
}

bool FunctionCallVisitorPrivate::argumentsMatchRestrictions(ExpressionAST* arguments, FunctionQuery::Ptr query, AbstractFunctionDeclaration* funDec)
{
  Q_ASSERT(query);

  if (query->argumentRestrictions(FunctionQuery::LiteralRestriction).isEmpty() &&
      query->argumentRestrictions(FunctionQuery::QidRestriction).isEmpty())
    return true; // No restrictions.

  reset();
  mQuery = query;

  int max = 0; // The maximum argument offset for which a restriction is set.
  foreach (int offset, query->argumentRestrictions(FunctionQuery::LiteralRestriction).keys())
    if (offset > max)
      max = offset;
  foreach (int offset, query->argumentRestrictions(FunctionQuery::QidRestriction).keys())
    if (offset > max)
      max = offset;

  ifDebug(dumpNode(arguments, mSession); qDebug() << "looking up at least" << (max + 1) << "argument restrictions" << "for func with default params:" << funDec->defaultParametersSize();)
  bool match = true;
  // offset of last handled argument
  int lastHandledArgument = -1;

  if (arguments) {
    Q_ASSERT(arguments->kind == AST::Kind_BinaryExpression || arguments->kind == AST::Kind_PrimaryExpression);
    if ( arguments->kind == AST::Kind_BinaryExpression ) {
      // function call has two or more arguments
      visit(reinterpret_cast<BinaryExpressionAST*>(arguments));
      match = mRestrictionsMatch;
      lastHandledArgument = mCurrentArgument - 1;
    } else {
      // The function call has only one argument.
      match = matches(reinterpret_cast<PrimaryExpressionAST*>(arguments));
      lastHandledArgument = 0;
    }
  }

  ifDebug(qDebug() << "currenlty:" << match << "after evaluating" << lastHandledArgument << "arguments";)
  if ( match && lastHandledArgument < max ) {
    // Gaah, who comes up with returning unsigned ints at some random point in
    // the api?!
    const int currentRestrictedArgOffset = funDec->defaultParametersSize() + lastHandledArgument;
    if ( max > currentRestrictedArgOffset ) {
      match = false;
    } else {
      ++lastHandledArgument;
      for ( ; match && lastHandledArgument <= max; ++lastHandledArgument ) {
        if (hasRestrictionForArgument(FunctionQuery::QidRestriction, lastHandledArgument)) {
          /* TODO: this gets interesting when we want to match against the actual value but not against the QidRestriction
          Cpp::ExpressionParser parser;
          Cpp::ExpressionEvaluationResult result = parser.evaluateExpression(funDec->defaultParameterForArgument(lastHandledArgument).c_str(), DUContextPointer(funDec->internalFunctionContext()));
          match = query->argumentRestrictions(FunctionQuery::QidRestriction).value(lastHandledArgument) == result.identifier().toString();
          */
          match = funDec->defaultParameterForArgument(lastHandledArgument).str()
                    == query->argumentRestrictions(FunctionQuery::QidRestriction).value(lastHandledArgument);
        } else if (hasRestrictionForArgument(FunctionQuery::LiteralRestriction, lastHandledArgument)) {
          QRegExp exp(query->argumentRestrictions(FunctionQuery::LiteralRestriction).value(lastHandledArgument).toUtf8());
          // At this point we assume that the regexp, weherever it comes from, is
          // checked for validity
          Q_ASSERT(exp.isValid());
          match = exp.indexIn(funDec->defaultParameterForArgument(lastHandledArgument).str()) != -1;
        }
      }
      ifDebug(qDebug() << "currently:" << match << "after evaluating" << lastHandledArgument << "arguments (inclusive default arguments)";)
    }
  }

  return match;
}

void FunctionCallVisitorPrivate::visit(BinaryExpressionAST *argument)
{
  bool isExpression = true;
  if (argument->left_expression->kind == AST::Kind_BinaryExpression) {
    BinaryExpressionAST *beAST = reinterpret_cast<BinaryExpressionAST*>(argument->left_expression);
    Token const &t = mSession->token_stream->token(beAST->op);
    if (t.kind == ',') {
      isExpression = false;
      visit(reinterpret_cast<BinaryExpressionAST*>(argument->left_expression));
    } else {
      mArguments << variableForNode(argument->left_expression);
    }
  } else if (mSession->token_stream->token(argument->op).kind != ',') {
    mArguments << variableForNode(argument);
    return;
  } else {
    mArguments << variableForNode(argument->left_expression);
  }


  // The right expression is always one of the arguments.
  mArguments << variableForNode(argument->right_expression);

  if (argument->left_expression->kind == AST::Kind_PrimaryExpression)
    mRestrictionsMatch =
      (mRestrictionsMatch && matches(reinterpret_cast<PrimaryExpressionAST*>(argument->left_expression)));
  else if (isExpression && hasRestrictionForArgument(mCurrentArgument))
    mRestrictionsMatch = false;

  if (isExpression) ++mCurrentArgument;

  if (argument->right_expression->kind == AST::Kind_PrimaryExpression)
    mRestrictionsMatch =
      (mRestrictionsMatch && matches(reinterpret_cast<PrimaryExpressionAST*>(argument->right_expression)));
  else if (hasRestrictionForArgument(mCurrentArgument))
    mRestrictionsMatch = false;

  ++mCurrentArgument;

  // Skip all other kind of nodes on both left and right side. Those are not
  // literals we can check (though they might contain literals but that falls
  // out of the scope of this check for now.
}

void FunctionCallVisitorPrivate::visit(ParameterDeclarationClauseAST* arguments)
{
  if (!arguments->parameter_declarations)
        return; // NOTE: ellipsis set, we don't support that (yet).

    // Get the FunctionCallAST which should be the first FunctionCallAST after
    // the PrimaryExpression with optionally template parameter in between.
    const ListNode<ParameterDeclarationAST*> *it = arguments->parameter_declarations->toFront();
    const ListNode<ParameterDeclarationAST*> *end = it;

    ParameterDeclarationAST *pdAST;
    do {
      // NOTE: I don't think this is complete
      pdAST = it->element;
      if (pdAST->type_specifier) {
        mArguments << variableForNode(pdAST->type_specifier);
      } else if (pdAST->expression) {
        mArguments << variableForNode(pdAST->expression);
      }

      it = it->next;
    } while (it != end);
}

/// FunctionCallVisitor

FunctionCallVisitor::FunctionCallVisitor(ParseSession* session)
  : d(new FunctionCallVisitorPrivate(session))
{ }

FunctionCallVisitor::~FunctionCallVisitor()
{
  delete d;
}

Variable::List FunctionCallVisitor::arguments(AST *node)
{
  Q_ASSERT(node);
  Q_ASSERT(node->kind == AST::Kind_FunctionCall || node->kind == AST::Kind_NewInitializer || node->kind == AST::Kind_MemInitializer
           || node->kind == AST::Kind_ParameterDeclarationClause || node->kind == AST::Kind_Initializer);

  ExpressionAST* arguments = 0;
  if ( node->kind == AST::Kind_FunctionCall ) {
    arguments = reinterpret_cast<FunctionCallAST*>(node)->arguments;
  } else if ( node->kind == AST::Kind_NewInitializer ) {
    arguments = reinterpret_cast<NewInitializerAST*>(node)->expression;
  } else if ( node->kind == AST::Kind_MemInitializer ) {
    arguments = reinterpret_cast<MemInitializerAST*>(node)->expression;
  } else if ( node->kind == AST::Kind_ParameterDeclarationClause ) {
    ParameterDeclarationClauseAST* paramters = reinterpret_cast<ParameterDeclarationClauseAST*>(node);
    d->reset();
    d->visit(paramters);
    return d->mArguments;
  } else if ( node->kind == AST::Kind_Initializer ) {
    InitializerAST *iast = reinterpret_cast<InitializerAST*>(node);
    if ( iast->initializer_clause )
      return Variable::List() << d->variableForNode( iast->initializer_clause );
    else
      arguments = iast->expression;
  }

  if (!arguments) {
    return Variable::List();
  } else if (arguments->kind != AST::Kind_BinaryExpression) { // single argument
    return Variable::List() << d->variableForNode(arguments);
  } else { // more than one argument
    d->reset();
    d->visit(reinterpret_cast<BinaryExpressionAST*>(arguments));
    return d->mArguments;
  }
}

bool FunctionCallVisitor::matchesRestrictions(FunctionCallAST *fcall,
                                              FunctionQuery::Ptr const &query,
                                              AbstractFunctionDeclaration* funDec) const
{
  Q_ASSERT(fcall);
  Q_ASSERT(query);

  return d->argumentsMatchRestrictions(fcall->arguments, query, funDec);
}

bool FunctionCallVisitor::matchesRestrictions(NewExpressionAST *ctor,
                                              FunctionQuery::Ptr const &query,
                                              AbstractFunctionDeclaration* funDec) const
{
  Q_ASSERT(ctor);
  Q_ASSERT(query);

  ExpressionAST* arguments = 0;

  if (ctor->new_initializer) {
    arguments = ctor->new_initializer->expression;
  }

  return d->argumentsMatchRestrictions(arguments, query, funDec);
}
