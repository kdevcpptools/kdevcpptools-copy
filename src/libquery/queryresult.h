#ifndef QUERYRESULT_H
#define QUERYRESULT_H

#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QMap>

#include <language/editor/simplerange.h>

#include "cppqueryengine_export.h"

#include "queryuses/queryhit.h"
#include "queryuses/classuse.h"
#include "queryuses/functioncall.h"
#include "queryuses/classfunctioncall.h"

class QueryHit;
class QueryResultPrivate;

class CPPQUERYENGINE_EXPORT QueryResult
{
public:
  enum Status {
    Invalid,
    Match,                  /// The query was executed successful and found a match.
    NoMatch                 /// The query was executed successful but no match was found.
  };

public:
  /** Creates an invalid QueryResult */
  QueryResult();
  QueryResult( const QString &queryId, const KDevelop::IndexedString &url );

  ~QueryResult();

  QueryResult( const QueryResult&other );
  QueryResult &operator=( const QueryResult &other );

  KDevelop::IndexedString url() const;

  /// Returns the ID of the query, which has it's result stored in this class.
  QString queryId() const;

  void insert( const QueryHit::Ptr &hit );

  /// Returns the uses sorted by QueryUse::range()
  QList<QueryHit::Ptr> hits() const;

  /// Returns the uses of type @p type sorted by QueryUse::range()
  template<typename T>
  QList< QSharedPointer<T> > hits() const;

  Status status() const;
  void setStatus(Status status);

private:
  QueryResultPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(QueryResult)
};

template<typename T>
QList< QSharedPointer< T > > QueryResult::hits() const
{
  QList< QSharedPointer< T > > matches;
  foreach ( QueryHit::Ptr use, hits() ) {
    if ( QSharedPointer< T > tUse = use.dynamicCast<T>() ) {
      matches << tUse;
    }
  }
  return matches;
}

#endif // QUERYRESULT_H
