/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef QUERIESRESULT_H
#define QUERIESRESULT_H

#include <QtCore/qglobal.h>

#include "cppqueryengine_export.h"

class QStringList;

class QueryResult;
class QueriesResultPrivate;

class CPPQUERYENGINE_EXPORT QueriesResult
{
public:
  QueriesResult();
  ~QueriesResult();

  QueriesResult &operator=( const QueriesResult &other );

  /**
   * Adds the results for given query id if there where one or more hits.
   *
   * A result for a query must only be reported <em>once</em>
   */
  void insertQueryResult( const QString &queryId, const QueryResult &result );

  /** Returns the ids of the queries for which results where reported. */
  QStringList queryIds() const;

  /**
   * Returns the result for given queryId
   *
   * @precondition: queryIds().contains( queryId ) == true.
   */
  QueryResult result( const QString &queryId ) const;

private:
  QueriesResultPrivate * const d_ptr;
  Q_DECLARE_PRIVATE( QueriesResult )
};

#endif // QUERIESRESULT_H
