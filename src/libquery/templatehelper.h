/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef TEMPLATEARGUMENTINFOEXTRACTOR_H
#define TEMPLATEARGUMENTINFOEXTRACTOR_H

namespace Cpp {
class TemplateDeclaration;
}

namespace KDevelop {
class Declaration;
}

class ParseSession;
class QString;
class UnqualifiedNameAST;

class TemplateHelper
{
  public:
    TemplateHelper(ParseSession *session);
    ~TemplateHelper();

  public: /// static methods.
    /**
     * Returns the identifier of the template parameter at @param offset in @param decl.
     * If @param decl is not a template declaration or it has less than @param offset
     * parameters, this will return an empty string.
     */
    static QString templateArgumentIdentifier(Cpp::TemplateDeclaration *decl, int offset);

  private:
    class TemplateHelperPrivate * const d;
};

#endif // TEMPLATEARGUMENTINFOEXTRACTOR_H
