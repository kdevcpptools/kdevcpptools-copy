/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "astmodelbuilder.h"

#include <QtCore/QStack>

#include <ast.h>
#include <default_visitor.h>

#include "util.h"

void ASTModelBuilder::visit(AST* node)
{
  if (node) {
    createAppendAndPushChild(node);
    DefaultVisitor::visit(node);
    mCurrentParent.pop();
  }
}

void ASTModelBuilder::createAppendAndPushChild(AST *node)
{
  QList<QStandardItem*> items;
  items << new QStandardItem(nodeName(node));
  KDevelop::RangeInRevision range = mEditor->findRange(node);
  items << new QStandardItem(
    QString("[(%1, %2) -> (%3, %4)]")
      .arg(range.start.line).arg(range.start.column)
      .arg(range.end.line).arg(range.end.column)
  );
  QString contents = mEditor->tokensToStrings(node->start_token, node->end_token);
  if ( contents.length() > 50 ) {
    contents.resize(47);
    contents.append("...");
  }
  items << new QStandardItem(contents);

  if (mCurrentParent.isEmpty()) {
    mModel->appendRow(items);
  } else {
    QStandardItem *parent = mCurrentParent.top();
    parent->appendRow(items);
  }

  mCurrentParent.push(items.first());
}

QStandardItemModel* ASTModelBuilder::createModel(ParseSession *session)
{
  mModel = new QStandardItemModel();
  mModel->setHorizontalHeaderItem(0, new QStandardItem("AST Node"));
  mModel->setHorizontalHeaderItem(1, new QStandardItem("Range"));
  mModel->setHorizontalHeaderItem(2, new QStandardItem("Contents"));
  
  mEditor = new CppEditorIntegrator(session);
  visit(session->topAstNode());
  delete mEditor;

  return mModel;
}

// #include "astmodel.moc"

