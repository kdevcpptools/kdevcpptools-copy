/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef LIBQUERYUTIL_H
#define LIBQUERYUTIL_H

#include "parsesession.h"
#include "ast.h"
#include "../cppduchain/cppeditorintegrator.h"
#include "dumptree.h"

#include <KDebug>

/**
 * Returns the @p level ancestor of @p node. Level defaults to 1, meaning
 * the parent node will be returned.
 */
inline AST* ancestorNode(AST* const node, ParseSession* session, unsigned int level = 1)
{
  AST* ret = node;
  while (ret && level) {
    ret = session->parentAstNode(ret);
    --level;
  }
  Q_ASSERT(level == 0);

  return ret;
}

/**
 * Helper function that returns the human readable name of an AST node.
 */
inline QString nodeName(AST* node)
{
  if ( node->kind > 0 && node->kind < AST::NODE_KIND_COUNT ) {
    return QString::fromAscii(names[node->kind]);
  } else {
    return QString("<unknown node kind %1>").arg(node->kind);
  }
}

/**
 * Debug function that prints an AST token
 */
inline void dumpNode(AST* node, ParseSession* session)
{
  if ( node ) {
    CppEditorIntegrator editor(session);
    QString contents = editor.tokensToStrings(node->start_token, node->end_token);
    if ( contents.length() > 50 ) {
      contents.resize(47);
      contents.append("...");
    }
    qDebug() << node << nodeName(node) << editor.findRange(node).castToSimpleRange().textRange() << contents;
  } else {
    qDebug() << "null node";
  }
}

/**
 * Find the function call for a given name. Assums that @p node is in a PrimaryExpression
 * and that the next sibling will be the function call.
 */
inline FunctionCallAST* findFunctionCall(UnqualifiedNameAST* node, ParseSession* session)
{
  AST* needle = 0;
  // NameAST -> PrimaryExpressionAST
  AST* parent = ancestorNode(node, session, 2);
  Q_ASSERT(parent);
  Q_ASSERT(parent->kind == AST::Kind_ClassMemberAccess || parent->kind == AST::Kind_PrimaryExpression);
  // -> PostFixExpressionAST
  AST* postFixParent = ancestorNode(parent, session, 1);
  Q_ASSERT(postFixParent);
  const ListNode< ExpressionAST* >* subExprs = 0;
  if (postFixParent->kind == AST::Kind_CppCastExpression) {
    subExprs = static_cast<CppCastExpressionAST*>(postFixParent)->sub_expressions;
  } else {
    Q_ASSERT(postFixParent->kind == AST::Kind_PostfixExpression);
    PostfixExpressionAST *peParent = static_cast<PostfixExpressionAST*>(postFixParent);
    Q_ASSERT(peParent);
    subExprs = peParent->sub_expressions;
    if ( parent->kind == AST::Kind_PrimaryExpression ) {
      // simple function call, i.e. somethink like foo(bar);
      Q_ASSERT(peParent->expression == parent);
      Q_ASSERT(peParent->sub_expressions->count() == 1);
      needle = peParent->sub_expressions->toFront()->element;
      Q_ASSERT(needle);
    }
  }
  Q_ASSERT(subExprs->count());

  if (!needle) {
    // classMemberAccess, i.e. something like foo->...->bar(asdf)->...
    // we have to find the parent and get the next node for it
    const ListNode< ExpressionAST* >* it = subExprs->toFront();
    do {
      if ( it->element == parent ) {
        needle = it->next->element;
        break;
      }
      it = it->next;
    } while ( it->hasNext() );
  }
  Q_ASSERT(needle);
  Q_ASSERT(needle->kind == AST::Kind_FunctionCall);
  return static_cast<FunctionCallAST*>(needle);
}

#endif // LIBQUERYUTIL_H
