/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CPPQUERYENGINEPLUGIN_H
#define CPPQUERYENGINEPLUGIN_H

#include <QtCore/QVariant>

#include <interfaces/iplugin.h>

class CppQueryEnginePlugin : public KDevelop::IPlugin
{
    Q_OBJECT

public:
    CppQueryEnginePlugin(QObject* parent, const QVariantList&);

    virtual ~CppQueryEnginePlugin();

    virtual void unload();

    virtual KDevelop::ContextMenuExtension contextMenuExtension(KDevelop::Context*);

private slots:
    void getQuery();

private:
    class CppQueryEngineFactory* m_factory;
    QAction* m_getQuery;
};

#endif // CPPQUERYENGINEPLUGIN_H
