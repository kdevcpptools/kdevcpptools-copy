#include "bardelegate.h"

#include <QtGui/QPainter>

#include <QtCore/QDebug>

QColor calculateColor(double factor, int value)
{
  return QColor::fromHsv(240 - factor * 240, 255, value);
}

BarDelegate::BarDelegate(QObject *parent)
  : QAbstractItemDelegate(parent)
  , mColumnScale(ToTotal)
  , mFontSize(8.0)
  , mShowCounts(true)
  , mMinimumFontSize(5.5)
{ }

void BarDelegate::paint(QPainter *painter,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index) const
{
  if (option.state & QStyle::State_Selected)
    painter->fillRect(option.rect, option.palette.highlight());

  const int value = index.model()->data(index, Qt::DisplayRole).toInt();
  int totalMaxVal;
  switch (mColumnScale) {
  case ToColumnMax:
    totalMaxVal = index.model()->data(index, Qt::UserRole + 1).toInt();
    break;
  default: // ToTal;
    totalMaxVal = index.model()->data(index, Qt::UserRole).toInt();
  }

  const double factor = totalMaxVal ? (double)value/totalMaxVal : 0;
  if ((int) (factor * option.rect.width()) != 0) {
    QLinearGradient gradient(option.rect.topLeft(), option.rect.topRight());
    gradient.setColorAt(0, calculateColor(factor, 50));
    gradient.setColorAt(0.25, calculateColor(factor, 200));
    gradient.setColorAt(1.0, calculateColor(factor, 255));

    painter->setPen(Qt::NoPen);
    painter->setBrush(gradient);
    painter->drawRect(option.rect.x(), option.rect.y(),
                      (int) (factor * option.rect.width()), option.rect.height());

    if (mShowCounts && mFontSize >= mMinimumFontSize) {
      QFont font = option.font;
      font.setPointSizeF(mFontSize);

      QFontMetrics fm(font);
      QRect painterRect = fm.boundingRect(option.rect, Qt::AlignHCenter | Qt::AlignBottom, QString::number(value));

      painter->setPen(Qt::black);
      painter->setBrush(Qt::black);
      painter->setFont(font);
      painter->drawText(painterRect, QString::number(value));
    }
  }
}

void BarDelegate::setColumnScale(ColumnScale scale)
{
  mColumnScale = scale;
}

void BarDelegate::setFontSize(qreal size)
{
  mFontSize = size;
}

void BarDelegate::setMinimumFontSize(qreal size)
{
  mMinimumFontSize = size;
}

void BarDelegate::showCounts(bool show)
{
  mShowCounts = show;
}

QSize BarDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
  return QSize(50, 18);
}
