#ifndef RESULTDATAMODEL_H
#define RESULTDATAMODEL_H

#include <libquery/query.h>
#include <libquery/queryresult.h>

#include <QtCore/QPair>

class ResultDataModelPrivate;

/**
 * Represents the raw data which is the result of an Query.
 */
class ResultDataModel : public QObject
{
  Q_OBJECT

  ResultDataModel(ResultDataModel const &other);
  ResultDataModel operator=(ResultDataModel const &other);

public:
  typedef QSharedPointer<ResultDataModel> Ptr;

public:
  ResultDataModel();
  ~ResultDataModel();

  /**
   * Returns the ith file. Files are sorted by maximum number of hits.
   * I must be greater than zero and less than fileCount().
   */
  QString file(int i) const;

  /**
   * Returns the number of files which contains hits (at least one), for one or
   * more of the queries.
   */
  int fileCount() const;

  /**
   * Adds the @param result of the query for @param file to the model.

   * @note This method does not check for duplicate adds. So if you call this
   * method with the same data twice, the data will get in twice.
   */
  void add(QString const &file, QueryResult const &result);

  /// Removes all data from the model.
  void clear();

  /// Returns the number of hits for the query that had most hits in @param file.
  int maxHitCountForFile(QString const &file) const;

  /// Returns the number of hits for the file that had most hits for @param query.
  int maxHitCountForQuery(QString const &query) const;

  /**
   * Returns a pair containing the file which has the most hits. The number of
   * hits is an aggregation of all hits from queries in this file.
   */
  QPair<QString, int> maxFileResult() const;

  /**
   * Returns a pair containing the query which has the most uses and the number
   * of uses. The number of uses is an aggregation of all files having uses for
   * this query.
   */
  QPair<QString, int> maxQueryResult() const;

  /// Returns the ith query id (queries are sorted by maximum number of hits)
  QString query(int i) const;

  /// Returns the @param ith query for @param file.
  QString query(QString const &file, int i) const;

  /**
   * Returns the number of unique queries for which results are reported in
   * @param file. If file is empty, this will return the total number of unique
   * queries for which results where reported.
   */
  int queryCount(QString const &file = QString()) const;

  /// Returns the ranges in @param file that are found by @param query.
  QList<KDevelop::SimpleRange> ranges(const QString &query, const QString &file) const;

  void removeResultsForFile(const QString &file);

  /// Returns the locations for @param query in @param fileName.
  QueryResult result(const QString &query, const QString &fileName) const;

  /// Returns the number of hits for @param query in @param fileName.
  int resultCount(const QString &query, const QString &fileName) const;

  /// Returns the total number of hits over all files for all queries.
  int resultTotal() const;

  /// Returns the total number hits of all queries in @param file.
  int resultTotalForFile(const QString &file) const;

  /// Returns the total number hits over all files for @param query.
  int resultTotalForQuery(const QString &query) const;

signals:
  /// Emitted after all data is removed from the model by a call to clear()
  void cleared();

  void fileAboutToBeInserted();

  /// Emitted when due to a call to add the number of files has changed.
  void fileInserted(const QString &);

  void querySortOrderAboutToBeChanged();

  /// Emitted when the sort order of the query results got changed.
  void querySortOrderChanged();

private:
  ResultDataModelPrivate * const d_ptr;
  Q_DECLARE_PRIVATE(ResultDataModel)
};

#endif // RESULTDATAMODEL_H
