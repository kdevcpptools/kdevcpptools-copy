#include "horizontalfileitem.h"

#include <language/duchain/topducontext.h>

using namespace KDevelop;

/// HorizontalFileItemPrivate

class HorizontalFileItemPrivate
{
public:
  enum StructType {
    Class,
    Function
  };


public: /// Members
  QString                       mQueryId;
  QList<SimpleRange>            mQueryResultRanges;
  QMap<StructType, SimpleRange> mStructRanges;

public: /// Functions
  void extractGlobalStructRanges(KDevelop::DUContext *context);
};

void HorizontalFileItemPrivate::extractGlobalStructRanges(DUContext *context)
{
  foreach (DUContext *childContext, context->childContexts()) {
    switch (childContext->type()) {
    case DUContext::Namespace:
      extractGlobalStructRanges(childContext);
      break;
    case DUContext::Class:
      childContext->range();
      break;
    case DUContext::Function:
      break;
    default: // We're not interested in the others.
      break;
    }
  }
}

/// HorizontalFileItem

HorizontalFileItem::HorizontalFileItem(ReferencedTopDUContext const &context)
  : d(new HorizontalFileItemPrivate)
{
  d->extractGlobalStructRanges(context);
}

HorizontalFileItem::~HorizontalFileItem()
{
  delete d;
}

QRectF HorizontalFileItem::boundingRect() const
{
  return QRectF();
}

void HorizontalFileItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}
