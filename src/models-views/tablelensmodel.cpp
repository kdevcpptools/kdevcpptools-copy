#include "tablelensmodel.h"

class TableLensModelPrivate
{
public:
  ResultDataModel::Ptr mData;
  bool                 mFilesHorizontal;

public:
  TableLensModelPrivate(const ResultDataModel::Ptr &data);

  QVariant dataHorizontal(const QModelIndex &index) const;
  QVariant dataVertical(const QModelIndex &index) const;

  QVariant headerHorizontal(int section, Qt::Orientation orientation) const;
  QVariant headerVertical(int section, Qt::Orientation orientation) const;
};

TableLensModelPrivate::TableLensModelPrivate(const ResultDataModel::Ptr &data)
  : mData(data), mFilesHorizontal(false)
{ }

QVariant TableLensModelPrivate::dataHorizontal(const QModelIndex &index) const
{
  const QString query = mData->query(index.row());

  if (index.column()) {
    const QString file = mData->file(index.column() - 1);
    return mData->resultCount(query, file);
  } else {
    return mData->resultTotalForQuery(query);
  }
}

QVariant TableLensModelPrivate::dataVertical(const QModelIndex &index) const
{
  const QString file = mData->file(index.row());

  if (index.column()) {
    const QString query = mData->query(index.column() - 1);
    return mData->resultCount(query, file);
  } else {
    return mData->resultTotalForFile(file);
  }
}

QVariant TableLensModelPrivate::headerHorizontal(int section, Qt::Orientation orientation) const
{
  if (orientation == Qt::Horizontal) {
    if (section)
      return mData->file(section - 1); // Files as column labels
    else
      return QString("Total");         // The first column is the total column
  } else
    return mData->query(section);      // Queries as row labels
}

QVariant TableLensModelPrivate::headerVertical(int section, Qt::Orientation orientation) const
{
  if (orientation == Qt::Horizontal) {
    if (section)
      return mData->query(section - 1); // Queries as column labels
    else
      return QString("Total");          // The first column is the total column
  } else
    return mData->file(section);        // Files as row labels
}

/// TableLensModel

TableLensModel::TableLensModel(const ResultDataModel::Ptr &data)
  : d_ptr(new TableLensModelPrivate(data))
{
  // TODO: Use [rows|cols][Inserted|Deleted]
  connect(data.data(), SIGNAL(cleared()), SIGNAL(layoutChanged()));
  connect(data.data(), SIGNAL(fileAboutToBeInserted()), SIGNAL(layoutAboutToBeChanged()));
  connect(data.data(), SIGNAL(fileInserted(QString)), SIGNAL(layoutChanged()));
  connect(data.data(), SIGNAL(querySortOrderAboutToBeChanged()), SIGNAL(layoutAboutToBeChanged()));
  connect(data.data(), SIGNAL(querySortOrderChanged()), SIGNAL(layoutChanged()));
}

TableLensModel::~TableLensModel()
{
  delete d_ptr;
}

int TableLensModel::columnCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;

  if (d_ptr->mFilesHorizontal)
    return d_ptr->mData->fileCount() + 1;
  else
    return d_ptr->mData->queryCount() + 1;
}

QVariant TableLensModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  switch(role) {
  case Qt::ToolTipRole:
  {
    QString fileName = headerData(index.row(), Qt::Vertical, Qt::DisplayRole).toString();
    QString queryId = headerData(index.column(), Qt::Horizontal, Qt::DisplayRole).toString();
    int count = d_ptr->dataVertical(index).toInt();
    if (d_ptr->mFilesHorizontal) {
      qSwap<QString>(fileName, queryId);
      count = d_ptr->dataHorizontal(index).toInt();
    }

    if (index.column() == 0) {
      int projectTotal = d_ptr->mData->resultTotal();

      return "File: " + fileName + "\n"
        + "Total hits in file: " + QString::number(count) + "\n"
        + "Total hits in project: " + QString::number(projectTotal);
    } else {
      int fileTotal = d_ptr->mData->resultTotalForFile(fileName);
      int queryTotal = d_ptr->mData->resultTotalForQuery(queryId);

      return "File: " + fileName + "\n"
        + "Query: " + queryId + + " [" + QString::number(count) + " hits]\n"
        + "Total hits in file: " + QString::number(fileTotal) + "\n"
        + "Total hits for query in project: " + QString::number(queryTotal);
    }
  }
  case Qt::DisplayRole:
    if (d_ptr->mFilesHorizontal)
      return d_ptr->dataHorizontal(index);
    else
      return d_ptr->dataVertical(index);
  case Qt::UserRole:
    if (d_ptr->mFilesHorizontal)
      return d_ptr->mData->maxQueryResult().second;
    else
      return d_ptr->mData->maxFileResult().second;
  case Qt::UserRole + 1:
    if (d_ptr->mFilesHorizontal) {
      if (index.column() == 0)
        return d_ptr->mData->maxQueryResult().second;

      const QString file = d_ptr->mData->file(index.column() - 1);
      return d_ptr->mData->maxHitCountForFile(file);
    } else {
      if (index.column() == 0 )
        return d_ptr->mData->maxFileResult().second;

      const QString query = d_ptr->mData->query(index.column() - 1);
      return d_ptr->mData->maxHitCountForQuery(query);
    }
  };

  return QVariant(); // Invalid.
}

QVariant TableLensModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant();

  if (d_ptr->mFilesHorizontal)
    return d_ptr->headerHorizontal(section, orientation);
  else
    return d_ptr->headerVertical(section, orientation);
}

int TableLensModel::rowCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;

  if (d_ptr->mFilesHorizontal)
    return d_ptr->mData->queryCount();
  else
    return d_ptr->mData->fileCount();
}

void TableLensModel::switchHeaders()
{
  layoutAboutToBeChanged();
  d_ptr->mFilesHorizontal = !d_ptr->mFilesHorizontal;
  layoutChanged();
}
