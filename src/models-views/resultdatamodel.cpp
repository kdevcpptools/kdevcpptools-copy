#include "resultdatamodel.h"

#include <QtCore/QMultiMap>

#include <language/editor/simplerange.h>

using namespace KDevelop;

class ResultDataModelPrivate
{
public:
  QMap<QString, QMap<QString, QueryResult> >  mResults; /// File => [QueryId => QueryResult]

  QPair<QString, int>          mFileWithMostHits;
  QPair<QString, int>          mQueryWithMostHits;

  QMap<QString, int>           mTotalHitsPerFile;
  QMap<QString, int>           mTotalHitsPerQuery;

  QMap<QString, int>           mMaxCountPerFile;  /// Filename, number of hits of the query with most hits in this file
  QMultiMap<int, QString>      mFileForMaxCount;  /// used for sorting files by maximum hits.
  QMap<QString, int>           mMaxCountPerQuery; /// Query name, number hits in the file in which this query has most hits
  QMultiMap<int, QString>      mQueryForMaxCount; /// used for sorting the Query name by total hits
};

ResultDataModel::ResultDataModel()
  : d_ptr(new ResultDataModelPrivate)
{ }

ResultDataModel::~ResultDataModel()
{
  delete d_ptr;
}

void ResultDataModel::add(QString const &file, QueryResult const &result)
{
  Q_D(ResultDataModel);

  int oldValue = 0;
  int newValue = result.hits().size();
  if (!d->mResults.contains(file)) {
    emit fileAboutToBeInserted();
    d->mResults[file][result.queryId()] = result;
    Q_ASSERT(!d->mFileForMaxCount.values().contains(file));
    d->mFileForMaxCount.insert(0, file);
    emit fileInserted(file);
  } else {
    if ( d->mResults[file].contains(result.queryId()) ) {
      oldValue = d->mResults[file][result.queryId()].hits().size();
    }
    d->mResults[file][result.queryId()] = result;
  }
  int diff = newValue - oldValue;

  // Update cached maximums
  int cnt = d->mTotalHitsPerFile.value(file) + diff;
  d->mTotalHitsPerFile[file] = cnt;
  if (cnt > d->mFileWithMostHits.second) {
    d->mFileWithMostHits.first = file;
    d->mFileWithMostHits.second = cnt;
  }

  cnt = d->mTotalHitsPerQuery.value(result.queryId()) + diff;
  d->mTotalHitsPerQuery[result.queryId()] = cnt;
  if (cnt > d->mQueryWithMostHits.second) {
    d->mQueryWithMostHits.first = result.queryId();
    d->mQueryWithMostHits.second = cnt;
  }

  if (newValue == 0 && oldValue) {
    emit querySortOrderAboutToBeChanged();
    if (d->mMaxCountPerFile.value(file, -1) == oldValue) {
      int newMax = 0;
      QMap< QString, QueryResult >::const_iterator it = d->mResults[file].constBegin();
      while ( it != d->mResults[file].constEnd() ) {
        newMax = qMax(newMax, it.value().hits().size());
        ++it;
      }
      d->mFileForMaxCount.remove(d->mFileForMaxCount.key(file, -1), file);
      d->mFileForMaxCount.insert(newMax, file);
      d->mMaxCountPerFile[file] = newMax;
      Q_ASSERT(d->mMaxCountPerFile.size() == d->mFileForMaxCount.keys().size());
    }
    if (d->mMaxCountPerQuery.value(result.queryId(), -1) == oldValue) {
      int newMax = 0;
      QMap< QString, QMap< QString, QueryResult > >::const_iterator it = d->mResults.constBegin();
      while ( it != d->mResults.constEnd() ) {
        newMax = qMax(newMax, it->value(result.queryId()).hits().size());
        ++it;
      }
      d->mQueryForMaxCount.remove(d->mQueryForMaxCount.key(result.queryId(), -1), result.queryId());
      d->mQueryForMaxCount.insert(newMax, result.queryId());
      d->mMaxCountPerQuery[result.queryId()] = newMax;
      Q_ASSERT(d->mMaxCountPerQuery.size() == d->mQueryForMaxCount.keys().size());
    }
    emit querySortOrderChanged();
    return;
  }

  cnt = newValue;
  if ( cnt > d->mMaxCountPerQuery.value(result.queryId(), -1)
      || cnt > d->mMaxCountPerFile.value(file, -1) )
  {
    emit querySortOrderAboutToBeChanged();
    if (cnt > d->mMaxCountPerQuery.value(result.queryId(), -1)) {
      d->mQueryForMaxCount.remove(d->mQueryForMaxCount.key(result.queryId(), -1), result.queryId());
      d->mQueryForMaxCount.insert(cnt, result.queryId());

      d->mMaxCountPerQuery.insert(result.queryId(), cnt);
      Q_ASSERT(d->mMaxCountPerQuery.size() == d->mQueryForMaxCount.keys().size());
    }

    if (cnt > d->mMaxCountPerFile.value(file, -1)) {
      d->mFileForMaxCount.remove(d->mFileForMaxCount.key(file, -1), file);
      d->mFileForMaxCount.insert(cnt, file);

      d->mMaxCountPerFile.insert(file, cnt);
      Q_ASSERT(d->mMaxCountPerFile.size() == d->mFileForMaxCount.keys().size());
    }
    emit querySortOrderChanged();
  }

}

void ResultDataModel::clear()
{
  d_ptr->mResults.clear();

  d_ptr->mFileWithMostHits = qMakePair(QString(), 0);
  d_ptr->mQueryWithMostHits = qMakePair(QString(), 0);

  d_ptr->mTotalHitsPerFile.clear();
  d_ptr->mTotalHitsPerQuery.clear();

  d_ptr->mMaxCountPerFile.clear();
  d_ptr->mFileForMaxCount.clear();

  d_ptr->mMaxCountPerQuery.clear();
  d_ptr->mQueryForMaxCount.clear();

  emit cleared();
}

int ResultDataModel::maxHitCountForFile(QString const &file) const
{
  return d_ptr->mMaxCountPerFile.value(file);
}

int ResultDataModel::maxHitCountForQuery(QString const &query) const
{
  return d_ptr->mMaxCountPerQuery.value(query);
}

QPair<QString, int> ResultDataModel::maxFileResult() const
{
  return d_ptr->mFileWithMostHits;
}

QPair<QString, int> ResultDataModel::maxQueryResult() const
{
  return d_ptr->mQueryWithMostHits;
}


QString ResultDataModel::file(int i) const
{
  Q_ASSERT(i >= 0 && i < d_ptr->mFileForMaxCount.size());
  return d_ptr->mFileForMaxCount.values().at(d_ptr->mFileForMaxCount.values().size() - i - 1);
}

int ResultDataModel::fileCount() const
{
  return d_ptr->mFileForMaxCount.size();
}

QString ResultDataModel::query(int i) const
{
  Q_ASSERT(i >= 0 && i < d_ptr->mQueryForMaxCount.values().size());
  return d_ptr->mQueryForMaxCount.values().at(d_ptr->mQueryForMaxCount.values().size() - i - 1);
}

QString ResultDataModel::query(QString const &file, int i) const
{
  Q_ASSERT(d_ptr->mResults.contains(file));

  return d_ptr->mResults[file].keys().at(i);
}

int ResultDataModel::queryCount(QString const &file) const
{
  if (file.isEmpty()) {
    return d_ptr->mQueryForMaxCount.count();
  } else {
    if (!d_ptr->mResults.contains(file)) {
      return 0;
    }
    return d_ptr->mResults[file].count();
  }
}

QList<SimpleRange> ResultDataModel::ranges(const QString &query, const QString &file) const
{
  Q_ASSERT(d_ptr->mResults.contains(file));
  Q_ASSERT(d_ptr->mResults[file].contains(query));

  QList<SimpleRange> ranges;
  foreach( QueryHit::Ptr use, d_ptr->mResults[file][query].hits() ) {
    ranges << use->range().castToSimpleRange();
  }
  return ranges;
}

void ResultDataModel::removeResultsForFile(const QString &file)
{
  emit querySortOrderAboutToBeChanged();
  QMap< QString, QueryResult >::const_iterator it = d_ptr->mResults[file].constBegin();
  while ( it != d_ptr->mResults[file].constEnd() ) {
    d_ptr->mTotalHitsPerQuery[it.key()] -= it.value().hits().size();
    ++it;
  }
  d_ptr->mResults.remove(file);
  d_ptr->mMaxCountPerFile.remove(file);
  d_ptr->mFileForMaxCount.remove(d_ptr->mFileForMaxCount.key(file, -1), file);
  d_ptr->mTotalHitsPerFile.remove(file);

  emit querySortOrderChanged();
  // TODO: Emit signals that items are (going to be) removed
}

QueryResult ResultDataModel::result(const QString &queryId, const QString &fileName) const
{
  Q_ASSERT(d_ptr->mResults.contains(fileName));
  Q_ASSERT(d_ptr->mResults[fileName].contains(queryId));
  return d_ptr->mResults[fileName][queryId];
}

int ResultDataModel::resultCount(const QString &query, const QString &fileName) const
{
  Q_ASSERT(d_ptr->mResults.contains(fileName));
  if (!d_ptr->mResults[fileName].contains(query)) {
    // this can happen when we execute more than one query on more than one file:
    // X queries get executed on a single file
    // first query gets executed on second file => all other already executed queries
    //                                             will not have any result for this new file
    return 0;
  }
  return result(query, fileName).hits().count();
}

int ResultDataModel::resultTotal() const
{
  int total = 0;
  foreach (int count, d_ptr->mTotalHitsPerQuery.values())
    total += count;

  return total;
}

int ResultDataModel::resultTotalForFile(const QString &file) const
{
  return d_ptr->mTotalHitsPerFile.value(file);
}

int ResultDataModel::resultTotalForQuery(const QString &query) const
{
  return d_ptr->mTotalHitsPerQuery.value(query);
}
