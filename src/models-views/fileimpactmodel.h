#ifndef FILEIMPACTMODEL_H
#define FILEIMPACTMODEL_H

#include <QtCore/QAbstractTableModel>

#include "resultdatamodel.h"

class FileImpactModelPrivate;

class FileImpactModel : public QAbstractTableModel
{
public:
  FileImpactModel(QString const &file, ResultDataModel::Ptr const &concreteModel);

  virtual int	columnCount(const QModelIndex &parent = QModelIndex()) const;

  virtual QVariant data(const QModelIndex &index, int role) const;

  virtual QVariant headerData(int section, Qt::Orientation orientation,
                              int role = Qt::DisplayRole) const;

  virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

  void setFile(QString const &file);

private:
  FileImpactModelPrivate *d_ptr;
  Q_DECLARE_PRIVATE(FileImpactModel)
  Q_DISABLE_COPY(FileImpactModel)
};

#endif // FILEIMPACTMODEL_H
