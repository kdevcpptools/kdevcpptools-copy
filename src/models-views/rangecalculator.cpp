#include "rangecalculator.h"

#include <QtCore/QRect>

#include <language/editor/simplerange.h>

using namespace KDevelop;

/// RangeCalculatorPrivate

class RangeCalculatorPrivate
{
public:
  CodeRepresentation::Ptr mCodeRepresentation;
  QRectF             mCurrentDocumentRect;
  double             mCurrentZoomCenter;
  double             mZoomLevel;

  RangeCalculatorPrivate();

  /// Returns the document position, given a pos in the visual part (i.e. in the
  /// range [0..1].
  double normalizedDocumentPosition(double pos) const;

  /// Returns the document length (number of chars)
  int codeLength() const;

  /// Returns the char offset for the given @p cursor.
  uint offsetForCursor(const SimpleCursor& cursor) const;
  /// Returns a cursor for the given @p offset.
  SimpleCursor cursorForOffset(uint offset) const;
};

int RangeCalculatorPrivate::codeLength() const
{
  return mCodeRepresentation->text().length();
}

SimpleCursor RangeCalculatorPrivate::cursorForOffset(uint offset) const
{
  for(int l = 0; l < mCodeRepresentation->lines(); ++l) {
    unsigned int lineLength = mCodeRepresentation->line(l).length();
    if (offset < lineLength) {
      return SimpleCursor(l, offset);
    } else {
      // +1 for the linefeed
      offset -= lineLength + 1;
    }
  }

  return SimpleCursor::invalid();
}

uint RangeCalculatorPrivate::offsetForCursor(const KDevelop::SimpleCursor& cursor) const
{
  uint offset = 0;

  if (!cursor.isValid() || cursor.line >= mCodeRepresentation->lines()) {
    qDebug() << "[RangeCalculatorPrivate::offsetForCursor] SOMETHING WENT WRONG";
    return 0;
  }

  for(int l = 0; l < cursor.line - 1; ++l) {
    // +1 for the linefeed
    offset += mCodeRepresentation->line(l).length() + 1;
  }

  return offset + cursor.column;
}

RangeCalculatorPrivate::RangeCalculatorPrivate()
  : mCurrentDocumentRect(0,0,1,1) // x, y, width, height (normalized from 0..1)
  , mCurrentZoomCenter(0.5)
  , mZoomLevel(1)
{ }

double RangeCalculatorPrivate::normalizedDocumentPosition(double pos) const
{
  Q_ASSERT(pos >= 0 && pos <= 1);

  // The mCurrentDocumentRect ranges on the x-as from 0..1 when at 100 %. This is also
  // the minumum because showing more than the file does not make sense.

  // When zoomed in mCurrentDocumentRect will range from x..y with x < 0 and y > 1
  // depending on the zoom and the center of the zoom.

  // The pos, passed to this function lies in the range 0..1
  // this must be translated to the absolutem position in the document.

  // First calculate the position in the range x..y. mCurrentDocumentRect.left() is
  // negative when zoomed in, 0 otherwise, so subtract it.
  return 1 / (mCurrentDocumentRect.width() / (pos - mCurrentDocumentRect.left()));
}

/// RangeCalculator

RangeCalculator::RangeCalculator()
  : d_ptr(new RangeCalculatorPrivate())
{ }

RangeCalculator::~RangeCalculator()
{
  delete d_ptr;
}

QRectF RangeCalculator::calculateVisibleRect(const SimpleRange &range, const QRect &visibleRect, int level) const
{
  Q_D(const RangeCalculator);

  const double startfactor = static_cast<double>(d->offsetForCursor(range.start)) / d->codeLength();
  const double endFactor = static_cast<double>(d->offsetForCursor(range.end)) / d->codeLength();

  // Now translate the start and end factor to x ranges in mCurrentDocument.
  double curStart = d->mCurrentDocumentRect.left() + startfactor * d->mCurrentDocumentRect.width();
  if (curStart < 0)
    curStart = 0;
  if (curStart > 1)
    return QRectF();

  double curEnd = d->mCurrentDocumentRect.left() + endFactor * d->mCurrentDocumentRect.width();
  if (curEnd > 1)
    curEnd = 1;
  if (curEnd < 0)
    return QRectF();


  // QRect ( int x, int y, int width, int height )
  QRectF result(visibleRect.left() + curStart * visibleRect.width(),
                visibleRect.top(),
                curEnd * visibleRect.width() - curStart * visibleRect.width(),
                visibleRect.height());

  double adjustment = level == 1 ? level : level / 2.0;
  result.setTop(result.top() + adjustment);
  result.setBottom(result.bottom() - adjustment);
  return result;
}

SimpleCursor RangeCalculator::cursosForX(double x) const
{
  Q_D(const RangeCalculator);

  // Normalized position in the document before zooming (further).
  double posInDoc = d->normalizedDocumentPosition(x);

  // now lets see which byte the normalized value is referring too.
  uint byte = posInDoc * d->codeLength();
  return d->cursorForOffset(byte);
}

void RangeCalculator::init(CodeRepresentation::Ptr representation)
{
  Q_D(RangeCalculator);

  d->mCodeRepresentation = representation;
  d->mCurrentDocumentRect = QRectF(0, 0, 1, 1); // x, y, width, height (both normalized from 0..1)
  d->mCurrentZoomCenter = 0.5;
  d->mZoomLevel = 1;
}

double RangeCalculator::xForCursor(const KDevelop::SimpleCursor &cursor) const
{
  if (!cursor.isValid())
    return 0;

  Q_D(const RangeCalculator);
  return static_cast<double>(d->offsetForCursor(cursor)) / d->codeLength();
}

/// zoompos is normalized in the visual part of the document.
void RangeCalculator::zoom(double zoomPos, ZoomDirection direction)
{
  Q_D(RangeCalculator);

  /// Normalized position in the document before zooming (further).
  double posBeforeZoom = d->normalizedDocumentPosition(zoomPos);

  double newRectWidth;
  if (direction == In) {
    newRectWidth = d->mCurrentDocumentRect.width() * 1.1;
    d->mZoomLevel *= 1.1;
  } else {
    newRectWidth = d->mCurrentDocumentRect.width() * 0.9;
    d->mZoomLevel *= 0.9;
    if (d->mZoomLevel < 1) {
      d->mZoomLevel = 1;
      newRectWidth = 1;
      d->mCurrentDocumentRect.setLeft(0);
    }
  }

  d->mCurrentDocumentRect.setWidth(newRectWidth); // First asume it only grows to the right
  double posAfterZoom = d->mCurrentDocumentRect.left() + newRectWidth * posBeforeZoom;
  double delta = posBeforeZoom - posAfterZoom;
  d->mCurrentDocumentRect.translate(delta, 0);    // Now adjust it to keep the posAfterZoom at zoomPos
}
