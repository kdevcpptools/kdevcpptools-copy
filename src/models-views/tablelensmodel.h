#ifndef COUNTMODEL_H
#define COUNTMODEL_H

#include <QtCore/QAbstractTableModel>
#include <QtCore/QSharedPointer>

#include "resultdatamodel.h"

class TableLensModelPrivate;

class TableLensModel : public QAbstractTableModel
{
  TableLensModel(const TableLensModel &other);
  TableLensModel &operator=(const TableLensModel &other);

public:
  TableLensModel(const ResultDataModel::Ptr &data);

  ~TableLensModel();

  virtual int	columnCount(const QModelIndex &parent = QModelIndex()) const;

  /**
   * Returns:
   *
   * Files in the horizontal headers:
   *
   * + Qt::ToolTipRole || Qt::DisplayRole
   *   - column >  0: The number of hits for query[row] in file[column].
   *   - column == 0: The total number of hits of query[row] in all files.
   *
   * + Qt::UserRole
   *   - Always     : The maximum of the sum of hits per query over all files.
   *                  i.e. The number of hits of the query that has most hits in
   *                  the project.
   *
   * + Otherwise:
   *   - Always     : QVariant().
   *
   * Files in the vertical headers:
   *
   * + Qt::ToolTipRole || Qt::DisplayRole
   *   - column >  0: The number of hits for query[column] in file[row].
   *   - column == 0: The total number of hits of all queries in file[row].
   *
   * + Qt::UserRole
   *   - Always     : The maximum of the sum of hits per query over all files.
   *                  i.e. The number of hits of the query that has most hits in
   *                  the project.
   *
   * + Otherwise:
   *   - Always     : QVariant().
   */
  virtual QVariant data(const QModelIndex &index, int role) const;

  virtual QVariant headerData(int section, Qt::Orientation orientation,
                              int role = Qt::DisplayRole) const;

  virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

  /// Sets the current horizontal headers as vertical headers and the current
  /// vertical headers as the horizontal headers.
  void switchHeaders();

private:
  TableLensModelPrivate *d_ptr;
  Q_DECLARE_PRIVATE(TableLensModel)
};

#endif // COUNTMODEL_H
