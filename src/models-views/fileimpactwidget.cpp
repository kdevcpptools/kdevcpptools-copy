#include "fileimpactwidget.h"

#include <QtCore/QDir>
#include <QtGui/QSortFilterProxyModel>

#include <KDE/KTextEditor/Document>
#include <KDE/KTextEditor/View>

#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>

#include "bardelegate.h"
#include "fileimpactmodel.h"
#include "rangedelegate.h"
#include "textdelegate.h"
#include "ui_fileimpactwidget.h"

using namespace KDevelop;

FileImpactWidget::FileImpactWidget(ResultDataModel::Ptr const &dataModel,
                                   QString const &projectPath,
                                   QString const &fileName)
  : mFile(fileName)
  , mModel(new FileImpactModel(fileName, dataModel))
  , mProjectPath(projectPath)
  , mRangeDelegate(new RangeDelegate(fileName, this))
  , mTextDelegate(new TextDelegate(this))
  , mUi(new Ui::FileImpactWidget())
  , mView(0)
{
  mModel->setParent(this);

  QSortFilterProxyModel *sortFilter = new QSortFilterProxyModel(this);
  sortFilter->setDynamicSortFilter(true);
  sortFilter->setSourceModel(mModel);

  mUi->setupUi(this);
  mUi->mImpactTable->viewport()->installEventFilter(mRangeDelegate);
  mUi->mImpactTable->setItemDelegateForColumn(0, mTextDelegate);
  mUi->mImpactTable->setItemDelegateForColumn(1, mRangeDelegate);
  mUi->mImpactTable->setItemDelegateForColumn(2, new BarDelegate(this));
  mUi->mImpactTable->setModel(sortFilter);
  mUi->mImpactTable->horizontalHeader()->setSortIndicator(2, Qt::DescendingOrder);
  const int width = QFontMetrics(mUi->mImpactTable->font()).width("SomeLongTypeName<Template>");
  mUi->mImpactTable->horizontalHeader()->setDefaultSectionSize(width);
  mUi->mImpactTable->horizontalHeader()->resizeSection(1, 450);

  // Pass edit action through
  connect(mRangeDelegate, SIGNAL(applyAllRequested()),
          SIGNAL(applyAllRequested()));
  connect(mRangeDelegate, SIGNAL(applyAllForQueryRequested(QString)),
          SIGNAL(applyAllForQueryRequested(QString)));
  connect(mRangeDelegate, SIGNAL(applyForQueryAtRangeRequested(QString, KDevelop::SimpleRange)),
          SIGNAL(applyForQueryAtRangeRequested(QString, KDevelop::SimpleRange)));

  // View related signals
  connect(mRangeDelegate, SIGNAL(heightChanged(int)),
          SLOT(adjustRowHeight(int)));
  connect(mRangeDelegate, SIGNAL(cursorClicked(KDevelop::SimpleCursor)),
          SLOT(scrollToPosition(KDevelop::SimpleCursor)));
  connect(mRangeDelegate, SIGNAL(queryResultClicked(KDevelop::SimpleRange)),
          SLOT(selectRange(KDevelop::SimpleRange)));
  connect(mRangeDelegate, SIGNAL(zoomLevelChanged()),
          SLOT(updateViewPort()));

  connect(ICore::self()->documentController(), SIGNAL(documentActivated(KDevelop::IDocument*)),
          SLOT(handleDocumentActivation(KDevelop::IDocument*)));
}

/// Public slots

void FileImpactWidget::setFile(QString const &file)
{
  if (mFile == file) {
    mModel->setFile(file); // Hack, needed to make sure that the model is reset
    return;
  }

  mFile = file;

  handleDocumentActivation(ICore::self()->documentController()->documentForUrl(currentFile()));

  mModel->setFile(file);
  mRangeDelegate->setFile(currentFile());

  mTextDelegate->setText(QString());

  mUi->mImpactTable->sortByColumn(2);
  updateViewPort();
}

void FileImpactWidget::setProjectPath(QString const &path)
{
  mProjectPath = path;
}

void FileImpactWidget::setSelectedQuery(const QString &query)
{
  mTextDelegate->setText(query);
  updateViewPort();
}

/// Private Slots

void FileImpactWidget::adjustRowHeight(int newHeight)
{
  mUi->mImpactTable->verticalHeader()->setDefaultSectionSize(newHeight);
}

void FileImpactWidget::handleDocumentActivation(KDevelop::IDocument *activatedDocument)
{
  if (!activatedDocument || activatedDocument->url() != currentFile()) {
    if (mView) {
      disconnect(mView, SIGNAL(cursorPositionChanged(KTextEditor::View*, KTextEditor::Cursor)));
      mView = 0;
    }
    mRangeDelegate->setCursor(SimpleCursor::invalid());
    return;
  }

  KTextEditor::Document *textDoc = activatedDocument->textDocument();
  if (!textDoc) {
    return;
  }

  mView = textDoc->activeView();
  if (!mView) {
    return;
  }
  connect(mView, SIGNAL(cursorPositionChanged(KTextEditor::View*, KTextEditor::Cursor)),
          SLOT(updateCursorPos(KTextEditor::View*, KTextEditor::Cursor)));
  mRangeDelegate->setCursor(SimpleCursor(mView->cursorPosition()));
}

void FileImpactWidget::selectRange(KDevelop::SimpleRange const &range)
{
  ICore::self()->documentController()->openDocument(
    KUrl(mProjectPath + QDir::separator() + mFile), range.textRange());
}

void FileImpactWidget::scrollToPosition(KDevelop::SimpleCursor const &cursor)
{
  ICore::self()->documentController()->openDocument(
    KUrl(mProjectPath + QDir::separator() + mFile));
  IDocument *doc = ICore::self()->documentController()->activeDocument();
  doc->setCursorPosition(cursor.textCursor());
}

void FileImpactWidget::updateCursorPos(KTextEditor::View *view, const KTextEditor::Cursor &cursor)
{
    if (mView != view)
        return;

    SimpleCursor cur(cursor);
    mRangeDelegate->setCursor(cur);
    updateViewPort();
}

void FileImpactWidget::updateViewPort()
{
  mUi->mImpactTable->viewport()->update();
}

/// Private methods
QString FileImpactWidget::currentFile() const
{
  return QFileInfo(mProjectPath + QDir::separator() + mFile).canonicalFilePath();
}

