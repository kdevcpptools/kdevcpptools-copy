#ifndef TEXTDELEGATE_H
#define TEXTDELEGATE_H

#include <QtGui/QItemDelegate>

class TextDelegate : public QItemDelegate
{
public:
  TextDelegate(QObject *parent = 0);

  virtual void paint(QPainter *painter,
                     const QStyleOptionViewItem &option,
                     const QModelIndex &index) const;

  void setText(const QString &text);

private:
  QString mText;
};

#endif // TEXTDELEGATE_H
