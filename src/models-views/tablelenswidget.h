#ifndef TABLELENSWIDGET_H
#define TABLELENSWIDGET_H

#include <QtGui/QWidget>

#include "resultdatamodel.h"

namespace Ui {
class TableLensWidget;
}

class BarDelegate;
class TableLensFilter;
class TableLensModel;

class TableLensWidget : public QWidget
{
  Q_OBJECT

public:
  TableLensWidget(ResultDataModel::Ptr const &dataModel);

  QFont font() const;
  void setFont(QFont const &font);

  /**
   * When due to zooming the font gets smaller than minimumFontSize() the
   * vertical header will be hidden. The default value is 5.5.
   *
   * Note: When the mimumFontSize > font.pointSizeF() the vertical header will
   *       never show up.
   */
  qreal minimumFontSize() const;
  void setMinimumFontSize(qreal size);

private slots:
  void switchColumnScale(bool enabled);
  void switchHeaders();
  void toggleGrid(bool toggle);
  void toggleShowCounts(bool toggle);
  void updateFilter(int filter);
  void zoom(int pct);

private:
  BarDelegate         *mBarDelegate;
  qreal                mMinimumFontSize;
  TableLensFilter     *mTableLensFilter;
  Ui::TableLensWidget *mUi;
};

#endif // TABLELENSWIDGET_H
