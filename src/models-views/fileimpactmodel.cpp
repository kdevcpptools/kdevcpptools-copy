#include "fileimpactmodel.h"

#include <language/editor/simplerange.h>

using namespace KDevelop;

Q_DECLARE_METATYPE(QList<SimpleRange>)

class FileImpactModelPrivate
{
public:
  FileImpactModelPrivate(QString const &file, ResultDataModel::Ptr const &concreteModel);

public:
  ResultDataModel::Ptr mData;
  QString              mFile;
};

FileImpactModelPrivate::FileImpactModelPrivate(QString const &file, ResultDataModel::Ptr const &concreteModel)
  : mData(concreteModel), mFile(file)
{
}

FileImpactModel::FileImpactModel(QString const &file, ResultDataModel::Ptr const &concreteModel)
  : d_ptr(new FileImpactModelPrivate(file, concreteModel))
{
}

int FileImpactModel::columnCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;

  // return 3; // If you also want to see the bardelegate next to the rangebar.
  return 2;
}

QVariant FileImpactModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  switch (role) {
  case Qt::DisplayRole:
    {
      const QString query = d_ptr->mData->query(d_ptr->mFile, index.row());
      if (index.column() == 0)
        return query;

      if (index.column() == 1)
        return QVariant::fromValue(d_ptr->mData->ranges(query, d_ptr->mFile));

      if (index.column() == 2)
        return d_ptr->mData->resultCount(query, d_ptr->mFile);
    }
  case Qt::UserRole:
  case Qt::UserRole + 1:
    if (index.column() == 2)
      return d_ptr->mData->maxHitCountForFile(d_ptr->mFile);
  }

  return QVariant();
}

QVariant FileImpactModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
    if (section == 0)
      return "Query Id";

    if (section == 1)
      return "Ranges";

    if (section == 2)
      return "Count";
  }

  return QVariant();
}

int FileImpactModel::rowCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;

  return d_ptr->mData->queryCount(d_ptr->mFile);
}

void FileImpactModel::setFile(QString const &file)
{
  if (file != d_ptr->mFile) {
    //emit layoutAboutToBeChanged(); // This crashes ModelTest, for some reason unclear to me.
    d_ptr->mFile = file;
    emit layoutChanged();
  } else {
    reset();
  }
}
