#ifndef RANGEDELEGATE_P_H
#define RANGEDELEGATE_P_H

#include <KDE/KConfigGroup>

#include <language/duchain/topducontext.h>
#include <languages/cpp/parser/default_visitor.h>
#include <languages/cpp/parser/parsesession.h>

#include "rangecalculator.h"

class QAction;
class QPainter;
class RangeDelegate;

namespace KDevelop {
class SimpleRange;
}

using namespace KDevelop;

class RangeDelegatePrivate : public QObject, public DefaultVisitor
{
  Q_OBJECT

public:
  RangeDelegatePrivate(RangeDelegate *q, QString const &fileName);

  /// Returns the best matching declaration, i.e. the declaration containing x
  /// and which start lies closest to x.
  KDevelop::DeclarationPointer declarationForX(int x) const;

  void loadFile(QString const &fileName);

  KDevelop::SimpleRange rangeForX(int row, int x);

  ParseSession *session();

  /// Visitor functions, implement the ones for which we want to show colored patches
  /// on the bar.
  int nodeLevel(AST *node);
  void paintAST(QPainter *painter, QRect const &visibleRect);
  QRectF paintNode(AST *, const QString &name);

  virtual void visitCastExpression(CastExpressionAST *);
  virtual void visitCatchStatement(CatchStatementAST *);
  virtual void visitClassSpecifier(ClassSpecifierAST *);
  virtual void visitDoStatement(DoStatementAST *);
  virtual void visitForStatement(ForStatementAST *);
  virtual void visitFunctionCall(FunctionCallAST *);
  virtual void visitFunctionDefinition(FunctionDefinitionAST *);
  virtual void visitIfStatement(IfStatementAST *);
  virtual void visitSignalSlotExpression(SignalSlotExpressionAST *);
  virtual void visitStringLiteral(StringLiteralAST *);
  virtual void visitSwitchStatement(SwitchStatementAST *);
  virtual void visitTryBlockStatement(TryBlockStatementAST *);
  virtual void visitWhileStatement(WhileStatementAST *);

private slots:
  void applyAllTransforms();
  void applySelectedTransform();
  void applyTransformsForSelectedQuery();

public: /// Members
  // we assume rows, x offset should be the same
  QList< QPair<QRectF, QString> >       mPainterCache;
  // the rect based on which the mPainterCache was calculated
  QRect                                 mCacheRect;
  RangeDelegate                        *q;
  RangeCalculator                       mCalculator;
  QMap<QRectF, DeclarationPointer>      mRectDeclarationMap;
  int                                   mCurrentHeight;
  QString                               mFileName;
  QMap<int, QMap<SimpleRange, QRectF> > mRangeRectMap;
  SimpleCursor                          mCursor;

  double                                mTmpZoom;

  static const int                      mMinimumHeight = 18;
  static const int                      mMaximumHeight = 36;

  // DUChain related members
  ReferencedTopDUContext                mTopContext;

  // Needed for the visitor
  const KConfigGroup                    mColorConfig;
  QRect                                 mRect;

  // Actions supported by this view
  QAction                              *mApplyAction;
  QAction                              *mApplyAllAction;
  QAction                              *mApplyFromAllQueriesAction;
  QString                               mCurrentQueryId;
  SimpleRange                           mCurrentRange;
};

#endif // RANGEDELEGATE_P_H
