#include "rangedelegate_p.h"
#include "rangedelegate.h"

#include <QtGui/QAction>
#include <QtGui/QAbstractItemView>
#include <QtGui/QPainter>
#include <QtGui/QWheelEvent>

#include <KDE/KSharedConfig>

#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/functiondeclaration.h>
#include <language/duchain/functiondefinition.h>
#include <languages/cpp/cppduchain/cppeditorintegrator.h>

#include "util.h"

bool operator <(QRectF const &lhs, QRectF const &rhs)
{
  return lhs.left() < rhs.left();
}

RangeDelegatePrivate::RangeDelegatePrivate(RangeDelegate *q, QString const &fileName)
  : q(q)
  , mCurrentHeight(mMinimumHeight)
  , mCursor(SimpleCursor::invalid())
  , mTmpZoom(0)
  , mColorConfig(KGlobal::config()->group("cpp_query_engine"))
  , mApplyAction(new QAction("Apply", q))
  , mApplyAllAction(new QAction("Apply All For Current Query", q))
  , mApplyFromAllQueriesAction(new QAction("Apply All for All Queries", q))
{
  loadFile(fileName);

  connect(mApplyAction, SIGNAL(triggered()), SLOT(applySelectedTransform()));
  connect(mApplyAllAction, SIGNAL(triggered()), SLOT(applyTransformsForSelectedQuery()));
  connect(mApplyFromAllQueriesAction, SIGNAL(triggered()), SLOT(applyAllTransforms()));
}

DeclarationPointer RangeDelegatePrivate::declarationForX(int x) const
{
  DeclarationPointer result;
  QMapIterator<QRectF, DeclarationPointer> i(mRectDeclarationMap);
  while (i.hasNext()) {
    i.next();

    const QRectF curRect = i.key();

    // The map is sorted on the left() position of the QRectF, so if key.left() > x
    // we don't have to expect any matching result after i.
    if (curRect.left() > x)
      return result;

    // A QRectF that contains x is a candidate for being the best match.
    if (curRect.contains(x, curRect.top() + curRect.height() / 2))
      result = i.value();
  }

  return result;
}

void RangeDelegatePrivate::loadFile(QString const &fileName)
{
  mFileName = fileName;

  mCalculator.init(createCodeRepresentation(IndexedString(fileName)));
}

SimpleRange RangeDelegatePrivate::rangeForX(int row, int x)
{
  const QMap<SimpleRange, QRectF> rangeMapForRow = mRangeRectMap.value(row);
  foreach (QRectF const &rect, rangeMapForRow) {
    // Give one pixel extra space on both sides for the really small rects.
    if (x >= (rect.left() - 1) && x <= (rect.right() + 1))
      return rangeMapForRow.key(rect);
  }

  return SimpleRange::invalid();
}

ParseSession *RangeDelegatePrivate::session()
{
  Q_ASSERT( mTopContext );
  Q_ASSERT( mTopContext->ast() );
  return static_cast<ParseSession*>(mTopContext->ast().data());
}

int RangeDelegatePrivate::nodeLevel(AST *node)
{
  Q_ASSERT(node);

  AST* parent = ancestorNode(node, session());
  if (parent && parent->kind != AST::Kind_TranslationUnit) {
    if (node->kind == AST::Kind_ClassSpecifier)
      // ClassSpecifiers are alwasy child of SimpleDeclarationAST, so reduce the
      // depth a bit here by not counting that extra step.
      return 0 + nodeLevel(parent);
    else
      return 1 + nodeLevel(parent);
  }

  return 0; // No parent or parent is TranslationUnitAST.
}

void RangeDelegatePrivate::paintAST(QPainter *painter, QRect const &visibleRect)
{
  mRect = visibleRect;

  if (mPainterCache.isEmpty()) {
    DUChainReadLocker lock;
    ParseSession *parseSession = session();
    if (!parseSession)
      return;

    mCacheRect = mRect;
    visit(parseSession->topAstNode());
  }

  typedef QPair<QRectF, QString> CachePair;
  int yOffset = mCacheRect.top() - mRect.top();
  int xOffset = mCacheRect.x() - mRect.x();
  foreach(const CachePair cacheEntry, mPainterCache) {
    const QColor brushColor = mColorConfig.readEntry<QColor>(cacheEntry.second, QColor());
    const QColor penColor = brushColor.darker(175);

    if (!brushColor.isValid())
      continue;

    QPen pen;
    pen.setColor(penColor);
    pen.setWidth(1);
    pen.setCapStyle(Qt::FlatCap);
    pen.setJoinStyle(Qt::MiterJoin);

    QRectF rect = cacheEntry.first;
    rect.setTop(rect.top() - yOffset);
    rect.setBottom(rect.bottom() - 1 - yOffset);
    rect.moveLeft(rect.left() - xOffset);

    painter->setPen(pen);
    painter->setBrush(brushColor);
    painter->drawRect(rect);
  }
}

QRectF RangeDelegatePrivate::paintNode(AST *node, const QString &name)
{
  if (!mColorConfig.hasKey(name))
    return QRectF();

  CppEditorIntegrator editor(session());
  const SimpleRange range = editor.findRange(node).castToSimpleRange();

  QRectF nodeRect;
  if (range.isValid()) {
    nodeRect = mCalculator.calculateVisibleRect(range, mRect, nodeLevel(node));
    if (nodeRect.width() > 4 && nodeRect.height() > 4) {
      mPainterCache << qMakePair(nodeRect, name);
    }
  }

  return nodeRect;
}

void RangeDelegatePrivate::visitCastExpression(CastExpressionAST *ceast)
{
  paintNode(ceast, "CastExpressionAST");
  DefaultVisitor::visitCastExpression(ceast);
}

void RangeDelegatePrivate::visitCatchStatement(CatchStatementAST *csast)
{
  paintNode(csast, "CatchStatementAST");
  DefaultVisitor::visitCatchStatement(csast);
}

void RangeDelegatePrivate::visitClassSpecifier(ClassSpecifierAST *csast)
{
  QRectF nodeRect = paintNode(csast, "ClassSpecifierAST");
  if (nodeRect.isValid()) {
    DeclarationPointer pointer = session()->declarationFromAstNode(csast);
    if (pointer)
      mRectDeclarationMap.insert(nodeRect, pointer);
  }

  DefaultVisitor::visitClassSpecifier(csast);
}

void RangeDelegatePrivate::visitDoStatement(DoStatementAST *dsast)
{
  paintNode(dsast, "DoStatementAST");
  DefaultVisitor::visitDoStatement(dsast);
}

void RangeDelegatePrivate::visitForStatement(ForStatementAST *fsast)
{
  paintNode(fsast, "ForStatementAST");
  DefaultVisitor::visitForStatement(fsast);
}

void RangeDelegatePrivate::visitFunctionCall(FunctionCallAST *fcast)
{
  paintNode(fcast, "FunctionCallAST");
  DefaultVisitor::visitFunctionCall(fcast);
}

void RangeDelegatePrivate::visitFunctionDefinition(FunctionDefinitionAST *fdast)
{
  DeclarationPointer decl = session()->declarationFromAstNode(fdast);
  Q_ASSERT(decl);
  Q_ASSERT(decl->isFunctionDeclaration());

  ClassFunctionDeclaration *cfDecl = 0;
  if (FunctionDefinition *fDef = dynamic_cast<FunctionDefinition *>(decl.data())) {
    DUChainReadLocker lock(DUChain::lock());
    cfDecl = dynamic_cast<ClassFunctionDeclaration *>(fDef->declaration(mTopContext));
  } else
    cfDecl = dynamic_cast<ClassFunctionDeclaration *>(decl.data());

  QString name = "FunctionDefinitionAST_global";

  if (cfDecl) {
    switch (cfDecl->accessPolicy()) {
    case ClassMemberDeclaration::Public:
      name = "FunctionDefinitionAST_public";
      break;
    case ClassMemberDeclaration::Protected:
      name = "FunctionDefinitionAST_protected";
      break;
    case ClassMemberDeclaration::Private:
      name = "FunctionDefinitionAST_private";
      break;
    default:
      break;
    }
  }

  QRectF nodeRect = paintNode(fdast, name);
  if (nodeRect.isValid()) {
    DeclarationPointer pointer = session()->declarationFromAstNode(fdast);
    if (pointer)
      mRectDeclarationMap.insert(nodeRect, pointer);
  }
  DefaultVisitor::visitFunctionDefinition(fdast);
}

void RangeDelegatePrivate::visitIfStatement(IfStatementAST *isast)
{
  paintNode(isast, "IfStatementAST");
  DefaultVisitor::visitIfStatement(isast);
}

void RangeDelegatePrivate::visitSignalSlotExpression(SignalSlotExpressionAST *sseast)
{
  paintNode(sseast, "SignalSlotExpressionAST");
  DefaultVisitor::visitSignalSlotExpression(sseast);
}

void RangeDelegatePrivate::visitStringLiteral(StringLiteralAST *slast)
{
  paintNode(slast, "StringLiteralAST");
  DefaultVisitor::visitStringLiteral(slast);
}

void RangeDelegatePrivate::visitSwitchStatement(SwitchStatementAST *ssast)
{
  paintNode(ssast, "SwitchStatementAST");
  DefaultVisitor::visitSwitchStatement(ssast);
}

void RangeDelegatePrivate::visitTryBlockStatement(TryBlockStatementAST *tbast)
{
  paintNode(tbast, "TryBlockStatementAST");
  DefaultVisitor::visitTryBlockStatement(tbast);
}

void RangeDelegatePrivate::visitWhileStatement(WhileStatementAST *wsast)
{
  paintNode(wsast, "WhileStatementAST");
  DefaultVisitor::visitWhileStatement(wsast);
}

void RangeDelegatePrivate::applyAllTransforms()
{
  emit q->applyAllRequested();
}

void RangeDelegatePrivate::applySelectedTransform()
{
  emit q->applyForQueryAtRangeRequested(mCurrentQueryId, mCurrentRange);
}

void RangeDelegatePrivate::applyTransformsForSelectedQuery()
{
  emit q->applyAllForQueryRequested(mCurrentQueryId);
}
