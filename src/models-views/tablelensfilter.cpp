#include "tablelensfilter.h"

#include "tablelensmodel.h"
#include "modeltest.h"

TableLensFilter::TableLensFilter(ResultDataModel::Ptr const &dataModel)
  : mCurrentFilter(EmptyRowsAndColumns)
{
  setDynamicSortFilter(true);
  setSourceModel(new TableLensModel(dataModel));

  // Enable the next line to test the model.
  //new ModelTest(sourceModel(), this);
}

void TableLensFilter::setFilter(Filter filter)
{
  mCurrentFilter = filter;
  this->filterChanged();
}

TableLensModel *TableLensFilter::tableLensModel() const
{
  Q_ASSERT(dynamic_cast<TableLensModel *>(sourceModel()));
  return static_cast<TableLensModel *>(sourceModel());
}

/// Protected functions.


bool TableLensFilter::filterAcceptsColumn(int source_column, const QModelIndex &) const
{
  if (mCurrentFilter != EmptyColumns && mCurrentFilter != EmptyRowsAndColumns)
    return true;

  if (source_column == 0)
    return true; /// Always accept the Total column.

  const int rowCount = sourceModel()->rowCount();

  for (int i = 0; i < rowCount; ++i) {
    QModelIndex index = sourceModel()->index(i, source_column);
    if (index.data(Qt::DisplayRole).toInt() > 0)
      return true;
  }

  return false;
}

bool TableLensFilter::filterAcceptsRow(int source_row, const QModelIndex &) const
{
  if (mCurrentFilter != EmptyRows && mCurrentFilter != EmptyRowsAndColumns)
    return true;

  // Accept the row if the total > 0, only in that case there's at least on other
  // cell in that row which has a non zero value.
  return sourceModel()->index(source_row, 0).data(Qt::DisplayRole).toInt() > 0;
}

