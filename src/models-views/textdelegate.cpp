#include "textdelegate.h"

#include <QtGui/QPainter>

TextDelegate::TextDelegate(QObject *parent)
  : QItemDelegate(parent)
{ }

void TextDelegate::paint(QPainter *painter,
                         const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
  if (mText != index.model()->data(index)) {
    QItemDelegate::paint(painter, option, index);
    return;
  }

  QStyleOptionViewItem customOption = option;
  customOption.font.setBold(true);
  //customOption.palette.setColor(QPalette::Text, Qt::red);
  QItemDelegate::paint(painter, customOption, index);
}

void TextDelegate::setText(const QString &text)
{
  mText = text;
}
