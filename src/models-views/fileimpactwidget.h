#ifndef FILEIMPACTWIDGET_H
#define FILEIMPACTWIDGET_H

#include <QtGui/QWidget>

#include <language/editor/simplecursor.h>
#include <language/editor/simplerange.h>

#include "resultdatamodel.h"

namespace KTextEditor {
    class View;
}

namespace KDevelop {
    class IDocument;
}

namespace Ui {
class FileImpactWidget;
}

class FileImpactModel;
class RangeDelegate;
class TextDelegate;

class FileImpactWidget : public QWidget
{
  Q_OBJECT

public:
  FileImpactWidget(const ResultDataModel::Ptr &dataModel,
                   const QString &projectPath = QString(),
                   const QString &file = QString());

  QString currentFile() const;

public slots:
  void setFile(const QString &file);
  void setProjectPath(const QString &path);
  void setSelectedQuery(const QString &query);

signals:
  void applyAllRequested();
  void applyAllForQueryRequested(const QString &queryId);
  void applyForQueryAtRangeRequested(const QString &queryId, const KDevelop::SimpleRange &range);

private slots:
  void adjustRowHeight(int newHeight);
  void handleDocumentActivation(KDevelop::IDocument *activatedDocument);
  void scrollToPosition(const KDevelop::SimpleCursor &range);
  void selectRange(const KDevelop::SimpleRange &range);
  void updateCursorPos(KTextEditor::View *view, const KTextEditor::Cursor &cursor);

  void updateViewPort();

private:
  QString               mFile;
  FileImpactModel      *mModel;
  QString               mProjectPath;
  RangeDelegate        *mRangeDelegate;
  TextDelegate         *mTextDelegate;
  Ui::FileImpactWidget *mUi;
  KTextEditor::View    *mView;
};

#endif // FILEIMPACTWIDGET_H
