#ifndef HORIZONTALFILEITEM_H
#define HORIZONTALFILEITEM_H

#include <QtGui/QGraphicsItem>

namespace KDevelop {
class DUContext;
class ReferencedTopDUContext;
}

class HorizontalFileItemPrivate;

class HorizontalFileItem : public QGraphicsItem
{
public:
  HorizontalFileItem(KDevelop::ReferencedTopDUContext const &context);
  ~HorizontalFileItem();
  HorizontalFileItem &operator=(HorizontalFileItem const &other);

  QRectF boundingRect() const;

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
  HorizontalFileItemPrivate *d;
};

#endif // HORIZONTALFILEITEM_H
