#ifndef ASTCOLORDIALOG_H
#define ASTCOLORDIALOG_H

#include <QtGui/QDialog>

class QCheckBox;

class KConfigGroup;
class KColorButton;

namespace Ui {
class AstColorDialog;
}

class AstColorDialog : public QDialog
{
  Q_OBJECT

public:
  AstColorDialog();

  static void writeDefaultConfig();

private slots:
  void loadConfiguration();
  void loadConfiguration(const KConfigGroup &group,
                         const QString &name,
                         QCheckBox *check,
                         KColorButton *button);
  void resetConfiguration();
  void storeConfiguration();
  void updateConfiguration(KConfigGroup *group,
                           const QString &name,
                           QCheckBox *check,
                           KColorButton *button);

private:
  Ui::AstColorDialog *mUi;
};

#endif // ASTCOLORDIALOG_H
