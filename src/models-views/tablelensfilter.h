#ifndef TABLELENSFILTER_H
#define TABLELENSFILTER_H

#include <QtGui/QSortFilterProxyModel>

#include "resultdatamodel.h"

class TableLensModel;

class TableLensFilter : public QSortFilterProxyModel
{
public:
  enum Filter {
    None,
    EmptyColumns,
    EmptyRows,
    EmptyRowsAndColumns
  };

public:
  /// Creates a new filter which by default filters out empty rows and columns.
  TableLensFilter(ResultDataModel::Ptr const &dataModel);

  /// Returns the source model casted to TableLensModel. Added for convenience.
  TableLensModel *tableLensModel() const;

  void setFilter(Filter filter);

protected:
  virtual bool filterAcceptsColumn(int source_column, const QModelIndex & source_parent) const;
  virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
  Filter mCurrentFilter;
};

#endif // TABLELENSFILTER_H
