#include "resulttreewidget.h"
#include "resulttreewidget_p.h"

#include <QtGui/QSortFilterProxyModel>


/// ResultTreeWidgetItem

// Ugly hack, use a QStandardItemModel in stead of the QTreeWidget

class ResultTreeWidgetItem : public QTreeWidgetItem
{
public:
  ResultTreeWidgetItem(QTreeWidgetItem * parent);

  virtual bool operator<(const QTreeWidgetItem & other) const;
};

ResultTreeWidgetItem::ResultTreeWidgetItem(QTreeWidgetItem *parent)
  : QTreeWidgetItem(parent)
{ }

bool ResultTreeWidgetItem::operator<(const QTreeWidgetItem &other) const
{
  if (treeWidget()->sortColumn() == 0)
    return QTreeWidgetItem::operator<(other);

  return data(1, Qt::UserRole).toInt() < other.data(1, Qt::UserRole).toInt();
}

/// ResultTreeWidgetPrivate

ResultTreeWidgetPrivate::ResultTreeWidgetPrivate(ResultTreeWidget *q, const ResultDataModel::Ptr &model)
  : mModel(model)
  , q_ptr(q)
{
  connect(q, SIGNAL(itemActivated(QTreeWidgetItem*,int)),
          SLOT(handleItemChange(QTreeWidgetItem*)));
  connect(q, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),
          SLOT(handleItemDoubleClick(QTreeWidgetItem*)));
}

void ResultTreeWidgetPrivate::handleItemChange(QTreeWidgetItem *item)
{
  Q_Q(ResultTreeWidget);
  if (mCurrentFile != q->currentFile()) {
    mCurrentFile = q->currentFile();
    mCurrentQuery.clear();
    emit q->currentFileChanged(q->currentFile());
    emit q->currentQueryChanged(mCurrentQuery);
  } else {
    mCurrentQuery.clear();
    emit q->currentQueryChanged(mCurrentQuery);
  }

  const QString query = item->text(0);
  if (item->parent() && query != mCurrentQuery) {
    mCurrentQuery = query;
    emit q->currentQueryChanged(mCurrentQuery);
  }

}

void ResultTreeWidgetPrivate::handleItemDoubleClick(QTreeWidgetItem*)
{
  Q_Q(ResultTreeWidget);
  emit q->fileDoubleClicked(q->currentFile());
}

/// ResultTreeWidget

ResultTreeWidget::ResultTreeWidget(const ResultDataModel::Ptr &model, QWidget *parent)
  : QTreeWidget(parent)
  , d_ptr(new ResultTreeWidgetPrivate(this, model))
{
  const int colWidth = QFontMetrics(font()).width("AlongerQueryId<Blah>");
  setColumnCount(2);
  setColumnWidth(0, colWidth + 50);
  setColumnWidth(1, QFontMetrics(font()).width("999"));
  setHeaderLabels(QStringList() << "QueryId" << "Count");
  setSortingEnabled(true);
  sortByColumn(1, Qt::DescendingOrder);
}

ResultTreeWidget::~ResultTreeWidget()
{
  delete d_ptr;
}

void ResultTreeWidget::setCurrentFile(const QString& file)
{
  foreach (QTreeWidgetItem* item, findItems(file, Qt::MatchExactly)) {
    if (!item->parent()) {
      setCurrentItem(item);
      break;
    }
  }
}

QString ResultTreeWidget::currentFile() const
{
  QTreeWidgetItem *item = currentItem();

  if (!item)
    return QString();

  QString url;
  if (item->parent()) {
    return item->parent()->text(0);
  } else {
    return item->text(0);
  }
}

void ResultTreeWidget::update(const QString &file, const QueryResult &result)
{
  Q_D(ResultTreeWidget);

  const int count = d->mModel->resultTotalForFile(file);

  QTreeWidgetItem *item = 0;
  if (d->mFileItemMap.contains(file)) {
    item = d->mFileItemMap.value(file);
    if (count == 0) {
      invisibleRootItem()->removeChild(item);
      d->mFileItemMap.remove(file);
      return;
    }
    item->setData(1, Qt::DisplayRole, count);
    item->setData(1, Qt::UserRole, count);
  } else if (count > 0) {
    item = new ResultTreeWidgetItem(invisibleRootItem());
    item->setText(0, file);
    item->setData(1, Qt::DisplayRole, count);
    item->setData(1, Qt::UserRole, count);

    d->mFileItemMap.insert(file, item);
  } else {
    return;
  }
  Q_ASSERT(item);

  if (result.hits().count() > 0) {
    bool containsQuery = false;
    for (int i = 0; i < item->childCount(); ++i) {
      if (item->child(i)->text(0) == result.queryId()) {
        item->child(i)->setData(1, Qt::DisplayRole, result.hits().count());
        item->child(i)->setData(1, Qt::UserRole, result.hits().count());
        containsQuery = true;
        break;
      }
    }

    if (!containsQuery) {
      QTreeWidgetItem *queryItem = new ResultTreeWidgetItem(item);
      queryItem->setText(0, result.queryId());
      queryItem->setData(1, Qt::DisplayRole, result.hits().count());
      queryItem->setData(1, Qt::UserRole, result.hits().count());
    }
  } else if (item) {
    for (int i = 0; i < item->childCount(); ++i) {
      if (item->child(i)->text(0) == result.queryId()) {
        QTreeWidgetItem* c = item->child(i);
        item->removeChild(c);
        delete c;
      }
    }
  }
}

/// Slots

void ResultTreeWidget::clear()
{
  Q_D(ResultTreeWidget);
  d->mFileItemMap.clear();
  QTreeWidget::clear();
}

