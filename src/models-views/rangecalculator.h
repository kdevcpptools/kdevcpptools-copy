#ifndef RANGECALCULATOR_H
#define RANGECALCULATOR_H

#include <QtCore/QtGlobal>

#include <language/codegen/coderepresentation.h>

class QRect;
class QRectF;

namespace KDevelop {
class SimpleCursor;
class SimpleRange;
}

class RangeCalculatorPrivate;

class RangeCalculator
{
public:
  enum ZoomDirection
  {
    In,
    Out
  };

public:
  RangeCalculator();

  ~RangeCalculator();

  QRectF calculateVisibleRect(const KDevelop::SimpleRange &range, const QRect &visibleRect, int level) const;

  /// Calculates the cursor position for x. Where x is the normalized position in
  /// the visual part of the document.
  KDevelop::SimpleCursor cursosForX(double x) const;

  double xForCursor(const KDevelop::SimpleCursor &cursor) const;

  /// Initializes the calculator for given representation. Intial zoomlevel is 100%.
  void init(KDevelop::CodeRepresentation::Ptr representation);

  /// Normalized position in the visual rect and the zoom direction.
  void zoom(double zoomPos, ZoomDirection direction);

private:
  RangeCalculatorPrivate *d_ptr;
  Q_DECLARE_PRIVATE(RangeCalculator)
  Q_DISABLE_COPY(RangeCalculator)
};

#endif // RANGECALCULATOR_H
