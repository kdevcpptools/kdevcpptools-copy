#ifndef BARDELEGATE_H
#define BARDELEGATE_H

#include <QtGui/QAbstractItemDelegate>

class BarDelegate : public QAbstractItemDelegate
{
public:
  enum ColumnScale {
    ToTotal,
    ToColumnMax
  };

public:
  BarDelegate(QObject *parent = 0);

  void paint(QPainter *painter,
             const QStyleOptionViewItem &option,
             const QModelIndex &index) const;

  void setColumnScale(ColumnScale scale);

  void setFontSize(qreal size);
  void setMinimumFontSize(qreal size);

  void showCounts(bool show);

  QSize sizeHint(const QStyleOptionViewItem &option,
                 const QModelIndex &index) const;

private:
  ColumnScale mColumnScale;
  qreal       mFontSize;
  bool        mShowCounts;
  qreal       mMinimumFontSize;
};

#endif // BARDELEGATE_H
