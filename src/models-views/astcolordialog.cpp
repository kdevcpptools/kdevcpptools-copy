#include "astcolordialog.h"

#include <QtCore/QDebug>


#include "ui_astcolordialog.h"

AstColorDialog::AstColorDialog()
  : mUi(new Ui::AstColorDialog)
{
  mUi->setupUi(this);

  loadConfiguration();

  connect(mUi->mBtnReset, SIGNAL(clicked()), SLOT(resetConfiguration()));
  connect(this, SIGNAL(accepted()), SLOT(storeConfiguration()));
}

void AstColorDialog::writeDefaultConfig()
{
  KSharedConfigPtr config = KGlobal::config();
  KConfigGroup group = config->group("cpp_query_engine");

  group.writeEntry("Colors Configured", true);
  group.writeEntry("ClassSpecifierAST", QColor("#C9E6C9"));
  group.deleteEntry("CastExpressionAST");
  group.deleteEntry("CatchStatementAST");
  group.writeEntry("DoStatementAST", QColor("#DEBC85"));
  group.writeEntry("ForStatementAST", QColor("#DEBC85"));
  group.deleteEntry("FunctionCallAST");
  group.writeEntry("FunctionDefinitionAST_global", QColor("#C0FFFF"));
  group.writeEntry("FunctionDefinitionAST_public", QColor("#BFFFBF"));
  group.writeEntry("FunctionDefinitionAST_protected", QColor("#FFE0BF"));
  group.writeEntry("FunctionDefinitionAST_private", QColor("#FFBFBF"));
  group.writeEntry("IfStatementAST", QColor("#DFBFFF"));
  group.writeEntry("SwitchStatementAST", QColor("#DFBFFF"));
  group.deleteEntry("SignalSlotExpressionAST");
  group.deleteEntry("StringLiteralAST");
  group.deleteEntry("TryBlockStatementAST");
  group.writeEntry("WhileStatementAST", QColor("#DEBC85"));

  config->sync();
}

/// Private functions

void AstColorDialog::loadConfiguration()
{
  KSharedConfigPtr config = KGlobal::config();
  KConfigGroup group = config->group("cpp_query_engine");

  loadConfiguration(group, "ClassSpecifierAST", mUi->mCheckCS, mUi->mButtonCS);
  loadConfiguration(group, "CastExpressionAST", mUi->mCheckCE, mUi->mButtonCE);
  loadConfiguration(group, "CatchStatementAST", mUi->mCheckCaS, mUi->mButtonCaS);
  loadConfiguration(group, "DoStatementAST", mUi->mCheckDS, mUi->mButtonDS);
  loadConfiguration(group, "ForStatementAST", mUi->mCheckFS, mUi->mButtonFS);
  loadConfiguration(group, "FunctionCallAST", mUi->mCheckFC, mUi->mButtonFC);
  loadConfiguration(group, "FunctionDefinitionAST_global", mUi->mCheckFD, mUi->mButtonFDGlob);
  loadConfiguration(group, "FunctionDefinitionAST_public", mUi->mCheckFD, mUi->mButtonFDPub);
  loadConfiguration(group, "FunctionDefinitionAST_protected", mUi->mCheckFD, mUi->mButtonFDProt);
  loadConfiguration(group, "FunctionDefinitionAST_private", mUi->mCheckFD, mUi->mButtonFDPriv);
  loadConfiguration(group, "IfStatementAST", mUi->mCheckIS, mUi->mButtonIS);
  loadConfiguration(group, "SwitchStatementAST", mUi->mCheckSS, mUi->mButtonSS);
  loadConfiguration(group, "SignalSlotExpressionAST", mUi->mCheckSSE, mUi->mButtonSSE);
  loadConfiguration(group, "StringLiteralAST", mUi->mCheckSL, mUi->mButtonSL);
  loadConfiguration(group, "TryBlockStatementAST", mUi->mCheckTBS, mUi->mButtonTBS);
  loadConfiguration(group, "WhileStatementAST", mUi->mCheckWS, mUi->mButtonWS);
}

void AstColorDialog::loadConfiguration(const KConfigGroup &group,
                                       const QString &name,
                                       QCheckBox *check,
                                       KColorButton *button)
{
  const bool hasKey = group.hasKey(name);
  check->setChecked(hasKey);
  button->setEnabled(hasKey);

  if (hasKey)
    button->setColor(group.readEntry<QColor>(name, QColor()));
  else
    button->setColor(QColor());
}

void AstColorDialog::resetConfiguration()
{
  writeDefaultConfig();
  loadConfiguration();
}

void AstColorDialog::storeConfiguration()
{
  KSharedConfigPtr config = KGlobal::config();
  KConfigGroup group = config->group("cpp_query_engine");

  group.writeEntry("Colors Configured", true);

  updateConfiguration(&group, "ClassSpecifierAST", mUi->mCheckCS, mUi->mButtonCS);
  updateConfiguration(&group, "CastExpressionAST", mUi->mCheckCE, mUi->mButtonCE);
  updateConfiguration(&group, "CatchStatementAST", mUi->mCheckCaS, mUi->mButtonCaS);
  updateConfiguration(&group, "DoStatementAST", mUi->mCheckDS, mUi->mButtonDS);
  updateConfiguration(&group, "ForStatementAST", mUi->mCheckFS, mUi->mButtonFS);
  updateConfiguration(&group, "FunctionCallAST", mUi->mCheckFC, mUi->mButtonFC);
  updateConfiguration(&group, "FunctionDefinitionAST_global", mUi->mCheckFD, mUi->mButtonFDGlob);
  updateConfiguration(&group, "FunctionDefinitionAST_public", mUi->mCheckFD, mUi->mButtonFDPub);
  updateConfiguration(&group, "FunctionDefinitionAST_protected", mUi->mCheckFD, mUi->mButtonFDProt);
  updateConfiguration(&group, "FunctionDefinitionAST_private", mUi->mCheckFD, mUi->mButtonFDPriv);
  updateConfiguration(&group, "IfStatementAST", mUi->mCheckIS, mUi->mButtonIS);
  updateConfiguration(&group, "SwitchStatementAST", mUi->mCheckSS, mUi->mButtonSS);
  updateConfiguration(&group, "SignalSlotExpressionAST", mUi->mCheckSSE, mUi->mButtonSSE);
  updateConfiguration(&group, "StringLiteralAST", mUi->mCheckSL, mUi->mButtonSL);
  updateConfiguration(&group, "TryBlockStatementAST", mUi->mCheckTBS, mUi->mButtonTBS);
  updateConfiguration(&group, "WhileStatementAST", mUi->mCheckWS, mUi->mButtonWS);

  config->sync();
}

void AstColorDialog::updateConfiguration(KConfigGroup *group,
                                         const QString &name,
                                         QCheckBox *check,
                                         KColorButton *button)
{
  if (!check->isChecked()) {
    group->deleteEntry(name);
  } else {
    group->writeEntry(name, button->color());
  }
}
