#ifndef RESULTTREEWIDGET_H
#define RESULTTREEWIDGET_H

#include <QtGui/QTreeWidget>

#include "resultdatamodel.h"

class ResultTreeWidgetPrivate;

class ResultTreeWidget : public QTreeWidget
{
  Q_OBJECT

public:
  ResultTreeWidget(const ResultDataModel::Ptr &data, QWidget *parent = 0);

  ~ResultTreeWidget();

  void setCurrentFile(const QString& file);
  QString currentFile() const;

  void update(const QString &file, const QueryResult &result);

public slots:
  void clear();

signals:
  void currentFileChanged(const QString &file);
  void currentQueryChanged(const QString &query);
  void fileDoubleClicked(const QString &file);

private:
  ResultTreeWidgetPrivate *d_ptr;

  Q_DECLARE_PRIVATE(ResultTreeWidget)
  Q_DISABLE_COPY(ResultTreeWidget)
};

#endif // RESULTTREEWIDGET_H
