#include "rangedelegate.h"
#include "rangedelegate_p.h"

#include <QtCore/QFile>
#include <QtGui/QAbstractItemView>
#include <QtGui/QMenu>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainter>
#include <QtGui/QToolTip>

#include <language/duchain/declaration.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <languages/cpp/parser/ast.h>

#include "rangecalculator.h"

Q_DECLARE_METATYPE(QList<KDevelop::SimpleRange>)

using namespace KDevelop;

/// RangeDelegate

RangeDelegate::RangeDelegate(QString const &fileName, QObject *parent)
  : QAbstractItemDelegate(parent)
  , d_ptr(new RangeDelegatePrivate(this, fileName))
{ }

RangeDelegate::~RangeDelegate()
{
  delete d_ptr;
}

bool RangeDelegate::editorEvent(QEvent *event,
                                QAbstractItemModel *model,
                                const QStyleOptionViewItem &option,
                                const QModelIndex &index)
{
  Q_D(RangeDelegate);

  QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent*>(event);
  if (!mouseEvent)
    return false; // We don't handle non-mouse events.

  d->mCurrentRange = d->rangeForX(index.row(), mouseEvent->pos().x());
  if (mouseEvent->button() == Qt::RightButton) {
    // MouseButtonRelease does not reach editorEvent.

    if (d->mCurrentRange.isValid())
      emit queryResultClicked(d->mCurrentRange);

    d->mApplyAction->setEnabled(d->mCurrentRange.isValid());
    d->mCurrentQueryId = model->data(model->index(index.row(), 0), Qt::DisplayRole).toString();

    QMenu menu;
    menu.addAction(d->mApplyAction);
    menu.addAction(d->mApplyAllAction);
    //menu.addSeparator();
    //menu.addAction(d->mApplyFromAllQueriesAction);

    menu.exec(mouseEvent->globalPos());
  }

  if (mouseEvent->button() == Qt::LeftButton && mouseEvent->type() == QMouseEvent::MouseButtonRelease) {
    if (d->mCurrentRange.isValid())
      emit queryResultClicked(d->mCurrentRange);
    else {
      double normCenterInVisualRect = ((1.0 * mouseEvent->pos().x() - option.rect.left()) / (option.rect.right() - option.rect.left()));
      emit cursorClicked(d->mCalculator.cursosForX(normCenterInVisualRect));
    }
    return true;
  }

  return false;
}

bool RangeDelegate::eventFilter(QObject *obj, QEvent *event)
{
  Q_D(RangeDelegate);
  if (event->type() == QEvent::Wheel) {
    QWheelEvent *wheelEvent = static_cast<QWheelEvent*>(event);
    if (wheelEvent->modifiers() & Qt::ControlModifier && wheelEvent->orientation() == Qt::Vertical) {
      // It currently doesn't make sense if the RangeDelegate is installed as
      // eventFilter on any other object than the table view on which it is set
      // as a delegate. So for now we assume at this point that the event comes
      // from the viewport of the tableview on which the delegate is installed.
      Q_ASSERT(dynamic_cast<QAbstractItemView*>(obj->parent()));

      QAbstractItemView *itemView = static_cast<QAbstractItemView*>(obj->parent());
      QModelIndex index = itemView->indexAt(wheelEvent->pos());
      if (!index.isValid() || index.column() != 1) {
        // Zooming only makes sense if we can determine the point on which to
        // in the file. For this we need one of the cells in the second column.
        // (Which one, doesn't realy matter).
        wheelEvent->accept();
        return true;
      }

      d->mTmpZoom += wheelEvent->delta() / 8.0 / 360;
      if (d->mTmpZoom >= 0.1 || d->mTmpZoom <= -0.1) { // Zoom only when at least +/- 10 %
        d->mPainterCache.clear();
        int newHeight = d->mCurrentHeight;
        newHeight += 2 * d->mTmpZoom * d->mMinimumHeight;
        if (newHeight < d->mMinimumHeight)
          newHeight = d->mMinimumHeight;
        if (newHeight > d->mMaximumHeight)
          newHeight = d->mMaximumHeight;

        if (newHeight != d->mCurrentHeight) {
          d->mCurrentHeight = newHeight;
          emit heightChanged(d->mCurrentHeight);
        }

        // Calculate normalized point in the visible rect.
        QRect rect = itemView->visualRect(index);
        double normCenterInVisualRect = ((1.0 * wheelEvent->pos().x() -rect.left()) / (rect.right() - rect.left()));
        if (d->mTmpZoom > 0)
          d->mCalculator.zoom(normCenterInVisualRect, RangeCalculator::In);
        else {
          d->mCalculator.zoom(normCenterInVisualRect, RangeCalculator::Out);
        }

        emit zoomLevelChanged();
        d->mTmpZoom = 0;
      }

      wheelEvent->accept();
      return true;
    }
  }

  return QObject::eventFilter(obj, event);
}

bool RangeDelegate::helpEvent(QHelpEvent *event,
                              QAbstractItemView *,
                              const QStyleOptionViewItem &,
                              const QModelIndex &)
{
  Q_D(RangeDelegate);

  if (event->type() == QHelpEvent::ToolTip) {
    DeclarationPointer decl = d->declarationForX(event->pos().x());
    if (decl)
      QToolTip::showText(event->globalPos(), decl->toString());
    else
      QToolTip::showText(event->globalPos(), QString());

    return true;
  }

  return false;
}

void RangeDelegate::paint(QPainter *painter,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
  Q_D(const RangeDelegate);
  Q_ASSERT( d->mTopContext );

  d_ptr->paintAST(painter, option.rect);

  QMap<SimpleRange, QRectF> rangeMapForRow;
  QList<SimpleRange> ranges = index.model()->data(index, Qt::DisplayRole).value<QList<SimpleRange> >();
  foreach (SimpleRange const &range, ranges) {
    if (!range.isValid()) {
      continue;
    }
    QRectF rangeRect = d->mCalculator.calculateVisibleRect(range, option.rect, 0);
    if (rangeRect.width() < 3) {
      const float mid = (rangeRect.left() + rangeRect.right()) / 2;
      rangeRect.setLeft(mid - 1.5);
      rangeRect.setRight(mid + 1.5);
    }

    rangeMapForRow.insert(range, rangeRect);
    painter->fillRect(rangeRect, QColor(255,0,0));
  }

  d_ptr->mRangeRectMap.insert(index.row(), rangeMapForRow);

  // paint cursor position
  if (d->mCursor.isValid()) {
    double x = d->mCalculator.xForCursor(d->mCursor);
    if (x >= 0 && x <= 1) {
      const int pos = option.rect.left() + option.rect.width() * x;
      painter->setPen(Qt::blue);
      painter->drawLine(pos, option.rect.top(), pos, option.rect.bottom());
    }
  }
}

void RangeDelegate::setCursor(const SimpleCursor &cursor)
{
    Q_D(RangeDelegate);
    d->mCursor = cursor;
}

void RangeDelegate::setFile(const QString &fileName)
{
  d_ptr->loadFile(fileName);

  TopDUContext *topContext = 0;

  if (!fileName.isEmpty()) {
    DUChainReadLocker lock(DUChain::lock());
    topContext = DUChain::self()->chainForDocument(KUrl(d_ptr->mFileName));
  }

  if (topContext && !topContext->ast()) {
    TopDUContext::Features features = TopDUContext::AllDeclarationsContextsAndUses;
    features = (TopDUContext::Features) (features | TopDUContext::AST);
    topContext = DUChain::self()->waitForUpdate(topContext->url(), features);
  }

  d_ptr->mTopContext = ReferencedTopDUContext(topContext);

  // File changed, change perspective to 100%
  if (d_ptr->mCurrentHeight != d_ptr->mMinimumHeight) {
    d_ptr->mCurrentHeight = d_ptr->mMinimumHeight;
    emit heightChanged(d_ptr->mCurrentHeight);
  }

  // Clear those maps, they're probably not valid anymore.
  d_ptr->mRectDeclarationMap.clear();
  d_ptr->mRangeRectMap.clear();
  d_ptr->mPainterCache.clear();
}

QSize RangeDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
  return QSize(300, d_ptr->mCurrentHeight);
}
