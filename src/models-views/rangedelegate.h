#ifndef RANGEDELEGATE_H
#define RANGEDELEGATE_H

#include <QtGui/QAbstractItemDelegate>

namespace KDevelop {
class SimpleCursor;
class SimpleRange;
}

class RangeDelegatePrivate;

class RangeDelegate : public QAbstractItemDelegate
{
  Q_OBJECT

public:
  RangeDelegate(QString const &fileName, QObject *parent = 0);
  ~RangeDelegate();

  virtual bool editorEvent(QEvent * event,
                           QAbstractItemModel * model,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index);

  void paint(QPainter *painter,
             const QStyleOptionViewItem &option,
             const QModelIndex &index) const;

  void setCursor(const KDevelop::SimpleCursor &cursor);

  void setFile(const QString &fileName);

  QSize sizeHint(const QStyleOptionViewItem &option,
                 const QModelIndex &index) const;

signals:
  void applyAllRequested();
  void applyForQueryAtRangeRequested(const QString &queryId, const KDevelop::SimpleRange &range);
  void applyAllForQueryRequested(const QString &queryId);
  void cursorClicked(KDevelop::SimpleCursor const &cursor);
  void heightChanged(int newHeight);
  void queryResultClicked(KDevelop::SimpleRange const &range);
  void zoomLevelChanged();

public Q_SLOTS:
  bool eventFilter(QObject *obj, QEvent *event);
  bool helpEvent(QHelpEvent *event,
                 QAbstractItemView *view,
                 const QStyleOptionViewItem &option,
                 const QModelIndex &index);

private:
  RangeDelegatePrivate *d_ptr;
  Q_DECLARE_PRIVATE(RangeDelegate)
  Q_DISABLE_COPY(RangeDelegate)
};

#endif // RANGEDELEGATE_H
