#ifndef RESULTTREEWIDGET_P_H
#define RESULTTREEWIDGET_P_H

#include <QtCore/QObject>
#include <QtCore/QMap>

#include "resultdatamodel.h"

class QTreeWidgetItem;
class ResultTreeWidget;

class ResultTreeWidgetPrivate : QObject
{
  Q_OBJECT

public: /// Members
  QString                         mCurrentFile;
  QString                         mCurrentQuery;
  ResultDataModel::Ptr            mModel;
  QMap<QString, QTreeWidgetItem*> mFileItemMap;

  ResultTreeWidget               *q_ptr;
  Q_DECLARE_PUBLIC(ResultTreeWidget)

public: /// Methods
  ResultTreeWidgetPrivate(ResultTreeWidget *q, const ResultDataModel::Ptr &model);

public slots:
  void handleItemChange(QTreeWidgetItem*);
  void handleItemDoubleClick(QTreeWidgetItem*);
};


#endif // RESULTTREEWIDGET_P_H
