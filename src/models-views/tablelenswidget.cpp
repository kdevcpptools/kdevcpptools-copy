#include "tablelenswidget.h"

#include <QtGui/QPushButton>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QItemDelegate>
#include <QtGui/QSortFilterProxyModel>
#include <QtGui/QTableView>

#include "bardelegate.h"
#include "tablelensfilter.h"
#include "tablelensmodel.h"

#include "ui_tablelenswidget.h"

TableLensWidget::TableLensWidget(ResultDataModel::Ptr const &dataModel)
  : mBarDelegate(new BarDelegate(this))
  , mMinimumFontSize(5.5)
  , mTableLensFilter(new TableLensFilter(dataModel))
  , mUi(new Ui::TableLensWidget())
{
  mUi->setupUi(this);
  mUi->splitter->setStretchFactor(1,2);
  mUi->mTableView->verticalHeader()->setResizeMode(QHeaderView::Fixed);
  mUi->mTableView->setItemDelegate(mBarDelegate);
  mUi->mTableView->setModel(mTableLensFilter);
  mUi->mTableView->horizontalHeader()->setSortIndicator(0, Qt::DescendingOrder);
  mUi->mTableView->viewport()->update();
  mUi->mShowCountsCheck->setChecked(true);

  connect(mUi->mFilterCombo, SIGNAL(currentIndexChanged(int)), SLOT(updateFilter(int)));
  connect(mUi->mShowCountsCheck, SIGNAL(toggled(bool)), SLOT(toggleShowCounts(bool)));
  connect(mUi->mShowGridCheck, SIGNAL(toggled(bool)), SLOT(toggleGrid(bool)));
  connect(mUi->mFilesHorizontalCheck, SIGNAL(toggled(bool)), SLOT(switchHeaders()));
  connect(mUi->mScaleToTotalCheck, SIGNAL(toggled(bool)), SLOT(switchColumnScale(bool)));
  connect(mUi->mZoomSlider, SIGNAL(valueChanged(int)), SLOT(zoom(int)));

  mUi->mZoomSlider->setValue(80);
}

QFont TableLensWidget::font() const
{
  return mUi->mTableView->font();
}

void TableLensWidget::setFont(QFont const &font)
{
  mUi->mTableView->setFont(font);
}

qreal TableLensWidget::minimumFontSize() const
{
  return mMinimumFontSize;
}

void TableLensWidget::setMinimumFontSize(qreal size)
{
  mBarDelegate->setMinimumFontSize(size);
  mMinimumFontSize = size;
}

/// Private slots

void TableLensWidget::switchColumnScale(bool toTotal)
{
  if (toTotal)
    mBarDelegate->setColumnScale(BarDelegate::ToTotal);
  else
    mBarDelegate->setColumnScale(BarDelegate::ToColumnMax);

  mUi->mTableView->viewport()->update();
}

void TableLensWidget::switchHeaders()
{
  mTableLensFilter->tableLensModel()->switchHeaders();
}

void TableLensWidget::toggleGrid(bool toggle)
{
  mUi->mTableView->setShowGrid(toggle);
}

void TableLensWidget::toggleShowCounts(bool toggle)
{
  mBarDelegate->showCounts(toggle);
  mUi->mTableView->viewport()->update();
}

void TableLensWidget::updateFilter(int filter)
{
  switch (filter) {
  case 1:
    mTableLensFilter->setFilter(TableLensFilter::EmptyColumns);
    break;
  case 2:
    mTableLensFilter->setFilter(TableLensFilter::EmptyRows);
    break;
  case 3:
    mTableLensFilter->setFilter(TableLensFilter::EmptyRowsAndColumns);
    break;
  default:
    mTableLensFilter->setFilter(TableLensFilter::None);
    break;
  }
}

void TableLensWidget::zoom(int pct)
{
  QFont font = mUi->mTableView->font();

  const int maxRowHeight = QFontMetrics(font).height() + 4;
  const int newHeight = pct * static_cast<double>(maxRowHeight) / 100;

  mUi->mTableView->verticalHeader()->setDefaultSectionSize(newHeight == 0 ? 1 : newHeight);

  const qreal newSize = pct * font.pointSizeF() / 100;
  if (newSize >= mMinimumFontSize) {
    mBarDelegate->setFontSize(newSize);
    font.setPointSizeF(newSize);
    mUi->mTableView->verticalHeader()->setFont(font);
    mUi->mTableView->verticalHeader()->setVisible(true);
    if (mUi->mShowGridCheck->isChecked())
      mUi->mTableView->setShowGrid(true);
  } else {
    mBarDelegate->setFontSize(newSize);
    mUi->mTableView->verticalHeader()->setVisible(false);
    mUi->mTableView->setShowGrid(false);
  }
}
