/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "cppquerywidget.h"

#include <QtGui/QBoxLayout>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSortFilterProxyModel>
#include <QtGui/QTextEdit>
#include <QtGui/QTreeWidget>

#include <KDE/KFileDialog>
#include <KDE/KTextEditor/Document>
#include <KDE/KMessageBox>

#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/ilanguage.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>

#include <language/backgroundparser/backgroundparser.h>
#include <language/backgroundparser/parsejob.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/parsingenvironment.h>
#include <language/duchain/topducontext.h>
#include <language/duchain/types/functiontype.h>
#include <language/duchain/types/integraltype.h>
#include <language/duchain/types/pointertype.h>
#include <language/editor/simplerange.h>
#include <language/interfaces/iastcontainer.h>
#include <language/interfaces/ilanguagesupport.h>
#include <languages/cpp/parser/parsesession.h>

#include <libportdb/xmlparser.h>
#include <libquery/query.h>
#include <libquery/queryjob.h>
#include <libquery/cppqueryengine.h>
#include <libtransform/cpptransformengine.h>

#include "astmodelbuilder.h"
#include "models-views/astcolordialog.h"
#include "models-views/fileimpactwidget.h"
#include "models-views/resulttreewidget.h"
#include "models-views/tablelenswidget.h"

#include "ui_cppqueryenginewidget.h"

using namespace KDevelop;

// static QString const sModifiedDocumentPostFix = QString(" (uncommitted changes)");

CppQueryWidget::CppQueryWidget(QWidget *parent /*, m_plugin */)
    : QWidget(parent)
    , mCurrentProject(0)
    , mResultModel(new ResultDataModel), mCurFileResultModel(new ResultDataModel)
    , mJob(0)
    , mTransformEngine(new CppTransformEngine)
    , mUi(new Ui::CppQueryEngineWidget())
    , mStartingQuery(false)
{
  mUi->setupUi(this);
  mUi->mTableSplitter->setStretchFactor(1,2);

  /// Plugin our custom TableLensWidget
  mUi->mTabWidget->addTab(new TableLensWidget(mResultModel), i18n("Project Overview"));

  /// Plugin our custom FileImpactWidget.
  mFileImpactWidget = new FileImpactWidget(mResultModel, QString());
  mFileImpactWidget->setObjectName("Project FileImpactWidget");
  //mFileImpactWidget->setEnabled(false);

  QGridLayout *layout = new QGridLayout(mUi->mFIWPlaceHolder);
  layout->setContentsMargins(0,0,0,0);
  layout->addWidget(mFileImpactWidget);

  mResultTreeWidget = new ResultTreeWidget(mResultModel, this);
  //mResultTreeWidget->setEnabled(false);

  layout = new QGridLayout(mUi->mRTWPlaceHolder);
  layout->setContentsMargins(0,0,0,0);
  layout->addWidget(mResultTreeWidget);

  mCurFileImpactWidget = new FileImpactWidget(mCurFileResultModel, QString());
  mCurFileImpactWidget->setObjectName("Cur File ImpactWidget");
  mCurFileImpactWidget->setEnabled(false);
  layout = new QGridLayout(mUi->mCurFileFIWPlaceHolder);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(mCurFileImpactWidget);

  /// Add currently opened projects to the combo
  foreach (IProject *project, ICore::self()->projectController()->projects())
    mUi->mProjectCombo->addItem(project->name());

  if (mUi->mProjectCombo->count() > 0) {
    mUi->mProjectCombo->setEnabled(true);
    projectActivated(mUi->mProjectCombo->currentText());
  }

  /// Set a sane default (Queries and Transforms) for filtering.
  mUi->mCmbFilter->setCurrentIndex(2);

  connect(mUi->mbtnViewAst, SIGNAL(clicked(bool))
    , SLOT(showAST()));
  connect(mUi->mDatabaseSelector, SIGNAL(urlSelected(KUrl))
    , SLOT(openDatabase(KUrl)));
  connect(mUi->mBtnColors, SIGNAL(clicked())
    , SLOT(configureColors()));
  connect(mUi->mProjectCombo, SIGNAL(activated(QString const&))
    , SLOT(projectActivated(QString const&)));
  connect(mUi->mBtnRunFileQuery, SIGNAL(clicked())
    , SLOT(fileQueryButtonClicked()));
  connect(mUi->mBtnRunProjectQuery, SIGNAL(clicked())
    , SLOT(projectQueryButtonClicked()));
  connect(mUi->mCmbFilter, SIGNAL(activated(QString const&))
    , SLOT(fillQueryCombo()));
  connect(mUi->mCBoxAllQueries, SIGNAL(toggled(bool))
    , SLOT(setCorrectQueryControlWidgetsStatus()));

  connect(mFileImpactWidget, SIGNAL(applyAllRequested()),
          SLOT(applyAll()));
  connect(mFileImpactWidget, SIGNAL(applyAllForQueryRequested(QString)),
          SLOT(applyAllForQuery(QString)));
  connect(mFileImpactWidget, SIGNAL(applyForQueryAtRangeRequested(QString, KDevelop::SimpleRange)),
          SLOT(applyQueryForRange(QString, KDevelop::SimpleRange)));

  connect(mCurFileImpactWidget, SIGNAL(applyAllRequested()),
          SLOT(applyAll()));
  connect(mCurFileImpactWidget, SIGNAL(applyAllForQueryRequested(QString)),
          SLOT(applyAllForQuery(QString)));
  connect(mCurFileImpactWidget, SIGNAL(applyForQueryAtRangeRequested(QString, KDevelop::SimpleRange)),
          SLOT(applyQueryForRange(QString, KDevelop::SimpleRange)));

  connect(mResultTreeWidget, SIGNAL(currentFileChanged(QString))
    , SLOT(handleFileSelectionChange(QString)));
  connect(mResultTreeWidget, SIGNAL(currentQueryChanged(QString))
    , SLOT(handleQuerySelectionChange(QString)));
  connect(mResultTreeWidget, SIGNAL(fileDoubleClicked(QString))
  , SLOT(handleFileDoubleClick(QString)));

  connect(ICore::self()->projectController(), SIGNAL(projectOpened(KDevelop::IProject*))
    , SLOT(projectOpened(KDevelop::IProject*)));
  connect(ICore::self()->projectController(), SIGNAL(projectClosed(KDevelop::IProject*))
    , SLOT(projectClosed(KDevelop::IProject*)));
  connect(ICore::self()->documentController(), SIGNAL(documentActivated(KDevelop::IDocument*))
    , SLOT(setCurrentDocument(KDevelop::IDocument*)));
  connect(ICore::self()->documentController(), SIGNAL(documentClosed(KDevelop::IDocument*))
    , SLOT(currentDocumentClosed()));

  connect(DUChain::self()->notifier(), SIGNAL(branchAdded(KDevelop::DUContextPointer)),
          this, SLOT(duchainChanged(KDevelop::DUContextPointer)));
  connect(DUChain::self()->notifier(), SIGNAL(branchModified(KDevelop::DUContextPointer)),
          this, SLOT(duchainChanged(KDevelop::DUContextPointer)));
  connect(DUChain::self()->notifier(), SIGNAL(branchRemoved(KDevelop::DUContextPointer)),
          this, SLOT(duchainChanged(KDevelop::DUContextPointer)));

  projectActivated(mUi->mProjectCombo->currentText());
  setCurrentDocument(ICore::self()->documentController()->activeDocument());

  /// Open the last used database.
  KSharedConfigPtr config = KGlobal::config();
  KConfigGroup group = config->group("cpp_query_engine");
  const KUrl xmlFile = group.readEntry("prev_database");
  if (QFile::exists(xmlFile.toLocalFile()))
    QMetaObject::invokeMethod(this, "openDatabase", Qt::QueuedConnection, Q_ARG(KUrl, xmlFile));

  if(!group.hasKey("Colors Configured"))
    AstColorDialog::writeDefaultConfig();
}

CppQueryWidget::~CppQueryWidget()
{ }

void CppQueryWidget::duchainChanged(DUContextPointer p)
{
  //TODO: optimize
  if (p->url() == mCurrentFile && !mJob) {
    // update current file
    startFileQuery();
  }
}

void CppQueryWidget::handleJobResult( KJob *job )
{
  handleJobDeletion( job );
}

void CppQueryWidget::handleJobDeletion( QObject *job )
{
  if ( mJob == job ) {
    mJob = 0;
    setCorrectQueryControlWidgetsStatus();
  }
}

void CppQueryWidget::handleResult(const QueryResult &result)
{
  Q_ASSERT( result.status() == QueryResult::Match );

  // FIXME: Get rid of removeProjectPath, at least at this level
  const QString file = removeProjectPath(result.url());
  mResultModel->add(file, result);
  mResultTreeWidget->update(file, result);
  if (result.url() == mCurrentFile) {
    mCurFileResultModel->add(result.url().str(), result);
    mCurFileImpactWidget->setFile(mCurrentFile.toUrl().toLocalFile());
    mFileImpactWidget->setFile(file);
    mResultTreeWidget->setCurrentFile(file);
  }

  setCorrectQueryControlWidgetsStatus();
}

/// Private functions

QList<Query::Ptr> CppQueryWidget::currentQueries() const
{
  QList<Query::Ptr> result;

  if (mUi->mCmbQueries->count() == 0 || mUi->mCmbQueries->currentIndex() == -1)
    return result;

  if (mUi->mCBoxAllQueries->isChecked()) {
    for (int i = 0; i < mUi->mCmbQueries->count(); ++i) {
      QVariant data = mUi->mCmbQueries->itemData( i );
      result << data.value<Query::Ptr>();
    }
  } else {
    QVariant data = mUi->mCmbQueries->itemData( mUi->mCmbQueries->currentIndex() );
    result << data.value<Query::Ptr>();
  }

  return result;
}

QString CppQueryWidget::removeProjectPath(KDevelop::IndexedString const &url)
{
  QString result = url.str();
  if (mCurrentProject) {
    result.remove(mCurrentProject->folder().toLocalFile());
  }
  return result;
}

/// Protected slots

void CppQueryWidget::applyAll()
{
  // TODO: Implement
}

void CppQueryWidget::applyAllForQuery(const QString &queryId)
{
  Transform::Ptr transform = mDatabase.transform(queryId);
  if (!transform) {
    qDebug() << "Trying to apply a transform for a query which has no transform assigned";
    return;
  }

  const QString fileUrl = removeProjectPath(IndexedString(mCurrentFile));
  mTransformEngine->apply(transform, mResultModel->result(queryId, fileUrl));

  startFileQuery();
}

void CppQueryWidget::applyQueryForRange(const QString &queryId, const KDevelop::SimpleRange &range)
{
  Transform::Ptr transform = mDatabase.transform(queryId);
  if (!transform) {
    qDebug() << "Trying to apply a transform for a query which has no transform assigned";
    return;
  }

  const QString fileUrl = removeProjectPath(IndexedString(mCurrentFile));
  foreach (QueryHit::Ptr use, mResultModel->result(queryId, fileUrl).hits()) {
    if (use->range().castToSimpleRange() == range) {
      mTransformEngine->apply(transform, use);
      startFileQuery();
      break;
    }
  }
}

void CppQueryWidget::configureColors()
{
  AstColorDialog dialog;
  dialog.exec();
}

void CppQueryWidget::handleFileSelectionChange(const QString &file)
{
  mFileImpactWidget->setFile(file);
}

void CppQueryWidget::handleFileDoubleClick(const QString &file)
{
  ICore::self()->documentController()->openDocument(KUrl(file));
}

void CppQueryWidget::handleQuerySelectionChange(const QString &query)
{
  mFileImpactWidget->setSelectedQuery(query);
}

void CppQueryWidget::fillQueryCombo()
{
  mUi->mCmbQueries->clear();

  QList<Query::Ptr> queries;
  switch (mUi->mCmbFilter->currentIndex()) {
    case 0: // Queries Only
      queries = mDatabase.queries(CppPortingDatabase::QueriesOnly);
      break;
    case 1: // Transforms Only
      queries = mDatabase.queries(CppPortingDatabase::TransformsOnly);
      break;
    case 2: // Queries and transforms
      queries = mDatabase.queries(CppPortingDatabase::All);
      break;
  };

  foreach (Query::Ptr const &query, queries) {
    QVariant data;
    data.setValue<Query::Ptr>(query);
    mUi->mCmbQueries->addItem(query->id(), data);
  }

  setCorrectQueryControlWidgetsStatus();
}

void CppQueryWidget::openDatabase(KUrl const &fileName)
{
  if (fileName.isEmpty() || !fileName.isValid())
    return;

  mUi->mDatabaseSelector->setUrl(fileName);

  const QString file = fileName.toLocalFile();

  qDebug() << "opening xml database" << file;

  mDatabase = CppPortingDatabase(); // Reset the database
  XmlParser parser;
  XmlParser::ParseResult result = parser.parse(file, &mDatabase);

  if (result != XmlParser::Ok) {
    mUi->mCmbQueries->clear();
    setCorrectQueryControlWidgetsStatus();

    if (result == XmlParser::FileNotFound) {
      KMessageBox::error(QApplication::activeWindow(), i18n("Could not find C++ Query Database %1.", file), i18n("File Not Found"));
    } else if (result == XmlParser::FileNotReadable) {
      KMessageBox::error(QApplication::activeWindow(), i18n("Could not read C++ Query Database %1.", file), i18n("File Not Readable"));
    } else if (result == XmlParser::Invalid) {
      KMessageBox::errorList(QApplication::activeWindow(), i18n("The file %1 is not a valid C++ Query Database.", file), parser.errorMessages(),
                            i18n("Invalid Query Database"));
    }

    return;
  }

  KSharedConfigPtr config = KGlobal::config();
  KConfigGroup group = config->group("cpp_query_engine");
  group.writeEntry("prev_database", fileName);
  config->sync();

  fillQueryCombo();
}

void CppQueryWidget::projectActivated(QString const &project)
{
  IProject *iProject = ICore::self()->projectController()->findProjectByName(project);

  if (!iProject)
    return; // TODO: Remove from combo also maybe.

  if (mCurrentProject == iProject)
    return; // Nothing todo.

  mCurrentProject = iProject;

  mResultTreeWidget->clear();
  mFileImpactWidget->setProjectPath(iProject->folder().toLocalFile());

  setCorrectQueryControlWidgetsStatus();
}

void CppQueryWidget::projectClosed(IProject* project)
{
  Q_ASSERT(project);

  int index = mUi->mProjectCombo->findText(project->name());
  if (index != -1)
    mUi->mProjectCombo->removeItem(index);

  if (mCurrentProject == project) {
    mCurrentProject = 0;
    mResultTreeWidget->clear();
  }

  bool enable = mUi->mProjectCombo->count() > 0;
  mUi->mProjectCombo->setEnabled(enable);
  mFileImpactWidget->setEnabled(enable);
  mResultTreeWidget->setEnabled(enable);

  if (enable && !mCurrentProject) {
    projectActivated(mUi->mProjectCombo->currentText());
  }
}

void CppQueryWidget::projectOpened(IProject* project)
{
  Q_ASSERT(project);
  mUi->mProjectCombo->addItem(project->name());
  mUi->mProjectCombo->setEnabled(true);
  mFileImpactWidget->setEnabled(true);
  mResultTreeWidget->setEnabled(true);

  if (mUi->mProjectCombo->count() == 1)
    projectActivated(project->name());
}

void CppQueryWidget::currentDocumentClosed()
{
  mCurrentFile = IndexedString();
  mCurFileResultModel->clear();
  mCurFileImpactWidget->setFile(QString());
  setCorrectQueryControlWidgetsStatus();
}

void CppQueryWidget::setCurrentDocument(IDocument* doc)
{
  if (!doc) {
    return;
  }
  mCurrentFile = IndexedString(doc->url());
  setCorrectQueryControlWidgetsStatus();
  startFileQuery();
}

void CppQueryWidget::showAST()
{
  if ( mCurrentFile.isEmpty() )
    return;

  mUi->mbtnViewAst->setEnabled(false);

  ReferencedTopDUContext document;
  {
    DUChainReadLocker lock(DUChain::self()->lock());
    document = (DUChain::self()->chainForDocument(mCurrentFile));
  }

  if (!document) {
    mUi->mbtnViewAst->setEnabled(true);
    return;
  }

  if (!document->ast()) {
    TopDUContext::Features features = TopDUContext::AllDeclarationsContextsAndUses;
    features = (TopDUContext::Features) (features | TopDUContext::AST);
    document = DUChain::self()->waitForUpdate(document->url(), features);
  }

  Q_ASSERT(document->ast());

  ParseSession* session = dynamic_cast<ParseSession*>(document->ast().data());

  ASTModelBuilder modelBuilder;
  QStandardItemModel *model = modelBuilder.createModel(session);

  QWidget *widget = new QWidget();
  model->setParent(widget);

  QTreeView *tree = new QTreeView(widget);
  tree->setEditTriggers(QAbstractItemView::NoEditTriggers);
  tree->setModel(model);
  tree->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QGridLayout *layout = new QGridLayout(widget);
  layout->addWidget(tree, 0, 0);
  widget->setLayout(layout);
  widget->resize(400,400);
  widget->setWindowTitle(i18n("AST for file %1", mCurrentFile.toUrl().pathOrUrl()));
  widget->show();

  mUi->mbtnViewAst->setEnabled(true);
}

void CppQueryWidget::setCorrectQueryControlWidgetsStatus()
{
  bool const enable = mUi->mCmbQueries->count() > 0
    && mCurrentProject && (mJob == 0);

  mUi->mCmbQueries->setEnabled(enable && !mUi->mCBoxAllQueries->isChecked());
  mUi->mCmbFilter->setEnabled(!mDatabase.queries().isEmpty());
  mUi->mCBoxAllQueries->setEnabled(enable);
  mUi->mBtnRunFileQuery->setEnabled(enable);
  mUi->mBtnRunProjectQuery->setEnabled(enable);

  mUi->mbtnViewAst->setEnabled(!mCurrentFile.isEmpty());
  mCurFileImpactWidget->setEnabled(mCurFileResultModel->fileCount() > 0);
}

void CppQueryWidget::fileQueryButtonClicked()
{
  mUi->mTabWidget->setCurrentWidget(mUi->mCurrentFileTab);
  startFileQuery();
}

void CppQueryWidget::startFileQuery()
{
  if (mCurrentFile.isEmpty())
    return;

  if (currentQueries().isEmpty())
    return;

  if (mJob != 0 || mStartingQuery)
    return;

  mStartingQuery = true;

  mUi->mBtnRunFileQuery->setEnabled(false);
  mUi->mBtnRunProjectQuery->setEnabled(false);

  mCurFileResultModel->clear();

  if (mFileImpactWidget->currentFile() != mCurrentFile.toUrl().toLocalFile()) {
    mFileImpactWidget->setFile(mCurrentFile.toUrl().toLocalFile());
    mResultTreeWidget->setCurrentFile(mCurrentFile.toUrl().toLocalFile());
  }

  Q_ASSERT( !mJob );
  mJob = new QueryJob(mCurrentFile, currentQueries());
  connect(mJob, SIGNAL(resultFound(QueryResult)), SLOT(handleResult(QueryResult)) );
  connect(mJob, SIGNAL(result(KJob*)), SLOT(handleJobResult(KJob*)));
  connect(mJob, SIGNAL(destroyed(QObject*)), SLOT(handleJobDeletion(QObject*)));

  mStartingQuery = false;
}

void CppQueryWidget::projectQueryButtonClicked()
{
  mUi->mTabWidget->setCurrentWidget(mUi->mProjectTab);
  startProjectQuery();
}

void CppQueryWidget::startProjectQuery()
{
  if ( !mCurrentProject )
    return;

  if (currentQueries().isEmpty())
    return;

  if (mJob != 0 || mStartingQuery)
    return;

  mStartingQuery = true;

  mUi->mBtnRunFileQuery->setEnabled(false);
  mUi->mBtnRunProjectQuery->setEnabled(false);

  mResultTreeWidget->clear();
  mCurFileResultModel->clear();
  mResultModel->clear();

  Q_ASSERT( !mJob );
  mJob = new QueryJob(mCurrentProject, currentQueries());
  connect(mJob, SIGNAL(resultFound(QueryResult)), SLOT(handleResult(QueryResult)));
  connect(mJob, SIGNAL(result(KJob*)), SLOT(handleJobResult(KJob*)));
  connect(mJob, SIGNAL(destroyed(QObject*)), SLOT(handleJobDeletion(QObject*)));

  mStartingQuery = false;
}

#include "cppquerywidget.moc"
