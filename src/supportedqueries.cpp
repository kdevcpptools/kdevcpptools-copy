/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "supportedqueries.h"

#include <languages/cpp/cppduchain/expressionparser.h>

#include <insertiontransform.h>
#include <replacementtransform.h>
#include <usequery.h>

using namespace KDevelop;

void SupportedQueries::init(CppTransformEngine::Ptr const &engine)
{
  typeQPtrList(engine);
  ctorQStringConstCharPtr(engine);
  ctorQStringConstStdStringRef(engine);
}

void SupportedQueries::ctorQStringConstCharPtr(CppTransformEngine::Ptr const &engine)
{
  // Construct the UseQuery
  UseQuery query(QualifiedIdentifier("QString"));
  query.setMember(Identifier("QString"));
  query.appendMemberArgumentType("const char*");
  query.addRestriction(UseQuery::CharLiteralRestriction, 0, "%(\\S+).*%\\1");

  engine->registerTransform(Transform::Ptr(new Transform(engine, query)));
}

void SupportedQueries::ctorQStringConstStdStringRef(CppTransformEngine::Ptr const &engine)
{
  // QString(const std::string&);
  // Construct the UseQuery
  UseQuery query(QualifiedIdentifier("QString"));
  query.setMember(Identifier("QString"));
  query.appendMemberArgumentType("std::string const &");

  // NOTE: Currently inserts always happen at the use location. This is the default
  //       location for insert actions.
  Condition c(DeclarationUse::ImplicitCtorCall, QVariant::fromValue<bool>(true));
  TransformRule rule(c);

  rule.setIfAction(InsertAction::Ptr(new InsertAction("= QString::fromStdString")));
  rule.setElseAction(InsertAction::Ptr(new InsertAction("::fromStdString")));

  InsertionTransform *transform = new InsertionTransform(engine, query);
  transform->addTransformRule(rule);
  engine->registerTransform(Transform::Ptr(transform));
}

void SupportedQueries::typeQPtrList(CppTransformEngine::Ptr const &engine)
{
  UseQuery query(QualifiedIdentifier("QPtrList"));
  engine->registerTransform(Transform::Ptr(new Transform(engine, query)));

  // Finds only instantations with int
  query.addRestriction(UseQuery::TemplateArgumentRestriction, 0, "int");

  // NOTE: Template are not restricted to Types, but can also have contants.
  // ReplaceAction action("@type", "QList");
  // action.addReplacement("@templateArg[0]", "${@templateArg[0]} *");
  ReplaceAction::Ptr action(new ReplaceAction("QList")); // Replaces the type with QList
  // Replaces the first template arg with its value and " *" appended.
  action->addItemReplacement(ReplaceAction::TemplateArg, 0, "${templateArg} *");

  TransformRule rule(Condition(true));
  rule.setIfAction(action);

  ReplacementTransform *transform = new ReplacementTransform(engine, query);
  transform->addTransformRule(rule);
  engine->registerTransform(Transform::Ptr(transform));
}
