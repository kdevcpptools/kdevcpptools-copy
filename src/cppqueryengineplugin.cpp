/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "cppqueryengineplugin.h"

#include <kaboutdata.h>
#include <klocale.h>
#include <kpluginfactory.h>
#include <kpluginloader.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>

#include <interfaces/context.h>
#include <interfaces/contextmenuextension.h>
#include <language/interfaces/codecontext.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/functiondeclaration.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/types/functiontype.h>

#include <QtCore/QMimeData>
#include <QtGui/QApplication>
#include <QtGui/QClipboard>
#include <QtGui/QAction>

#include "cppquerywidget.h"
#include <QTextDocument>

K_PLUGIN_FACTORY(KDevCppQueryEngineFactory, registerPlugin<CppQueryEnginePlugin>(); )
K_EXPORT_PLUGIN(KDevCppQueryEngineFactory(
                    KAboutData("kdevcppqueryengine"
                    ,"kdevcppqueryengine"
                    , ki18n("Cpp Query Engine")
                    , "0.1"
                    , ki18n("Queries cpp code for various constructs.")
                    , KAboutData::License_GPL)))

class CppQueryEngineFactory: public KDevelop::IToolViewFactory
{
  public:
    CppQueryEngineFactory(CppQueryEnginePlugin *plugin): m_plugin(plugin) {}

    virtual QWidget* create(QWidget *parent = 0)
    {
      return new CppQueryWidget(parent /*, m_plugin */);
    }

    virtual Qt::DockWidgetArea defaultPosition()
    {
      return Qt::BottomDockWidgetArea;
    }

    virtual QString id() const
    {
      return "org.kdevelop.CppQueryEngineView";
    }

  private:
    CppQueryEnginePlugin *m_plugin;
};

CppQueryEnginePlugin::CppQueryEnginePlugin(QObject *parent, const QVariantList&)
    : KDevelop::IPlugin(KDevCppQueryEngineFactory::componentData(), parent)
    , m_factory(new CppQueryEngineFactory(this))
{
  core()->uiController()->addToolView(i18n("C++ Query Engine"), m_factory);
  setXMLFile( "kdevcppqueryengine.rc" );

  m_getQuery = new QAction(i18n("Copy XML Query"), this);
  connect(m_getQuery, SIGNAL(triggered(bool)), this, SLOT(getQuery()));
}

CppQueryEnginePlugin::~CppQueryEnginePlugin()
{ }

void CppQueryEnginePlugin::unload()
{
  core()->uiController()->removeToolView(m_factory);
}

KDevelop::ContextMenuExtension CppQueryEnginePlugin::contextMenuExtension(KDevelop::Context* context)
{
  KDevelop::ContextMenuExtension menuExt = KDevelop::IPlugin::contextMenuExtension(context);
  KDevelop::DeclarationContext *codeContext = dynamic_cast<KDevelop::DeclarationContext*>(context);

  if (!codeContext)
      return menuExt;

  KDevelop::DUChainReadLocker lock;

  if(!codeContext->declaration().data())
    return menuExt;

  qRegisterMetaType<KDevelop::IndexedDeclaration>("KDevelop::IndexedDeclaration");

  m_getQuery->setData(QVariant::fromValue(codeContext->declaration()));
  menuExt.addAction(KDevelop::ContextMenuExtension::ExtensionGroup, m_getQuery);

  return menuExt;
}

QString queryForDeclaration(KDevelop::Declaration* decl)
{
  QString s;

  //TODO: template declarations
  //TODO: enumerator
  //TODO: enumerations
  if (decl->isFunctionDeclaration()) {
    bool isClass = dynamic_cast<KDevelop::ClassFunctionDeclaration*>(decl);
    if (isClass) {
      s = "<class-function-query";
    } else {
      s = "<global-function-query";
    }

    s += " uid=\"" + Qt::escape(decl->toString()) + '"'
       + " qid=\"" + Qt::escape(decl->qualifiedIdentifier().toString()) + '"';

    KDevelop::FunctionType::Ptr type = decl->abstractType().cast<KDevelop::FunctionType>();

    if (!type || !type->indexedArgumentsSize()) {
      s += " />\n";
    } else {
      s += ">\n";
      foreach ( const KDevelop::AbstractType::Ptr& arg, type->arguments()) {
        s += " <argument type=\"" + Qt::escape(arg->toString()) + "\" />\n";
      }
      if (isClass) {
        s += "</class-function-query>\n";
      } else {
        s += "</global-function-query>\n";
      }
    }
  } else if (KDevelop::ClassDeclaration* classDecl = dynamic_cast<KDevelop::ClassDeclaration*>(decl)) {
    s += "<class-query uid=\"" + Qt::escape(classDecl->toString()) + '"'
      + " qid=\"" + Qt::escape(decl->qualifiedIdentifier().toString()) + "\" />\n";
  }

  return s;
}

void CppQueryEnginePlugin::getQuery()
{
  KDevelop::IndexedDeclaration indexedDecl = m_getQuery->data().value<KDevelop::IndexedDeclaration>();

  KDevelop::DUChainReadLocker lock;
  Q_ASSERT(KDevelop::DUChain::lock()->currentThreadHasReadLock() || KDevelop::DUChain::lock()->currentThreadHasWriteLock());
  KDevelop::Declaration* decl = indexedDecl.declaration();
  if (!decl) {
    return;
  }

  QMimeData *data = new QMimeData();

  QString s = queryForDeclaration(decl);
  // add more child queries
  if (KDevelop::DUContext* ctx = decl->logicalInternalContext(decl->topContext())) {
    if (ctx->type() == KDevelop::DUContext::Class) {
      foreach(KDevelop::Declaration* decl, ctx->localDeclarations()) {
        s += queryForDeclaration(decl);
      }
    } //TODO: enums, namespaces
  }

  data->setHtml(s);
  data->setText(s);

  QApplication::clipboard()->setMimeData(data);
}

#include "cppqueryengineplugin.moc"
