/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "cppportingdb_export.h"

class CppPortingDatabase;
class QString;
class QStringList;

class CPPPORTDB_EXPORT XmlParser
{
  public:
    enum ParseResult {
      FileNotFound,    /// Xml file does not exists.
      FileNotReadable, /// Xml file could not be opened for reading
      Ok,              /// Parse finished, no Errors occured.
      Invalid          /// The Xml file is not a valid porting description.
                       /// See errorMessage() for details.
    };

  public: /// Methods
    XmlParser();
    ~XmlParser();

    /**
     * Parses the file and adds the queries and transformation according to
     * parsed descriptions to the database. The parser will try to parse as much
     * as possible, ignoring invalid queries and transforms.
     */
    ParseResult parse(QString const &fileUrl, CppPortingDatabase * const db);

    QStringList errorMessages() const;

  private: /// Members
    class XmlParserPrivate * const d;
};

#endif // XMLPARSER_H
