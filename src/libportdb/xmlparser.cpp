/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "xmlparser.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QStack>
#include <QtCore/QStringList>
#include <QtXmlPatterns/QXmlQuery>

#include <libquery/queries/classquery.h>
#include <libquery/queries/functionquery.h>
#include <libquery/queries/classfunctionquery.h>
#include <libquery/queries/enumquery.h>

#include <language/duchain/identifier.h>

#include "cppportingdatabase.h"
#include "transformrule.h"

using namespace KDevelop;

/// Class that adds an item to the path stack and removes it when it goes out of scope
class PathTracker
{
  public:
    explicit PathTracker(QStack<QString>& paths, QString path)
      : m_paths(&paths)
    {
      if (!m_paths->isEmpty()) {
        m_paths->push(paths.top() + path);
      } else {
        m_paths->push(path);
      }
    }
    ~PathTracker()
    {
      m_paths->pop();
    }

  private:
    QStack<QString>* m_paths;
};

/// XmlParserPrivate

class XmlParserPrivate
{
  public: /// Members
    QStack<QString>        mCurrentPath;
    CppPortingDatabase    *mDatabase;
    QStringList            mErrorMessages;
    QFile                  mFile;
    QXmlQuery              mQuery;
    QString                mQueryStrPreamble;
    XmlParser::ParseResult mResult;

  public: /// Functions
    XmlParserPrivate();
    ~XmlParserPrivate();
    bool openXmlFile(QString const &url);

    XmlParser::ParseResult parsePortingDescription();

    InsertAction::Ptr parseInsertAction();
    ReplaceAction::Ptr parseReplaceAction();
    QList<Action::Ptr> parseActions();
    QList<Condition> parseConditions();
    bool parseArguments(FunctionQuery::Ptr const &query);

    void parseQueries();
    void parseGlobalFunctionQuery(int i);
    void parseClassQuery(unsigned i);
    void parseClassFunctionQuery(unsigned i);
    void parseEnumQuery(unsigned i);

    void parseSourceApi();
    void parseTemplateArguments(FunctionQuery::Ptr const &query);
    void parseTransform(int i);
    void parseTransformations();
    bool parseTransformRule(Transform::Ptr const &transform, int i);
    bool parseTransformRules(Transform::Ptr const &transform);


    /// Helper methods
    int count(QString const &path);
    Condition createCondition(QString const &property, QString const &expectedValue);
    bool has(QString const &path);
    int readInt(QString const &path);
    QString readString(QString const &path);
    FunctionQuery::ArgumentRestriction restrictionKind(QString const &kind, bool *ok);
};

XmlParserPrivate::XmlParserPrivate() : mDatabase(0)
{ }

XmlParserPrivate::~XmlParserPrivate()
{
  mFile.close();
  mDatabase = 0;
}

bool XmlParserPrivate::openXmlFile(QString const &url)
{
  mFile.setFileName(url);
  if (!mFile.exists()) {
    mResult = XmlParser::FileNotFound;
    return false;
  }

  if (!mFile.open(QIODevice::ReadOnly)) {
    mResult = XmlParser::FileNotReadable;
    return false;
  }

  mQuery.bindVariable("inputDocument", &mFile);
  mQueryStrPreamble = QString("declare variable $inputDocument external;\n");

  return true;
}

/// XmlPrivate parsing methods

XmlParser::ParseResult XmlParserPrivate::parsePortingDescription()
{
  PathTracker tracker(mCurrentPath, "doc($inputDocument)/porting-description");
  mResult = XmlParser::Ok;

  parseSourceApi();
  parseQueries();
  parseTransformations();

//   Q_ASSERT_X(mErrorMessages.isEmpty(), "XmlParserPrivate::parsePortingDescription", mErrorMessages.join("\n").toUtf8());
  return mResult;
}

bool isItem(QString const &item)
{
  /// @see declarationuse.h
  return item == "FunctionId"
      || item == "TypeId"
      || item == "ObjectId"
      || item == "Accessor"
      || item == "MemberId"
      || item == "All";
}

QueryHit::Item determineItem(QString const &item)
{
  if (item == "FunctionId" || item == "TypeId" || item == "MemberId")
    return QueryHit::Identifier;
  if (item == "ObjectId")
    return QueryHit::ObjectId;
  if (item == "Accessor")
    return QueryHit::Accessor;
  if (item == "All")
    return QueryHit::All;

  Q_ASSERT_X(0, "determineItem", QString("invalid item: %1").arg(item).toUtf8());
  return QueryHit::All;
}

static QRegExp sItemWithOffsetRegExp("(Arg|FunctionTemplateArg|TypeTemplateArg|MemberTemplateArg)\\[(\\d+)\\]");

bool isItemWithOffset(QString const &item)
{
  /// @see declarationuse.h
  return sItemWithOffsetRegExp.indexIn(item) == 0;
}

QPair<QueryHit::ItemWithOffset, unsigned> determineItemAndOffset(QString const &itemAndOffset)
{
  int index = sItemWithOffsetRegExp.indexIn(itemAndOffset);
  Q_ASSERT(index == 0);

  QString item = sItemWithOffsetRegExp.cap(1);
  if (item == "Arg" || item == "FunctionTemplateArg")
    return qMakePair(QueryHit::Arg, sItemWithOffsetRegExp.cap(2).toUInt());
  if (item == "TypeTemplateArg")
  if (item == "MemberTemplateArg")
    return qMakePair(QueryHit::TemplateArg, sItemWithOffsetRegExp.cap(2).toUInt());

  Q_ASSERT(false);
  return qMakePair(QueryHit::Arg, 0u);
}

InsertAction::Ptr XmlParserPrivate::parseInsertAction()
{
  InsertAction::Location location;
  QString sLocation = readString(mCurrentPath.top() + "/string(@location)");
  if (sLocation == "Before")
    location = InsertAction::Before;
  else if (sLocation == "After")
    location = InsertAction::After;
  else {
    mErrorMessages << "Invalid location for insert action: " + mCurrentPath.top();
    return InsertAction::Ptr();
  }

  if (isItem(readString(mCurrentPath.top() + "/string(@item)"))) {
    QueryHit::Item item = determineItem(readString(mCurrentPath.top() + "/string(@item)"));
    QString replacement = readString(mCurrentPath.top() + "/string(.)");
    return InsertAction::Ptr(new InsertAction(location, item, replacement));
  } else {
    mErrorMessages << "Invalid item at:" + mCurrentPath.top() + "/@item";
    return InsertAction::Ptr();
  }
}

ReplaceAction::Ptr XmlParserPrivate::parseReplaceAction()
{
  if (isItem(readString(mCurrentPath.top() + "/string(@item)"))) {
    QueryHit::Item item = determineItem(readString(mCurrentPath.top() + "/string(@item)"));
    QString replacement = readString(mCurrentPath.top() + "/string(.)");
    return ReplaceAction::Ptr(new ReplaceAction(item, replacement));
  } else if (isItemWithOffset(readString(mCurrentPath.top() + "/string(@item)"))) {
    QPair<QueryHit::ItemWithOffset, unsigned> itemAndOffset = determineItemAndOffset(readString(mCurrentPath.top() + "/string(@item)"));
    QString replacement = readString(mCurrentPath.top() + "/string(.)");
    return ReplaceAction::Ptr(new ReplaceAction(itemAndOffset.first, itemAndOffset.second, replacement));
  } else {
    mErrorMessages << "Invalid item at:" + mCurrentPath.top() + "/@item";
    return ReplaceAction::Ptr();
  }
}

QList<Action::Ptr> XmlParserPrivate::parseActions()
{
  QList<Action::Ptr> actions;

  {
    PathTracker tracker(mCurrentPath, "/insert-action");

    int cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++ i) {
      PathTracker tracker_i(mCurrentPath, '[' + QString::number(i) + ']');
      Action::Ptr action = parseInsertAction();
      if (action)
        actions << action;
      else {
        // Something went wrong when parsing the action. The error message is set,
        // return an empty list to make sure that no half transforms are performed.
        return QList<Action::Ptr>();
      }
    }
  }

  {
    PathTracker tracker(mCurrentPath, "/replace-action");
    int cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++ i) {
      PathTracker tracker_i(mCurrentPath, '[' + QString::number(i) + ']');
      Action::Ptr action = parseReplaceAction();
      if (action)
        actions << action;
      else {
        // Something went wrong when parsing the action. The error message is set,
        // return an empty list to make sure that no half transforms are performed.
        return QList<Action::Ptr>();
      }
    }
  }

  if (actions.isEmpty()) {
    mErrorMessages << "Expected at least one action at: " + mCurrentPath.top();
    return actions;
  }
  return actions;
}

QList< Condition > XmlParserPrivate::parseConditions()
{
  if (!has(mCurrentPath.top() + "/condition")) {
    mResult = XmlParser::Invalid;
    mErrorMessages << "If must contain at least one condition.";
    return QList<Condition>();
  }
  const int conditionCount = count(mCurrentPath.top() + "/condition");
  QList<Condition> conditions;
  for (int i = 1; i <= conditionCount; ++i) {
    const QString nr = QString::number(i);
    const QString property = readString(mCurrentPath.top() + "/condition[" + nr + "]/string(@property)");
    const QString expectedValue = readString(mCurrentPath.top() + "/condition[" + nr + "]/string(@expected-value)");

    Condition c = createCondition(property, expectedValue);
    if (!c.isValid()) {
      return QList<Condition>();
    }

    conditions << c;
  }
  return conditions;
}

bool XmlParserPrivate::parseArguments(FunctionQuery::Ptr const &query)
{
  PathTracker tracker(mCurrentPath, "/argument");
  int cnt = count(mCurrentPath.top());

  for (int i = 1; i <= cnt; ++i) {
    PathTracker tracker_i(mCurrentPath, '[' + QString::number(i) + ']');

    QString type = readString(mCurrentPath.top() + "/string(@type)");
    query->appendArgumentType(type);

    if (has(mCurrentPath.top() + "/restriction")) {
      int resCnt = count(mCurrentPath.top() + "/restriction");
      QStringList restrictionKinds;

      for (int j = 1; j <= resCnt; ++j) {
        PathTracker tracker_restriction(mCurrentPath, "/restriction[" + QString::number(j) + ']');
        QString kind = readString(mCurrentPath.top() + "/string(@kind)");

        if (restrictionKinds.contains(kind)) {
          mResult = XmlParser::Invalid;
          mErrorMessages << "Duplicate restriction kind " + kind + " at: " + mCurrentPath.top();
          return false;
        }

        QString restriction = readString(mCurrentPath.top() + "/string(@value)");

        bool ok = true;
        FunctionQuery::ArgumentRestriction resKind = restrictionKind(kind, &ok);
        if (!ok) {
          mResult = XmlParser::Invalid;
          mErrorMessages << "Invalid restriction " + kind + " kind at: " + mCurrentPath.top();
          return false;
        }

        query->addArgumentRestriction(resKind, i - 1, restriction);
      }
    }
  }
  return true;
}

void XmlParserPrivate::parseGlobalFunctionQuery(int i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  QString uid = readString(mCurrentPath.top() + "/string(@uid)");
  if (uid.isEmpty()) {
    mErrorMessages << "Invalid uid for query: " + mCurrentPath.top();
    return;
  }

  if (mDatabase->containsQuery(uid)) {
    mErrorMessages << ("A query with uid " + uid + " is already registered ["
                       + mCurrentPath.top() +']');
    return;
  }

  QString typeQid = readString(mCurrentPath.top() + "/string(@qid)");
  FunctionQuery::Ptr query = FunctionQuery::Ptr(new FunctionQuery(uid, QualifiedIdentifier(typeQid)));

  if (has(mCurrentPath.top() + "/template-argument"))
    parseTemplateArguments(query);

  // Parse member if necessesary
  if (has(mCurrentPath.top() + "/argument")) {
    // Search for a global function
    if (!parseArguments(query)) {
      return;
    }
  }

  // Parse type template parameter restrictions if necessesary
  if (has(mCurrentPath.top() + "/template-parameters")) {
    Q_ASSERT_X(false, "XmlParserPrivate::parseQuery", "Template parameters not implemented yet");
    // FIXME: Implement
//     if (!parseTemplateParameters(&query) ) {
//       mCurrentPath.pop();
//       return;
//     }
  }

  mDatabase->addQuery(query);
}

void XmlParserPrivate::parseQueries()
{
  PathTracker tracker(mCurrentPath, "/queries");
  int cnt = count(mCurrentPath.top());
  if (cnt == 0) {
    mResult = XmlParser::Invalid;
    mErrorMessages << "Missing mandotory queries element";
    return;
  } else if (cnt > 1) {
    mResult = XmlParser::Invalid;
    mErrorMessages << "Only one queries element allowed";
    return;
  }
  // Read the global function queries.
  {
    PathTracker tracker_funcs(mCurrentPath, "/global-function-query");
    cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++i)
      parseGlobalFunctionQuery(i);
  }

  // Read the Class queries, must be done before member function queries.
  {
    PathTracker tracker_classes(mCurrentPath, "/class-query");
    cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++i)
      parseClassQuery(i);
  }

  // Read the class function queries.
  {
    PathTracker tracker_classfuncs(mCurrentPath, "/class-function-query");
    cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++i)
      parseClassFunctionQuery(i);
  }

  // Read the enum queries
  {
    PathTracker tracker_enums(mCurrentPath, "/enum-query");
    cnt = count(mCurrentPath.top());

    for (int i = 1; i <= cnt; ++i)
      parseEnumQuery(i);
  }
}

void XmlParserPrivate::parseSourceApi()
{
  // FIXME: Implement.
}

void XmlParserPrivate::parseTemplateArguments(FunctionQuery::Ptr const &query)
{
  PathTracker tracker(mCurrentPath, "/template-argument");
  int cnt = count(mCurrentPath.top());

  for (int i = 1; i <= cnt; ++i) {
    PathTracker tracker_i(mCurrentPath, '[' + QString::number(i) + ']');

    QString type = readString(mCurrentPath.top() + "/string(@restriction)");
    query->appendTemplateRestriction(type);
  }
}

void XmlParserPrivate::parseTransform(int i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  QString queryId = readString(mCurrentPath.top() + "/string(@queryId)");
  if (!mDatabase->containsQuery(queryId)) {
    mErrorMessages << "Invalid queryId for transform: " + mCurrentPath.top();
    mResult = XmlParser::Invalid;
    return;
  }

  Query::Ptr query = mDatabase->query(queryId);
  Transform::Ptr transform(new Transform(query));
  if (parseTransformRules(transform))
    mDatabase->addTransform(queryId, transform);
}

void XmlParserPrivate::parseTransformations()
{
  PathTracker tracker(mCurrentPath, "/transformations");

  int cnt = count(mCurrentPath.top());
  if (cnt > 1) {
    mResult = XmlParser::Invalid;
    mErrorMessages << "Only one transformations element allowed";
    return;
  }

  PathTracker tracker_transform(mCurrentPath, "/transform");
  cnt = count(mCurrentPath.top());

  // NOTE: Start at 1 otherwise the xml queries won't work as expected.
  for (int i = 1; i <= cnt; ++i)
    parseTransform(i);
}

bool XmlParserPrivate::parseTransformRule(Transform::Ptr const &transform, int i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  TransformRule rule;

  if (has(mCurrentPath.top() + "/if")) {
    QList<Condition> conditions;
    QList<Action::Ptr> actions;
    {
      PathTracker tracker_if(mCurrentPath, "/if");
      conditions = parseConditions();
      actions = parseActions();
    }

    if (!actions.isEmpty() && !conditions.isEmpty()) { // If there is an if element there should be at least one action.
      rule.setIfActions(conditions, actions);
    } else { // parseAction() will have set the error message.
      return false;
    }
    // parse Else action;
    if (has(mCurrentPath.top() + "/else")) {
      PathTracker tracker_else(mCurrentPath, "/else");
      actions = parseActions();

      if (!actions.isEmpty()) {
        rule.setElseActions(actions);
      } else { // parseAction() will have set the error message.
        return false;
      }
    }
  }

  if (has(mCurrentPath.top() + "/insert-action") || has(mCurrentPath.top() + "/replace-action") ) {
    rule.setActions(parseActions());
  }
  Q_ASSERT(rule.isValid());
  // This call will only be executed if the actions where parsed correctly.
  transform->addTransformRule(rule);

  return true;
}

bool XmlParserPrivate::parseTransformRules(Transform::Ptr const &transform)
{
  PathTracker tracker(mCurrentPath, "/rule");

  int cnt = count(mCurrentPath.top());
  if (cnt == 0) {
    mErrorMessages << "A transformation requires at least one rule: " + mCurrentPath.top();
    return false;
  }

  for (int i = 1; i <= cnt; ++i) {
    if (!parseTransformRule(transform, i)) {
      return false;
    }
  }
  return true;
}

//<class-query id="Test" qid="Test" />
//
//<class-query id="Test&lt;T&gt;" qid="Test">
//  <template-argument />
//<class-query/>
//<class-query id="Test&lt;T&gt;" qid="Test">
//  <template-argument restriction="QString" />
//<class-query/>
void XmlParserPrivate::parseClassQuery(unsigned i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  QString id = readString(mCurrentPath.top() + "/string(@uid)");
  Q_ASSERT(!id.isEmpty());

  if (!mDatabase->containsQuery(id)) {
    QualifiedIdentifier qid(readString(mCurrentPath.top() + "/string(@qid)"));
    ClassQuery::Ptr qry(new ClassQuery(id, qid));

    QString parentClass = readString(mCurrentPath.top() + "/string(@subclass-of)");
    if (!parentClass.isEmpty()) {
      qry->setSubclassOf(QualifiedIdentifier(parentClass));
    }

    PathTracker tracker_tplarg(mCurrentPath, "/template-argument");
    int cnt = count(mCurrentPath.top());
    for (int i = 1; i <= cnt; ++i) {
      PathTracker tracker_tplarg_i(mCurrentPath, '[' + QString::number(i) + ']');
      QString restriction = readString(mCurrentPath.top() + "/string(@restriction)");
      qry->appendTemplateRestriction(restriction);
    }

    mDatabase->addQuery(qry);
  } else
    mErrorMessages << "Duplicate TypeQuery Id: " + id + mCurrentPath.top();
}

//<class-function-query id="Test::asdf()" qid="Test::asdf" />
//<class-function-query id="Test::asdf()" qid="Test::asdf" const="true" />
//<class-function-query id="Test::asdf()" qid="Test::asdf" const="false" />
//
// See also global-function-query for the arguments & template restrictions
// TODO: parent class query for apriory filtering
/*
bool XmlParserPrivate::parseMember(UseQuery::Ptr const &query)
{
  QString memberConstness = readString(mCurrentPath.top() + "/string(@const)");
  if (!memberConstness.isEmpty()) {
    if (memberConstness == "true")
      query->setMemberIsConst(true);
  }

  if (has(mCurrentPath.top() + "/template-argument"))
    parseTemplateArguments(query);

  bool ok = true;
  if (ok && has(mCurrentPath.top() + "/argument"))
    ok = parseArguments(query);

  mCurrentPath.pop(); // /member
  return ok;
}
*/
void XmlParserPrivate::parseClassFunctionQuery(unsigned int i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  QString uid = readString(mCurrentPath.top() + "/string(@uid)");
  if (uid.isEmpty()) {
    mErrorMessages << "Invalid uid for query: " + mCurrentPath.top();
    return;
  }

  if (mDatabase->containsQuery(uid)) {
    mErrorMessages << ("A query with uid " + uid + " is already registered ["
                       + mCurrentPath.top() +']');
    return;
  }

  const QString& typeQid = readString(mCurrentPath.top() + "/string(@qid)");
  ClassFunctionQuery::Ptr query(new ClassFunctionQuery(uid, QualifiedIdentifier(typeQid)));

  const QString& constness = readString(mCurrentPath.top() + "/string(@const)");
  if (!constness.isEmpty()) {
    if (constness == "true")
      query->setConstnessRestriction(ClassFunctionQuery::RestrictConst);
    else
      query->setConstnessRestriction(ClassFunctionQuery::RestrictNonConst);
  } // else default == NoRestrictConst

  if (has(mCurrentPath.top() + "/argument")) {
    if (!parseArguments(query)) {
      return;
    }
  }

  if (has(mCurrentPath.top() + "/template-argument"))
    parseTemplateArguments(query);

  // Parse type template parameter restrictions if necessesary
  if (has(mCurrentPath.top() + "/template-parameters")) {
    Q_ASSERT_X(false, "XmlParserPrivate::parseQuery", "Template parameters not implemented yet");
    // FIXME: Implement
//     if (!parseTemplateParameters(&query) ) {
//       mCurrentPath.pop();
//       return;
//     }
  }

  mDatabase->addQuery(query);
}

void XmlParserPrivate::parseEnumQuery(unsigned int i)
{
  PathTracker tracker(mCurrentPath, '[' + QString::number(i) + ']');

  QString uid = readString(mCurrentPath.top() + "/string(@uid)");
  if (uid.isEmpty()) {
    mErrorMessages << "Invalid uid for query: " + mCurrentPath.top();
    return;
  }

  if (mDatabase->containsQuery(uid)) {
    mErrorMessages << ("A query with uid " + uid + " is already registered ["
                       + mCurrentPath.top() +']');
    return;
  }

  const QString& typeQid = readString(mCurrentPath.top() + "/string(@qid)");
  EnumQuery::Ptr query(new EnumQuery(uid, QualifiedIdentifier(typeQid)));

  mDatabase->addQuery(query);
}

/// XmlPrivate helper methods

int XmlParserPrivate::count(QString const &path)
{
  return readInt("count(" + path + ')');
}

Condition XmlParserPrivate::createCondition(QString const &property, QString const &expectedValue)
{
  if (property == "ImplicitCtorCall") {
    if (expectedValue == "true" || expectedValue == "false")
      return Condition(Condition::ImplicitCtorCall, QVariant(expectedValue == "true"));
    else {
      mErrorMessages << "Expected 'true' or 'false' as expected value at: " << mCurrentPath.top();
      return Condition();
    }
  } else if (property == "ArgCount") {
    bool ok = false;
    int argc = expectedValue.toInt(&ok);
    if (ok && argc >= 0)
      return Condition(Condition::ArgCount, QVariant(argc));

    mErrorMessages << "Expected positive integer as expected value at: " << mCurrentPath.top();
    return Condition();
  } else if (property == "ImplicitOnThis") {
    if (expectedValue == "true" || expectedValue == "false")
      return Condition(Condition::ImplicitOnThis, QVariant(expectedValue == "true"));
    else {
      mErrorMessages << "Expected 'true' or 'false' as expected value at: " << mCurrentPath.top();
      return Condition();
    }
  } else {
    mErrorMessages << "Unsupported property " + property + " at: " << mCurrentPath.top();
    return Condition();
  }
}

bool XmlParserPrivate::has(QString const &path)
{
  return readInt("count(" + path + ')');
}

int XmlParserPrivate::readInt(QString const &path)
{
  QString intString = readString(path);

  bool ok = false;
  int number = intString.toInt(&ok);
  Q_ASSERT(ok);

  return number;
}

QString XmlParserPrivate::readString(QString const &path)
{
  QString query = mQueryStrPreamble;
  query += path;
  mQuery.setQuery(query);

  Q_ASSERT(mQuery.isValid());

  QString result;
  mQuery.evaluateTo(&result);
  result.replace("&amp;", "&"); // There must be a way to prevent this I guess.
  result.replace("&lt;", "<");
  result.replace("&gt;", ">");
  return result.trimmed();
}

FunctionQuery::ArgumentRestriction XmlParserPrivate::restrictionKind(QString const &kind, bool *ok)
{
  *ok = true;
  if (kind == "LiteralRestriction") {
    return FunctionQuery::LiteralRestriction;
  } else if (kind == "QidRestriction") {
    return FunctionQuery::QidRestriction;
  }

  *ok = false;
  return FunctionQuery::LiteralRestriction;
}

/// XmlParser

XmlParser::XmlParser() : d(new XmlParserPrivate)
{ }

XmlParser::~XmlParser()
{
  delete d;
}

XmlParser::ParseResult XmlParser::parse(QString const &fileUrl, CppPortingDatabase * const db)
{
  if (!d->openXmlFile(fileUrl))
    return d->mResult;

  d->mDatabase = db;

  XmlParser::ParseResult result = d->parsePortingDescription();
  d->mDatabase = 0;
  return result;;
}

QStringList XmlParser::errorMessages() const
{
  return d->mErrorMessages;
}
