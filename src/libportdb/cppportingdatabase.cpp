/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "cppportingdatabase.h"

#include <QtCore/QMap>
#include <QtCore/QString>

#include <libquery/queries/classquery.h>

/// PortDatabasePrivate

class CppPortingDatabasePrivate
{
public:
  QMap<QString, Query::Ptr>    mQueries;
  QMap<QString, Transform::Ptr> mTransforms;
};

/// PortDatabase

CppPortingDatabase::CppPortingDatabase() : d(new CppPortingDatabasePrivate)
{ }

CppPortingDatabase::~CppPortingDatabase()
{
  delete d;
}

CppPortingDatabase& CppPortingDatabase::operator=(CppPortingDatabase const &other)
{
  if (this == &other)
    return *this;

  *d = *other.d;
  return *this;
}

void CppPortingDatabase::addQuery(Query::Ptr const &query)
{
  Q_ASSERT(!containsQuery(query->id()));
  d->mQueries[query->id()] = query;
}

bool CppPortingDatabase::containsQuery(QString const &id) const
{
  return d->mQueries.contains(id);
}

Query::Ptr CppPortingDatabase::query(QString const &id) const
{
  return d->mQueries.value(id);
}

QList<Query::Ptr> CppPortingDatabase::queries(Filter filter) const
{
  QList<Query::Ptr> queries;
  switch (filter) {
  case All:         // Fall through for now
    queries = d->mQueries.values();
    break;
  case QueriesOnly:
    {
      QMap< QString, Query::Ptr >::const_iterator it = d->mQueries.constBegin();
      while (it != d->mQueries.constEnd()) {
        if (!d->mTransforms.contains(it.key())) {
          queries << it.value();
        }
        ++it;
      }
    }
    break;
  case TransformsOnly:
    {
      QMap< QString, Transform::Ptr >::const_iterator it = d->mTransforms.constBegin();
      while (it != d->mTransforms.constEnd()) {
        queries << d->mQueries.value(it.key());
        ++it;
      }
    }
  }
  return queries;
}

void CppPortingDatabase::addTransform(const QString& id, const Transform::Ptr transform) const
{
  d->mTransforms.insert(id, transform);
}

Transform::Ptr CppPortingDatabase::transform(QString const &id) const
{
  return d->mTransforms.value(id);
}

QList<Transform::Ptr> CppPortingDatabase::transforms() const
{
  return d->mTransforms.values();
}