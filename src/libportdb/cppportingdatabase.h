/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef CPPPORTINGDATABASE_H
#define CPPPORTINGDATABASE_H

#include <libquery/queries/classquery.h>
#include <libquery/queries/functionquery.h>
#include <libtransform/transform.h>

#include <QSharedPointer>

#include "cppportingdb_export.h"

class UseQuery;

class CPPPORTDB_EXPORT CppPortingDatabase
{
  /// Disable for now.
  CppPortingDatabase(CppPortingDatabase const &other);

  public:
    enum Filter {
      All,
      QueriesOnly,
      TransformsOnly
    };

  public:
    CppPortingDatabase();
    ~CppPortingDatabase();

    CppPortingDatabase& operator=(CppPortingDatabase const &other);

    /**
     * Adds @param query to the database. The query id <em>must</em> be unique.
     */
    void addQuery(Query::Ptr const &query);

    /**
     * @return true when a query identified by @p id is contained in the database, false otherwise.
     */
    bool containsQuery(QString const &id) const;

    /**
     * @return List of all queries.
     */
    QList<Query::Ptr> queries(Filter filter = All) const;

    /**
     * @return The query identified by @p id.
     */
    Query::Ptr query(QString const &id) const;

    /**
     * @return The query identified by @p id, dynamic-casted to type @p T.
     *         Hence @p T should be a class extending Query.
     * @example database.query<ClassQuery::Ptr>(id);
     */
    template<typename T>
    QSharedPointer<T> query(QString const &id) const;

    /**
     * Associates @p transform with the query identified by @p id.
     */
    void addTransform(QString const &id, Transform::Ptr const transform) const;

    /**
     * @return a list of all Transforms.
     */
    QList<Transform::Ptr> transforms() const;

    /**
     * @return the Transform for given @p query.
     */
    Transform::Ptr transform(QString const &query) const;

  private:
    class CppPortingDatabasePrivate * const d;
};

template<typename T>
QSharedPointer<T> CppPortingDatabase::query(const QString& id) const
{
  return query(id).dynamicCast<T>();
}


#endif // PORTDATABASE_H
