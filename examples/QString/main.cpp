#include <string>

#define QT_NO_CAST_ASCII 1
#include <qstring.h>

using namespace std;

void do_something(const QString &) {}

int main( int, char *[] )
{
  string stlstring("blah");

  QString qstring1(stlstring);
  QString qstring2 = QString(stlstring);
  QString qstring3 = stlstring;

  const char *latin1 = qstring3.latin1();
  const char *latin2 = qstring3.ascii();

  do_something( "blah" );
  do_something( QString( "blah" ) );

  return 0;
}

#undefine QT_NO_CAST_ASCII