<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE porting-description SYSTEM "portingdatabase.dtd">
<porting-description>
  <queries>
    <!-- QObject::child(const char *objName, const char *inheritsClass, bool recursiveSearch) -->
    <class-function-query uid="QObject::child (all)" qid="QObject::child">
      <argument type="const char *" />
      <argument type="const char *" />
      <argument type="bool" />
    </class-function-query>
    <class-function-query uid="QObject::child (non recursive)" qid="QObject::child">
      <argument type="const char *" />
      <argument type="const char *" />
      <argument type="bool" >
        <restriction kind="QidRestriction" value="FALSE" />
      </argument>
    </class-function-query>

    <!-- QToolTip::add(QWidget *widget, const QString &text ) -->
    <class-function-query uid="QToolTip::add(QWidget *, const QString &amp;)" qid="QToolTip::add">
      <argument type="QWidget *" />
      <argument type="QString const &amp;" />
    </class-function-query>

    <!-- QWhatsThis::add(QWidget *widget, const QString &text) -->
    <class-function-query uid="QWhatsThis::add(QWidget *, const QString &amp;)" qid="QWhatsThis::add">
      <argument type="QWidget *" />
      <argument type="QString const &amp;" />
    </class-function-query>

    <!-- QObject::queryList(const char *inheritsClass,
                            const char *objName,
                            bool regexpMatch,
                            bool recursiveSearch) const
    -->
    <class-function-query uid="QObject::queryList" qid="QObject::queryList" const="true">
      <argument type="const char *" />
      <argument type="const char *" />
      <argument type="bool" />
      <argument type="bool" />
    </class-function-query>
    <class-function-query uid="QObject::queryList (non recursive)" qid="QObject::queryList" const="true">
      <argument type="const char *" />
      <argument type="const char *" />
      <argument type="bool" />
      <argument type="bool">
        <restriction kind="LiteralRestriction" value="false|FALSE|0" />
      </argument>
    </class-function-query>

    <class-query uid="QObjectList" qid="QObjectList" />

    <!-- QPtrList queries -->
    <class-query uid="QPtrList&lt;T&gt;" qid="QPtrList">
      <template-argument />
      <!-- <template-argument type="QString" /> -->
    </class-query>

    <class-function-query uid="QPtrList&lt;T&gt;::isEmpty()" qid="QPtrList::isEmpty" const="true" />

    <class-function-query uid="QPtrList&lt;T&gt;::next()" qid="QPtrList::next" />

    <class-function-query uid="QPtrList&lt;T&gt;::contains(const T*)" qid="QPtrList::contains" const="true">
      <argument type="const ${templateArg[0]} *" />
    </class-function-query>

    <class-function-query uid="QPtrList&lt;T&gt;::remove(const T*)" qid="QPtrList::remove">
      <argument type="const ${templateArg[0]} *" />
    </class-function-query>

    <class-function-query uid="QPtrList&lt;T&gt;::remove(uint)" qid="QPtrList::remove">
      <argument type="uint" />
    </class-function-query>

    <class-function-query uid="QObjectList::next()" qid="QObjectList::next" />

    <class-function-query uid="QString::QString(const std::string &amp;)" qid="QString::QString">
      <argument type="const std::string &amp;" />
    </class-function-query>

    <!-- This method does not exist in Q3PopupMenu, unlike the other insertItem overloads. -->
    <class-function-query uid="QMenuData::insertItem(QWidget*, int, int)" qid="QMenuData::insertItem">
      <argument type="QWidget *" />
      <argument type="int id" />
      <argument type="int index" />
    </class-function-query>

    <global-function-query uid="qt_cast&lt;T&gt;(const QObject *)" qid="qt_cast">
      <template-argument />
      <argument type="const QObject *" />
    </global-function-query>

    <class-query uid="QIconSet" qid="QIconSet" />
  </queries>


  <transformations>
    <!-- TODO: Make type queries more specific e.g. find QObjectList* variables -->
    <transform queryId="QObjectList">
      <rule>
        <replace-action item="All">QList&lt;QObject*&gt;</replace-action>
      </rule>
    </transform>

    <transform queryId="QIconSet">
      <rule>
        <replace-action item="TypeId">QIcon</replace-action>
      </rule>
    </transform>

    <transform queryId="QObject::queryList">
      <rule>
        <replace-action item="All">qFindChildren&lt;${literalVal(Arg[0])}&gt;(${ObjectId}, ${Arg[1]})</replace-action>
      </rule>
    </transform>

    <!--
      Reasoning: All QPtrList<T> variables will be changed to QList<T*> variables
                 and these will be passed by value and not as pointers.
    -->
    <!--
    <transform queryId="QPtrList&lt;T&gt;::isEmtpy">
      <rule><action type="Replace" item="Accessor">.</action></rule>
    </transform>
    -->

    <transform queryId="QPtrList&lt;T&gt;"><!-- no condition, execute on each use found -->
      <rule>
        <replace-action item="All">QList&lt;${TypeTemplateArg[0]} *&gt;</replace-action>
      </rule>
    </transform>

<!--
    <transform queryId="QString::QString(const std::string &amp;)">
      <rule>
        <action type="Replace" item="*">qFindChild&lt;${literalVal(Arg[1])}&gt;(${ObjectId}, ${Arg[0]})</action>
      </rule>
    </transform>
 -->

    <transform queryId="QString::QString(const std::string &amp;)">
      <rule>
        <if>
          <condition property="ImplicitCtorCall" expected-value="true" />
          <insert-action item="All" location="After">= QString::fromStdString</insert-action>
        </if>
        <else>
          <insert-action item="All" location="After">::fromStdString</insert-action>
        </else>
      </rule>
    </transform>

    <transform queryId="QObject::child (all)">
      <!-- foo->child("objName") becomes qFindChild<QObject *>(foo, "objName") -->
      <rule>
        <if>
          <condition property="ArgCount" expected-value="1" />
          <replace-action item="All">qFindChild&lt;QObject *&gt;(${ObjectId}, ${Arg[0]})</replace-action>
        </if>
      </rule>
      <!-- foo->child("objName", "Class") becomes qFindChild<Class *>(foo, "objName") -->
      <rule>
        <if>
          <condition property="ArgCount" expected-value="2" />
          <replace-action item="All">qFindChild&lt;${literalVal(Arg[1])} *&gt;(${ObjectId}, ${Arg[0]})</replace-action>
        </if>
      </rule>
    </transform>

    <transform queryId="QObject::child (non recursive)">
      <!-- foo->child("objName", "Class", FALSE) becomes KGlobal::findDirectChild<Class *>(foo, "objName") -->
      <rule>
        <if>
          <condition property="ArgCount" expected-value="3" />
          <replace-action item="All">KGlobal::findDirectChild&lt;${literalVal(Arg[1])} *&gt;(${ObjectId}, ${Arg[0]})</replace-action>
        </if>
      </rule>
    </transform>

    <transform queryId="QObject::queryList">
      <!-- foo->queryList("Class") becomes qFindChildren<Class *>(foo) -->
      <rule>
        <if>
          <condition property="ImplicitOnThis" expected-value="false" />
          <condition property="ArgCount" expected-value="1" />
          <replace-action item="All">qFindChildren&lt;${literalVal(Arg[0])} *&gt;(${ObjectId})</replace-action>
        </if>
      </rule>

      <!-- queryList("Class") becomes qFindChildren<Class *>(this) -->
      <rule>
        <if>
          <condition property="ImplicitOnThis" expected-value="true" />
          <condition property="ArgCount" expected-value="1" />
          <replace-action item="All">qFindChildren&lt;${literalVal(Arg[0])} *&gt;(this)</replace-action>
        </if>
      </rule>

      <!-- foo->queryList("Class", "objName") becomes qFindChildren<Class *>(foo, "objName") -->
      <rule>
        <if>
          <condition property="ImplicitOnThis" expected-value="false" />
          <condition property="ArgCount" expected-value="2" />
          <replace-action item="All">qFindChildren&lt;${literalVal(Arg[0])} *&gt;(${ObjectId}, ${Arg[1]})</replace-action>
        </if>
      </rule>

      <!-- foo->queryList("Class", "objName") becomes qFindChildren<Class *>(foo, "objName") -->
      <rule>
        <if>
          <condition property="ImplicitOnThis" expected-value="true" />
          <condition property="ArgCount" expected-value="2" />
          <replace-action item="All">qFindChildren&lt;${literalVal(Arg[0])} *&gt;(this, ${Arg[1]})</replace-action>
        </if>
      </rule>
    </transform>

    <transform queryId="QObject::queryList (non recursive)">
      <rule>
        <replace-action item="All">KGlobal::findDirectChildren&lt;${literalVal(Arg[0])} *&gt;(${ObjectId}, ${Arg[1]})</replace-action>
      </rule>
    </transform>

  </transformations>

</porting-description>
